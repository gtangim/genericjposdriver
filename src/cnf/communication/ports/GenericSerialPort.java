package cnf.communication.ports;
import javax.comm.*;

import cnf.communication.ports.PortDataEvent.PortDataEventType;
import apu.jpos.util.*;

import java.util.*;
import java.io.*;

import jpos.JposException;
import jpos.config.JposEntry;

public class GenericSerialPort extends BasePortImplementation implements PortInterface, SerialPortEventListener{
	private final String _classname = this.getClassName();
	int baud;
	int databits;
	int flowcontrol;
	int stopbits;
	int parity;
    int readEventDelay;
	boolean openflag;
	private SerialPort comport;
	private CommPortIdentifier portid;
	private InputStream comportinputstream;
	private OutputStream comportoutputstream;
	private boolean portopen;
	private boolean busy;
	private void reset()
	{
		openflag = false;
		comport = null;
		portid = null;
		comportinputstream = null;
		comportoutputstream = null;
		portopen = false;
		busy = false;
	}
	private void initializeProperties()
	{
		resetProperties();
		//this.setPropertyLogger(logger);
		this.setPropertyModuleName(portname);
		//log(portname + " >> " + "Setting property names for this port.");
		registerProperty("PortName", "Name of the Serial Port (i.e. COM1)", "COM1", null);
		registerProperty("BaudRate", "Baud Rate (i.e. 9600)", "9600", null);
		registerProperty("DataBits", "Number of Data Bits (5, 6, 7, 8)", "8", new String[] {"5","6","7","8"});
		registerProperty("Parity", "Parity (None, Odd, Even)", "None", new String[] {"None","Odd","Even"});
		registerProperty("StopBits", "Stop Bits (1, 1.5, 2)", "1", new String[] {"1","1.5","2"});
		registerProperty("FlowControl", "Flow Control (None, Hardware, Software)", "None", new String[] {"None","Hardware","Software"});
        registerProperty("WriteTimeout", "maximum time to wait to let a write operation complete", "5000", null);
        registerProperty("ReadEventDelay", "A delay before triggering read to prevent ack-wait lockup", "0", null);
	}
	
	public GenericSerialPort()
	{
		super();
		reset();	
		initializeProperties();
        //log(portname + " >> " + "New port instance created!");
	}
	public GenericSerialPort(String portName) throws PortIOException
	{
		super();
		assignPortName(portName);
		reset();
		initializeProperties();
		if (portName!=null)
			initialize(portName,9600, SerialPort.DATABITS_8, SerialPort.PARITY_NONE, SerialPort.STOPBITS_1, SerialPort.FLOWCONTROL_NONE);
        //log(portname + " >> " + "New port instance created!");
	}
	public GenericSerialPort(Map<String,String> config) throws PortIOException
	{
		reset();
		initializeProperties();
		initialize(config);
	}

	protected void validatePortName() throws PortIOException
	{
		Enumeration portList = CommPortIdentifier.getPortIdentifiers();
		if (!portList.hasMoreElements())
            logWarning(portname + " >> "
                    + "WARNING: System did not return any portname. Comm API may not be installed correctly.");
		boolean portFound = false;
		while (portList.hasMoreElements()) {			
		    portid = (CommPortIdentifier) portList.nextElement();
		    if (portid.getPortType() == CommPortIdentifier.PORT_SERIAL) {
		    	
		    	if (portid.getName().toLowerCase().equals(portname.toLowerCase())) {
		    		portFound = true;
		    		break;		    	
		    	}
			} 
		}
		if (!portFound)
            error("Cannot initialize port "
                    +portname+". Port may be in use by another process or it may not exist.");
	}	
	public void initialize(String PortName, int baudRate, int dataBits, int Parity, int stopBits, int flowControl) throws PortIOException
	{
        log(portname + " >> " + "Loading port settings...");
		if (openflag) error("Cannot initialize com port because it is already open!");
		reset();
		assignPortName(PortName);

		validatePortName();
		baud = baudRate;
		databits = dataBits;
		parity = Parity;
		stopbits = stopBits;
		flowcontrol = flowControl;
	}


	//@Override
	public void initialize(Map<String, String> config) throws PortIOException {
        //log(portname + " >> " + "Loading port settings...");

		try
		{
			loadPropertyValues(config);
		}
		catch(JposException e)
		{
			throw new PortIOException(e.getMessage());
		}
		// Now retrieve all properties...
		initializePropertyValues();

	}

	//@Override
	public void initialize(JposEntry config) throws PortIOException {
        //log(portname + " >> " + "Loading port settings...");

		try
		{
			loadPropertyValues(config);
		}
		catch(JposException e)
		{
			this.error("Failed to initialize port! Reason: " + e.getMessage());
		}
		// Now retrieve all properties...
		initializePropertyValues();

	}



	private void initializePropertyValues() throws PortIOException
	{
		assignPortName(getPropertyValue("PortName"));
		validatePortName();
		String temp;
		if (portname==null)
            error("Unable to open serial port. You must assign a PortName in the configuration!");
		try
		{
			baud = Integer.parseInt(getPropertyValue("BaudRate"));
		}
		catch(NumberFormatException ex)
		{
			error("Unable to open serial port. You must assign a valid integer BaudRate in the configuration (i.e. 9600)!");
		}
		temp = getPropertyValue("DataBits");
		if (temp==null) error("Unable to open serial port. You must assign a valid DataBits value in the configuration!");
		else if (temp.equals("5")) databits = SerialPort.DATABITS_5;
		else if (temp.equals("6")) databits = SerialPort.DATABITS_6;
		else if (temp.equals("7")) databits = SerialPort.DATABITS_7;
		else if (temp.equals("8")) databits = SerialPort.DATABITS_8;

		temp = getPropertyValue("Parity").toLowerCase();
		if (temp==null) error("Unable to open serial port. You must assign a valid Parity value in the configuration!");
		else if (temp.startsWith("n")) parity = SerialPort.PARITY_NONE;
		else if (temp.startsWith("o")) parity = SerialPort.PARITY_ODD;
		else if (temp.startsWith("e")) parity = SerialPort.PARITY_EVEN;
		
		temp = getPropertyValue("StopBits");
		if (temp==null) error("Unable to open serial port. You must assign a valid StopBits value in the configuration!");
		else if (temp.equals("1")) stopbits = SerialPort.STOPBITS_1;
		else if (temp.equals("1.5")) stopbits = SerialPort.STOPBITS_1_5;
		else if (temp.equals("2")) stopbits = SerialPort.STOPBITS_2;

		temp = getPropertyValue("FlowControl").toLowerCase();
		if (temp==null) error("Unable to open serial port. You must assign a valid StopBits value in the configuration!");
		else if (temp.startsWith("n"))
            flowcontrol = SerialPort.FLOWCONTROL_NONE;
		else if (temp.startsWith("h"))
            flowcontrol = SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT;
		else if (temp.startsWith("s"))
            flowcontrol = SerialPort.FLOWCONTROL_XONXOFF_IN | SerialPort.FLOWCONTROL_XONXOFF_OUT;

        try
        {
            readEventDelay = Integer.parseInt(getPropertyValue("ReadEventDelay"));
        }
        catch(NumberFormatException ex)
        {
            error("Unable to open serial port. You must assign a ReadEventDelay (i.e. 100) in milliseconds!");
        }

	}

	//@Override
	public void open() throws PortIOException
	{
		if (portopen) error("Port is already connected!");
		if (portid==null) error("You must initialize the port first.");
		try {
		    comport = (SerialPort) portid.open("GenericPortDriver", 2000);
		} catch (PortInUseException e) 
		{
			error("Unable to open serial port. Port is in use by another program! Details: " + e.getMessage());
		}
		
		try {
		    comportinputstream = comport.getInputStream();
		} catch (IOException e) 
		{
			error("Unable to get port input stream. reason: " + e.toString());
		}
		try {
		    comportoutputstream = comport.getOutputStream();
		} catch (IOException e) 
		{
			error("Unable to get port output stream. reason: " + e.toString());
		}

		try {
		    comport.addEventListener(this);
		} catch (TooManyListenersException e) 
		{
			error("Unable to attach event listener. Too many already attached!");
		}

		comport.notifyOnDataAvailable(true);
		comport.notifyOnOutputEmpty(true);

		try {
		    comport.setSerialPortParams(baud, databits, stopbits, parity);
		    comport.setFlowControlMode(flowcontrol);
		} 
		catch (UnsupportedCommOperationException e) 
		{
			error("Unable to setup serial port with given settings. Reason: " + e.getMessage());
		}	
		portopen = true;
        log(portname + " >> " + "Port connection established!");
		
	}


	//@Override
	public void close(){		
		if (portopen) 
		{
			comport.notifyOnDataAvailable(false);
			comport.notifyOnOutputEmpty(false);
			comport.removeEventListener();
			try
			{
				comportinputstream.close();
				comportoutputstream.close();
			}
			catch(IOException ex)
			{				
			}
			comport.close();
		}
        log(portname + " >> " + "Port connection closed!");
		portopen = false;
	}


	//@Override
	public boolean isOpen() {
		return portopen;
	}

	//@Override
	public void write(char data) throws PortIOException {
        setBusy();
		try {
            //log(portname + ".tx >> " +  Character.toString(data));
			comportoutputstream.write((int)data);
			synchronized(this)
			{
				try
				{
					if (isBusy())
						this.wait(writeTimeout);
				}catch(InterruptedException ex){}
				if (isBusy()) throw new IOException("Write Timeout!");
			}
		}
		catch (IOException e) {
			error("Unable to write data to serial port. Reason:" + e.getMessage());
		}
        finally {
            clearBusy();
        }
	}
	//@Override
	public void write(char[] data) throws PortIOException {
		write(StringUtil.charArrayToBytes(data));
	}
	//@Override
	public void write(byte[] data) throws PortIOException {
        setBusy();
		try {
            //log(portname + ".tx >> " + new String(data));
			comportoutputstream.write(data);
			synchronized(this)
			{
				try
				{
					if (isBusy())
						this.wait(writeTimeout);
				}catch(InterruptedException ex){}
				if (isBusy()) throw new IOException("Write Timeout!");
			}
		}
		catch (IOException e) {
			error("Unable to write data to serial port. Reason:" + e.getMessage());
		}
        finally {
            clearBusy();
        }
	}
	//@Override
	public void write(String data) throws PortIOException {
        setBusy();
		try {
            //log(portname + ".tx >> " + data);
			comportoutputstream.write(data.getBytes());
			synchronized(this)
			{
				try
				{
					if (isBusy())
						this.wait(writeTimeout);
				}catch(InterruptedException ex){}
				if (isBusy()) throw new IOException("Write Timeout!");
			}
		}
		catch (IOException e) {
			error("Unable to write data to serial port. Reason:" + e.getMessage());
		}
        finally {
            clearBusy();
        }
	}

	
	//@Override
	public void writeAsync(char data) throws PortIOException {
        setBusy();
		try {
            //log(portname + ".tx >> " +  Character.toString(data));
			comportoutputstream.write((int) data);
		}
		catch (IOException e) {
			error("Unable to write data to serial port. Reason:" + e.getMessage());
		}
        finally {
            clearBusy();
        }
	}
	//@Override
	public void writeAsync(byte[] data) throws PortIOException {
        setBusy();
		try {
            //log(portname + ".tx >> " + new String(data));
			comportoutputstream.write(data);
		}
		catch (IOException e) {
			error("Unable to write data to serial port. Reason:" + e.getMessage());
		}
        finally {
            clearBusy();
        }
	}
	//@Override
	public void writeAsync(char[] data) throws PortIOException {
		writeAsync(StringUtil.charArrayToBytes(data));
	}
	//@Override
	public void writeAsync(String data) throws PortIOException {
        setBusy();
		try {
            //log(portname + ".tx >> " + data);
			comportoutputstream.write(data.getBytes());
		}
		catch (IOException e) {
			error("Unable to write data to serial port. Reason:" + e.getMessage());
		}
        finally {
            clearBusy();
        }
	}
	
	private void setBusy() throws PortIOException
	{
		synchronized(this)
		{
			if (busy) throw new PortIOException("Another write operation is in progress. Must use blocked write or wait until WRITE_COMPLETE event.");
			busy = true;
		}
	}
	
	private void clearBusy()
	{
		synchronized(this)
		{
			busy=false;
		}
	}

	//@Override
	public boolean isBusy()
	{
		return busy;
	}
	
	//@Override
	public void cancelWrite()
	{
		synchronized(this)
		{
			busy = false;
			try{
				this.notifyAll();
			} catch(Exception ex) {}
		}
	}
	
	//@Override
	public String getClassName() {
		return _classname;
	}
	
    public void serialEvent(SerialPortEvent event){
    	switch (event.getEventType()) {

    	case SerialPortEvent.BI:

    	case SerialPortEvent.OE:

    	case SerialPortEvent.FE:

    	case SerialPortEvent.PE:

    	case SerialPortEvent.CD:

    	case SerialPortEvent.CTS:

    	case SerialPortEvent.DSR:

    	case SerialPortEvent.RI:
    		break;

    	case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
    		clearBusy();
    		synchronized(this)
    		{
    			try
    			{
    				this.notifyAll();
    			}
    			catch(IllegalMonitorStateException ex){}
    		}
       	    eventManager.fireEvent(new PortDataEvent(this, PortDataEventType.WRITE_COMPLETE, null));
    		break;

    	case SerialPortEvent.DATA_AVAILABLE:
    	    try {
        	    int sz = comportinputstream.available();
        		byte[] readBuffer = new byte[sz];
    		    int numBytes = comportinputstream.read(readBuffer);
    		    String data = new String(readBuffer);
    		    if (readEventDelay>0)
                    try {
                        Thread.sleep(readEventDelay);
                    } catch (InterruptedException e) {
                    }
                //log(portname+".rx >> " + data);
    		    
    		    eventManager.fireEvent(new PortDataEvent(this, PortDataEventType.DATA_RECEIVED,data));
    		    
    	    } catch (IOException e) 
    	    {
    	    	logError("error: Exception while receiving serial data. Reason: " + e.getMessage());
    	    }

    	    break;
    	}
    }
}
