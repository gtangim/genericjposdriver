package cnf.communication.ports;

//import java.util.HashMap;
import java.util.*;
import jpos.config.JposEntry;


public interface PortInterface {
	
	// port initialization routines...
	public void initialize(Map<String,String> config)  throws PortIOException;
	public void initialize(JposEntry config)  throws PortIOException;
	public void open() throws PortIOException;
	public void close();	
	public boolean isOpen();
	public boolean isBusy(); // Indicates a port write operation is in progress
	public void cancelWrite();
	
	// Thread blocking writes....
	public void setWriteTimeout(int timeout);
	public void write(char data) throws PortIOException;
	public void write(char[] data) throws PortIOException;
	public void write(byte[] data) throws PortIOException;
	public void write(String data) throws PortIOException;
	
	// non-blocking asynchronous writes...
	public void writeAsync(char data) throws PortIOException;
	public void writeAsync(char[] data) throws PortIOException;
	public void writeAsync(byte[] data) throws PortIOException;
	public void writeAsync(String data) throws PortIOException;

	// Read Operations in no event mode...
	public int readBufferLength();  // Bytes available to be read...
	public char[] ReadChars(); 
	public String Read();

	// Event Handler Management...
	public void setDataEventMode(boolean enabled);
	public void addDataListener(PortDataListener listener);
	public void removeDataListener(PortDataListener listener);
	public int getDataListenerCount();
	
	// Other methods...
	public String getClassName();
	public List<String> getPropertNames();
	public String getPropertyDescription(String propertyName);
	public String getPropertyValue(String propertyName);
	public String getPortName();
	
}
