package cnf.communication.ports;

import apu.jpos.com.surrogate.CSEventListener;
import apu.jpos.com.surrogate.ComSurrogate;
import apu.jpos.com.surrogate.net.SocketInfo;
import apu.jpos.com.surrogate.net.protocol.CSServerEvent;
import apu.jpos.util.StringUtil;
import cnf.communication.ports.PortDataEvent.PortDataEventType;
import cnf.jpos.factory.CnfJposServiceFactory;
import jpos.JposException;
import jpos.config.JposEntry;

import javax.xml.bind.DatatypeConverter;
import java.util.Map;

public class ComSurrogatePort extends BasePortImplementation implements PortInterface, CSEventListener {
	private final String _classname = this.getClassName();
	String surrogateAddress;
    int surrogatePort;
	boolean openflag;

    private ComSurrogate api;
	private boolean portopen;
	private boolean busy;

	private void reset()
	{
        surrogateAddress=null;
        surrogatePort=2000;
		openflag = false;
        api = null;
		portopen = false;
		busy = false;
	}
	private void initializeProperties()
	{
		resetProperties();
		//this.setPropertyLogger(logger);
		this.setPropertyModuleName(portname);
		//log(portname + " >> " + "Setting property names for this port.");
        registerProperty("PortName", "Name of the Serial Port (i.e. Scanner, Scale, Pinpad)", null, null);
        registerProperty("ComSurrogateServerAddress", "IpAddress/name of the Cnf ComPort Surrogate Server","127.0.0.1",null);
        registerProperty("ComSurrogatePort", "TCP Port number for Cnf ComPort Surrogate","2000",null);
	}

	public ComSurrogatePort()
	{
		super();
		reset();
		initializeProperties();
        //log(portname + " >> " + "New port instance created!");
	}
	public ComSurrogatePort(String portName) throws PortIOException
	{
		super();
		assignPortName(portName);
		reset();
		initializeProperties();
        //log(portname + " >> " + "New port instance created!");
	}
	public ComSurrogatePort(Map<String, String> config) throws PortIOException
	{
		reset();
		initializeProperties();
		initialize(config);
	}

	//@Override
	public void initialize(Map<String, String> config) throws PortIOException {
        //log(portname + " >> " + "Loading port settings...");

		try
		{
			loadPropertyValues(config);
		}
		catch(JposException e)
		{
			throw new PortIOException(e.getMessage());
		}
		// Now retrieve all properties...
		initializePropertyValues();

	}

	//@Override
	public void initialize(JposEntry config) throws PortIOException {
        //log(portname + " >> " + "Loading port settings...");

		try
		{
			loadPropertyValues(config);
		}
		catch(JposException e)
		{
			this.error("Failed to initialize port! Reason: " + e.getMessage());
		}
		// Now retrieve all properties...
		initializePropertyValues();

	}



	private void initializePropertyValues() throws PortIOException
	{
        String pname = getPropertyValue("PortName");
        assignPortName(pname);
        surrogateAddress = getPropertyValue("ComSurrogateServerAddress");
        surrogatePort = Integer.parseInt(getPropertyValue("ComSurrogatePort"));
	}

	//@Override
	public void open() throws PortIOException
	{
        try {
            api = ComSurrogate.getInstance(surrogateAddress, surrogatePort, "JPOS Moneris Driver 1.0");
            api.addEventHandler(this);
            api.waitForLogin();
            api.sendCommand("SetReadDelay", 200);
        } catch (Exception e) {
            log(e);
            throw new PortIOException("Unable to connect to the ComPort Surrogate Server! " + e.getMessage());
        }

    }


	//@Override
	public void close(){		
		if (portopen) 
		{
		}
        log(portname + " >> " + "Port connection closed!");
		portopen = false;
	}


	//@Override
	public boolean isOpen() {
		return portopen;
	}

	//@Override
	public void write(char data) throws PortIOException {
        setBusy();
        //log(portname + ".tx >> " +  Character.toString(data));

        try {
            String dataEncoded = DatatypeConverter.printBase64Binary(Character.toString(data).getBytes());
            api.sendCommand("send", portname, dataEncoded);
        } catch (Exception e) {
            error("Unable to write data to serial port. Reason:" + e.getMessage());
        }
        finally {
            clearBusy();
        }
	}
	//@Override
	public void write(char[] data) throws PortIOException {
		write(StringUtil.charArrayToBytes(data));
	}
	//@Override
	public void write(byte[] data) throws PortIOException {
        setBusy();
        //log(portname + ".tx >> " + new String(data));
        try {
            String dataEncoded = DatatypeConverter.printBase64Binary(data);
            api.sendCommand("send", portname, dataEncoded);
        } catch (Exception e) {
            error("Unable to write data to serial port. Reason:" + e.getMessage());
        }
        finally {
            clearBusy();
        }
	}
	//@Override
	public void write(String data) throws PortIOException {
        setBusy();
        //log(portname + ".tx >> " + data);
        try {
            String dataEncoded = DatatypeConverter.printBase64Binary(data.getBytes());
            api.sendCommand("send", portname, dataEncoded);
        } catch (Exception e) {
            error("Unable to write data to serial port. Reason:" + e.getMessage());
        }
        finally {
            clearBusy();
        }
	}

	
	//@Override
	public void writeAsync(char data) throws PortIOException {
        setBusy();
        //log(portname + ".tx >> " +  Character.toString(data));

        try {
            String dataEncoded = DatatypeConverter.printBase64Binary(Character.toString(data).getBytes());
            api.sendCommand("send", portname, dataEncoded);
        } catch (Exception e) {
            error("Unable to write data to serial port. Reason:" + e.getMessage());
        }
        finally {
            clearBusy();
        }
	}
	//@Override
	public void writeAsync(byte[] data) throws PortIOException {
        setBusy();
        //log(portname + ".tx >> " + new String(data));
        try {
            String dataEncoded = DatatypeConverter.printBase64Binary(data);
            api.sendCommand("send", portname, dataEncoded);
        } catch (Exception e) {
            error("Unable to write data to serial port. Reason:" + e.getMessage());
        }
        finally {
            clearBusy();
        }
	}
	//@Override
	public void writeAsync(char[] data) throws PortIOException {
		writeAsync(StringUtil.charArrayToBytes(data));
	}
	//@Override
	public void writeAsync(String data) throws PortIOException {
        setBusy();
        //log(portname + ".tx >> " + data);
        try {
            String dataEncoded = DatatypeConverter.printBase64Binary(data.getBytes());
            api.sendCommand("send", portname, dataEncoded);
        } catch (Exception e) {
            error("Unable to write data to serial port. Reason:" + e.getMessage());
        }
        finally {
            clearBusy();
        }
	}
	
	private void setBusy() throws PortIOException
	{
		synchronized(this)
		{
			if (busy) throw new PortIOException("Another write operation is in progress. Must use blocked write or wait until WRITE_COMPLETE event.");
			busy = true;
		}
	}
	
	private void clearBusy()
	{
		synchronized(this)
		{
			busy=false;
		}
	}

	//@Override
	public boolean isBusy()
	{
		return busy;
	}
	
	//@Override
	public void cancelWrite()
	{
		synchronized(this)
		{
			busy = false;
			try{
				this.notifyAll();
			} catch(Exception ex) {}
		}
	}
	
	//@Override
	public String getClassName() {
		return _classname;
	}
	

    public void onServerEvent(SocketInfo socket, CSServerEvent event) {
        if (event.getEventType().equals("data"))
        {
            String source = event.getParameters().get(0);
            if (source.equals(portname))
            {
                try {

                    byte[] dataBytes = DatatypeConverter.parseBase64Binary(event.getParameters().get(1));
                    String data = new String(dataBytes);
                    //log(portname+".rx >> " + data);
                    eventManager.fireEvent(new PortDataEvent(this, PortDataEventType.DATA_RECEIVED,data));
                } catch (Exception e)
                {
                    logError("error: Exception while receiving serial data. Reason: " + e.getMessage());
                }
            }
        }
    }
}
