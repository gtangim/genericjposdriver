package cnf.communication.ports;


import java.util.EventObject;


public class PortDataEvent extends EventObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8590013705114939017L;
	public enum PortDataEventType {DATA_RECEIVED, WRITE_COMPLETE, IO_ERROR, UNKNOWN}
	private String _value;
	private PortDataEventType _type;
	
    public PortDataEvent(Object source, PortDataEventType eventType, String value) {
        super(source);
        _type = eventType;
        _value = value;        
    }
    
	
	public String getValue() {return _value;}
	public PortDataEventType getEventType() {return _type;}
	
	
}
