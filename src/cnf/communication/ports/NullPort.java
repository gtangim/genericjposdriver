package cnf.communication.ports;
import javax.comm.*;

import cnf.communication.ports.*;
import cnf.communication.ports.PortDataEvent.PortDataEventType;
import cnf.util.StringUtil;

import java.util.*;
import java.io.*;

import jpos.JposException;
import jpos.config.JposEntry;

public class NullPort extends BasePortImplementation implements PortInterface{
	private final String _classname = this.getClassName();

	protected boolean portOpen = false;
	
	private void initializeProperties()
	{
		resetProperties();
	}
	
	public NullPort()
	{
		super();
		assignPortName("null");
		initializeProperties();
		log(portname + " >> " + "New port instance created!");
	}
	public NullPort(String portName) throws PortIOException
	{
		super();
		assignPortName(portName);
		initializeProperties();
        log(portname + " >> " + "New port instance created!");
	}
	public NullPort(Map<String,String> config) throws PortIOException
	{
		initializeProperties();
		initialize(config);
	}

	public void initialize(String PortName, int baudRate, int dataBits, int Parity, int stopBits, int flowControl) throws PortIOException
	{
        log(portname + " >> " + "Loading port settings...");
		assignPortName(PortName);
	}
	

    //@Override
	public void initialize(Map<String, String> config) throws PortIOException {
        log(portname + " >> " + "Loading port settings...");

		try
		{
			loadPropertyValues(config);
		}
		catch(JposException e)
		{
			throw new PortIOException(e.getMessage());
		}
		// Now retrieve all properties...
		initializePropertyValues();				
		
	}
	
	//@Override
	public void initialize(JposEntry config) throws PortIOException {
        log(portname + " >> " + "Loading port settings...");
		try{			
			loadPropertyValues(config);
		}
		catch(JposException e)
		{
			throw new PortIOException(e.getMessage());
		}
		// Now retrieve all properties...
		initializePropertyValues();				
		
	}
	
	
	
	private void initializePropertyValues() throws PortIOException
	{
		//assignPortName(getPropertyValue("PortName"));
	}

	//@Override
	public void open() throws PortIOException
	{
		portOpen = true;
        log(portname + " >> " + "Opened NULL port!");
	}


	//@Override
	public void close(){		
		portOpen=false;
        log(portname + " >> " + "Closed NULL Port!");
	}


	//@Override
	public boolean isOpen() {
		return portOpen;
	}

	//@Override
	public void write(char data) throws PortIOException {
        log(portname + ".tx >> " + Character.toString(data));
	}
	//@Override
	public void write(char[] data) throws PortIOException {
		write(StringUtil.charArrayToBytes(data));
	}
	//@Override
	public void write(byte[] data) throws PortIOException {
        log(portname + ".tx >> " +  new String(data));
	}
	//@Override
	public void write(String data) throws PortIOException {
        log(portname + ".tx >> " +  data);
	}

	
	//@Override
	public void writeAsync(char data) throws PortIOException {
        log(portname + ".tx >> " +  Character.toString(data));
	}
	//@Override
	public void writeAsync(byte[] data) throws PortIOException {
        log(portname + ".tx >> " +  new String(data));
	}
	//@Override
	public void writeAsync(char[] data) throws PortIOException {
		writeAsync(StringUtil.charArrayToBytes(data));
	}
	//@Override
	public void writeAsync(String data) throws PortIOException {
        log(portname + ".tx >> " +  data);
	}
	

	//@Override
	public boolean isBusy()
	{
		return false;
	}
	
	//@Override
	public void cancelWrite()
	{
	}
	
	//@Override
	public String getClassName() {
		return _classname;
	}
	
}
