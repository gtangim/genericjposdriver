package cnf.communication.ports;
import apu.jpos.util.*;

public abstract class BasePortImplementation extends JposPropertyExtender implements PortInterface{

    // Create the listener list
	protected String portname;
	//protected SimpleLogger logger;
    protected PortDataEventManager eventManager = new PortDataEventManager();
    protected int writeTimeout = 100000;
	

    public BasePortImplementation()
    {
    	//logger = SimpleLogger.getDefaultLogger();
    	portname = nextGenericID();
    	resetProperties();
    }
    /*public void setLogger(SimpleLogger loggerInstance)
    {
    	logger = loggerInstance;
    	this.setPropertyLogger(logger);
    }*/
    
    
    // This methods allows classes to register for MyEvents
    //@Override
	public void addDataListener(PortDataListener listener) {
    	eventManager.addDataListener(listener);
        //log("Added new data event listener.");
    }

    // This methods allows classes to unregister for MyEvents
    //@Override
    public void removeDataListener(PortDataListener listener) {
    	eventManager.removeDataListener(listener);
    }
    
    //@Override
    public int getDataListenerCount()
    {
    	return eventManager.getDataListenerCount();
    }

    //@Override 
    public void setWriteTimeout(int t)
    {
    	writeTimeout = t;
    }
    
    //@Override
	public char[] ReadChars()
	{
		char[] retBuffer = null;
		synchronized(this)
		{
			StringBuilder localBuffer = eventManager.getLocalBuffer();
			int sz = localBuffer.length();
			retBuffer = new char[sz];
			localBuffer.getChars(0, sz, retBuffer, 0);
			localBuffer.setLength(0);
		}
		//log( portname + " >> Read " + Integer.toString(retBuffer.length) + " bytes.");
		return retBuffer;
	}

    //@Override
	public String Read()
	{
		String ret = null;
		synchronized(this)
		{
			StringBuilder localBuffer = eventManager.getLocalBuffer();
			ret = localBuffer.toString();
			localBuffer.setLength(0);
		}
        //log( portname + " >> Read " + Integer.toString(ret.length()) + " bytes.");
		return ret;
	}

    //@Override
	public int readBufferLength()
	{
		int sz;
		synchronized(this)
		{
			sz = eventManager.getLocalBuffer().length();
		}
		return sz;
	}

    //@Override
	public void setDataEventMode(boolean enabled)
	{
		eventManager.setDataEventMode(enabled);
		log("Port event mode is tunred " + (enabled?"on":"off") + ".");
	}
	
    
    
	public void error(String errorMessage) throws PortIOException
	{
		logError("error: " + errorMessage);
		throw new PortIOException(errorMessage);
	}
	
	protected void assignPortName(String newPortName)
	{
		if (newPortName!=null && !newPortName.equals(portname)) {
            //log("renaming port: " + portname + "->" + newPortName);
            portname = newPortName;
            this.setPropertyModuleName(portname);
        }
	}
	
	private static int nextPortID = 0;
	public static String nextGenericID()
	{
		return "Port_"+ Integer.toString(nextPortID);
	}

	//@Override
	public String getPortName() {
		return portname;
	}
	
	
}
