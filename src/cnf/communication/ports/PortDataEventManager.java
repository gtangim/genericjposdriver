package cnf.communication.ports;

import cnf.communication.ports.PortDataEvent.PortDataEventType;


public class PortDataEventManager {
  
    protected javax.swing.event.EventListenerList listenerList;
    private boolean eventsEnabled;
    private StringBuilder localBuffer;

    public PortDataEventManager()
    {
    	resetEventManagement();
    }
    
    
    public void resetEventManagement()
    {
    	eventsEnabled = false;
    	listenerList = new javax.swing.event.EventListenerList();
    	localBuffer = new StringBuilder();    	
    }
    
    public void addDataListener(PortDataListener listener) {
        listenerList.add(PortDataListener.class, listener);
    }

    // This methods allows classes to unregister for MyEvents
    public void removeDataListener(PortDataListener listener) {
        listenerList.remove(PortDataListener.class, listener);
    }
    
    public int getDataListenerCount()
    {
    	return listenerList.getListenerCount();
    }
	public void setDataEventMode(boolean enabled)
	{
		eventsEnabled = enabled;
	}
	public boolean getDataEventMode()
	{
		return eventsEnabled;
	}
	public StringBuilder getLocalBuffer()
	{
		return localBuffer;
	}

    // This private class is used to fire MyEvents
    public void fireEvent(PortDataEvent evt) {
    	if (eventsEnabled)
    	{
    		Object[] listeners = listenerList.getListenerList();
    		// Each listener occupies two elements - the first is the listener class
    		// and the second is the listener instance
    		for (int i=0; i<listeners.length; i+=2) {
    			if (listeners[i]==PortDataListener.class) {
    				((PortDataListener)listeners[i+1]).onPortData(evt);
    			}
    		}
    	}
    	else if (evt.getEventType()==PortDataEventType.DATA_RECEIVED)
    	{
    		synchronized(this)
    		{
    			localBuffer.append(evt.getValue());
    		}
    	}
    }

}
