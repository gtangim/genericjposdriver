package cnf.communication.protocols;

import cnf.communication.ports.*;
import apu.jpos.util.*;

public abstract class BaseProtocolImplementation extends JposPropertyExtender implements ProtocolInterface, PortDataListener{

    // Create the listener list
	protected String protocolname;
	//protected SimpleLogger logger;
	protected PortInterface port;
    protected PortDataEventManager eventManager = new PortDataEventManager();
	

    public BaseProtocolImplementation()
    {
    	//logger = SimpleLogger.getDefaultLogger();
    	protocolname = nextGenericID();
    	port = null;
    	resetProperties();
    }
    /*public void setLogger(SimpleLogger loggerInstance)
    {
    	logger = loggerInstance;
    	this.setPropertyLogger(logger);
    } */
    
    //@Override
    public void setPort(PortInterface portInstance)
    {
    	port = portInstance;
    	this.setPropertyModuleName(getName());
    	port.addDataListener(this);
        log(getName() + " >> " +  "Protocol " + protocolname + " now encapsulates " + port.getPortName());
    }
    //@Override
    public PortInterface getPort()
    {
    	return port;
    }
    // This methods allows classes to register for MyEvents
    //@Override
	public void addDataListener(PortDataListener listener) {
    	eventManager.addDataListener(listener);
        log(getName() + " >> " +  "Added new data event listener.");
    }

    // This methods allows classes to unregister for MyEvents
    //@Override
    public void removeDataListener(PortDataListener listener) {
    	eventManager.removeDataListener(listener);
    }
    
    //@Override
    public int getDataListenerCount()
    {
    	return eventManager.getDataListenerCount();
    }


    //@Override
	public void setDataEventMode(boolean enabled)
	{
		eventManager.setDataEventMode(enabled);
        log(getName() + " >> " +  "Protocol event mode is tunred " + (enabled?"on":"off") + ".");
	}

    //@Override
    public String getProtocolName()
    {
    	return protocolname;
    }

    protected String getName()
    {
    	return (port==null?"null":port.getPortName())+"."+protocolname;
    }
    
    
	public void error(String module, String errorMessage) throws PortIOException
	{
        logError(getName() + " >> " +  "error: " + errorMessage);
		throw new PortIOException(module+": "+errorMessage);
	}
	
	
	private static int nextPortID = 0;
	public static String nextGenericID()
	{
		return "Proto_"+ Integer.toString(nextPortID);
	}

	
}
