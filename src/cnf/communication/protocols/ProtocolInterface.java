package cnf.communication.protocols;

//import java.util.HashMap;
import java.util.*;
import jpos.config.JposEntry;
import cnf.communication.ports.*;

public interface ProtocolInterface {
	
	// port initialization routines...
	public void initialize(Map<String,String> config)  throws PortIOException;
	public void initialize(JposEntry config)  throws PortIOException;
	
	public void setPort(PortInterface port);
	public PortInterface getPort();
	public boolean isBusy(); // Indicates a port write operation is in progress
	public void cancelCommand();
	
	// Thread blocking writes....
	public void SendPacket(String data) throws PortIOException;
	
	// non-blocking asynchronous writes...
	public void sendPacketAsync(String data) throws PortIOException;

	// Read Operations in no event mode...
	public void clearPackets();
	public int getPacketCount();  // Bytes available to be read...
	public String readPacket() throws PortIOException;
    public void startAsyncRead();
    public void endAsyncRead();
    public int getAsyncReadElapsed();
    public String readPacketAsync(int customWait) throws PortIOException;
    public String peekPacket(int customWait);
	// Event Handler Management...
	public void setDataEventMode(boolean enabled);
	public void addDataListener(PortDataListener listener);
	public void removeDataListener(PortDataListener listener);
	public int getDataListenerCount();
	
	// Other methods...
	public String getClassName();
	public List<String> getPropertNames();
	public String getPropertyDescription(String propertyName);
	public String getPropertyValue(String propertyName);
	public String getProtocolName();

    public void setRetryForNextPacket(boolean retryEnabled);
}
