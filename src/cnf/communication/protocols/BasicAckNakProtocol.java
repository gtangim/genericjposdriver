package cnf.communication.protocols;

import cnf.communication.ports.*;
import cnf.communication.ports.PortDataEvent.PortDataEventType;
import cnf.util.Expression.*;

import java.util.*;
import java.util.logging.Logger;

import jpos.JposException;
import jpos.config.JposEntry;
import org.joda.time.DateTime;
import org.joda.time.Duration;


public class BasicAckNakProtocol extends BaseProtocolImplementation implements ProtocolInterface, Runnable{
	private final String _classname = this.getClassName();
	protected boolean busy;
	protected boolean async;
	protected boolean waitingForAck = false;
	//protected boolean commandInProgress;
	
	protected boolean ackNakEnabled;
	protected ExpressionParser ex_ack;
	protected ExpressionParser ex_nak;
	protected int ackTimeout;
	protected int packetRetry;
	protected int responseTimeout;
	protected ExpressionParser ex_out;
	protected ExpressionParser ex_in;
	protected ExpressionParser ex_verify;

	protected String lastrequest;
	protected String lastresponse;
	protected String ack_out;
	
	protected List<String> rlist;
	protected StringBuilder localBuffer;
	
	protected boolean asyncDataSequence=false;
    protected boolean enableRetryForNextPacket =true;
	
	private DateTime asyncReadStart = null;
	
	
	private void reset()
	{
        cancelCommand();
		async = false;
		//commandInProgress = false;
		waitingForAck = false;
		ackNakEnabled = false;
		ackTimeout = 1000;
		packetRetry = 1;
		responseTimeout = 2000;
		ex_ack = null;
		ex_nak = null;
		ex_in = null;
		ex_out = null;
		ex_verify = null;
		this.protocolname = "BasicAckNak";
		rlist = new ArrayList<String>();
		localBuffer = new StringBuilder();
		lastrequest = "";
		lastresponse = "";
		ack_out="";
	}
	
	private void initializeProperties()
	{
		resetProperties();
		//this.setPropertyLogger(logger);
		this.setPropertyModuleName(getName());
        log(getName() + " >> " +  "Setting property names for this protocol.");
		registerProperty("AckNakEnabled", "Whether each packet receipt requires acknowledgement", "true", new String[] {"true","false"});
		registerProperty("AckExpression", "What sequence is sent as acknowledgement", null, null);
		registerProperty("NakExpression", "What sequence is sent as negative acknowledgement", null, null);
		registerProperty("AckNakTimeout", "How long to wait for an acknowledgement from other device", "1000", null);
		registerProperty("PacketRetry", "How many times to try sending a packet on negative ack.", "10", null);
		registerProperty("ResponseTimeout", "How long to wait for a response once a command packet is sent", "30000", null);
		registerProperty("OutgoingPacketExpression", "this expresssion packetizes a command/request (use request as a variable in the expression)", null, null);
		registerProperty("IncomingPacketDecompileExpression", "This expression extracts the data part.", null, null);
		registerProperty("IncomingPacketVerifyExpression", "This expression checks packet integrity", null, null);
	}
	
	public BasicAckNakProtocol()
	{
		super();
		reset();	
		initializeProperties();
        log(getName() + " >> " +  "New protocol instance created!");
	}
	public BasicAckNakProtocol(PortInterface portInstance)
	{
		super();
		reset();
		initializeProperties();
		this.setPort(portInstance);
        log(getName() + " >> " +  "New protocol instance created!");
	}
	public BasicAckNakProtocol(Map<String,String> config) throws PortIOException
	{
		reset();
		initializeProperties();
		initialize(config);
	}


	

	//@Override
	public void initialize(Map<String, String> config) throws PortIOException {
		this.setPropertyModuleName(getName());
        log(getName() + " >> " +  "Loading protocol settings...");
		try
		{
			loadPropertyValues(config);
		}
		catch(JposException e)
		{
			this.error("BasicAckNakProtocol.initialize", "Failed to initialize protocol! Reason: " + e.getMessage());
		}
		// Now retrieve all properties...
		initializePropertyValues();
		
	}
	
	//@Override
	public void initialize(JposEntry config) throws PortIOException {
		this.setPropertyModuleName(getName());
        log(getName() + " >> " +  "Loading protocol settings...");
		
		try
		{
			loadPropertyValues(config);
		}
		catch(JposException e)
		{
			this.error("BasicAckNakProtocol.initialize", "Failed to initialize protocol! Reason: " + e.getMessage());
		}
		// Now retrieve all properties...
		initializePropertyValues();				
		
	}
	
	
	private ExpressionParser create_exp(String propName, HashMap symbolTable, HashMap functionTable) throws PortIOException
	{
		String t = getPropertyValue(propName);
		if (t==null || t.trim().isEmpty()) 
			error("BasicAckNakProtocol.initialize","You must assign a valid expression for " + propName);
		ExpressionParser e=null;
		if (symbolTable==null && functionTable==null)
		{
			e = new ExpressionParser();
			e.addStandardConstants();
			e.addStandardFunctions();
			e.setAutoVariableDeclare(true);
		}
		else if (symbolTable!=null && functionTable!=null)
		{	
			e = new ExpressionParser(symbolTable,functionTable);
			e.setAutoVariableDeclare(true);
		}
		else error("BasicAckNakProtocol.initialize","Illegal attempt to initialize " 
				+ propName + " with null symbol/function table!");
		e.parseExpression(t);
		if (e.hasError()) 
			error("BasicAckNakProtocol.initialize","Illegal expression in " 
					+ propName + ": " + e.getErrorInfo());
		return e;
	}
	
	private int create_int(String propName, int min, int max) throws PortIOException
	{
		int t = 0;
		try
		{
			t = getPropertyValueInteger(propName);
		}
		catch(JposException e)
		{
			error("BasicAckNakProtocol.initialize","You must assign a valid integer for " + propName);		
		}
		if (t<min || t>max) 
			error("BasicAckNakProtocol.initialize","Property " + propName + "=" + t 
					+" is out of range! Valid range is " 
					+ Integer.toString(min) + ".." + Integer.toString(max));					
		
		return t;
		
	}
	
	private void initializePropertyValues() throws PortIOException
	{
		ackNakEnabled = Boolean.parseBoolean(getPropertyValue("AckNakEnabled"));
		ackTimeout = create_int("AckNakTimeout",0,1000000);
		packetRetry = create_int("PacketRetry",0,100);
		responseTimeout = create_int("ResponseTimeout",0,1000000);
		ex_ack = create_exp("AckExpression",null,null);
		ex_nak = create_exp("NakExpression",ex_ack.getSymbolTable(),ex_ack.getFunctionTable());
		ex_out = create_exp("OutgoingPacketExpression",ex_ack.getSymbolTable(),ex_ack.getFunctionTable());
		ex_in = create_exp("IncomingPacketDecompileExpression",ex_ack.getSymbolTable(),ex_ack.getFunctionTable());
		ex_verify = create_exp("IncomingPacketVerifyExpression",ex_ack.getSymbolTable(),ex_ack.getFunctionTable());
	}




	//@Override
	public String getClassName() {
		return _classname;
	}

    public void setRetryForNextPacket(boolean retryEnabled) {
        synchronized (this)
        {
            enableRetryForNextPacket =retryEnabled;
        }
    }


    public void startAsyncRead()
    {
        synchronized (this) {
            asyncReadStart=new DateTime();
        }
    }

    public void endAsyncRead()
    {
        synchronized (this) {
            asyncReadStart=null;
        }
    }

    public int getAsyncReadElapsed()
    {
        synchronized (this) {
            if (asyncReadStart==null) return -1;
            return (int)(new Duration(asyncReadStart,new DateTime()).getMillis());
        }
    }


    public String readPacketAsync(int customWait) throws PortIOException
    {
        synchronized (this)
        {
            try
            {
                String r;
                if (rlist.size()>0) return (rlist.remove(0));
                else {
                    this.wait(customWait);
                    if (rlist.size()>0) return (rlist.remove(0));
                    else if (getAsyncReadElapsed()>responseTimeout)
                        throw new PortIOException("Response timeout!");
                }
            }
            catch(PortIOException e)
            {
                asyncReadStart=null;
                error("BasicAckNakProtocol.readPacket",e.getMessage());
            }
            catch(Exception e)
            {
                asyncReadStart=null;
                error("BasicAckNakProtocol.readPacket","Packet buffer error! " + e.getMessage());
            }
            return null;

        }
    }

    //@Override
	public String readPacket() throws PortIOException{
		synchronized(this)
		{
			try
			{
				String r;
				if (rlist.size()>0) return (rlist.remove(0));
				else {
                    this.wait(responseTimeout);
                    if (rlist.size()>0) return (rlist.remove(0));
                        else throw new PortIOException("Response timeout!");
				}
			}
			catch(PortIOException e)
			{
				 error("BasicAckNakProtocol.readPacket",e.getMessage());				
			}
			catch(Exception e)
			{
				 error("BasicAckNakProtocol.readPacket","Packet buffer error! " + e.getMessage());
			}
			return null;
		}
	}

    public String peekPacket(int customWait)
    {
        synchronized (this)
        {
            if (rlist!=null && rlist.size()>0) return rlist.get(0);
            else if (customWait>0)
            {
                try {
                    this.wait(customWait);
                } catch (InterruptedException e) {
                }
                if (rlist!=null && rlist.size()>0) return rlist.get(0);
                else return null;
            }
            else return null;
        }
    }
	
	//@Override 
	public void clearPackets()
	{
		synchronized(this)
		{
			rlist.clear();
            localBuffer.setLength(0);
		}
	}
	
	private void commenceAckSequence()
	{
        setBusy();
		try
		{			
			Thread.sleep(200);
			port.write(ack_out);
		}
		catch(Exception e)
        {
            logError(getName() + " >> " +  "Error: Unable to send ACK!");
        }
        finally {
            synchronized(this)
            {
                this.asyncDataSequence = false;
                busy=false;
            }
        }
	}

	private void commencePacketSequence() throws PortIOException
	{
        setBusy();
		try
		{
            String packet = null;
            String r = null;
            String ack = null;
            String nak = null;
            log(getName() + " >> " +  "Sending packet...");
			// Setup the symbol table for the expression parser
			synchronized(this){
				ex_ack.addVariable("request", lastrequest);
				ex_ack.addVariable("response", lastresponse);
			}
			// Now send a command (and retry sequence)...
			boolean cmdsuccess = false;
            boolean skipOnTimeout=!enableRetryForNextPacket;
            enableRetryForNextPacket = true;
            for (int i=0;i<=packetRetry;i++)
			{
				// Reset response and get the packet sequence
				synchronized(this)
				{
					lastresponse = "";
					if (!busy) return;
					packet = ex_out.getValue().getString();
				}
				// Send the packet to the port...
                port.write(packet);
                port.write(packet);
				if (!ackNakEnabled)
				{
					cmdsuccess = true;
					break;
				}
				synchronized(this)
				{
					// Wait for an acknowledgment..
					waitingForAck = true;
					this.wait(ackTimeout);
					waitingForAck = false;
					// Check if a valid acknowledgment was received...
					if (!busy) return;
					r=lastresponse;
					lastresponse = "";
					ack = ex_ack.getValue().getString();
					nak = ex_nak.getValue().getString();
				}
				if(r!=null && !r.isEmpty())
				{
					if (r.equals(nak)) cmdsuccess=false;
                    else {cmdsuccess=true;break;}
					//if (r.equals(ack)) {cmdsuccess=true; break;}
					//else if (r.equals(nak)) cmdsuccess=false;
					//else throw new Exception("Protocol violation, ack/nak expected after a command!");
				}
                else if (skipOnTimeout) break;
			}
			if (!busy) return;
			if (cmdsuccess==false) throw new Exception("Exceeded number retries while attempting to transmit packet!"); 
			// At this point the command is sent to the device...
            log(getName() + " >> " +  "packet sent...");
			// Command Sequence is now complete!					
		}
		catch(Exception e)
		{
			error("BasicAckNakProtocol.commencePacketSequence","Device communication failed! " + e.getMessage());
		}
        finally
        {
            clearBusy();
        }
	}

	//@Override
	public void SendPacket(String data) throws PortIOException {
		synchronized(this)
		{
            setBusyBlocking();
			this.lastrequest = data;
			async=false;
			this.asyncDataSequence=false;
		}
		commencePacketSequence();
	}
	//@Override
	public void cancelCommand() {
		synchronized(this)
		{
            clearBusy();
            if (port!=null && port.isBusy()) port.cancelWrite();
			try{
				this.notifyAll();
			} catch(Exception ex) {}
		}		
	}
	//@Override
	public int getPacketCount() {
		return rlist.size();
	}
	//@Override
	public void sendPacketAsync(String data) throws PortIOException {
		synchronized(this)
		{
            setBusyBlocking();
			this.lastrequest = data;
			async=true;
			this.asyncDataSequence=false;
		}
		try
		{
			new Thread(this).start();
		}catch(Exception e)
		{
			error("BasicAckNakProtocol.sendPacketAsync","Could not start async command thread! " + e.getMessage());
		}		
	}
	
	
	public void run()
	{
		try
		{
			if (asyncDataSequence==false)
				commencePacketSequence();
			else 
				commenceAckSequence();
		}
		catch(PortIOException e)
		{
			PortDataEvent event = new PortDataEvent(this, PortDataEventType.IO_ERROR, e.getMessage());
			eventManager.fireEvent(event);
		}
	}

	//@Override
	public void onPortData(PortDataEvent evt) {
		if (evt.getEventType()==PortDataEventType.DATA_RECEIVED)
		{
			int i;
			String ack,nak;
			try{											
				synchronized(this)
				{
					localBuffer.append(evt.getValue());
					// Add response to the symbol table
					ex_verify.addVariable("response", localBuffer.toString());
					ack = ex_ack.getValue().getString();
					nak = ex_nak.getValue().getString();
					
					if (ackNakEnabled && waitingForAck)
					{
                        int iAck = localBuffer.indexOf(ack);
                        int iNak = localBuffer.indexOf(nak);

                        if (iAck!=-1)
						{
							lastresponse = ack;
							// if there is data to digest before ack, dont delete
                            if (iAck<10) localBuffer.delete(0, iAck+ack.length());
							waitingForAck=false;
							this.notifyAll();
						}
						else if (iNak!=-1)
						{
							lastresponse = nak;
							// if there is data to digest before ack, dont delete
							if (iNak<10) localBuffer.delete(0, iNak+nak.length());
							waitingForAck=false;
							this.notifyAll();					
						}
                        // Generate error if you received a small data and wasnt the ack sequence...
						//else if (localBuffer.length()<11) error("BasicAckNakProtocol.sendPacket"
                        //        ,"Received invalid response while waiting for ack/nak:"+localBuffer.toString());
					}																										
				}
												
				if (localBuffer.length()>0)
				{
					String r = ex_in.getValue().getString();
					if (r!=null && !r.isEmpty())
					{
                        /* TODO: the following only gets the first packet from the buffer and resets everything
                         If the buffer contains more than one digestable packets, or one digestable packet
                         and part of another digestable packet, it will lose the next packet...
                         Right now, I dont care, but in future this may cause problem and then I will fix it....
                        */

						localBuffer.setLength(0);
						String v = ex_verify.getValue().getString();
						if (!v.equals("0")) 
						{
							// a valid response was received!
							lastresponse = r;
							rlist.add(r);
							synchronized(this)
							{
								try
								{
									this.notifyAll();
								}
								catch(Exception e){}
							}
							if (ackNakEnabled)
							{
								if (waitingForAck)
                                {

                                    waitingForAck=false;
                                    this.notifyAll();
                                }
                                ack_out = ack;
								asyncDataSequence = true;
                                new Thread(this).start();
							}
							if (eventManager.getDataEventMode())
							{
								PortDataEvent event = new PortDataEvent(this, PortDataEventType.DATA_RECEIVED, r);
								eventManager.fireEvent(event);
							}
						}
						else
						{
							// an invalid response was received (checksum error)!
							if (ackNakEnabled)
							{
								ack_out = nak;
								asyncDataSequence = true;
								new Thread(this).start();
							}
						}
					}
					
						
				}
			}
			catch(Exception ex)
			{
				// Create an error Event!
			}
		}
		else if (evt.getEventType()==PortDataEventType.WRITE_COMPLETE)
		{
			// Protocol doesn't care when write is complete!
		}
		else if (evt.getEventType()==PortDataEventType.IO_ERROR)
		{
			// Nothing for now...
		}
		
	}

    private void setBusy()
    {
        synchronized (this)
        {
            busy=true;
        }
    }
    private void setBusyBlocking() throws PortIOException
    {
        synchronized(this)
        {
            if (busy)
                error("BasicAckNakProtocol.sendPacket","Basic ACK/NAK: Another operation is in progress.");
            busy = true;
        }
    }

    private void clearBusy()
    {
        synchronized(this)
        {
            busy=false;
        }
    }

    //@Override
    public boolean isBusy()
    {
        return busy;
    }



}
