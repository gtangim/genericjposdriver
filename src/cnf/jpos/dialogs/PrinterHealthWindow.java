package cnf.jpos.dialogs;

import cnf.jpos.services.EpsonPrinterService;
import jpos.CATConst;
import jpos.JposException;
import jpos.POSPrinterConst;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PrinterHealthWindow extends JDialog implements ActionListener{
	   /**
	 *
	 */
	private static final long serialVersionUID = 1L;
    private JScrollPane infos = null;
    private JTextArea info = null;
    private JTextField txt = null;
	private EpsonPrinterService printer;
	public PrinterHealthWindow(EpsonPrinterService printerServiceInstance) { // constructor builds GUI
        this.printer = printerServiceInstance;
        Container c = this.getContentPane();
        c.setLayout(new FlowLayout(FlowLayout.CENTER));
        //JPanel upperPanel = new JPanel();
        JPanel lowerPanel = new JPanel();
        c.add(new JLabel("Amount: "));
        Dimension ad = new Dimension(250,20);
        txt = new JTextField();
        txt.setText("Hello World!");
        txt.setMinimumSize(ad);
        txt.setMaximumSize(ad);
        txt.setPreferredSize(ad);

        c.add(txt);

        //c.add(upperPanel, "North");
        info = new JTextArea();
        info.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        Dimension td = new Dimension(490,270);
        //info.setMinimumSize(td);
        //info.setMaximumSize(td);
        //info.setPreferredSize(td);
        info.setFont(new Font("Courier New", Font.PLAIN, 12));

        infos = new JScrollPane(info);
        infos.setMinimumSize(td);
        infos.setMaximumSize(td);
        infos.setPreferredSize(td);
        infos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        c.add(infos);
        c.add(lowerPanel, "South");

        addButton(lowerPanel,"Print Text","print");
        addButton(lowerPanel,"Print Barcode","barcode");
        addButton(lowerPanel,"Cut Paper","cut");
        addButton(lowerPanel,"Open Drawer","open");
        addButton(lowerPanel,"Start Tx Mode","starttx");
        addButton(lowerPanel,"End Tx Mode","endtx");
        addButton(lowerPanel,"Check Status","status");
        lowerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        Dimension ld = new Dimension(500,100);
        lowerPanel.setMinimumSize(ld);
        lowerPanel.setMaximumSize(ld);
        lowerPanel.setPreferredSize(ld);

        this.setAlwaysOnTop(true);
        Dimension d = new Dimension(500,430);
        this.setMinimumSize(d);
        this.setMaximumSize(d);
        this.setPreferredSize(d);
        this.setResizable(false);
        try{
            this.setTitle(printer.getPhysicalDeviceName() + ": Printer Operation");
        }catch(Exception e) {}

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int w = this.getSize().width;
        int h = this.getSize().height;
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;

        // Move the window
        this.setLocation(x, y);

        this.pack();   // does layout of components.
    }
	
	/*public void updateData()
	{
			info.append("New Scale Data Received: " + Double.toString(((double)scale.getWeight())/1000.0)+ " " + scale.getWeightUnitName() + "\r\n");
	} */


    private void addButton(JPanel container,String buttonCaption, String commandID)
    {
        JButton b = new JButton(buttonCaption);
        container.add(b);
        b.setActionCommand(commandID);
        b.addActionListener(this);
    }



    public void clear()
	{
		info.setText("");
	}


	//@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equalsIgnoreCase("close"))
		{
			this.setVisible(false);
		}
        else if (e.getActionCommand().equalsIgnoreCase("print"))
        {
            info.append("Printing Text...\r\nText to print: >> "+txt.getText()+"\r\n");
            try
            {
                printer.printNormal(POSPrinterConst.PTR_S_RECEIPT,txt.getText());
            }
            catch(JposException ex)
            {
                info.append("Error: " + ex.getMessage() + "\r\n");
            }
        }
        else if (e.getActionCommand().equalsIgnoreCase("cut"))
        {
            info.append("Cutting Paper...\r\n");
            try
            {
                printer.cutPaper(90);
            }
            catch(JposException ex)
            {
                info.append("Error: " + ex.getMessage() + "\r\n");
            }
        }
        else if (e.getActionCommand().equalsIgnoreCase("open"))
        {
            info.append("Opening the Cash Drawer...\r\n");
            try
            {
                printer.openDrawer();
            }
            catch(JposException ex)
            {
                info.append("Error: " + ex.getMessage() + "\r\n");
            }
        }
        else if (e.getActionCommand().equalsIgnoreCase("starttx"))
        {
            info.append("Begining Transaction Mode...\r\n");
            try
            {
                printer.transactionPrint(POSPrinterConst.PTR_S_RECEIPT,POSPrinterConst.PTR_TP_TRANSACTION);
            }
            catch(JposException ex)
            {
                info.append("Error: " + ex.getMessage() + "\r\n");
            }
        }
        else if (e.getActionCommand().equalsIgnoreCase("endtx"))
        {
            info.append("Committing Transaction Mode...\r\n");
            try
            {
                printer.transactionPrint(POSPrinterConst.PTR_S_RECEIPT,POSPrinterConst.PTR_TP_NORMAL);
            }
            catch(JposException ex)
            {
                info.append("Error: " + ex.getMessage() + "\r\n");
            }
        }
        else if (e.getActionCommand().equalsIgnoreCase("barcode"))
        {
            info.append("Printing Barcode...\r\nCode39 Barcode=" + txt.getText()+"\r\n");
            try
            {
                printer.printBarCode(POSPrinterConst.PTR_S_RECEIPT,txt.getText(),POSPrinterConst.PTR_BCS_Code39,
                        100,3,POSPrinterConst.PTR_BC_CENTER,POSPrinterConst.PTR_BC_TEXT_BELOW);
            }
            catch(JposException ex)
            {
                info.append("Error: " + ex.getMessage() + "\r\n");
            }
        }
        else if (e.getActionCommand().equalsIgnoreCase("status"))
        {
            info.append("Checking Printer Status...\r\n");
            try
            {
                printer.getCoverOpen();
                info.append("       Offline = " + printer.isPrinterOffline() + "\r\n");
                info.append("         Error = " + printer.isPrinterOffline() + "\r\n");
                info.append("    Cover Open = " + printer.isCoverOpen() + "\r\n");
                info.append("     Paper Out = " + printer.isPaperOut() + "\r\n");
                info.append("Paper Near Out = " + printer.isPaperNearEnd() + "\r\n");
                info.append("   Drawer Open = " + printer.isDrawerOpen() + "\r\n");
            }
            catch(JposException ex)
            {
                info.append("Error: " + ex.getMessage() + "\r\n");
            }
        }
	}


}
