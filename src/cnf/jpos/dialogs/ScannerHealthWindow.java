package cnf.jpos.dialogs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;


import cnf.communication.ports.*;
import cnf.jpos.services.GenericScannerService;
import cnf.util.*;
import jpos.*;
public class ScannerHealthWindow extends JDialog implements ActionListener{
	   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea info = null;
	private JButton cButton = null;
	private GenericScannerService scanner;

	public ScannerHealthWindow(GenericScannerService scannerServiceInstance) { // constructor builds GUI
		scanner = scannerServiceInstance;
		Container c = this.getContentPane();
		JPanel upperPanel = new JPanel();
	    JPanel lowerPanel = new JPanel();
	    c.add(upperPanel, "North");
		c.add(lowerPanel, "South");
		info = new JTextArea();
		info.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		Dimension td = new Dimension(380,180);
		info.setMinimumSize(td);
		info.setMaximumSize(td);
		info.setPreferredSize(td);
		c.add(new JScrollPane(info));
		cButton = new JButton("Close");
		lowerPanel.add(cButton);
		cButton.setActionCommand("close");
	    cButton.addActionListener(this);
		this.setAlwaysOnTop(true);
		Dimension d = new Dimension(400,200);
		this.setMinimumSize(d);
		this.setMaximumSize(d);
		this.setPreferredSize(d);
		this.setTitle("JavaPos Scanner Health Test");
        this.pack();   // does layout of components.
	    }	
	

	public void updateData()
	{
		try
		{
			info.append("New Barcode: " + StringUtil.asciiFilter(new String(scanner.getScanData()))+"\r\n");
			if (scanner.getScanDataType()!=ScannerConst.SCAN_SDT_UNKNOWN)
			{
				info.append("   Label: " + new String(scanner.getScanDataLabel())+"\r\n");
				info.append("   Type: " + Integer.toString(scanner.getScanDataType())+"\r\n");
			}
		}
		catch(JposException ex)
		{
			info.append(ex.toString());
		}
	}
	
	public void clear()
	{
		info.setText("");
	}


	//@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equalsIgnoreCase("close")) 
		{
			this.setVisible(false);
		}
	}

}
