package cnf.jpos.dialogs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;


import cnf.communication.ports.*;
import cnf.jpos.services.GenericScaleService;
import cnf.util.*;
import jpos.*;
public class ScaleHealthWindow extends JDialog implements ActionListener{
	   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea info = null;
	private JButton cButton = null;
	private JButton rButton = null;
	private GenericScaleService scale;
	public ScaleHealthWindow(GenericScaleService scannerServiceInstance) { // constructor builds GUI
		scale = scannerServiceInstance;
		Container c = this.getContentPane();
		JPanel upperPanel = new JPanel();
	    JPanel lowerPanel = new JPanel();
	    c.add(upperPanel, "North");
		c.add(lowerPanel, "South");
		info = new JTextArea();
		info.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		Dimension td = new Dimension(380,180);
		info.setMinimumSize(td);
		info.setMaximumSize(td);
		info.setPreferredSize(td);
		c.add(new JScrollPane(info));

		rButton = new JButton("Read Weight");
		lowerPanel.add(rButton);
		rButton.setActionCommand("read");
	    rButton.addActionListener(this);
		
		cButton = new JButton("Close");
		lowerPanel.add(cButton);
		cButton.setActionCommand("close");
	    cButton.addActionListener(this);
	    
		this.setAlwaysOnTop(true);
		Dimension d = new Dimension(400,200);
		this.setMinimumSize(d);
		this.setMaximumSize(d);
		this.setPreferredSize(d);
		this.setTitle("JavaPos Scanner Health Test");
        this.pack();   // does layout of components.
	    }
	
	public void updateData()
	{
			info.append("New Scale Data Received: " + Double.toString(((double)scale.getWeight())/1000.0)+ " " + scale.getWeightUnitName() + "\r\n");
	}
	
	public void clear()
	{
		info.setText("");
	}


	//@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equalsIgnoreCase("close")) 
		{
			this.setVisible(false);
		}
		else if (e.getActionCommand().equalsIgnoreCase("read"))
		{
			int[] ret = new int[1];
			info.append("Reading weight...\r\n");
			try
			{
				scale.readWeight(ret, 10000);
			}
			catch(JposException ex)
			{
				info.append("Error: " + ex.getMessage() + "\r\n");
			}
		}
	}


}
