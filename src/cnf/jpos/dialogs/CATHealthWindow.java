package cnf.jpos.dialogs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.*;
import javax.swing.border.*;


import cnf.communication.ports.*;
import cnf.jpos.services.GenericCATService;
import cnf.util.*;
import jpos.*;
import jpos.services.CATService111;

public class CATHealthWindow extends JDialog implements ActionListener{
	   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JScrollPane infos = null;
	private JTextArea info = null;
	private JTextField amt = null;
	//private JButton cButton = null;
	private CATService111 cat;
	
	private void addButton(JPanel container,String buttonCaption, String commandID)
	{
		JButton b = new JButton(buttonCaption);
		container.add(b);
		b.setActionCommand(commandID);
	    b.addActionListener(this);
	}
	public CATHealthWindow(CATService111 CATServiceInstance) { // constructor builds GUI
		cat = CATServiceInstance;
		Container c = this.getContentPane();
		c.setLayout(new FlowLayout(FlowLayout.CENTER));
		//JPanel upperPanel = new JPanel();
	    JPanel lowerPanel = new JPanel();
	    c.add(new JLabel("Amount: "));
		Dimension ad = new Dimension(150,20);
	    amt = new JTextField();
	    amt.setText("2.99");
		amt.setMinimumSize(ad);
		amt.setMaximumSize(ad);
		amt.setPreferredSize(ad);
	    
	    c.add(amt);
	    
	    //c.add(upperPanel, "North");
		info = new JTextArea();
		info.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		Dimension td = new Dimension(490,270);
		//info.setMinimumSize(td);
		//info.setMaximumSize(td);
		//info.setPreferredSize(td);
		info.setFont(new Font("Courier New", Font.PLAIN, 12));

		infos = new JScrollPane(info);
		infos.setMinimumSize(td);
		infos.setMaximumSize(td);
		infos.setPreferredSize(td);
		infos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		c.add(infos);
		c.add(lowerPanel, "South");
		
		addButton(lowerPanel,"Sales","sales");
		addButton(lowerPanel,"Void","void");
		addButton(lowerPanel,"Refund","refund");
		addButton(lowerPanel,"Check Card","check");
		addButton(lowerPanel,"Cancel","cancel");
		addButton(lowerPanel,"Close","close");
		addButton(lowerPanel,"Pre-Sales","pre");
		addButton(lowerPanel,"Void Pre-Sales","voidpre");
        addButton(lowerPanel,"Complete Pre-Sales","comppre");
        addButton(lowerPanel,"Get Running Totals","logtotal");
        addButton(lowerPanel,"Settle","logsettle");
		lowerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		Dimension ld = new Dimension(500,100);
		lowerPanel.setMinimumSize(ld);
		lowerPanel.setMaximumSize(ld);
		lowerPanel.setPreferredSize(ld);
	    
		this.setAlwaysOnTop(true);
        Dimension d = new Dimension(500,430);
		this.setMinimumSize(d);
		this.setMaximumSize(d);
		this.setPreferredSize(d);
		this.setResizable(false);
		try{
		this.setTitle(cat.getPhysicalDeviceName() + ": Transaction Opetation");
		}catch(Exception e) {}
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = this.getSize().width;
		int h = this.getSize().height;
		int x = (dim.width-w)/2;
		int y = (dim.height-h)/2;

		// Move the window
		this.setLocation(x, y);

		this.pack();   // does layout of components.
	    }
	
	public void add(String s)
	{
		info.append(s);
		info.setCaretPosition(info.getText().length());
	}
	
	public void clear()
	{
		info.setText("");
	}
	
	public void updateResult()
	{
		add("Done!\r\nTransaction Complete!\r\n\r\n");
		try{
			add("   Transaction Number: " + cat.getTransactionNumber()+"\r\n");
            add("     Transaction Type: " + cat.getTransactionType()+"\r\n");
			add("               Amount: "  + new DecimalFormat("#,###,##0.00").format((double)cat.getSettledAmount()/100.0)+"\r\n");
			add("       Invoice Number: " + cat.getSlipNumber()+"\r\n");
			add("       Account Number: " + cat.getAccountNumber()+"\r\n");
			add("      Card Company ID: " + cat.getCardCompanyID()+"\r\n");
			add("        Approval Code: " + cat.getApprovalCode()+"\r\n");
			add("          Result Code: " + cat.getCenterResultCode()+"\r\n");
			add("      Sequence Number: " + cat.getSequenceNumber()+"\r\n");
			add("                 Info: " + cat.getAdditionalSecurityInformation()+"\r\n\r\n\r\n");		
		}catch(Exception e){}
	}

    public void updateLogResult()
    {
        add("Done!\r\nLog/Settlement Complete!\r\n\r\n");
        try{
            add("Log Results:\r\n");
            add(cat.getDailyLog()+"\r\n\r\n");
        }catch(Exception e){}
    }
	
	public void updateError(String failName, String errorDescription)
	{
		add(failName+"\r\n\r\n");
		try{
			add("    Error Description: " + errorDescription+"\r\n");
			add("   Transaction Number: " + cat.getTransactionNumber()+"\r\n");
			add("          Result Code: " + cat.getCenterResultCode()+"\r\n");
			add("      Sequence Number: " + cat.getSequenceNumber()+"\r\n\r\n\r\n");
		}catch(Exception e){}
	}


	//@Override
	public void actionPerformed(ActionEvent e) {
		String c = e.getActionCommand();
		try{
		if (c.equals("close"))
		{
			try{cat.clearOutput();}catch(Exception ex){}
			this.setVisible(false);
		}
		else if (c.equals("cancel"))
		{
			try{cat.clearOutput();}catch(Exception ex){}			
		}
		else if (c.equals("sales"))
		{
			double a = 0;
			try{a=Double.parseDouble(amt.getText());}catch(Exception ex){a=-1;}
			if (a>0.0)
			{
				add("Sending Sales Command...");
				cat.authorizeSales(getSeqNum(), (long)Math.round(a*100), 0, 1000);
			}
			else add("Please enter a positive number in the text box above!\r\n");			
		}
		else if (c.equals("void"))
		{
			double a = 0;
			try{a=Double.parseDouble(amt.getText());}catch(Exception ex){a=-1;}
			if (a>0.0)
			{
				add("Sending Void Command...");
				cat.authorizeVoid(sqn-1, (long)Math.round(a*100), 0, 1000);
			}
			else add("Please enter a positive number in the text box above!\r\n");			
		}
		else if (c.equals("refund"))
		{
			double a = 0;
			try{a=Double.parseDouble(amt.getText());}catch(Exception ex){a=-1;}
			if (a>0.0)
			{
				add("Sending Refund Command...");
				cat.authorizeRefund(getSeqNum(), (long)Math.round(a*100), 0, 1000);
			}
			else add("Please enter a positive number in the text box above!\r\n");			
		}
		else if (c.equals("pre"))
		{
			double a = 0;
			try{a=Double.parseDouble(amt.getText());}catch(Exception ex){a=-1;}
			if (a>0.0)
			{
				add("Sending Pre-Sales Command...");
				cat.authorizePreSales(getSeqNum(), (long)Math.round(a*100), 0, 1000);
			}
			else add("Please enter a positive number in the text box above!\r\n");			
		}
		else if (c.equals("voidpre"))
		{
			double a = 0;
			try{a=Double.parseDouble(amt.getText());}catch(Exception ex){a=-1;}
			if (a>0.0)
			{
				add("Sending Void Pre-Sales Command...");
				cat.authorizeVoidPreSales(getSeqNum(), (long)Math.round(a*100), 0, 1000);
			}
			else add("Please enter a positive number in the text box above!\r\n");			
		}
		else if (c.equals("comppre"))
		{
			double a = 0;
			try{a=Double.parseDouble(amt.getText());}catch(Exception ex){a=-1;}
			if (a>0.0)
			{
				add("Sending Complete Pre-Sales Command...");
				cat.authorizeCompletion(getSeqNum(), (long)Math.round(a*100), 0, 1000);
			}
			else add("Please enter a positive number in the text box above!\r\n");			
		}
        else if (c.equals("logtotal"))
        {
            add("Sending Daily Log access request (Intermediate Totals only)...");
            cat.accessDailyLog(getSeqNum(),CATConst.CAT_DL_REPORTING, 1000);
        }
        else if (c.equals("logsettle"))
        {
            add("Sending Daily Log access request (retrieve and settle)...");
            cat.accessDailyLog(getSeqNum(),CATConst.CAT_DL_SETTLEMENT, 1000);
        }
		else if (c.equals("check"))
		{
			add("Sending Check Card Command...");
			cat.checkCard(getSeqNum(), 1000);
		}
		}
		catch(JposException ex)
		{
			add("Failed!\r\nMessage:"+ex.getMessage()+"\r\n\r\n\r\n");
		}

	}

	public void closeWindow()
	{
		this.setVisible(false);
	}
	
	private static int sqn=10000;
	private static int getSeqNum()
	{
		return sqn++;
	}


}
