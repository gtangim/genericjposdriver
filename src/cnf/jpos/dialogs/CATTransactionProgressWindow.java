package cnf.jpos.dialogs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;


import cnf.communication.ports.*;
import cnf.jpos.services.GenericCATService;
import cnf.util.*;
import jpos.*;
import jpos.services.CATService111;

public class CATTransactionProgressWindow extends JDialog implements ActionListener{
	   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JScrollPane infos = null;
	private JTextArea info = null;
	private JButton cButton = null;
	private CATService111 cat;
	private boolean cancelled = false;
    public CATTransactionProgressWindow(CATService111 CATServiceInstance) { // constructor builds GUI
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		cat = CATServiceInstance;
		Container c = this.getContentPane();
		JPanel upperPanel = new JPanel();
	    JPanel lowerPanel = new JPanel();
	    c.add(upperPanel, "North");
		c.add(lowerPanel, "South");
		info = new JTextArea();
		info.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		Dimension td = new Dimension(360,400);
		info.setMinimumSize(td);
		info.setMaximumSize(td);
		info.setPreferredSize(td);
		info.setFont(new Font("Courier New", Font.PLAIN, 12));

		infos = new JScrollPane(info);
		c.add(infos);
		cButton = new JButton("Abort Transaction");
		lowerPanel.add(cButton);
		cButton.setActionCommand("cancel");
	    cButton.addActionListener(this);
		this.setAlwaysOnTop(true);
		Dimension d = new Dimension(400,500);
		this.setMinimumSize(d);
		this.setMaximumSize(d);
		this.setPreferredSize(d);
		try{
		this.setTitle(cat.getPhysicalDeviceName() + ": Transaction Opetation");
		}catch(Exception e) {}
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = this.getSize().width;
		int h = this.getSize().height;
		int x = (dim.width-w)/2;
		int y = (dim.height-h)/2;

		// Move the window
		this.setLocation(x, y);

		this.pack();   // does layout of components.
	    }
	
	public void add(String s)
	{
		info.append(s);
		info.setCaretPosition(info.getText().length());
	}
	
	public void clear()
	{
		info.setText("");
        cancelled=false;
	}


	//@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equalsIgnoreCase("cancel")) 
		{
            info.append("\r\nAborting....\r\n");
            ConfirmDialog cd = new ConfirmDialog(
                    "<p style=\"color:black; font-size:18px;\">"
                            +"You are about to abort thist transaction.<br>"
                            +"Make sure to cancel all ongoing transaction on the pinpad first!<br>"
                            +"</p><br><br><p style=\"color:red; font-size:24px;\">"
                            +"Do you really want to abort?</font>"
                    ,"Abort Transaction?");
            String resp = cd.confirm();
            if (resp.equals("yes")) {
                try {
                    cat.clearOutput();
                    try{Thread.sleep(1000);}catch(Exception ext){}
                    cancelled=true;
                    info.append("ABORTED!\r\n");
                    closeWindow();
                } catch (JposException ex) {
                    String m = ex.getMessage();
                    if (m!=null && (m.startsWith("806") || m.startsWith("163")))
                    {
                        info.append("Failed to Abort (Transaction is finalizing)!\r\n");
                        info.append("Please wait until transaction is finished...\r\n");
                    }
                    else
                    {
                        cancelled=true;
                        info.append("ABORTED!\r\n");
                        closeWindow();
                    }
                }
            }
            else info.append("User chose to resume transaction!");
		}
	}

    public boolean isCancelled()
    {
        return cancelled;
    }
	public void closeWindow()
	{
		try{Thread.sleep(250);}catch(Exception ext){}
		this.setVisible(false);
	}

}
