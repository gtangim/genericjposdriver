package cnf.jpos.dialogs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;


import cnf.communication.ports.*;
import cnf.jpos.services.GenericCATService;
import cnf.util.*;
import jpos.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 12/20/11
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConfirmDialog  extends JDialog implements ActionListener {
    private String action = "no";
    private JButton yesButton = null;
    private JButton noButton = null;

    public ConfirmDialog(String message, String title)
    {
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        Container c = this.getContentPane();
        JPanel upperPanel = new JPanel();
        JPanel lowerPanel = new JPanel();
        c.add(upperPanel, "North");
        c.add(lowerPanel, "South");

        JLabel msg = new JLabel("<html><center>"+message+"</center></html>");
        msg.setBackground(new Color(255,207,155));
        Dimension td = new Dimension(540,200);
        msg.setMinimumSize(td);
        msg.setMaximumSize(td);
        msg.setPreferredSize(td);
        upperPanel.add(msg);
        this.setTitle(title);

        yesButton = new JButton("Yes");
        yesButton.setFont(new Font("Arial", Font.BOLD, 24));
        lowerPanel.add(yesButton);
        yesButton.setActionCommand("yes");
        yesButton.addActionListener(this);

        lowerPanel.add(new JLabel("              "));
        
        noButton = new JButton("No");
        noButton.setFont(new Font("Arial", Font.BOLD, 24));
        lowerPanel.add(noButton);
        noButton.setActionCommand("no");
        noButton.addActionListener(this);


        this.setAlwaysOnTop(true);
        Dimension d = new Dimension(600,300);
        this.setMinimumSize(d);
        this.setMaximumSize(d);
        this.setPreferredSize(d);


        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int w = this.getSize().width;
        int h = this.getSize().height;
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;

        // Move the window
        this.setLocation(x, y);

        this.pack();
    }
    
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand()!=null && !e.getActionCommand().trim().equals(""))
            action = e.getActionCommand();
        closeWindow();
    }

    public void closeWindow()
    {
        this.setVisible(false);
    }
    
    public String confirm()
    {
        this.setModal(true);
        this.setAlwaysOnTop(true);
        this.setVisible(true);
        return action;
    }
    
}
