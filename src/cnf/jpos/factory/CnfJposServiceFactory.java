/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.jpos.factory;

import java.util.HashMap;
import java.util.Map;



import jpos.JposConst;
import jpos.JposException;
import jpos.config.JposEntry;
import jpos.config.simple.xml.SimpleXmlRegPopulator;
import jpos.loader.JposServiceInstance;
import jpos.loader.JposServiceInstanceFactory;

import cnf.jpos.services.BaseService;
import cnf.communication.ports.*;
import cnf.communication.protocols.ProtocolInterface;
import apu.jpos.util.*;

public class CnfJposServiceFactory extends Loggable implements JposServiceInstanceFactory {
    // 2.22 - 5/5/2015  - Implemented Store and forward,
    //                  - tackled issues with crash during settlement
    // 2.25 - 07/21/2015 - Minor fix to the scale driver so that it doesnt lockup on COM error while reading weight,
    //                        Convey to POS is moneris is in SAF Mode or not
    // 2.26 - 07/28/2015 - Implements workaround due to Moneris V2.24 bug (cannot get running total),
    //                      Running total is disabled, when they fix this bug, it must be re-enabled.
    // 2.27 - 07/29/2015 - Moneris response without a proper echo field can now be accepted by the driver
    // 2.28 - 01/27/2016 - Payment terminal check() function interfered with seq# when errored (all subsequent request failing)
    // 2.29 - 03/11/2016 - Changes to the client socket pool data receive mechanism so that the async event dont interfere with the command cycle
    // 2.30 - 04/29/2016 - Batch close disabled
    // 2.31 - 05/06/2016 - Fixing bug with sequence number
    // 2.32 - 11/29/2017 - Include support for moneris tokenization
    // 2.34 - 02/07/2018 - Long receipt support added
    // 2.35 - 04/12/2018 - Added a driver for a dummy payment terminal
    // 2.36 - 07/26/2018 - Changed message format for firmware 11.20


    public static String Version = "2.36";
    public static String CommSurrogateVersion = null;
    public static boolean IsMonerisInSAFMode = false;
    public static final String module = CnfJposServiceFactory.class.getName();
    private static Map<String,BaseService> serviceMap = new HashMap<String,BaseService>();
    private static Map<String,PortInterface> portMap = new HashMap<String, PortInterface>();
    //private static SimpleLogger logger = null;

    public CnfJposServiceFactory()
    {
        super(null);
    }

    public JposServiceInstance createInstance(String logicalName, JposEntry entry) throws JposException {
        // check to see if we have a service class property
        if (entry.hasPropertyWithName("LogFileName") &&        logger==null)
    	{
    		logger = new SimpleLogger();
            logger.setLogFile((String)entry.getPropertyValue("LogFileName"));
    		logger.setDelayedWrite(false);
    	    log(" ");
            log(" ");
            log(" ");
            log("==============================================");
            log("   Starting Generic Jpos Driver Engine v"+Version);
            log("==============================================");
    	  	SimpleXmlRegPopulator sp = new SimpleXmlRegPopulator();
            log("Enumerating devices from " + sp.getEntriesURL() + "/jpos.xml");
    	}
        if (!entry.hasPropertyWithName(JposEntry.SERVICE_CLASS_PROP_NAME)) {
            logError("error: serviceClass property not found in "+ entry.getLogicalName()+"!");
        	throw new JposException(JposConst.JPOS_E_NOSERVICE, "serviceClass property not found!");
        }
        
        String className = (String) entry.getPropertyValue(JposEntry.SERVICE_CLASS_PROP_NAME);
        BaseService service = serviceMap.get(className);
        if (service != null) {
            service.setEntry(entry);
        } else {
            try {
                log(" ");
                log(" ");
            	log(":::::::::: Initializing Device: "  + logicalName + " ::::::::::");
                //log("Creating service-class instance of " + className+"!");
                Object obj = getInstance(className);
                if (obj == null)
                    throw new JposException(JposConst.JPOS_E_NOSERVICE, "Could not locate serviceClass " + className + " for " + entry.getLogicalName()+"!");

                if (!(obj instanceof JposServiceInstance)) 
                    throw new JposException(JposConst.JPOS_E_NOSERVICE,  "serviceClass "+className+" in " + entry.getLogicalName() + " is not derived from jpos.loader.JposServiceInstance!");
                else if (!(obj instanceof BaseService)) 
                    throw new JposException(JposConst.JPOS_E_NOSERVICE,  "serviceClass "+className+" in " + entry.getLogicalName() + " is not derived from cnf.jpos.services.BaseService!");
                else {                	
                    service = (BaseService) obj;
                    service.setEntry(entry);
                    serviceMap.put(className, service);
                }
            } catch (Exception e) {
                logError("error: Failed to create service instance! Reason: " + e.getMessage());
            	throw new JposException(JposConst.JPOS_E_NOSERVICE, "Error creating the service instance [" + className + "]", e);
            }
        }

        // Driver Service class instantiated,... now setup the port...
        String portName =  (String) entry.getPropertyValue("PortName");
        /*if (portName==null)
        {
            logError("error: PortName property not found in "+ entry.getLogicalName()+"!");
            throw new JposException(JposConst.JPOS_E_NOSERVICE, "PortName property not found!");
        }*/

        PortInterface port = null;

        if (portName!=null)
        {
            //log("Port Name: "+ portName);
            port = portMap.get(portName.toLowerCase());

            if (port==null)
            {
                //log("Creating port instance...");
                try {
                    if (!entry.hasPropertyWithName("PortClass"))
                        throw new JposException(JposConst.JPOS_E_FAILURE, "PortClass property not found in " + entry.getLogicalName() + "!");
                    String portClassName = (String) entry.getPropertyValue("PortClass");
                    log("Device Port: " + portName + "[" + portClassName + "]");

                    Object portobj = getInstance(portClassName);
                    if (portobj == null)
                        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Could not locate PortClass " + className + " for " + entry.getLogicalName() + "!");

                    if (!(portobj instanceof PortInterface))
                        throw new JposException(JposConst.JPOS_E_NOSERVICE, "PortClass " + className + " in " + entry.getLogicalName() + " is not derived from cnf.communication.ports.PortInterface!");
                    else {
                        port = (PortInterface) portobj;
                        portMap.put(portName.toLowerCase(), port);
                    }
                }
                catch(Exception e)
                {
                    log("error: Failed to create driver port instance! Reason: " + e.getMessage());
                    throw new JposException(JposConst.JPOS_E_NOSERVICE, "Error creating the port instance.", e);
                }
            }
        }
        ProtocolInterface protocol = null;
        String protocolClassName =  (String) entry.getPropertyValue("ProtocolClass");
        if (protocolClassName!=null)
        {
            //log("Creating protocol instance...");
        	try
        	{            
        		Object protocolobj = getInstance(protocolClassName);
                if (protocolobj == null) 
                    throw new JposException(JposConst.JPOS_E_NOSERVICE, "Could not locate ProtocolClass " + protocolClassName + " for " + entry.getLogicalName()+"!");

                if (!(protocolobj instanceof ProtocolInterface)) 
                    throw new JposException(JposConst.JPOS_E_NOSERVICE,  "ProtocolClass "+protocolClassName+" in " + entry.getLogicalName() + " is not derived from cnf.communication.protocols.ProtocolInterface!");
                else {                	
                    protocol = (ProtocolInterface) protocolobj;
                    protocol.setPort(port);
                }
        	}
        	catch(Exception e)
        	{
                log("error: Failed to create driver protocol instance! Reason: " + e.getMessage());
            	throw new JposException(JposConst.JPOS_E_NOSERVICE, "Error creating the protocol instance.", e);
        	}
        }
        
        
        if (port!=null) service.setPort(port);
        if (protocol!=null) service.setProtocol(protocol);
        //log("Returning instance of " + className+"!");
        return service;
    }
    
    /**
     * Returns an instance of the specified class.  This uses the default
     * no-arg constructor to create the instance.
     * @param className Name of the class to instantiate
     * @return An instance of the named class
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static Object getInstance(String className) throws ClassNotFoundException,
            InstantiationException, IllegalAccessException {
        Class<?> c = loadClass(className);
        Object o = c.newInstance();

        //if (Debug.verboseOn()) Debug.logVerbose("Instantiated object: " + o.toString(), module);
        return o;
    }
 
    /**
     * Loads a class with the current thread's context classloader.
     * @param className The name of the class to load
     * @return The requested class
     * @throws ClassNotFoundException
     */
    public static Class<?> loadClass(String className) throws ClassNotFoundException {
 
        Class<?> theClass = null;

        if (theClass != null) return theClass;
        
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try {
            theClass = loader.loadClass(className);
        } catch (Exception e) {
        	theClass = Class.forName(className);
        }
        
        return theClass;
    }
   
}
