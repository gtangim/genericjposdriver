package cnf.jpos.services;

import apu.jpos.com.surrogate.CSEventListener;
import apu.jpos.com.surrogate.ComSurrogate;
import apu.jpos.com.surrogate.net.SocketInfo;
import apu.jpos.com.surrogate.net.protocol.CSServerEvent;
import apu.jpos.util.Utility;
import cnf.communication.ports.PortDataEvent;
import cnf.communication.ports.PortDataEvent.PortDataEventType;
import cnf.communication.ports.PortIOException;
import cnf.jpos.dialogs.PrinterHealthWindow;
import cnf.jpos.dialogs.ScaleHealthWindow;
import apu.jpos.util.StringUtil;
import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinterConst;
import jpos.ScaleConst;
import jpos.events.DataEvent;
import jpos.services.EventCallbacks;
import jpos.services.POSPrinterService111;
import org.joda.time.DateTime;

import javax.xml.bind.DatatypeConverter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EpsonPrinterService extends BaseService implements POSPrinterService111, CSEventListener {
    public static EpsonPrinterService CurrentPrinter = null;

    String id = "Printer";
    int commandTimeout = 30 * 1000;
    int statusCheckIntervalMs = 10 * 1000;
    String eol = "\n";

    protected boolean autoDisable = false;
	protected boolean asyncMode = false;
	protected boolean busy = false;
    protected String manufacturer = null;
    protected String model = null;
    protected boolean printerOffline = false;
    protected boolean printerError = false;
    protected boolean coverOpen = false;
    protected boolean paperOut = false;
    protected boolean paperNearEnd = false;
    protected boolean drawerOpen = false;



    protected String surrogateAddress = null;
    protected int surrogatePort = 2000;
    protected String logFileName = "GenericJposDriverLogs.txt";
    protected ComSurrogate api = null;
    protected boolean ready = false;
    protected boolean bufferMode = false;
    protected ByteBuffer writeBuffer = ByteBuffer.allocate(16*1024*1024);
    private DateTime lastStatusCheck = null;

    protected PrinterHealthWindow hwin = null;

	public EpsonPrinterService()
	{
		autoDisable = false;
	    asyncMode = false;

	}
	
	//@Override
    public void open(String deviceName, EventCallbacks ecb) throws JposException {
        autoDisable = false;
        asyncMode = false;

        reset();
        initializeProperties();
        initializePropertyValues();
        try {
            log("Connecting to the Epson Printer via ComPort Surrogate Server....");
            try {
                api = ComSurrogate.getInstance(surrogateAddress, surrogatePort, "JPOS Driver 1.0");
                api.addEventHandler(this);
                api.waitForLogin();
                ready = true;
            } catch (Exception ex) {
                log(ex);
                throw new JposException(JposConst.JPOS_E_FAILURE,"Failed to connect to Com Surrogate Server!", ex);
            }
            log("Initializing the Epson Printer....");
            api.sendCommand("SetReadDelay", 200);
            ByteBuffer cmd = ByteBuffer.allocate(10);
            cmd.put((byte)0x1d);
            cmd.put((byte)0x49);
            cmd.put((byte)66);
            manufacturer = sendCommand(cmd,true);
            if (manufacturer==null || !manufacturer.toLowerCase().contains("epson"))
                throw new Exception("Attached printer is not an epson model!");
            cmd.clear();
            cmd.put((byte)0x1d);
            cmd.put((byte)0x49);
            cmd.put((byte)67);
            model = sendCommand(cmd,true);
            if (model==null) throw new Exception("Could not retrieve printer model number!");
            log("Detected Printer >> " + manufacturer+" "+ model);
            getPrinterStatus();
            if (coverOpen) throw new Exception("Printer cover is open!");
            else if (paperOut) throw new Exception("Printer has no paper!");
            else if (printerOffline) throw new Exception("Printer is offline and not ready for printing!");

            // Setting up default print settings....
            cmd.clear();
            cmd.put((byte)0x1b);
            cmd.put((byte)0x61);
            cmd.put((byte)0);
            cmd.put((byte)0x1b);
            cmd.put((byte)0x21);
            cmd.put((byte)0x00);
            sendCommand(cmd,false);

            CurrentPrinter = this;

        } catch (Exception e) {
            error(JposConst.JPOS_E_NOHARDWARE, "Unable to initialize hardware!",e);
        }

        super.open(deviceName, ecb);
    }    	

    private void reset()
    {

    }

    //@Override
    public void close() throws JposException
    {
        log(deviceName+" >> " + "Closing device...");
    	autoDisable = false;
    	asyncMode = false;
    	super.close();
    }
	   
	

	private void initializeProperties()
	{
        this.resetProperties();

        //log(deviceName + " >> " + "Setting property names for this device.");

        registerProperty("ComSurrogateServerAddress", "IpAddress/name of the Cnf ComPort Surrogate Server","127.0.0.1",null);
        registerProperty("ComSurrogatePort", "TCP Port number for Cnf ComPort Surrogate","2000",null);
        registerProperty("LogFileName", "Driver activity Log File Path","GenericJposDriverLogs.txt",null);
	}
	
	private void initializePropertyValues() throws JposException 
	{
        this.loadPropertyValues(entry);
        surrogateAddress = getPropertyValue("ComSurrogateServerAddress");
        surrogatePort = create_int("ComSurrogatePort", 1000, 99000);
        logFileName = getPropertyValue("LogFileName");
	}


    private int create_int(String propName, int min, int max) throws JposException
    {
        int t = 0;
        try
        {
            t = getPropertyValueInteger(propName);
        }
        catch(JposException e)
        {
            error(JposConst.JPOS_E_NOSERVICE,"You must assign a valid integer for " + propName);
        }
        if (t<min || t>max)
            error(JposConst.JPOS_E_NOSERVICE, "Property " + propName + "=" + t
                    +" is out of range! Valid range is "
                    + Integer.toString(min) + ".." + Integer.toString(max));

        return t;

    }

    public boolean isReady() {
        if (ready) return true;
        try {
            api.waitForLogin();
            ready = true;
            return ready;
        } catch (Exception ex) {
            log(ex);
            return false;
        }

    }




    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapConcurrentPageMode() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecPageMode() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpPageMode() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapMapCharacterSet() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCapJrnCartridgeSensor() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCapJrnColor() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCapRecCartridgeSensor() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCapRecColor() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCapRecMarkFeed() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpBothSidesPrint() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCapSlpCartridgeSensor() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCapSlpColor() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
    public int getCapCharacterSet() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapConcurrentJrnRec() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapConcurrentJrnSlp() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapConcurrentRecSlp() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapCoverSensor() throws JposException {
        return true;
    }

    public boolean getCapJrn2Color() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnBold() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnDhigh() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnDwide() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnDwideDhigh() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnEmptySensor() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnItalic() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnNearEndSensor() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnPresent() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapJrnUnderline() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRec2Color() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecBarCode() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecBitmap() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecBold() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecDhigh() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecDwide() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecDwideDhigh() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecEmptySensor() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecItalic() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecLeft90() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecNearEndSensor() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecPapercut() throws JposException {
        return true;
    }

    public boolean getCapRecPresent() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecRight90() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecRotate180() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecStamp() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapRecUnderline() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlp2Color() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpBarCode() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpBitmap() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpBold() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpDhigh() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpDwide() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpDwideDhigh() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpEmptySensor() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpFullslip() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpItalic() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpLeft90() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpNearEndSensor() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpPresent() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpRight90() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpRotate180() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapSlpUnderline() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getCapTransaction() throws JposException {
        return true;
    }

    public int getCapPowerReporting() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }










    public void printNormal(int station, String data) throws JposException {
        check(true,true,true);
        getPrinterStatus();

        // Debug
        /*StringBuilder sb = new StringBuilder();
        for (int i=0;i<400;i++) sb.append("This is line " + Integer.toString(i+1)+"!\r\n");
        data = sb.toString();*/
        //Debug

        if (coverOpen) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer cover is open!");
        else if (paperOut) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer has no paper!");
        else if (printerOffline) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer is offline and not ready for printing!");
        if (station== POSPrinterConst.PTR_S_RECEIPT
                || station==POSPrinterConst.PTR_S_JOURNAL_RECEIPT
                || station==POSPrinterConst.PTR_S_RECEIPT_SLIP)
        {
            try {
                sendCommand(data+eol,false);
            } catch (Exception e) {
                log(e);
                throw new JposException(JposConst.JPOS_E_FAILURE,"Failed to print!",e);
            }

        }

    }

    public void setMapMode(int i) throws JposException {
        check(true,true,true);
        logWarning("The Map Mode feature is not implemented in this printer driver!");
    }


    public void printBarCode(int station, String data, int symbology, int height,
                             int width, int alignment, int textPosition) throws JposException {
        check(true,true,true);
        getPrinterStatus();
        if (coverOpen) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer cover is open!");
        else if (paperOut) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer has no paper!");
        else if (printerOffline) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer is offline and not ready for printing!");
        if (station==POSPrinterConst.PTR_S_RECEIPT) {
            ByteBuffer cmd = ByteBuffer.allocate(100);
            cmd.put(eol.getBytes());

            // Setting alignment
            byte align = 0;
            if (alignment==POSPrinterConst.PTR_BC_LEFT) align = 0;
            else if (alignment==POSPrinterConst.PTR_BC_CENTER) align = 1;
            else align = 2;
            cmd.put((byte)0x1b);
            cmd.put((byte)0x61);
            cmd.put(align);

            // Setting font
            cmd.put((byte)0x1d);
            cmd.put((byte)0x66);
            cmd.put((byte)1);

            // Setting text position
            byte txt=0;
            if (textPosition==POSPrinterConst.PTR_BC_TEXT_NONE) txt = 0;
            else if (textPosition==POSPrinterConst.PTR_BC_TEXT_ABOVE) txt = 1;
            else if (textPosition==POSPrinterConst.PTR_BC_TEXT_BELOW) txt = 2;
            else txt = 0;
            cmd.put((byte)0x1d);
            cmd.put((byte)0x48);
            cmd.put(txt);

            // Height and width
            if (width<1) width=1;
            else if (width>6) width=6;
            if (height<1) height=1;
            else if (height>255) height=255;
            cmd.put((byte)0x1d);
            cmd.put((byte)0x68);
            cmd.put((byte)height);
            cmd.put((byte)0x1d);
            cmd.put((byte)0x77);
            cmd.put((byte)width);


            byte bType = 0;
            if (symbology==POSPrinterConst.PTR_BCS_UPCA) bType = 65;
            else if (symbology==POSPrinterConst.PTR_BCS_UPCE) bType = 66;
            else if (symbology==POSPrinterConst.PTR_BCS_EAN13) bType = 67;
            else if (symbology==POSPrinterConst.PTR_BCS_EAN8) bType = 68;
            else if (symbology==POSPrinterConst.PTR_BCS_JAN13) bType = 67;
            else if (symbology==POSPrinterConst.PTR_BCS_JAN8) bType = 68;
            else if (symbology==POSPrinterConst.PTR_BCS_Code39) bType = 69;
            else if (symbology==POSPrinterConst.PTR_BCS_ITF) bType = 70;
            else if (symbology==POSPrinterConst.PTR_BCS_Codabar) bType = 71;
            else if (symbology==POSPrinterConst.PTR_BCS_Code93) bType = 72;
            else if (symbology==POSPrinterConst.PTR_BCS_Code128) bType = 73;
            else throw new JposException(JposConst.JPOS_E_ILLEGAL
                        ,"This barcode type is not supported by this printer!");

            cmd.put((byte)0x1d);
            cmd.put((byte)0x6b);
            cmd.put(bType);
            cmd.put((byte)data.length());
            cmd.put(data.getBytes());

            try {
                sendCommand(cmd,false);
            } catch (Exception e) {
                log(e);
                throw new JposException(JposConst.JPOS_E_FAILURE,"Failed to print!",e);
            }
        }
    }

    public void transactionPrint(int station, int control) throws JposException {
        check (true,true,true);
        if (station==POSPrinterConst.PTR_S_RECEIPT)
        {
            if (control==POSPrinterConst.PTR_TP_TRANSACTION)
            {
                clearOutput();
                bufferMode=true;
            }
            else if (control==POSPrinterConst.PTR_TP_NORMAL)
            {
                getPrinterStatus();
                if (coverOpen) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer cover is open!");
                else if (paperOut) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer has no paper!");
                else if (printerOffline) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer is offline and not ready for printing!");
                if (writeBuffer.position()>0)
                {
                    bufferMode=false;
                    try {
                        sendCommand(writeBuffer,false);
                    } catch (Exception e) {
                        log(e);
                        throw new JposException(JposConst.JPOS_E_FAILURE, "Unable to write to the printer!", e);
                    }
                }
                clearOutput();
            }
        }
    }

    public void cutPaper(int i) throws JposException {
        check(true,true,true);
        getPrinterStatus();
        if (coverOpen) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer cover is open!");
        else if (paperOut) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer has no paper!");
        else if (printerOffline) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer is offline and not ready for printing!");
        ByteBuffer cmd = ByteBuffer.allocate(4);
        cmd.put((byte)0x1d);
        cmd.put((byte)0x56);
        if (i==100) cmd.put((byte)48);
        else cmd.put((byte)49);
        try {
            sendCommand(cmd,false);
        } catch (Exception e) {
            log(e);
            throw new JposException(JposConst.JPOS_E_FAILURE,"Failed to print!",e);
        }
    }

    public void openDrawer() throws JposException
    {
        check(true,true,true);
        getPrinterStatus();
        if (coverOpen) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer cover is open!");
        else if (paperOut) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer has no paper!");
        else if (printerOffline) throw new JposException(JposConst.JPOS_E_FAILURE,"Printer is offline and not ready for printing!");
        ByteBuffer cmd = ByteBuffer.allocate(10);
        cmd.put((byte)0x1b);
        cmd.put((byte)0x70);
        cmd.put((byte)0x0);
        cmd.put((byte)0x80);
        cmd.put((byte)0x80);

        try {
            sendCommand(cmd,false);
            Thread.sleep(300);
        } catch (Exception e) {
            log(e);
            throw new JposException(JposConst.JPOS_E_FAILURE,"Failed to print!",e);
        }
    }


    public void getPrinterStatus() {
        try {
            if (lastStatusCheck!=null && Utility.getElapsed(lastStatusCheck)<statusCheckIntervalMs)
            {
                // Only skip test if everything is good....
                if (printerError==false && printerOffline==false && coverOpen==false)
                    return;
            }

            ByteBuffer cmd = ByteBuffer.allocate(10);

            cmd.put((byte)0x10);
            cmd.put((byte)0x04);
            cmd.put((byte)1);
            sendCommand(cmd,true);
            int r1 = responseCode;

            cmd.clear();
            cmd.put((byte)0x10);
            cmd.put((byte)0x04);
            cmd.put((byte)2);
            sendCommand(cmd,true);
            int r2 = responseCode;

            cmd.clear();
            cmd.put((byte)0x10);
            cmd.put((byte)0x04);
            cmd.put((byte)4);
            sendCommand(cmd,true);
            int r4 = responseCode;


            printerOffline = ((r1 & 0x08) != 0);
            drawerOpen = ((r1 & 0x04) == 0);

            printerError = ((r2 & 0x40) != 0);
            paperOut = ((r2 & 0x20) != 0);
            coverOpen = ((r2 & 0x04) != 0);

            paperNearEnd =  ((r4 & 0x0c) != 0);
            lastStatusCheck = new DateTime();
        } catch (Exception e) {
            log(e);
            printerError=true;
            printerOffline=true;
        }
    }

    public boolean getCoverOpen() throws JposException {
        check(true,true,true);
        getPrinterStatus();
        return coverOpen;
    }

    public void printImmediate(int station, String data) throws JposException {
        printNormal(station,data);
    }

    public boolean getAsyncMode() throws JposException {
        return asyncMode;
    }

    public void setAsyncMode(boolean b) throws JposException {
        asyncMode=b;
    }

    public void clearOutput() throws JposException {
        check(true,true,true);
        writeBuffer.clear();
    }

    public void onPortData(PortDataEvent evt) {
        // Not needed
    }





    //@Override
    public void checkHealth(int typ) throws JposException {
        boolean async = asyncMode;
        this.setAsyncMode(true);
        if (typ==JposConst.JPOS_CH_INTERNAL) healthText = "Successful";
        else if (typ==JposConst.JPOS_CH_EXTERNAL) healthText = "Successful";
        else if (typ==JposConst.JPOS_CH_INTERACTIVE)
        {
            healthText = "Checking";
            if (hwin==null) hwin = new PrinterHealthWindow(this);
            hwin.clear();
            hwin.setModal(false);
            hwin.setVisible(true);
            while (hwin.isVisible())
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
        else
        {
            this.setAsyncMode(async);
            error(JposConst.JPOS_E_ILLEGAL,"Invalid checkHealth Type invoked!");
        }
        this.setAsyncMode(async);
    }
    protected void check(boolean open, boolean claim, boolean enable) throws JposException
    {
        if (enable==true && this.deviceEnabled==false)
            error(JposConst.JPOS_E_DISABLED, "Device must be opened, claimed and enabled first!");
        else if (claim==true && this.deviceClaimed==false)
            error(JposConst.JPOS_E_NOTCLAIMED, "Device must be opened and claimed first!");
        else if (open==true && this.deviceOpen==false)
            error(JposConst.JPOS_E_CLOSED, "Device must be opened first!");
    }


    protected String sendCommand(ByteBuffer cmd, boolean waitForResponse) throws Exception
    {
        if (bufferMode && !waitForResponse)
        {
            int n = cmd.position();
            cmd.rewind();
            byte[] data = new byte[n];
            cmd.get(data);
            writeBuffer.put(data);
            return null;
        }
        if (waitForResponse) {
            synchronized (objWaiter) {
                initResponse();
                api.sendCommand("send", id, cmd);
                String res=waitForResponse(commandTimeout);
                if (res==null) throw new Exception("Command Timeout!");
                return res;
            }
        }
        else
        {
            api.sendData("send", id, cmd);
            //Thread.sleep(200);
            return null;
        }
    }
    protected String sendCommand(String cmd, boolean waitForResponse) throws Exception
    {
        if (bufferMode && !waitForResponse)
        {
            writeBuffer.put(StringUtil.asciiParseBinary(cmd));
            return null;
        }
        if (waitForResponse) {
            synchronized (objWaiter) {
                initResponse();
                api.sendCommand("send", id, DatatypeConverter.printBase64Binary(StringUtil.asciiParseBinary(cmd)));
                String res = waitForResponse(commandTimeout);
                if (res == null) throw new Exception("Command Timeout!");
                return res;
            }
        }
        else
        {
            api.sendData("send", id, DatatypeConverter.printBase64Binary(StringUtil.asciiParseBinary(cmd)));
            return null;
        }
    }


    private Object objWaiter = new Object();
    private String response;
    private int responseCode;
    private void initResponse()
    {
        synchronized (objWaiter) {
            response = null;
            responseCode = -1;
        }
    }
    private String waitForResponse(int timeout)
    {
        synchronized (objWaiter)
        {
            response=null;
            responseCode=-1;
            try {
                objWaiter.wait(timeout);
                if (response!=null) return response;
                else if (responseCode>=0) return Integer.toString(responseCode);
                else return null;
            } catch (InterruptedException e) {
                return null;
            }
        }
    }




    //@Override
    public void onServerEvent(SocketInfo socket, CSServerEvent event) {
        if (event.getEventType().equals("data"))
        {
            String source = event.getParameters().get(0);
            if (source.trim().equalsIgnoreCase(id))
            {
                byte[] data = DatatypeConverter.parseBase64Binary(event.getParameters().get(1));
                try {
                    synchronized (objWaiter)
                    {
                        if (data.length>=1 && data[data.length-1]==0)
                        {
                            StringBuilder sb = new StringBuilder(data.length);
                            sb.append(new String(data));
                            sb.setLength(data.length-1);
                            response = sb.toString();
                        }
                        else if (data.length>=4)
                        {
                            // The following is to filter out the printer error when drawer event is mixed with
                            // status command. Generally drawer status should be ignored (not used at this point).
                            response=new String(data);
                            if (response.contains("[14][00][00][0F]"))
                            {
                                log("Drawer has been closed!");
                                response = response.replace("[14][00][00][0F]","");
                            }
                            if (response.contains("[10][00][00][0F]"))
                            {
                                log("Drawer has been opened!");
                                response = response.replace("[10][00][00][0F]","");
                            }
                            if (response.length()==0) return; // Dont notify for command response!
                            if (response.length()>=4 && response.charAt(0)=='[' && response.charAt(3)==']')
                            {
                                // Actual code response...
                                String resp = response.substring(1,3);
                                responseCode= Integer.parseInt(resp,16);
                            }
                        }
                        else if (data.length==1) responseCode = ((int)data[0]) & 0xff;
                        else response=new String(data);
                        objWaiter.notifyAll();
                    }
                } catch (Exception e) {
                    log(e);
                }
            }
        }
    }


    public boolean isPrinterOffline() {
        return printerOffline;
    }

    public boolean isPrinterError() {
        return printerError;
    }

    public boolean isCoverOpen() {
        return coverOpen;
    }

    public boolean isPaperOut() {
        return paperOut;
    }

    public boolean isPaperNearEnd() {
        return paperNearEnd;
    }

    public boolean isDrawerOpen() {
        return drawerOpen;
    }

    public void printMemoryBitmap(int i, byte[] bytes, int i2, int i3, int i4) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getPageModeArea() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getPageModeDescriptor() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getPageModeHorizontalPosition() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setPageModeHorizontalPosition(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getPageModePrintArea() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setPageModePrintArea(String s) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getPageModePrintDirection() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setPageModePrintDirection(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getPageModeStation() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setPageModeStation(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getPageModeVerticalPosition() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setPageModeVerticalPosition(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void clearPrintArea() throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void compareFirmwareVersion(String s, int[] ints) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void pageModePrint(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void updateFirmware(String s) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void resetStatistics(String s) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void retrieveStatistics(String[] strings) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void updateStatistics(String s) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getMapCharacterSet() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setMapCharacterSet(boolean b) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getRecBitmapRotationList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getSlpBitmapRotationList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCartridgeNotify() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setCartridgeNotify(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getJrnCartridgeState() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getJrnCurrentCartridge() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setJrnCurrentCartridge(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecCartridgeState() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecCurrentCartridge() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setRecCurrentCartridge(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpCartridgeState() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpCurrentCartridge() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setSlpCurrentCartridge(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpPrintSide() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void changePrintSide(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void markFeed(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getPowerNotify() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setPowerNotify(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getPowerState() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getCharacterSet() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setCharacterSet(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCharacterSetList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getErrorLevel() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getErrorStation() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getErrorString() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getFlagWhenIdle() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setFlagWhenIdle(boolean b) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getFontTypefaceList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getJrnEmpty() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getJrnLetterQuality() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setJrnLetterQuality(boolean b) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getJrnLineChars() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setJrnLineChars(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getJrnLineCharsList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getJrnLineHeight() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setJrnLineHeight(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getJrnLineSpacing() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setJrnLineSpacing(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getJrnLineWidth() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getJrnNearEnd() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getMapMode() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getOutputID() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getRecBarCodeRotationList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getRecEmpty() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getRecLetterQuality() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setRecLetterQuality(boolean b) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecLineChars() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setRecLineChars(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getRecLineCharsList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecLineHeight() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setRecLineHeight(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecLineSpacing() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setRecLineSpacing(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecLinesToPaperCut() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecLineWidth() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getRecNearEnd() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecSidewaysMaxChars() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRecSidewaysMaxLines() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getRotateSpecial() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setRotateSpecial(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getSlpBarCodeRotationList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getSlpEmpty() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getSlpLetterQuality() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setSlpLetterQuality(boolean b) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpLineChars() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setSlpLineChars(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getSlpLineCharsList() throws JposException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpLineHeight() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setSlpLineHeight(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpLinesNearEndToEnd() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpLineSpacing() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setSlpLineSpacing(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpLineWidth() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpMaxLines() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean getSlpNearEnd() throws JposException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpSidewaysMaxChars() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getSlpSidewaysMaxLines() throws JposException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void beginInsertion(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void beginRemoval(int i) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void endInsertion() throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void endRemoval() throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void printBitmap(int i, String s, int i2, int i3) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void printTwoNormal(int i, String s, String s2) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void rotatePrint(int i, int i2) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setBitmap(int i, int i2, String s, int i3, int i4) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setLogo(int i, String s) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void validateData(int i, String s) throws JposException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

}
