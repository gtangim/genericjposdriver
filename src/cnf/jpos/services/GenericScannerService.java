/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.jpos.services;

import java.util.*;
import java.util.regex.*;

import cnf.communication.ports.PortDataEvent;
import cnf.jpos.dialogs.*;
//import cnf.util.StringUtil;

import jpos.JposConst;
import jpos.JposException;
import jpos.ScannerConst;
import jpos.services.EventCallbacks;
import jpos.config.JposEntry;
import jpos.events.DataEvent;


/**
 * Generic Scanner (Scanner/Scale) driver  
 * You need to know what pattern the data comes in as through the port.
 * Then you provide the format as a Java Regex (regular expression)
 * in the jpos.xml! 
 * 
 * The driver uses generic port interfaces. So you can implement
 * your own port interface (i.e. TCP/IP) and attach to this driver.
 * 
 * Some basic assumptions made in this driver are:
 *    a. The driver communicates through generic ports that hides protocol (direct IO, no packetization, ack/nak etc.)
 *         * If some non-trivial communication protocol is necessary then you should implement your own port interface
 *    b. When a barcode is scanned a stream of characters is sent through the port in some specific format
 *    c. The barcode byte stream that is sent after a scan contains the actual barcode as a substring. 
 *    d. Each type of barcode (i.e. UPCA) sends data in a particular format that is distinguishable from other types
 *    
 * The Driver disobeys the following JavaPOS specs:
 *    a. Timeout mechanism in claim is not implemented, currently it wouldn't wait for device to be claimed.
 *    b. It doesnt filter header trailer info from the ScanData. Header/Trailer varies greatly across scanner.
 *    c. We assume that user will decode the data and use getScanDataLabel instead of getting getScanData (scandata violates (b));
 *    d. Power Notify is not implemented
 */


public class GenericScannerService extends BaseService implements jpos.services.ScannerService17
{

	private static Map<String, Integer> codeMap = null;
	private List<CodeFormatEntry> codeList = null;

    protected byte[] scannedDataLabel = new byte[0];
    protected byte[] scannedData = new byte[0];

    protected boolean decodeData = false;
    protected boolean autoDisable = false;
    //protected int powerState = 1;
    
    protected StringBuffer localBuffer = null;
    protected String rawData;
    protected String dataLabel;
    protected int dataLabelType;
    protected ScannerHealthWindow hwin = null;

    public GenericScannerService() {
        initializeCodeMap();
    }

    public void open(String deviceName, EventCallbacks ecb) throws JposException {
        localBuffer = new StringBuffer();
        rawData=null;
        dataLabel=null;
        dataLabelType=ScannerConst.SCAN_SDT_UNKNOWN;
        decodeData = false;
        autoDisable = false;
    	super.open(deviceName, ecb);
        //log(deviceName + " >> " + "Initializing Code Filters...");
        initializeCodeValues();       
    }
    
    //@Override
    public void close() throws JposException
    {
        log(deviceName + " >> " + "Closing device...");
    	localBuffer = null;
    	super.close();
    }

    // ScannerService12
    //@Override
    public boolean getAutoDisable() throws JposException {
        return autoDisable;
    }

    //@Override
    public void setAutoDisable(boolean b) throws JposException {
        if (!deviceOpen) error(JposConst.JPOS_E_CLOSED, "Device must be opened before AutoDisable!");
    	this.autoDisable = b;
        log(deviceName + " >> " + "Auto_Disable = "+Boolean.toString(b));
    }

    //@Override
    public boolean getDecodeData() throws JposException {
        return this.decodeData;
    }

    //@Override
    public void setDecodeData(boolean b) throws JposException {
        if (!deviceOpen) error(JposConst.JPOS_E_CLOSED, "Device must be opened before setDecodeData!");
    	this.decodeData = b;
        //log(deviceName + " >> " + "Decode_Data = "+Boolean.toString(b));
    }

    //@Override
    public byte[] getScanData() throws JposException {
        if (rawData!=null)
        	return rawData.getBytes();
        else return new byte[0];
    }

    //@Override
    public byte[] getScanDataLabel() throws JposException {
        if (this.decodeData && dataLabel!=null) {
            return dataLabel.getBytes();
        } else {
            return new byte[0];
        }
    }

    //@Override
    public int getScanDataType() throws JposException {        
        return dataLabelType;
    }

    //@Override
    public void clearInput() throws JposException {
        
    	dataLabel = null;
    	rawData = null;
    	dataLabelType = ScannerConst.SCAN_SDT_UNKNOWN;
    	localBuffer = new StringBuffer();
    	clearDataEvents();
        log(deviceName + " >> " + "Buffer has been cleared!");
        /*
        this.codeId = new String();
        */
    }

    // ScannerService13
    //@Override
    public int getCapPowerReporting() throws JposException {
        // This driver does not have the ability to detect device power
    	return JposConst.JPOS_PR_NONE;
    }

    //@Override
    public int getPowerNotify() throws JposException {
        log(deviceName + " >> " + "WARNING: This is not implemented!");
        return 0;
    }

    public void setPowerNotify(int i) throws JposException {
        error(JposConst.JPOS_E_ILLEGAL, "Power Notify is not supported!");
    }

    public int getPowerState() throws JposException {
        log(deviceName + " >> " + "WARNING: This is not implemented!");
        return 0;
    }

 
    private void initializeCodeMap()
    {
    	codeMap = new HashMap<String,Integer>();
    	codeMap.put("AZTEC", ScannerConst.SCAN_SDT_AZTEC);
    	codeMap.put("CCA", ScannerConst.SCAN_SDT_CCA);
    	codeMap.put("CCB", ScannerConst.SCAN_SDT_CCB);
    	codeMap.put("CCC", ScannerConst.SCAN_SDT_CCC);
    	codeMap.put("CODABAR", ScannerConst.SCAN_SDT_Codabar);
    	codeMap.put("CODE128", ScannerConst.SCAN_SDT_Code128);
    	codeMap.put("CODE39", ScannerConst.SCAN_SDT_Code39);
    	codeMap.put("CODE93", ScannerConst.SCAN_SDT_Code93);
    	codeMap.put("DATAMATRIX", ScannerConst.SCAN_SDT_DATAMATRIX);
    	codeMap.put("EAN128", ScannerConst.SCAN_SDT_EAN128);
    	codeMap.put("EAN13", ScannerConst.SCAN_SDT_EAN13);
    	codeMap.put("EAN13_S", ScannerConst.SCAN_SDT_EAN13_S);
    	codeMap.put("EAN8", ScannerConst.SCAN_SDT_EAN8);
    	codeMap.put("EAN8_S", ScannerConst.SCAN_SDT_EAN8_S);
    	codeMap.put("ITF", ScannerConst.SCAN_SDT_ITF);
    	codeMap.put("JAN13", ScannerConst.SCAN_SDT_JAN13);
    	codeMap.put("JAN8", ScannerConst.SCAN_SDT_JAN8);
    	codeMap.put("MAXICODE", ScannerConst.SCAN_SDT_MAXICODE);
    	codeMap.put("OCRA", ScannerConst.SCAN_SDT_OCRA);
    	codeMap.put("OCRB", ScannerConst.SCAN_SDT_OCRB);
    	codeMap.put("OTHER", ScannerConst.SCAN_SDT_OTHER);
    	codeMap.put("PDF417", ScannerConst.SCAN_SDT_PDF417);
    	codeMap.put("QRCODE", ScannerConst.SCAN_SDT_QRCODE);
    	codeMap.put("RSS14", ScannerConst.SCAN_SDT_RSS14);
    	codeMap.put("RSS_EXPANDED", ScannerConst.SCAN_SDT_RSS_EXPANDED);
    	codeMap.put("TF", ScannerConst.SCAN_SDT_TF);
    	codeMap.put("UNKNOWN", ScannerConst.SCAN_SDT_UNKNOWN);
    	codeMap.put("UPCA", ScannerConst.SCAN_SDT_UPCA);
    	codeMap.put("UPCA_S", ScannerConst.SCAN_SDT_UPCA_S);
    	codeMap.put("UPCD1", ScannerConst.SCAN_SDT_UPCD1);
    	codeMap.put("UPCD2", ScannerConst.SCAN_SDT_UPCD2);
    	codeMap.put("UPCD3", ScannerConst.SCAN_SDT_UPCD3);
    	codeMap.put("UPCD4", ScannerConst.SCAN_SDT_UPCD4);
    	codeMap.put("UPCD5", ScannerConst.SCAN_SDT_UPCD5);
    	codeMap.put("UPCE", ScannerConst.SCAN_SDT_UPCE);
    	codeMap.put("UPCE_S", ScannerConst.SCAN_SDT_UPCE_S);
    	codeMap.put("UPDF417", ScannerConst.SCAN_SDT_UPDF417);
    	codeMap.put("UQRCODE", ScannerConst.SCAN_SDT_UQRCODE);
    }
    
    private void initializeCodeValues() throws JposException
    {
    	try
    	{
	    	codeList = new ArrayList<CodeFormatEntry>();
	    	Iterator<JposEntry.Prop> props = entry.getProps();
	    	while(props.hasNext())
	    	{
	    		JposEntry.Prop prop = props.next();
	    		if (prop.getName().contains("CodeFormat:"))
	    		{
	    			String cname = prop.getName().substring(11).trim().toUpperCase();
	    			if (codeMap.containsKey(cname)==false)
	    				throw new Exception("The barcode type " + cname + " is not supported by this driver!");
	    			codeList.add(new CodeFormatEntry(cname,codeMap.get(cname),prop.getValueAsString()));
                    //log(deviceName + " >> " + "New Filter for "+cname+": " + prop.getValueAsString());
	    			
	    		}
	    	}
    	}
    	catch(Exception ex)
    	{
            logError(deviceName + " >> " + "Error: Could not initialize barcode format strings. Reason: "
                    + ex.getMessage());
    		throw new JposException(JposConst.JPOS_E_NOSERVICE,"Error: Barcode format string initialization failed!",ex);
    	}
    	
    	
    }
    
    //@Override
    public void checkHealth(int typ) throws JposException {
    	if (typ==JposConst.JPOS_CH_INTERNAL) healthText = "Successful";
    	else if (typ==JposConst.JPOS_CH_EXTERNAL) healthText = "Successful";
    	else if (typ==JposConst.JPOS_CH_INTERACTIVE)
    	{
    		healthText = "Checking";
    		if (hwin==null) hwin = new ScannerHealthWindow(this);
    		hwin.clear();
    		hwin.setModal(true);
    		hwin.setVisible(true);
    	}
    	else error(JposConst.JPOS_E_ILLEGAL,"Invalid checkHealth Type invoked!");
    }
	//@Override
	public void onPortData(PortDataEvent evt) {
		if (deviceEnabled==false || localBuffer==null) return;
		String inputbuffer = null;
		synchronized(this)
		{
			localBuffer.append(evt.getValue());
			inputbuffer = localBuffer.toString();
		}
		for (CodeFormatEntry cf : codeList)
		{
            try {
                Matcher m = cf.getMatcher(inputbuffer);
                if(m.find())
                {
                    String str = m.group();
                    if (m.groupCount()>0) {
                        StringBuilder sb = new StringBuilder();
                        for (int i=1;i<=m.groupCount();i++) sb.append(m.group(i));
                        str=sb.toString();
                    }
                    int sz = str.length();
                    int ind = m.end();
                    synchronized(this)
                    {
                        rawData = localBuffer.substring(m.start(),ind);
                        if (ind>0)
                        {
                            localBuffer.delete(0, ind-1);
                            inputbuffer=localBuffer.toString();
                        }
                    }
                    if (decodeData)
                    {
                        dataLabel = str;
                        dataLabelType = cf.getCodeType();
                    }
                    else
                    {
                        dataLabel = null;
                        dataLabelType = ScannerConst.SCAN_SDT_UNKNOWN;
                    }

                    log(deviceName + " >> " + "Detected " + cf.getCodeName() + " using pattern " + cf.getRegexPattern());
                    // fire off the event notification
                    DataEvent event = new DataEvent(this.ecb.getEventSource(), 0);
                    if (autoDisable)
                        try
                        {
                            setDeviceEnabled(false);
                        } catch(Exception ex){}
                    if (hwin!=null && hwin.isVisible()) hwin.updateData();
                        this.fireEvent(event);
                }
            } catch (Exception e) {
                logError(deviceName + " >> " + "ERROR: " + " Scanner driver internal crash!" + e.toString());
                log(e);
                logError(deviceName + " >> " +"ERROR: Purging input: " + localBuffer.toString());
                if (localBuffer!=null) localBuffer.setLength(0);
            }
        }
	}
	
    public void completeHealthCheck()
    {
    	healthText = "Complete";
    }
	
	
	private class CodeFormatEntry {
		private String _codename;
		private String _codefilter;
		private int _codetype;
		private Pattern _p;
		public CodeFormatEntry(String codeName, int codeType, String filter) throws PatternSyntaxException
		{
			_codename = codeName;
			_codefilter = filter;
			_codetype = codeType;
			_p = Pattern.compile(_codefilter,Pattern.DOTALL);			
		}
		
		public String getCodeName()
		{
			return _codename;			
		}
		public int getCodeType()
		{
			return _codetype;
		}
		public String getRegexPattern()
		{
			return _codefilter;
		}
		
		public Matcher getMatcher(String inputString)
		{
			return _p.matcher(inputString);
		}
		public Pattern getPattern()
		{
			return _p;
		}
		
	}
}
