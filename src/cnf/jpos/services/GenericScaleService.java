package cnf.jpos.services;

import jpos.JposConst;
import jpos.JposException;
import jpos.ScaleConst;
import jpos.ScannerConst;
import jpos.events.DataEvent;
import jpos.services.EventCallbacks;
import cnf.communication.ports.*;
import cnf.communication.ports.PortDataEvent.PortDataEventType;
import cnf.jpos.dialogs.*;
import cnf.util.StringUtil;

import java.util.*;
import java.util.regex.*;

public class GenericScaleService  extends BaseService implements jpos.services.ScaleService17 {

	protected boolean autoDisable = false;
	protected boolean asyncMode = false;
	protected boolean busy = false;
	
	int weightUnit;
	int weight;
	int defaultTareWeight;
	int tareWeight;
	int maxWeight;
	long salesPrice;
	long unitPrice;
	int inputWeightMultiplier;
	
	String wcommand;
	Pattern rpattern;
	StringBuilder localBuffer = null;
	
	
	protected Map<Integer,String> weightUnitNames;
	protected Map<String, Integer> weightConstants;

    protected ScaleHealthWindow hwin = null;

	public GenericScaleService()
	{
		autoDisable = false;
	    asyncMode = false;
	    
	    weightUnit = ScaleConst.SCAL_WU_GRAM;
	    weight = 0;
	    tareWeight = 0;
	    defaultTareWeight = 0;
	    maxWeight = 100000000;
	    salesPrice = 0;
	    unitPrice = 0;
	    inputWeightMultiplier = 1;
	    
	    wcommand = "";
	    rpattern = null;
	    
	    initializeLookups();
	}
	
	//@Override
    public void open(String deviceName, EventCallbacks ecb) throws JposException {
        autoDisable = false;
        asyncMode = false;
        weightUnit = ScaleConst.SCAL_WU_GRAM;
        weight = 0;
        tareWeight = 0;
        maxWeight = 100000;
        salesPrice = 0;
        unitPrice = 0;
	    inputWeightMultiplier = 1;
	    localBuffer = new StringBuilder();
	    initializeProperties();
	    initializePropertyValues();
        
    	super.open(deviceName, ecb);    	
        //log(deviceName+" >> " + "Initializing Code Filters...");
    }    	
	
    //@Override
    public void close() throws JposException
    {
        log(deviceName+" >> " + "Closing device...");
    	autoDisable = false;
    	asyncMode = false;
    	super.close();
    }
	   
	
	//@Override
	public void clearInput() throws JposException {
		if(port!=null && port.isBusy()) port.cancelWrite();
		synchronized(this)
		{
			if (busy)
			{
				busy=false;
				try{
					this.notifyAll();
				}
				catch(Exception ex){}
			}
			localBuffer.setLength(0);
			weight = 0;
			salesPrice = 0;
			clearDataEvents();
		}
	}

	//@Override
	public void displayText(String arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"Text Display is not supported by this driver!");
	}

	//@Override
	public boolean getAsyncMode() throws JposException {
		return asyncMode;
	}

	//@Override
	public boolean getAutoDisable() throws JposException {
		return autoDisable;
	}

	//@Override
	public boolean getCapDisplayText() throws JposException {
		return false;
	}

	//@Override
	public int getCapPowerReporting() throws JposException {
		return JposConst.JPOS_PR_NONE;
	}

	//@Override
	public boolean getCapPriceCalculating() throws JposException {
		return true;
	}

	//@Override
	public boolean getCapTareWeight() throws JposException {
		return true;
	}

	//@Override
	public boolean getCapZeroScale() throws JposException {
		return false;
	}

	//@Override
	public int getMaxDisplayTextChars() throws JposException {		
		return 0;
	}

	//@Override
	public int getPowerNotify() throws JposException {
		return JposConst.JPOS_PN_DISABLED;
	}

	//@Override
	public int getPowerState() throws JposException {
		return JposConst.JPOS_PS_UNKNOWN;
	}

	//@Override
	public long getSalesPrice() throws JposException {
		if (!deviceOpen) error(JposConst.JPOS_E_CLOSED
                ,"device must be opened first before getting SalesPrice.");
		return salesPrice;
	}

	//@Override
	public int getTareWeight() throws JposException {
		if (!deviceOpen) error(JposConst.JPOS_E_CLOSED
                ,"device must be opened first before getting TareWeight.");
		return tareWeight;		
	}

	//@Override
	public long getUnitPrice() throws JposException {
		if (!deviceOpen) error(JposConst.JPOS_E_CLOSED
                ,"device must be opened first before getting UnitPrice.");
		return 0;
	}

	//@Override
	public void setAsyncMode(boolean b) throws JposException {
		if (!deviceOpen) error(JposConst.JPOS_E_CLOSED
                ,"device must be opened first before setting AsyncMode.");
		asyncMode=b;
        log(deviceName + " >> " + "Async_Mode = "+Boolean.toString(b));
	}

	//@Override
	public void setAutoDisable(boolean b) throws JposException {
        if (!deviceOpen) error(JposConst.JPOS_E_CLOSED
                , "Device must be opened before setting AutoDisable!");
    	this.autoDisable = b;
        log(deviceName+" >> " + "Auto_Disable = "+Boolean.toString(b));
	}

	//@Override
	public void setPowerNotify(int notifyMode) throws JposException {
		if (notifyMode==JposConst.JPOS_PN_ENABLED) 
			error(JposConst.JPOS_E_ILLEGAL
                    ,"Power Notify is not supported by this driver!");
	}

	//@Override
	public void setTareWeight(int tweight) throws JposException {
        if (!deviceOpen) error(JposConst.JPOS_E_CLOSED, "Device must be opened before setting TareWeight!");
    	this.tareWeight = tweight;
        log(deviceName+" >> " + "Tare_Weight = "+getWeightText(tweight));
	}

	//@Override
	public void setUnitPrice(long uprice) throws JposException {
        if (!deviceOpen) error(JposConst.JPOS_E_CLOSED
                , "Device must be opened before setting UnitPrice!");
    	this.unitPrice = uprice;
        log(deviceName+" >> " + "Unit_Price = "+getPriceText(uprice));
	}

	//@Override
	public void zeroScale() throws JposException {
		error(JposConst.JPOS_E_ILLEGAL
                ,"Zero_Scale feature is not supported by this driver!");
	}

	//@Override
	public boolean getCapDisplay() throws JposException {
		return false;
	}

	//@Override
	public int getMaximumWeight() throws JposException {
        if (!deviceOpen)
            error(JposConst.JPOS_E_CLOSED
                    , "Device must be opened before getting MaximumWeight!");
		return maxWeight;
	}

	//@Override
	public int getWeightUnit() throws JposException {		
        if (!deviceOpen)
            error(JposConst.JPOS_E_CLOSED
                    , "Device must be opened before getting WeightUnit!");
		return weightUnit;
	}

	//@Override
	public void readWeight(int[] ret, int timeout) throws JposException {
		if (busy)
            error(JposConst.JPOS_E_BUSY
                    ,"An asynchronous readWeight is in progress!");
		if (asyncMode)
		{
			try
			{
				port.writeAsync(wcommand);				
			}
			catch(PortIOException e)
			{
                busy=false;
				error(JposConst.JPOS_E_FAILURE
                        ,"Error issuing weight command through the port. Reason: " + e.getMessage(), e);
			}
		}
		else
		{
			synchronized(this)
			{
				busy = true;
				try
				{
					port.writeAsync(wcommand);				
				}
				catch(PortIOException e)
				{
                    busy=false;
					error(JposConst.JPOS_E_FAILURE
                            ,"Error issuing weight command through the port. Reason: " + e.getMessage(), e);
				}

				try
				{
					if (timeout==JposConst.JPOS_FOREVER)
						this.wait();
					else if (timeout>=0)
						this.wait(timeout);					
				}catch(InterruptedException ex){}
			}
			
			if (busy)
            {
                busy = false;
                error(JposConst.JPOS_E_TIMEOUT
                        ,"Timeout while attempting to read weight!");
            }
			else if (ret==null || ret.length<1)
                error(JposConst.JPOS_E_FAILURE
                        ,"Application must call this method with a return array[1].");
			else ret[0] = weight;
			
		}				
	}

    //@Override
    public void checkHealth(int typ) throws JposException {
    	if (typ==JposConst.JPOS_CH_INTERNAL) healthText = "Successful";
    	else if (typ==JposConst.JPOS_CH_EXTERNAL) healthText = "Successful";
    	else if (typ==JposConst.JPOS_CH_INTERACTIVE)
    	{
    		healthText = "Checking";
    		if (hwin==null) hwin = new ScaleHealthWindow(this);
    		hwin.clear();
    		hwin.setModal(true);
    		hwin.setVisible(true);
    	}
    	else error(JposConst.JPOS_E_ILLEGAL,"Invalid checkHealth Type invoked!");
    }
	//@Override
	public void onPortData(PortDataEvent evt) {
		
		if (evt.getEventType()==PortDataEventType.DATA_RECEIVED)
		{
			int lweight = 0;
			long lprice = 0;
			if (localBuffer==null) return;
			String inputbuffer = null;
			synchronized(this)
			{
				localBuffer.append(evt.getValue());
				inputbuffer = localBuffer.toString();
			}
			Matcher m = rpattern.matcher(inputbuffer);
			if(m.find())
			{
				String str = m.group();
				if (m.groupCount()>0) {
					StringBuilder sb = new StringBuilder();
					for (int i=1;i<=m.groupCount();i++) sb.append(m.group(i));
					str=sb.toString();
				}
				
				try
				{
					lweight = Integer.parseInt(str);
					lweight = lweight * inputWeightMultiplier;
					lweight = lweight - tareWeight - defaultTareWeight;					
					if (lweight<0) lweight = 0;
					lprice = ((long)lweight * unitPrice ) / 1000;
				}
				catch(Exception ex)
				{
                    logError(deviceName+" >> " + "invalid weight = "
                            + str + ". Weight data from scanner must be an integer.");
					return;
				}
                log(deviceName+" >> " + "Detected weight " + str + " using pattern " + rpattern);
				// fire off the event notification
		        synchronized(this)
		        {
		        	if (m.end()>0) localBuffer.delete(0, m.end()-1);			
		        }
		        if (asyncMode==false && busy)
		        {
		    		synchronized(this)
		    		{
		    			weight = lweight;
		    			salesPrice = lprice;
		    			if (hwin!=null && hwin.isVisible()) hwin.updateData();
		    			busy = false;
		    			try{
		    				this.notifyAll();
		    			} catch(Exception ex) {}
		    		}
		        }
		        else if (asyncMode==true)
		        {
		        	weight = lweight;
		        	salesPrice = lprice;
	    			if (hwin!=null && hwin.isVisible()) hwin.updateData();
			        DataEvent event = new DataEvent(this.ecb.getEventSource(), weight);
			        if (autoDisable) 
			        	try
			        	{
			        		setDeviceEnabled(false);
			        	} catch(Exception ex){}
					//if (hwin!=null && hwin.isVisible()) hwin.updateData();
			        this.fireEvent(event);
		        }
		        else
                    log(deviceName+" >> " + "WARNING: Ignoring data: " + Integer.toString(lweight)
                            + " because data arrived after readWeight timeout.");
			}
			else if (evt.getEventType()==PortDataEventType.WRITE_COMPLETE)
			{
			}
		}
	}
	
	private void initializeProperties()
	{
		this.resetProperties();
		//this.setPropertyLogger(logger);
		this.setPropertyModuleName(deviceName);
        //log(deviceName+" >> " + "Setting property names for this device.");
		registerProperty("WeightUnit","Unit of measurement for the scale (i.e. gram)","gram",new String[] {"gram","kilogram","pound","ounce"});
		registerProperty("DefaultTareWeight","This is always subtracted from the measured weight.","0",null);
		registerProperty("MaximumWeight","The upperlimit of weight that can be measured by this device.","100000",null);
		registerProperty("WeightCommand","The ascii stream that is sent to the port for a weight inquiry.",null,null);
		registerProperty("WeightResponseFormat","The format of the weight response in java regex pattern.",null,null);
		registerProperty("WeightResponseMultiplier","Weight_in_current_UOM x 1000 = raw_weight_from_scale * WeightResponseMultiplier.",null,null);
		
	}
	
	private void initializePropertyValues() throws JposException 
	{
		
		this.loadPropertyValues(entry);
		
		weightUnit = weightConstants.get(getPropertyValue("WeightUnit").toLowerCase());

    	defaultTareWeight = getPropertyValueInteger("DefaultTareWeight");
		defaultTareWeight = defaultTareWeight * 1000;

		maxWeight = getPropertyValueInteger("MaximumWeight");
    	maxWeight = maxWeight * 1000;

    	inputWeightMultiplier = getPropertyValueInteger("WeightResponseMultiplier");

    	try{
    		wcommand = StringUtil.asciiParse(getPropertyValue("WeightCommand"));
    	}
    	catch(Exception e)
    	{
    		error(JposConst.JPOS_E_NOSERVICE
                    ,"Must Specify valid WeightCommand (i.e. [02]11[03][03]");
    	}
    	if (wcommand==null || wcommand.isEmpty())
    		error(JposConst.JPOS_E_NOSERVICE, "Must Specify valid WeightCommand (i.e. [02]11[03][03]");
    	//log("WeightCommand = "+wcommand);

       	try
    	{
    		rpattern = Pattern.compile(getPropertyValue("WeightResponseFormat"), Pattern.DOTALL);
    	}
    	catch(Exception e)
    	{
    		error(JposConst.JPOS_E_NOSERVICE
                    ,getPropertyValue("WeightResponseFormat") + " is not a valid java regular expression.");
    	}		
    	
	}

	protected void initializeLookups()
	{
		weightUnitNames = new HashMap<Integer,String>();
		weightUnitNames.put(ScaleConst.SCAL_WU_GRAM, "gram");
		weightUnitNames.put(ScaleConst.SCAL_WU_KILOGRAM, "kilogram");
		weightUnitNames.put(ScaleConst.SCAL_WU_OUNCE, "ounce");
		weightUnitNames.put(ScaleConst.SCAL_WU_POUND, "pound");
		weightConstants = new HashMap<String,Integer>();
		weightConstants.put("gram",ScaleConst.SCAL_WU_GRAM);
		weightConstants.put("kilogram",ScaleConst.SCAL_WU_KILOGRAM);
		weightConstants.put("ounce",ScaleConst.SCAL_WU_OUNCE);
		weightConstants.put("pound",ScaleConst.SCAL_WU_POUND);
	}
	
	public String getWeightUnitName()
	{
		return weightUnitNames.get(weightUnit);
	}
	
	public int getWeight()
	{
		return weight;
	}
	
	private String getWeightText(int w)
	{
		return Double.toString((double)w/1000.0) + " " + getWeightUnitName();
	}
	private String getPriceText(long w)
	{
		return Double.toString((double)w/10000.0);
	}
	
}
