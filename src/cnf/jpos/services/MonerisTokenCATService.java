package cnf.jpos.services;

import apu.jpos.moneris.*;
import apu.jpos.util.StringUtil;
import cnf.communication.ports.PortDataEvent;
import cnf.jpos.dialogs.CATHealthWindow;
import cnf.jpos.dialogs.CATTransactionProgressWindow;
import cnf.jpos.factory.CnfJposServiceFactory;
import jpos.CATConst;
import jpos.JposConst;
import jpos.JposException;
import jpos.events.ErrorEvent;
import jpos.events.OutputCompleteEvent;
import jpos.services.CATService111;
import jpos.services.EventCallbacks;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;


public class MonerisTokenCATService extends BaseService implements CATService111, Runnable {
	private static final int SAF_INTERVAL_MS = 2 * 60 * 1000;   // After idle for two minutes...

	//protected String clerkId;
	//protected String storeId;
	//protected String terminalId;
    //protected boolean hasRecovered=false;
    //protected int recoveredPaymentMedia=0;

	protected String additionalSecurityInformation;
	protected String accountNumber;
	protected String approvalCode;
	protected int paymentCondition;
	protected int paymentMedia;
	protected String paymentDetail;
	//protected int sequenceNumber;
	protected String slipNumber;
	protected String transactionNumber;
	//protected String transactionType;
	protected String cardCompanyID;
	protected String centerResultCode;
	protected String cardExpiryDate;
	protected long totalAmount;
	//protected String declineCode;
	protected String transactionDate;
	//protected String transactionTime;
	protected long cardBalance;
    protected String dailyLog;

	protected boolean asyncMode;
	protected boolean busy;
    protected boolean safEnabled;
    protected DateTime nextSafExecute = new DateTime().plusMillis(10000);
    //protected DateTime nextSafExecute = new DateTime();

	protected String txnFileName;
	//protected String txnPrefix;
	//protected String request;
	protected MonerisTransactionResponse response;

	//private String tx_code;
	//private String tx_name;

    private boolean log_requested;
    private int log_type;
    private MonerisTransactionType tx_type = null;
	private int tx_sqn=-1;
	private BigDecimal tx_amt;
    private String tx_auth;
	//private BigDecimal tx_tax;
	private BigDecimal safDollarLimit = null;
    private int safTxLimit = 200;
    private boolean safExecuting = false;
    private Thread operationThread = null;

	protected CATTransactionProgressWindow cwin=null;
    protected CATHealthWindow hwin = null;

    protected String surrogateAddress = null;
    protected int surrogatePort = 2000;
    protected boolean skipInit = false;
    protected String logFileName = "GenericJposDriverLogs.txt";
    protected String merchantId = null;
    protected String ecrId = null;
    protected MonerisIPP320 monerisClient = null;
    protected Map<Integer,MonerisTransactionResponse> responseHistory
            = new HashMap<Integer,MonerisTransactionResponse>();
	
	protected void reset()
	{
		//this.clerkId = "";
		//this.storeId = "";
		//this.terminalId = "";
		
		this.additionalSecurityInformation = "";
		this.accountNumber = "";
		this.approvalCode = "";
		this.cardCompanyID = "";
		this.cardExpiryDate = "";
		this.centerResultCode = "";
		this.dataEventsEnabled = false;
		//this.declineCode = "";
		this.deviceClaimed = false;
		this.deviceEnabled = false;
		this.deviceOpen = false;
		this.freezeEvents = false;
		this.paymentCondition = CATConst.CAT_PAYMENT_LUMP;
		this.paymentDetail = "";
		this.paymentMedia = CATConst.CAT_MEDIA_UNSPECIFIED;
		this.slipNumber = "";
		this.totalAmount = 0;
		this.transactionNumber = "";
		//this.transactionTime = "";
		this.transactionDate = "";
		//this.transactionType = "";
		this.cardBalance = 0;
        this.dailyLog="";

		asyncMode = false;
		busy=false;
        safEnabled=false;
		
		//txnFileName="";
		//txnPrefix="";
		//request="";
		response=null;
		cwin=null;		
		hwin=null;
		this.serviceVer = 1011000;

	}


    /// Main Functions

	//@Override
    public void open(String deviceName, EventCallbacks ecb) throws JposException {
        reset();
	    initializeProperties();
	    initializePropertyValues();
        try {
            log("Connecting to the Moneris Pospad via ComPort Surrogate Server....");
            monerisClient = new MonerisIPP320(surrogateAddress,surrogatePort,txnFileName);
            log("Initializing the Moneris Pospad terminal....");
            //monerisClient.reset();
            monerisClient.initialize(ecrId, merchantId, skipInit);
            //else log("Skipping initialization due to JPOS Configuration override!");
            log("Initializing the Moneris Pospad terminal....OK");
            String ver = monerisClient.getFirmwareVersion();
            log("Detected Moneris IPP320 with firmware Version: " + ver);
            if (safEnabled)
            {
                log("SAF mode is ON! Setting SAF parameters...");
                monerisClient.initSAF(safDollarLimit, safTxLimit);
            }
            else monerisClient.disableSAF();
            operationThread = new Thread(this);
            operationThread.setDaemon(true);
            operationThread.setName("MONERIS");
            operationThread.start();
        } catch (Exception e) {
            error(JposConst.JPOS_E_NOHARDWARE, "Unable to initialize hardware!",e);
        }

        super.open(deviceName, ecb);
    }    	
	
    //@Override
    public void close() throws JposException
    {
        log(deviceName + " >> " +  "Closing device...");
    	//if (protocol.isBusy()) protocol.cancelCommand();
    	asyncMode = false;
    	busy=false;
    	super.close();
    }










    /// Device Capabilities

	//@Override
	public boolean getCapCashDeposit() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapCompareFirmwareVersion() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapLockTerminal() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapLogStatus() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapUnlockTerminal() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapUpdateFirmware() throws JposException {
		check(true,false,false);
		return false;
	}

    //@Override
    public boolean getCapStatisticsReporting() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapUpdateStatistics() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapAdditionalSecurityInformation() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapAuthorizeCompletion() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapAuthorizePreSales() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapAuthorizeRefund() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapAuthorizeVoid() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapAuthorizeVoidPreSales() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapCenterResultCode() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapCheckCard() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public int getCapDailyLog() throws JposException {
        check(true,false,false);
        return CATConst.CAT_DL_REPORTING_SETTLEMENT;
    }

    //@Override
    public boolean getCapInstallments() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapPaymentDetail() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public int getCapPowerReporting() throws JposException {
        check(true,false,false);
        return JposConst.JPOS_PR_NONE;
    }

    //@Override
    public boolean getCapTaxOthers() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapTrainingMode() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapTransactionNumber() throws JposException {
        check(true,false,false);
        return true;
    }












    /// PROPERTIES

    //@Override
	public int getLogStatus() throws JposException {
		return CATConst.CAT_LOGSTATUS_OK;
	}

	//@Override
	public long getSettledAmount() throws JposException {
		return totalAmount;
	}

	//@Override
	public void lockTerminal() throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Lock Terminal!");
	}

	//@Override
	public void unlockTerminal() throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Unlock Terminal!");
	}

	//@Override
	public void updateFirmware(String arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Update Firmware!");
	}

	//@Override
	public void resetStatistics(String arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
	}

	//@Override
	public void retrieveStatistics(String[] arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
	}

	//@Override
	public void updateStatistics(String arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
	}

	//@Override
	public int getPaymentMedia() throws JposException {
		check(true,false,false);
		return paymentMedia;
	}

	//@Override
	public void setPaymentMedia(int arg0) throws JposException {
		check(true,false,false);
		paymentMedia = arg0;
	}

    //@Override
    public String getAccountNumber() throws JposException {
        check(true,false,false);
        return accountNumber;
    }

    //@Override
    public String getAdditionalSecurityInformation() throws JposException {
        check(true,false,false);
        return additionalSecurityInformation;
    }

    //@Override
    public String getApprovalCode() throws JposException {
        check(true,false,false);
        return approvalCode;
    }

    //@Override
    public boolean getAsyncMode() throws JposException {
        check(true,false,false);
        return asyncMode;
    }


    //@Override
    public String getCardCompanyID() throws JposException {
        check(true,false,false);
        return cardCompanyID;
    }

    //@Override
    public String getCenterResultCode() throws JposException {
        check(true,false,false);
        return centerResultCode;
    }

    //@Override
    public String getDailyLog() throws JposException {
        return dailyLog;
    }

    //@Override
    public int getPaymentCondition() throws JposException {
        check(true,false,false);
        return paymentCondition;
    }

    //@Override
    public String getPaymentDetail() throws JposException {
        check(true,false,false);
        return paymentDetail;
    }

    //@Override
    public int getPowerNotify() throws JposException {
        check(true,false,false);
        return JposConst.JPOS_PN_DISABLED;
    }

    //@Override
    public int getPowerState() throws JposException {
        check(true,false,false);
        return JposConst.JPOS_PS_UNKNOWN;
    }

    //@Override
    public int getSequenceNumber() throws JposException {
        check(true,false,false);
        return tx_sqn;
    }

    //@Override
    public long getBalance() throws JposException {
        return cardBalance;
    }

    //@Override
    public String getSlipNumber() throws JposException {
        check(true,false,false);
        return slipNumber;
    }

    //@Override
    public boolean getTrainingMode() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public String getTransactionNumber() throws JposException {
        check(true,false,false);
        return transactionNumber;
    }

    //@Override
    public String getTransactionType() throws JposException {
        check(true,false,false);
        return tx_type.toString();
    }

    //@Override
    public void setAdditionalSecurityInformation(String arg0)
            throws JposException {
        check(true,false,false);
        additionalSecurityInformation = arg0;
    }

    //@Override
    public void setAsyncMode(boolean arg0) throws JposException {
        check(true,false,false);
        asyncMode=arg0;
        log(deviceName + " >> " + "Async_Mode = " + Boolean.toString(arg0));

    }

    //@Override
    public void setPowerNotify(int arg0) throws JposException {
        check(true,false,false);
        if (arg0==JposConst.JPOS_PN_ENABLED)
            error(JposConst.JPOS_E_ILLEGAL,"Power Notify is not implemented by the driver!");
    }

    //@Override
    public void setTrainingMode(boolean arg0) throws JposException {
        check(true,false,false);
        if (arg0==true)
            error(JposConst.JPOS_E_ILLEGAL,"Training Mode is not implemented by the driver!");
    }



    /// METHODS....

    //@Override
    public void cashDeposit(int arg0, long arg1, int arg2) throws JposException {
        error(JposConst.JPOS_E_ILLEGAL, "This driver does not support Cash Deposit!");
    }

    //@Override
    public void compareFirmwareVersion(String arg0, int[] arg1)
            throws JposException {
        error(JposConst.JPOS_E_ILLEGAL, "This driver does not support Compare Firmware Version!");
    }


    //@Override
	public void accessDailyLog(int seqNum, int type, int timeout)
			throws JposException {

          check(true,true,true);
            if (tx_sqn==-1 || seqNum==tx_sqn+1) tx_sqn = seqNum;
            else error(JposConst.JPOS_E_ILLEGAL,"Settlement Request is out of Sequence! Pos requested "
                    + Integer.toString(seqNum)+", but the last request processed was "+Integer.toString(tx_sqn));
        try
        {
            if (isBusy())
                error(JposConst.JPOS_E_BUSY,"Error: Another Pospad operation is in progress!");
            setBusy(true);
            log_requested=true;
            log_type = type;
            if (!asyncMode)
            {
                waitForSAF();
                executeOperation();
            }
            else
            {
                synchronized (this)
                {
                    this.notifyAll();
                }
            }
        }
        catch(JposException e)
        {
            setBusy(false);
            log(e);
            throw e;
        }
        catch(Exception e)
        {
            setBusy(false);
            log(e);
            error(JposConst.JPOS_E_FAILURE,"Driver Error! " + e.getMessage());
        }
        finally
        {
            if (!asyncMode) setBusy(false);
        }




	}




	private int elapsed(DateTime start)
    {
        if (start==null) return -1;
        return (int)(new Duration(start,new DateTime()).getMillis());
    }




	//@Override
	public void authorizeCompletion(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Authorize Completion Method!");
	}

	//@Override
	public void authorizePreSales(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Authorize Presales Method!");
	}

	//@Override
	public void authorizeRefund(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
        doTransaction(MonerisTransactionType.Refund,sequenceNumber,amount+taxOthers);
	}

	//@Override
	public void authorizeSales(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
        doTransaction(MonerisTransactionType.Purchase,sequenceNumber,amount+taxOthers);
	}

	//@Override
	public void authorizeVoid(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
        if (responseHistory.containsKey(sequenceNumber))
        {
            MonerisTransactionResponse tx = responseHistory.get(sequenceNumber);
            if (tx.getAmount().movePointRight(2).longValue()!=amount+taxOthers)
                error(JposConst.JPOS_E_FAILURE,"Transaction void failed because amount did not match original!");
            else
            {
                if (tx.getTransactionType()==MonerisTransactionType.Purchase)
                    doTransaction(MonerisTransactionType.PurchaseVoid,sequenceNumber,amount+taxOthers);
                else if (tx.getTransactionType()==MonerisTransactionType.Refund)
                    doTransaction(MonerisTransactionType.RefundVoid,sequenceNumber,amount+taxOthers);
                else
                    error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because sequence reference is not valid!");
            }
        }
        else
            error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because sequence reference is not valid!");

    }

	//@Override
	public void authorizeVoidPreSales(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Authorize Void Presales Method!");
	}

	//@Override
	public void checkCard(int sequenceNumber, int timeout) throws JposException {
		check(true,true,true);
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Check Card Method!");
	}



    protected void doTransaction(MonerisTransactionType txType, int sqn, long amount) throws JposException
    {
        if (txType==null) txType = MonerisTransactionType.Purchase;
        if (txType==MonerisTransactionType.VoidLast)
        {
            if (sqn!=tx_sqn || amount!=totalAmount)
                error(JposConst.JPOS_E_ILLEGAL
                        ,"Please call voidLast with last transaction amount and sequence number!");
        }
        else if (txType==MonerisTransactionType.PurchaseVoid || txType==MonerisTransactionType.RefundVoid)
        {
            if (responseHistory.containsKey(sqn))
            {
                MonerisTransactionResponse tx = responseHistory.get(sqn);
                if (!tx.isApproved())
                    error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because refererred transaction is illegal!");
                if (txType==MonerisTransactionType.PurchaseVoid
                        && tx.getTransactionType()!=MonerisTransactionType.Purchase)
                    error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because refererred transaction type mismatch!");
                else if (txType==MonerisTransactionType.RefundVoid
                        && tx.getTransactionType()!=MonerisTransactionType.Refund)
                    error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because refererred transaction type mismatch!");
                else
                {
                    tx_sqn=sqn;
                    tx_auth = tx.getApprovalCode();
                }

            }
            else error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because sequence reference is not valid!");
        }
        else
        {
            if (tx_sqn==-1 || sqn==tx_sqn+1) tx_sqn = sqn;
            else error(JposConst.JPOS_E_ILLEGAL,"Transactions are out of Sequence! Pos requested "
                    + Integer.toString(sqn)+", but the last transaction processed was "+Integer.toString(tx_sqn));
        }
        try
        {
            if (isBusy())
                error(JposConst.JPOS_E_BUSY,"Error: Another transaction is in progress!");
            setBusy(true);
            BigDecimal amt = new BigDecimal(amount).movePointLeft(2);
            if (amt.compareTo(BigDecimal.ZERO)==-1) amt = amt.negate();
            response=null;
            tx_amt = amt;
            tx_type = txType;
            log_requested=false;
            if (!asyncMode)
            {
                waitForSAF();
                executeOperation();
            }
            else
            {
                synchronized (this)
                {
                    this.notifyAll();
                }
            }
        }
        catch(JposException e)
        {
            if (cwin!=null && cwin.isVisible()) cwin.closeWindow();
            setBusy(false);
            throw e;
        }
        catch(Exception e)
        {
            if (cwin!=null) cwin.add("Error!\r\n");
            if (cwin!=null && cwin.isVisible()) cwin.closeWindow();
            setBusy(false);
            error(JposConst.JPOS_E_FAILURE,"Driver Error! " + e.getMessage());
        }
        finally
        {
            if (!asyncMode) setBusy(false);
        }
    }

    protected void executeOperation() throws JposException
    {
        long logTotalCount=0;
        long logTotalValue=0;
        BigDecimal logTotalValueDec = null;
        MonerisTransactionResponse lastResponse = null;
        if (log_requested)
        {
            StringBuilder sb = null;
            try {
                if (monerisClient.isInitRequired())
                {
                    try {
                        monerisClient.reinitialize();
                    } catch (Exception ex) {
                        error(JposConst.JPOS_E_FAILURE, "Terminal Initialization failed (but required to proceed)!", ex);
                    }
                }
                boolean settle = (log_type==CATConst.CAT_DL_SETTLEMENT);
                sb = new StringBuilder();
                MonerisBatchResponse batch = monerisClient.settleBatch(settle);
                if (batch==null) throw  new Exception("Unable to obtain the log/settlement from the PosPad!");
                String logs = batch.getTransactionLogs();
                if (logs==null) logs="";
                String[] logElements = logs.split("[\\r\\n]+");
                if (logs.length()>0) for (String element:logElements)
                {
                    MonerisResponse resp = new MonerisResponse(element);
                    if (resp==null) throw new Exception("Invalid transaction entry found in transaction batch file!");
                    /*if (resp.getCode()==607 || resp.getCode()==608) {
                        logWarning("Skipping 607 response (potentially malformed)!");
                        continue;
                    }*/

                    try {
                        response = new MonerisTransactionResponse(resp,null);
                    } catch (Exception e) {
                        log(e);
                        logError("Invalid Transaction Response in LOG: " + element);
                        continue;
                    }

                    extractFieldsFromResponse();
                    MonerisTransactionType t = response.getTransactionType();
                    if (response.isApproved())
                    {
                        // Append to the daily log only if a transaction is approved!
                        logTotalCount++;
                        long signedTotalAmount = totalAmount;

                        //if (t==MonerisTransactionType.Purchase) logTotalValue+=totalAmount;
                        if (t==MonerisTransactionType.VoidLast)
                        {
                            MonerisTransactionType t2 = lastResponse.getTransactionType();
                            if (t2==MonerisTransactionType.Purchase) signedTotalAmount=-totalAmount;
                            else if (t2!=MonerisTransactionType.Refund)
                                throw new Exception("Orphaned Void Last entry found in Transaction Batch File!");
                        }
                        else if (t==MonerisTransactionType.Refund) signedTotalAmount=-totalAmount;
                        else if (t==MonerisTransactionType.PurchaseVoid) signedTotalAmount=-totalAmount;
                        else if (t!=MonerisTransactionType.RefundVoid && t!=MonerisTransactionType.Purchase)
                            throw new Exception("Unknown transaction line found in Transaction Batch File!");

                        logTotalValue+=signedTotalAmount;

                        int txTyp = 0;
                        if (t==MonerisTransactionType.Purchase) txTyp = CATConst.CAT_TRANSACTION_SALES;
                        else if (t==MonerisTransactionType.Refund) txTyp = CATConst.CAT_TRANSACTION_REFUND;
                        else if (t==MonerisTransactionType.VoidLast) txTyp = CATConst.CAT_TRANSACTION_VOID;
                        else if (t==MonerisTransactionType.RefundVoid) txTyp = CATConst.CAT_TRANSACTION_VOID;
                        else if (t==MonerisTransactionType.PurchaseVoid) txTyp = CATConst.CAT_TRANSACTION_VOID;
                        sb.append(cardCompanyID);       // 0
                        sb.append(',');
                        sb.append(txTyp);               // 1
                        sb.append(',');
                        sb.append(transactionDate);     // 2
                        sb.append(',');
                        sb.append(transactionNumber);   // 3
                        sb.append(',');
                        sb.append(paymentCondition);    // 4
                        sb.append(',');
                        sb.append(slipNumber);          // 5
                        sb.append(',');
                        sb.append(approvalCode);        // 6
                        sb.append(',');
                        sb.append(',');                 // 7
                        sb.append(accountNumber);       // 8
                        sb.append(',');
                        sb.append(signedTotalAmount);   // 9
                        sb.append(',');
                        sb.append('0');                 // 10
                        sb.append(',');
                        sb.append('1');                 // 11
                        sb.append(',');
                        sb.append(additionalSecurityInformation);       // 12
                        sb.append("\r\n");
                    }
                    if (t==MonerisTransactionType.Purchase || t==MonerisTransactionType.Refund) lastResponse = response;
                }

                // Now Access the pinpad log...
                logTotalValueDec = new BigDecimal(logTotalValue).movePointLeft(2);
                if (!settle)
                {
                    batch.setPinPadTotals((int)logTotalCount, logTotalValueDec);
                    batch.setServerTotals((int)logTotalCount, logTotalValueDec);
                }
                sb.append("BALANCE");
                sb.append(',');
                String res = "OK";
                if (batch.getPinpadTotalCount()!=logTotalCount
                        || batch.getPinpadTotalValue().compareTo(logTotalValueDec)!=0)
                    res = "LOG_ERROR";
                else if (!batch.isBalanced())
                    res = "HOST_ERROR";
                sb.append(res);
                sb.append(',');
                sb.append(batch.getPinpadTotalCount()-logTotalCount);
                sb.append(',');
                sb.append(batch.getPinpadTotalValue().movePointRight(2).longValue()-logTotalValue);
                sb.append(',');
                sb.append(batch.getServerTotalCount()-batch.getPinpadTotalCount());
                sb.append(',');
                sb.append(batch.getServerTotalValue().subtract(batch.getPinpadTotalValue())
                        .movePointRight(2).longValue());
                sb.append("\r\n");
                dailyLog = sb.toString();
                if (asyncMode)
                {
                    if (hwin!=null && hwin.isVisible()==true) hwin.updateLogResult();
                    OutputCompleteEvent ocev = new OutputCompleteEvent(this.ecb.getEventSource(),0);
                    this.fireEvent(ocev);
                }
                if (settle)
                {
                    log("Resetting tx sequence number...");
                    tx_sqn=-1;
                    monerisClient.resetSequence();
                }
            } catch (Exception ex) {
                String message = ex.getMessage();
                if(message==null) message = "Null Reference Exception!";
                if (hwin!=null && hwin.isVisible()==true)
                    hwin.updateError("Transaction Failed!", message);
                if (cwin!=null) {
                    cwin.add("Transaction Failed!\r\n");
                    cwin.closeWindow();
                }
                error(JposConst.JPOS_E_FAILURE, "Transaction Failed!", ex);
            }

        }
        else
        {
            if (tx_type==null || tx_sqn<0 || tx_amt==null){
                if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Invalid!"
                        , "Invalid transaction arguments received!");
                error(JposConst.JPOS_E_ILLEGAL, "Invalid transaction arguments received!");
            }
            this.show_progress();
            if (monerisClient.isInitRequired())
            {
                cwin.add("Terminal must be initialized.\r\n");
                cwin.add("This may take a little bit of time!\r\n");
                cwin.add("Thank you for your patience.\r\n");
                cwin.add("Please wait....\r\n");
                try {
                    monerisClient.reinitialize();
                } catch (Exception ex) {
                    cwin.add("ERROR: Terminal Init Failed!\r\n");
                    cwin.closeWindow();
                    error(JposConst.JPOS_E_FAILURE, "Terminal Initialization failed!", ex);
                }
            }

            cwin.add("Initializing This Transaction...\r\n");

            try {
                buildAdditionalInfo();
                boolean cashBack = false;
                if (additionalInfo.containsKey("CASHBACK"))
                {
                    String c = additionalInfo.get("CASHBACK");
                    if (c!=null) c=c.toLowerCase();
                    if (c!=null && c.contains("t") || c.contains("y")) cashBack=true;
                }
                monerisClient.reset();
                cwin.add("Waiting for customer...\r\n");
                if (tx_type==MonerisTransactionType.Purchase)
                    response = monerisClient.purchase(tx_amt,cashBack);
                else if (tx_type==MonerisTransactionType.Refund)
                    response = monerisClient.refund(tx_amt);
                else if (tx_type==MonerisTransactionType.VoidLast)
                    response = monerisClient.voidLastTransaction(approvalCode);
                else if (tx_type==MonerisTransactionType.PurchaseVoid)
                    response = monerisClient.voidPurchase(approvalCode,tx_amt);
                else if (tx_type==MonerisTransactionType.RefundVoid)
                    response = monerisClient.voidRefund(approvalCode,tx_amt);
                else throw new Exception("This type of transaction is not implemented!");
                if (response==null) throw new Exception("Invalid Moneris Pospad response!");

                if (response.isApproved() && (response.getTransactionType()==MonerisTransactionType.Purchase
                            || response.getTransactionType()==MonerisTransactionType.Refund))
                    responseHistory.put(tx_sqn,response);
                else if (response.isApproved() && (response.getTransactionType()==MonerisTransactionType.PurchaseVoid
                        || response.getTransactionType()==MonerisTransactionType.RefundVoid
                        || response.getTransactionType()==MonerisTransactionType.VoidLast))
                {
                    responseHistory.remove(tx_sqn);
                    tx_sqn=-1;
                }
                extractFieldsFromResponse();
                if (response.isSAF()) {
                    nextSafExecute = (new DateTime()).plusMillis(SAF_INTERVAL_MS);
                    CnfJposServiceFactory.IsMonerisInSAFMode=true;
                }
                else CnfJposServiceFactory.IsMonerisInSAFMode=false;

                cwin.add("Done!\r\n");
                log("Payment Terminal Completed Transaction!");
                if (asyncMode)
                {
                    if (hwin!=null && hwin.isVisible()==true) hwin.updateResult();
                    OutputCompleteEvent ocev = new OutputCompleteEvent(this.ecb.getEventSource(),0);
                    this.fireEvent(ocev);
                }
                cwin.closeWindow();
            }
            catch (Exception ex)
            {
                String message = ex.getMessage();
                if(message==null) message = "Null Reference Exception!";
                if (hwin!=null && hwin.isVisible()==true)
                    hwin.updateError("Transaction Failed!", message);
                cwin.add("Transaction Failed!\r\n");
                cwin.closeWindow();
                error(JposConst.JPOS_E_FAILURE, "Transaction Failed!", ex);
            }
        }
    }




    //@Override
	public void clearOutput() throws JposException {

        check(true,true,false);
        try {
            monerisClient.reset();
            setBusy(false);
        } catch (Exception e) {
            String message = e.getMessage();
            error(JposConst.JPOS_E_FAILURE,e.getMessage());
        }
	}



    //@Override
    public void checkHealth(int typ) throws JposException {
    	boolean async = asyncMode;
    	this.setAsyncMode(true);
    	if (typ==JposConst.JPOS_CH_INTERNAL) healthText = "Successful";
    	else if (typ==JposConst.JPOS_CH_EXTERNAL) healthText = "Successful";
    	else if (typ==JposConst.JPOS_CH_INTERACTIVE)
    	{
    		healthText = "Checking";
    		if (hwin==null) hwin = new CATHealthWindow(this);
    		hwin.clear();
    		hwin.setModal(false);
    		hwin.setVisible(true);
            while (hwin.isVisible())
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
            }
    	}
    	else 
    	{
        	this.setAsyncMode(async);
    		error(JposConst.JPOS_E_ILLEGAL,"Invalid checkHealth Type invoked!");
    	}
    	this.setAsyncMode(async);
    }	

	protected void check(boolean open, boolean claim, boolean enable) throws JposException
	{
        try {
            if (enable==true && this.deviceEnabled==false)
                error(JposConst.JPOS_E_DISABLED, "Device must be opened, claimed and enabled first!");
            else if (claim==true && this.deviceClaimed==false)
                error(JposConst.JPOS_E_NOTCLAIMED, "Device must be opened and claimed first!");
            else if (open==true && this.deviceOpen==false)
                error(JposConst.JPOS_E_CLOSED, "Device must be opened first!");
        } catch (JposException e) {
            tx_sqn=-1;
            throw e;
        }
    }
	
	
    private int create_int(String propName, int min, int max) throws JposException
    {
        int t = 0;
        try
        {
            t = getPropertyValueInteger(propName);
        }
        catch(JposException e)
        {
            error(JposConst.JPOS_E_NOSERVICE,"You must assign a valid integer for " + propName);
        }
        if (t<min || t>max)
            error(JposConst.JPOS_E_NOSERVICE, "Property " + propName + "=" + t
                    +" is out of range! Valid range is "
                    + Integer.toString(min) + ".." + Integer.toString(max));

        return t;

    }

	private void initializeProperties()
	{
		this.resetProperties();

        //log(deviceName + " >> " + "Setting property names for this device.");

        registerProperty("ComSurrogateServerAddress", "IpAddress/name of the Cnf ComPort Surrogate Server","127.0.0.1",null);
        registerProperty("ComSurrogatePort", "TCP Port number for Cnf ComPort Surrogate","2000",null);
        registerProperty("LogFileName", "Driver activity Log File Path","GenericJposDriverLogs.txt",null);
        registerProperty("TransactionFileName", "Transaction Trace File Path","MonerisTransactions.txt",null);
        registerProperty("MerchantId", "The Merchant ID of this company from Moneris",null,null);
        registerProperty("EcrId", "The ECR ID of this terminal from Moneris",null,null);
        registerProperty("SkipInit", "If true, skips the long Pinpad Initialization","false",null);
        registerProperty("SafEnabled", "If true, pinpad allows offline transaction approval", "false", null);
        registerProperty("SafLimitPerCardType", "Dollar value limit for each type of card in offline mode (SAF)", "500.00", null);
        registerProperty("SafMaxTxCount", "Maximum number of transactions", "200", null);
	}
	
	private void initializePropertyValues() throws JposException
	{
		
		this.loadPropertyValues(entry);
        surrogateAddress = getPropertyValue("ComSurrogateServerAddress");
        surrogatePort = create_int("ComSurrogatePort", 1000, 99000);
        logFileName = getPropertyValue("LogFileName");
        txnFileName = getPropertyValue("TransactionFileName");
        ecrId = getPropertyValue("EcrId");
        merchantId = getPropertyValue("MerchantId");
        String si = getPropertyValue("SkipInit");
        skipInit = (si!=null && si.toLowerCase().contains("t") || si.contains("1"));
        String se = getPropertyValue("SafEnabled");
        safEnabled = (se!=null && se.toLowerCase().contains("t") || se.contains("1"));
        String sdl = getPropertyValue("SafLimitPerCardType");
        String stl = getPropertyValue("SafMaxTxCount");
        try {
            safDollarLimit = new BigDecimal(sdl);
        } catch (Exception e) {
            throw new JposException(JposConst.JPOS_E_FAILURE, "Invalid Saf Limit in Jpos Config!");
        }
        try {
            safTxLimit = Integer.parseInt(stl);
            if (safTxLimit<=0) safTxLimit=10;
            else if (safTxLimit>550) safTxLimit=550;
        } catch (Exception e) {
            throw new JposException(JposConst.JPOS_E_FAILURE, "Invalid Saf Max Transaction Count in Jpos Config!");
        }
    }
	
	

	protected void show_progress()
	{
		if (cwin==null) cwin=new CATTransactionProgressWindow(this);
		cwin.clear();
        //cwin.setModal(true);
		cwin.setVisible(true);
		cwin.add("Requested Operation: " + tx_type.toString()+"\r\n");
        cwin.add("    Sequence Number: " + Integer.toString(tx_sqn)+"\r\n");
		cwin.add("             Amount: $" + new DecimalFormat("#,###,##0.00").format(tx_amt.doubleValue())+"\r\n");
	}


	protected void extractFieldsFromResponse() throws Exception
	{
        if (response.getCardType()!=null) cardCompanyID = response.getCardType().toString();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyyMMddHHmmss");
        transactionDate = dtf.print(response.getTransactionDate());
        //transactionNumber = response.getUniqueId();

        //transactionNumber = monerisClient.getSerialNumber()+response.getUniqueId();
        transactionNumber = ecrId+response.getUniqueId();
        paymentCondition = CATConst.CAT_PAYMENT_LUMP;
        slipNumber = response.getSequenceNumber();
        approvalCode = response.getApprovalCode();
        accountNumber = response.getAccountNumber();
        totalAmount = response.getAmount().movePointRight(2).longValue();
        //if (response.isApproved()) centerResultCode = "OK";
        //else centerResultCode = response.getResponseCode();
        centerResultCode = response.getResponseCode();

        DateTimeFormatter dtf2 = DateTimeFormat.forPattern("dd MMM yyyy HH.mm.ss");
        String tDateString = dtf2.print(response.getTransactionDate());

        String langCode = response.getLanguage();
        //log("Language Code = " + langCode);
        if (langCode.trim().equals("4") || langCode.trim().equals("5")) langCode="F";
        else langCode="E";

        additionalSecurityInformation = "EXP:"+response.getExpiry()
                +"|VER:"+response.getVerificationMethod().toString()
                +"|SWI:"+response.getCardMethodIndicator()
                +"|TXD:"+tDateString
                +"|LAN:"+langCode
                +"|CND:"+response.getConditionCode();
        additionalSecurityInformation+="|ACT:"+response.getCardAccountType();
        if (response.getCashBackAmount()!=null && response.getCashBackAmount().compareTo(BigDecimal.ZERO)==1)
            additionalSecurityInformation+="|CSB:"+response.getCashBackAmount();
        additionalSecurityInformation+="|RSP:"+response.getResponseCode();
        additionalSecurityInformation+="|B24:"+response.getResponseBase24();
        if (!StringUtil.empty(response.getEmvApplicationId()))
        {
            additionalSecurityInformation+="|AID:"+response.getEmvApplicationId();
            additionalSecurityInformation+="|ANM:"+response.getEmvApplicationName();
            additionalSecurityInformation+="|TSI:"+response.getEmvTransactionStatusInformation();
            additionalSecurityInformation+="|TVR:"+response.getEmvTerminalVerificationResult();
        }
        if (response.isSAF()) additionalSecurityInformation+="|SAF:1";
        else additionalSecurityInformation+="|SAF:0";
	}

	
	/*protected String getNextTransactionNumber()
	{
		String l = FileUtil.readTextFile(txnFileName);
		int t = 0;
		try
		{
			t=Integer.parseInt(l);
		}
		catch(Exception e)
		{
			t=0;
		}		
		t++;
		l= Integer.toString(t);
		try
		{
			FileUtil.writeTextFile(l, txnFileName);
		}
		catch(Exception e){}
		return txnPrefix+"_"+l;
	} */

    private void waitForSAF()
    {
        while(safExecuting){
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }
    }

	//@Override
	public void run() {
        log("Moneris operation thread started!");
        while(true) {
            if (safEnabled && !isBusy() && nextSafExecute.isBefore(new DateTime()))
            {
                try {
                    safExecuting=true;
                    int n = monerisClient.getSAFCount();
                    if (n==0) CnfJposServiceFactory.IsMonerisInSAFMode = false;
                    for (int i=0;i<n;i++)
                    {
                        if (isBusy()) break;
                        monerisClient.releaseSAF();
                        CnfJposServiceFactory.IsMonerisInSAFMode=false;
                        if (isBusy()) break;
                    }
                } catch (Exception e) {
                    log(e);
                    logWarning("Could not release SAF at this time (will retry later) ...");
                    CnfJposServiceFactory.IsMonerisInSAFMode=true;
                }
                finally {
                    nextSafExecute = (new DateTime()).plusMillis(SAF_INTERVAL_MS);
                    safExecuting=false;
                }
            }


            if (asyncMode && isBusy()) {
                try {
                    this.executeOperation();
                } catch (JposException e) {
                    ErrorEvent ere = new ErrorEvent(this.ecb.getEventSource(), e.getErrorCode(), e.getErrorCodeExtended()
                            , JposConst.JPOS_EL_OUTPUT, JposConst.JPOS_ER_RETRY);
                    this.fireEvent(ere);
                } finally {
                    setBusy(false);
                }
            }
            synchronized (this)
            {
                try {
                    if (!isBusy()) this.wait(3000);
                } catch (InterruptedException e) {
                }
            }
        }
	}



    /// MISC Internal Routines

    private void setBusy(boolean isBusy)
    {
        synchronized (this)
        {
            busy = isBusy;
        }
    }

    private boolean isBusy()
    {
        synchronized (this)
        {
            return busy;
        }
    }


    private Map<String,String> additionalInfo = new HashMap<String, String>();
    private void buildAdditionalInfo()
    {
        additionalInfo.clear();
        if (additionalSecurityInformation!=null) {
            String[] infoElements = additionalSecurityInformation.split("\\|");
            for (String element : infoElements) {
                String[] pair = element.split("\\:");
                if (pair[0].startsWith("ASI")) pair[0] = pair[0].substring(3);
                if (pair.length == 1) additionalInfo.put(pair[0], null);
                else additionalInfo.put(pair[0], pair[1]);
            }
        }
    }



    public void onPortData(PortDataEvent evt) {
        // Nothing to do with this!
    }
}
