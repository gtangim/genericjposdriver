package cnf.jpos.services;

import apu.jpos.util.Utility;
import cnf.communication.ports.PortDataEvent;
import jpos.JposConst;
import jpos.JposException;
import jpos.services.CashDrawerService111;
import org.joda.time.DateTime;

/**
 * Created by russela on 7/17/2014.
 */
public class EpsonCashDrawerService extends BaseService implements CashDrawerService111 {


    public boolean getCapCompareFirmwareVersion() throws JposException {
        return false;
    }

    public boolean getCapUpdateFirmware() throws JposException {
        return false;
    }

    public void compareFirmwareVersion(String s, int[] ints) throws JposException {

    }

    public void updateFirmware(String s) throws JposException {

    }

    public boolean getCapStatisticsReporting() throws JposException {
        return false;
    }

    public boolean getCapUpdateStatistics() throws JposException {
        return false;
    }

    public void resetStatistics(String s) throws JposException {

    }

    public void retrieveStatistics(String[] strings) throws JposException {

    }

    public void updateStatistics(String s) throws JposException {

    }

    public boolean getCapStatusMultiDrawerDetect() throws JposException {
        return false;
    }

    public int getCapPowerReporting() throws JposException {
        return 0;
    }

    public int getPowerNotify() throws JposException {
        return 0;
    }

    public void setPowerNotify(int i) throws JposException {

    }

    public int getPowerState() throws JposException {
        return 0;
    }

    public boolean getCapStatus() throws JposException {
        return false;
    }

    public boolean getDrawerOpened() throws JposException {
        if (EpsonPrinterService.CurrentPrinter!=null)
        {
            EpsonPrinterService.CurrentPrinter.getPrinterStatus();
            return  EpsonPrinterService.CurrentPrinter.isDrawerOpen();
        }
        else throw new JposException(JposConst.JPOS_E_FAILURE,"No Epson printer is initialized!");
    }

    public void openDrawer() throws JposException {
        if (EpsonPrinterService.CurrentPrinter!=null)
        {
            EpsonPrinterService.CurrentPrinter.openDrawer();
        }
        else throw new JposException(JposConst.JPOS_E_FAILURE,"No Epson printer is initialized!");
    }

    public void waitForDrawerClose(int beepTimeout, int beepFrequency
            , int beepDuration, int beepDelay) throws JposException {

        if (EpsonPrinterService.CurrentPrinter!=null) {
            DateTime now = new DateTime();
            EpsonPrinterService p = EpsonPrinterService.CurrentPrinter;

            while (Utility.getElapsed(now) < beepTimeout)
            {
                p.getPrinterStatus();
                if (!p.isDrawerOpen()) return;
            }

            while (true)
            {
                try {
                    p.getPrinterStatus();
                    if (!p.isDrawerOpen()) return;
                    p.api.sendCommand("beep",beepFrequency,beepDelay);
                    p.getPrinterStatus();
                    if (!p.isDrawerOpen()) return;
                    Thread.sleep(beepDelay);
                } catch (Exception e) {
                    log(e);
                    throw new JposException(JposConst.JPOS_E_FAILURE,"Unable to Communicate with the Printer!", e);
                }
            }
        }



    }

    public void onPortData(PortDataEvent evt) {

    }
}
