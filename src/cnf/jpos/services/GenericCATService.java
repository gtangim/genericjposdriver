package cnf.jpos.services;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

import cnf.communication.ports.PortDataEvent;
import cnf.communication.ports.PortIOException;
import cnf.communication.ports.PortDataEvent.PortDataEventType;
import cnf.jpos.dialogs.*;
import apu.jpos.util.*;
import cnf.util.Expression.ExpressionParser;
import cnf.util.Expression.ParseException;
//import cnf.util.StringUtil;

import jpos.services.*;
import jpos.JposConst;
import jpos.JposException;
import jpos.CATConst;
import jpos.services.EventCallbacks;
//import jpos.config.JposEntry;
import jpos.events.ErrorEvent;
import jpos.events.OutputCompleteEvent;
import org.joda.time.DateTime;
import org.joda.time.Duration;


public class GenericCATService extends BaseService implements CATService111, Runnable {
	protected String clerkId;
	protected String storeId;
	protected String terminalId;
    protected boolean hasRecovered=false;
    protected int recoveredPaymentMedia=0;

	protected String additionalSecurityInformation;
	protected String accountNumber;
	protected String approvalCode;
	protected int paymentCondition;
	protected int paymentMedia;
	protected String paymentDetail;
	//protected int sequenceNumber;
	protected String slipNumber;
	protected String transactionNumber;
	protected String transactionType;
	protected String cardCompanyID;
	protected String centerResultCode;
	protected String cardExpiryDate;
	protected long totalAmount;
	//protected String declineCode;
	protected String transactionDate;
	protected String transactionTime;
	protected long cardBalance;
    protected int pingInterval;
    protected int pingResponseTimeout;

	protected boolean asyncMode;
	protected boolean busy;
    protected String lastSuccessfulResponse = null;
	
	protected ExpressionParser ex_cid;
	protected ExpressionParser ex_stid;
	protected ExpressionParser ex_trid;
	
	//protected ExpressionParser ex_asi_in;
	protected ExpressionParser ex_asi_out;
	protected ExpressionParser ex_an;
	protected ExpressionParser ex_ac;
	protected ExpressionParser ex_pc;
	protected ExpressionParser ex_pm;
	protected ExpressionParser ex_pd;
	//protected ExpressionParser ex_sqn;
	protected ExpressionParser ex_sln;
	//protected ExpressionParser ex_tn;
	//protected ExpressionParser ex_tt;
	protected ExpressionParser ex_ccid;
	protected ExpressionParser ex_crc;
	protected ExpressionParser ex_ced;
	protected ExpressionParser ex_ta;
	//protected ExpressionParser ex_dc;
	protected ExpressionParser ex_tdt;
	protected ExpressionParser ex_ttm;
	protected ExpressionParser ex_cb;

	protected ExpressionParser ex_det_ad;
	protected ExpressionParser ex_det_cd;
	protected ExpressionParser ex_det_dc;
	protected ExpressionParser ex_det_er;

	protected ExpressionParser ex_op_ac;
	protected ExpressionParser ex_op_aps;
	protected ExpressionParser ex_op_ar;
	protected ExpressionParser ex_op_as;
	protected ExpressionParser ex_op_av;
	protected ExpressionParser ex_op_avps;
	protected ExpressionParser ex_op_cc;
    protected ExpressionParser ex_op_r;
    protected ExpressionParser ex_op_ping;
    protected ExpressionParser ex_op_ping_r_idle;
    protected ExpressionParser ex_op_ping_r_tx;

	protected String txnFileName;
	protected String txnPrefix;
	protected String request;
	protected String response;
	//protected String info;
	
	private String tx_code;
	private String tx_name;
	private int tx_sqn;
	private long tx_amt;
	private long tx_tax;
	
	protected CATTransactionProgressWindow cwin=null;
    protected CATHealthWindow hwin = null;
	
	protected void reset()
	{
		this.clerkId = "";
		this.storeId = "";
		this.terminalId = "";
		
		this.additionalSecurityInformation = "";
		this.accountNumber = "";
		this.approvalCode = "";
		this.cardCompanyID = "";
		this.cardExpiryDate = "";
		this.cardExpiryDate = "";
		this.centerResultCode = "";
		this.dataEventsEnabled = false;
		//this.declineCode = "";
		this.deviceClaimed = false;
		this.deviceEnabled = false;
		this.deviceOpen = false;
		this.freezeEvents = false;
		this.paymentCondition = CATConst.CAT_PAYMENT_LUMP;
		this.paymentDetail = "";
		this.paymentMedia = CATConst.CAT_MEDIA_UNSPECIFIED;
		//this.sequenceNumber = 0;
		this.slipNumber = "";
		this.totalAmount = 0;
		this.transactionNumber = "";
		this.transactionTime = "";
		this.transactionDate = "";
		this.transactionType = "";
		this.cardBalance = 0;
		
		asyncMode = false;
		busy=false;
		
		txnFileName="";
		txnPrefix="";
		request="";
		response=null;
		cwin=null;		
		hwin=null;
		this.serviceVer = 1011000;
	}
	
	//@Override
    public void open(String deviceName, EventCallbacks ecb) throws JposException {
        reset();
	    initializeProperties();
	    initializePropertyValues();
    	super.open(deviceName, ecb);    	
    }    	
	
    //@Override
    public void close() throws JposException
    {
    	log("Closing Payment Terminal device...");
    	if (protocol.isBusy()) protocol.cancelCommand();
    	asyncMode = false;
    	busy=false;
    	super.close();
    }	
	
	//@Override
	public void cashDeposit(int arg0, long arg1, int arg2) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL, "This driver does not support Cash Deposit!");
	}

	//@Override
	public void compareFirmwareVersion(String arg0, int[] arg1)
			throws JposException {
		error(JposConst.JPOS_E_ILLEGAL, "This driver does not support Compare Firmware Version!");
		
	}

	//@Override
	public long getBalance() throws JposException {
		return cardBalance;
	}

	//@Override
	public boolean getCapCashDeposit() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapCompareFirmwareVersion() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapLockTerminal() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapLogStatus() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapUnlockTerminal() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapUpdateFirmware() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public int getLogStatus() throws JposException {
		return CATConst.CAT_LOGSTATUS_OK;
	}

	//@Override
	public long getSettledAmount() throws JposException {
		return totalAmount;
	}

	//@Override
	public void lockTerminal() throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Lock Terminal!");
	}

	//@Override
	public void unlockTerminal() throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Unlock Terminal!");
	}

	//@Override
	public void updateFirmware(String arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Update Firmware!");
	}

	//@Override
	public boolean getCapStatisticsReporting() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapUpdateStatistics() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public void resetStatistics(String arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
	}

	//@Override
	public void retrieveStatistics(String[] arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
	}

	//@Override
	public void updateStatistics(String arg0) throws JposException {
		error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
	}

	//@Override
	public int getPaymentMedia() throws JposException {
		check(true,false,false);
		return paymentMedia;
	}

	//@Override
	public void setPaymentMedia(int arg0) throws JposException {
		check(true,false,false);
		paymentMedia = arg0;
	}

	//@Override
	public void accessDailyLog(int arg0, int arg1, int arg2)
			throws JposException {
		error(JposConst.JPOS_E_ILLEGAL, "This driver does not support Daily Logs!");
	}


	private int elapsed(DateTime start)
    {
        if (start==null) return -1;
        return (int)(new Duration(start,new DateTime()).getMillis());
    }


	protected void executeOperation() throws JposException
	{
		if (tx_code==null || tx_code.isEmpty()) {
			
			if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Not Supported!", "Command Expression in jpos.xml for this operation is empty!");
			error(JposConst.JPOS_E_ILLEGAL,
				tx_name + " is not implemented by this driver!");
		}
		try{
            String ping=null;
            //String pingResponse=null;
            if (pingInterval>0)
            {
                ping = ex_op_ping.getValue().getString();
                //pingResponse = ex_op_ping_r.getValue().getString();
            }

			transactionNumber = this.getNextTransactionNumber();
			this.show_progress();
			cwin.add("Initializing....");
			this.clearOutput();
			synchronized(this)
			{
				busy = true;
			}
			ex_cid.addVariable("info", additionalSecurityInformation);
			ex_cid.addVariable("request", tx_code);
			
			clerkId = ex_cid.getValue().getString();
			terminalId = ex_trid.getValue().getString();
			storeId = ex_stid.getValue().getString();
			
			cwin.add("Done!\r\nCommunicating...");
			protocol.SendPacket(tx_code);
			cwin.add("Done!\r\nWaiting for client to finish...");
            response=null;
            try
			{
                DateTime pingTime = new DateTime();
                protocol.startAsyncRead();
                while(response==null)
                {
                    if (cwin.isCancelled()) throw new Exception("Transaction Cancelled!");
                    if (pingInterval>0)
                    {
                        if (elapsed(pingTime)>pingInterval)
                        {
                            if (!sendPing())
                                throw new Exception("Connection Lost!");
                            pingTime=new DateTime();
                        }
                    }
                    String currentResponse = protocol.readPacketAsync(500);
                    int respType = getPingResponseType(currentResponse);
                    //if (respType==1) throw new Exception("Communication Error, please try again!");
                    if (currentResponse!=null && respType!=2)
                        response=currentResponse;
                }
                protocol.endAsyncRead();
			} 
			catch(PortIOException e)
			{
				protocol.endAsyncRead();
                if (e.getMessage().contains("timeout"))
					throw new Exception("Transaction Timeout!");
				else throw e;
			}
			ex_cid.addVariable("response", response);
			extractFieldsFromResponse();
			injectFieldsIntoParser();
			this.additionalSecurityInformation = ex_asi_out.getValue().getString();

			String rs = ex_det_ad.getValue().getString();
			if (rs.equals("0"))
			{
				rs=ex_det_cd.getValue().getString();
				if (rs.equals("0"))
				{
					rs = ex_det_dc.getValue().getString();
					if (rs.equals("0"))
					{
						rs = ex_det_er.getValue().getString();
						if (rs.equals("0"))
							throw new Exception("Unknown Response!");
						else 
							throw new Exception("Transaction Error!");
					}
					else throw new Exception("Transaction Declined!");
				}
				else throw new Exception("Transaction Cancelled!");
			}
			else{
				cwin.add("Done!\r\n");
				log("Payment Terminal Completed Transaction!");
				if (asyncMode)
				{
					if (hwin!=null && hwin.isVisible()==true) hwin.updateResult();
					OutputCompleteEvent ocev = new OutputCompleteEvent(this.ecb.getEventSource(),0);
					this.fireEvent(ocev);
				}
			}
			cwin.closeWindow();
            lastSuccessfulResponse = response;
			
		}
		catch(PortIOException e)
		{
			if (cwin!=null) cwin.add("Failed!\r\n");
			cwin.closeWindow();
			if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Failed!", e.getMessage());
			error_ext(CATConst.JPOS_ECAT_COMMUNICATIONERROR, e.getMessage());
		}
		catch(Exception e)
		{
			if (cwin!=null) 
				if (e.getMessage()==null)
				{
					cwin.add("Failed!\r\n");
					cwin.closeWindow();
					if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Failed!", "Null reference encountered!");
					error(CATConst.JPOS_ECAT_CENTERERROR, "Null reference encountered!");
				}
				if (e.getMessage().contains("Timeout")) 
				{
					cwin.add("Timeout!\r\n");					
					cwin.closeWindow();
					if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Timeout!", e.getMessage());
					error(JposConst.JPOS_E_TIMEOUT, e.getMessage());
				}
				else if (e.getMessage().contains("Cancelled")) 
				{
					cwin.add("Cancelled!\r\n");					
					cwin.closeWindow();
					if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Cancelled!", e.getMessage());
					error_ext(CATConst.JPOS_ECAT_RESET, e.getMessage());
				}
				else if (e.getMessage().contains("Declined")) 
				{
					cwin.add("Declined!\r\n");					
					cwin.closeWindow();
					if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Declined!", e.getMessage());
					error(CATConst.JPOS_ECAT_CENTERERROR, e.getMessage());
				}
				else 
					{
						cwin.add("Failed!\r\n");
						cwin.closeWindow();
						if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Failed!", e.getMessage());
						error(CATConst.JPOS_ECAT_CENTERERROR, e.getMessage());
					}
		}
	}
	
	
	protected void doTransaction(String TxType, ExpressionParser tx, int sqn, long amount, long tax) throws JposException
	{
		if (busy)
			error(JposConst.JPOS_E_BUSY,"Error: Another transaction is in progress!");
		synchronized(this)
		{
			busy = true;
		}
        
        if (hasRecovered && response!=null && !response.equals(lastSuccessfulResponse))
        {
            BigDecimal amt = new BigDecimal(this.getSettledAmount()).movePointLeft(2);
            ConfirmDialog cd = new ConfirmDialog(
                    "<p style=\"color:black; font-size:14px;\">"
                            +"The pinpad recovered a transaction that already went through the payment provider.<br><br>"
                            +"This is a " + this.transactionType + " of amount " + amt.toString() + "<br>"
                            +"Card# " + this.getAccountNumber()+"<br>"
                            +"</p><br><br><p style=\"color:red; font-size:20px;\">"
                            +"Do you want to recover this transaction?</font>"
                    ,"Recover Transaction?");
            String resp = cd.confirm();
            if (resp.equals("yes"))
            {
                hasRecovered=false;
                paymentMedia=recoveredPaymentMedia;
                synchronized(this)
                {
                    busy = false;
                }
                if (cwin!=null) cwin.closeWindow();
                if (asyncMode)
                {
                    if (hwin!=null && hwin.isVisible()==true) hwin.updateResult();
                    OutputCompleteEvent ocev = new OutputCompleteEvent(this.ecb.getEventSource(),0);
                    this.fireEvent(ocev);
                }
                lastSuccessfulResponse=response;
                return;
            }
        }
        
		try
		{
			response=null;
            hasRecovered=false;
            tx_amt = amount;
			tx_tax = tax;
			ex_op_r.addVariable("amount", tx_amt+tx_tax);
			tx_code = tx.getValue().getString();
			tx_name = TxType;
			tx_sqn = sqn;
			//transactionType = Integer.toString(CATConst.CAT_TRANSACTION_SALES);
			transactionType = tx_name;
			if (!asyncMode)
			{
				executeOperation();
				synchronized(this)
				{
					busy = false;
				}
			}
			else new Thread(this).start();
		}
		catch(ParseException e)
		{
			if (cwin!=null) cwin.add("Error!\r\n");
			if (cwin!=null && cwin.isVisible()) cwin.closeWindow();
			synchronized(this)
			{				
				busy = false;
			}
			error(JposConst.JPOS_E_FAILURE,"Driver Setting Error (Check jpos.xml)! " + e.getMessage());
		}
		catch(JposException e)
		{
			if (cwin!=null && cwin.isVisible()) cwin.closeWindow();
			synchronized(this)
			{
				busy = false;
			}
			throw e;
		}
		catch(Exception e)
		{
			if (cwin!=null) cwin.add("Error!\r\n");
			if (cwin!=null && cwin.isVisible()) cwin.closeWindow();
			synchronized(this)
			{
				busy = false;
			}
			error(JposConst.JPOS_E_FAILURE,"Driver Error! " + e.getMessage());
		}
        finally
        {
            busy=false;
        }
	}
		
	
	//@Override
	public void authorizeCompletion(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
		doTransaction("AuthorizeCompletion", ex_op_ac, sequenceNumber, amount, taxOthers);
	}

	//@Override
	public void authorizePreSales(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
		doTransaction("AuthorizePreSales", ex_op_aps, sequenceNumber, amount, taxOthers);
	}

	//@Override
	public void authorizeRefund(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
		doTransaction("AuthorizeRefund", ex_op_ar, sequenceNumber, amount, taxOthers);
		
	}


	
	//@Override
	public void authorizeSales(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
		doTransaction("AuthorizeSales", ex_op_as, sequenceNumber, amount, taxOthers);
	}

	//@Override
	public void authorizeVoid(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
		doTransaction("AuthorizeVoid", ex_op_av, sequenceNumber, amount, taxOthers);
	}

	//@Override
	public void authorizeVoidPreSales(int sequenceNumber, long amount, long taxOthers, int timeout)
			throws JposException {
		check(true,true,true);
		doTransaction("AuthorizeVoidPreSales", ex_op_avps, sequenceNumber, amount, taxOthers);
	}

	//@Override
	public void checkCard(int sequenceNumber, int timeout) throws JposException {
		check(true,true,true);
		doTransaction("CheckCard", ex_op_cc, sequenceNumber, 0, 0);
	}

	//@Override
	public void clearOutput() throws JposException {
		check(true,true,false);
        protocol.clearPackets();
        try{
            String v = ex_op_r.getValue().getString();
            protocol.SendPacket(v);
		}
		catch(ParseException e)
		{
			error(JposConst.JPOS_E_FAILURE,"Invalid ResetCommandExpression in jpos.xml. " + e.getMessage());
		}
		catch(PortIOException e)
		{
			error_ext(CATConst.JPOS_ECAT_COMMUNICATIONERROR,"Communication error with device! " + e.getMessage());
		}

		protocol.clearPackets();
        synchronized(this)
		{
			busy = false;
		}
		if (protocol.isBusy()) protocol.cancelCommand();

        if (!sendPing())
            error_ext(CATConst.JPOS_ECAT_COMMUNICATIONERROR
                    ,"Communication Error: device not connected!");
        int pType = getPingResponseType(protocol.peekPacket(0));
        if (pType!=1) error_ext(CATConst.JPOS_ECAT_COMMUNICATIONERROR
                    ,"Communication Error: device not ready to accept transaction, try again!");
        protocol.clearPackets();
		try
		{
			synchronized(this)
			{
				this.notifyAll();
			}
		}
		catch(Exception ex){}
		
	}

    public boolean sendPing()
    {
        try{
            String v = ex_op_ping.getValue().getString();
            protocol.setRetryForNextPacket(false);
            protocol.SendPacket(v);
            DateTime pingStart=new DateTime();
            while(elapsed(pingStart)<=pingResponseTimeout)
            {
                if (protocol.peekPacket(200)!=null)
                {
                    try {
                        // We want to wait for the ack to be sent before taking the next step...
                        Thread.sleep(700);
                    } catch (InterruptedException e) {
                    }
                    return true;
                }
            }
            return false;
		}
		catch(ParseException e)
		{
            return false;
		}
		catch(PortIOException e)
		{
            return false;
		}
    }

    public int getPingResponseType(String resp)
    {
        synchronized (this)
        {
            try {
                if (resp==null) return 0;
                ex_op_ping_r_idle.addVariable("response",resp);
                String idleResponse = ex_op_ping_r_idle.getValue().getString();
                String txResponse = ex_op_ping_r_tx.getValue().getString();
                if (txResponse!=null && !txResponse.equals("0")) return 2;
                else if (idleResponse!=null && !idleResponse.equals("0")) return 1;
                else return 0;
            } catch (Exception e) {
                return -1;
            }
        }
    }

	//@Override
	public String getAccountNumber() throws JposException {
		check(true,false,false);
		return accountNumber;
	}

	//@Override
	public String getAdditionalSecurityInformation() throws JposException {
		check(true,false,false);
		return additionalSecurityInformation;
	}

	//@Override
	public String getApprovalCode() throws JposException {
		check(true,false,false);
		return approvalCode;
	}

	//@Override
	public boolean getAsyncMode() throws JposException {
		check(true,false,false);
		return asyncMode;
	}

	//@Override
	public boolean getCapAdditionalSecurityInformation() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public boolean getCapAuthorizeCompletion() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public boolean getCapAuthorizePreSales() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public boolean getCapAuthorizeRefund() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public boolean getCapAuthorizeVoid() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public boolean getCapAuthorizeVoidPreSales() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public boolean getCapCenterResultCode() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public boolean getCapCheckCard() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public int getCapDailyLog() throws JposException {
		check(true,false,false);
		return CATConst.CAT_DL_NONE;
	}

	//@Override
	public boolean getCapInstallments() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapPaymentDetail() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public int getCapPowerReporting() throws JposException {
		check(true,false,false);
		return JposConst.JPOS_PR_NONE;
	}

	//@Override
	public boolean getCapTaxOthers() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public boolean getCapTrainingMode() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public boolean getCapTransactionNumber() throws JposException {
		check(true,false,false);
		return true;
	}

	//@Override
	public String getCardCompanyID() throws JposException {
		check(true,false,false);
		return cardCompanyID;
	}

	//@Override
	public String getCenterResultCode() throws JposException {
		check(true,false,false);
		return centerResultCode;
	}

	//@Override
	public String getDailyLog() throws JposException {
		error(JposConst.JPOS_E_ILLEGAL, "This driver does not support Daily Logs!");
		return "";
	}

	//@Override
	public int getPaymentCondition() throws JposException {
		check(true,false,false);
		return paymentCondition;
	}

	//@Override
	public String getPaymentDetail() throws JposException {
		check(true,false,false);
		return paymentDetail;
	}

	//@Override
	public int getPowerNotify() throws JposException {
		check(true,false,false);
		return JposConst.JPOS_PN_DISABLED;
	}

	//@Override
	public int getPowerState() throws JposException {
		check(true,false,false);
		return JposConst.JPOS_PS_UNKNOWN;
	}

	//@Override
	public int getSequenceNumber() throws JposException {
		check(true,false,false);
		return tx_sqn;
	}

	//@Override
	public String getSlipNumber() throws JposException {
		check(true,false,false);
		return slipNumber;
	}

	//@Override
	public boolean getTrainingMode() throws JposException {
		check(true,false,false);
		return false;
	}

	//@Override
	public String getTransactionNumber() throws JposException {
		check(true,false,false);
		return transactionNumber;
	}

	//@Override
	public String getTransactionType() throws JposException {
		check(true,false,false);
		return transactionType;
	}

	//@Override
	public void setAdditionalSecurityInformation(String arg0)
			throws JposException {
		check(true,false,false);
		additionalSecurityInformation = arg0;
	}

	//@Override
	public void setAsyncMode(boolean arg0) throws JposException {
		check(true,false,false);
		asyncMode=arg0;
		log("Async_Mode = " + Boolean.toString(arg0));
		
	}

	//@Override
	public void setPowerNotify(int arg0) throws JposException {
		check(true,false,false);
		if (arg0==JposConst.JPOS_PN_ENABLED)
			error(JposConst.JPOS_E_ILLEGAL,"Power Notify is not implemented by the driver!");
	}

	//@Override
	public void setTrainingMode(boolean arg0) throws JposException {
		check(true,false,false);
		if (arg0==true)
			error(JposConst.JPOS_E_ILLEGAL,"Training Mode is not implemented by the driver!");
	}

    //@Override
    public void checkHealth(int typ) throws JposException {
    	boolean async = asyncMode;
    	this.setAsyncMode(true);
    	if (typ==JposConst.JPOS_CH_INTERNAL) healthText = "Successful";
    	else if (typ==JposConst.JPOS_CH_EXTERNAL) healthText = "Successful";
    	else if (typ==JposConst.JPOS_CH_INTERACTIVE)
    	{
    		healthText = "Checking";
    		if (hwin==null) hwin = new CATHealthWindow(this);
    		hwin.clear();
    		hwin.setModal(false);
    		hwin.setVisible(true);
            while (hwin.isVisible())
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
            }
    	}
    	else 
    	{
        	this.setAsyncMode(async);
    		error(JposConst.JPOS_E_ILLEGAL,"Invalid checkHealth Type invoked!");
    	}
    	this.setAsyncMode(async);
    }	
	//@Override
	public void onPortData(PortDataEvent evt) {
		if (evt.getEventType()==PortDataEventType.DATA_RECEIVED)
		{
			if (!busy)
            {
                try {
                    ex_cid.addVariable("response", evt.getValue());
                    extractFieldsFromResponse();
                    injectFieldsIntoParser();
                    this.additionalSecurityInformation = ex_asi_out.getValue().getString();

                    String rs = ex_det_ad.getValue().getString();
                    if (!rs.equals("0"))
                    {
                        // We have a valid transaction...
                        response = evt.getValue();
                        recoveredPaymentMedia = paymentMedia;
                        hasRecovered = true;

                    }
                    else logWarning("Out of context transaction packet detected! >> " + evt.getValue());
                } catch (ParseException e) {
                    logError("error: " + e.getMessage());
                }
            }
        }
	}
	
	protected void check(boolean open, boolean claim, boolean enable) throws JposException
	{
		if (enable==true && this.deviceEnabled==false) 
			error(JposConst.JPOS_E_DISABLED, "Device must be opened, claimed and enabled first!");
		else if (claim==true && this.deviceClaimed==false) 
			error(JposConst.JPOS_E_NOTCLAIMED, "Device must be opened and claimed first!");
		else if (open==true && this.deviceOpen==false)
			error(JposConst.JPOS_E_CLOSED, "Device must be opened first!");
	}
	
	
    private int create_int(String propName, int min, int max) throws JposException
    {
        int t = 0;
        try
        {
            t = getPropertyValueInteger(propName);
        }
        catch(JposException e)
        {
            error(JposConst.JPOS_E_NOSERVICE,"You must assign a valid integer for " + propName);
        }
        if (t<min || t>max)
            error(JposConst.JPOS_E_NOSERVICE, "Property " + propName + "=" + t
                    +" is out of range! Valid range is "
                    + Integer.toString(min) + ".." + Integer.toString(max));

        return t;

    }

	private ExpressionParser create_exp(String propName, HashMap symbolTable, HashMap functionTable) throws JposException
	{
		String t = getPropertyValue(propName);
		if (t==null || t.trim().isEmpty()) 
			error(JposConst.JPOS_E_NOSERVICE,"You must assign a valid expression for " + propName);
		ExpressionParser e=null;
		if (symbolTable==null && functionTable==null)
		{
			e = new ExpressionParser();
			e.addStandardConstants();
			e.addStandardFunctions();
			e.setAutoVariableDeclare(true);
		}
		else if (symbolTable!=null && functionTable!=null)
		{	
			e = new ExpressionParser(symbolTable,functionTable);
			e.setAutoVariableDeclare(true);
		}
		else error(JposConst.JPOS_E_NOSERVICE,"Illegal attempt to initialize "
				+ propName + " with null symbol/function table!");
		e.parseExpression(t);
		if (e.hasError()) 
			error(JposConst.JPOS_E_NOSERVICE,"Illegal expression in "
					+ propName + ": " + e.getErrorInfo());
		return e;
	}

	private void initializeProperties()
	{
		this.resetProperties();
		//this.setPropertyLogger(logger);
		this.setPropertyModuleName(deviceName);
		//log("Setting property names for this device.");
		
		registerProperty("ProtocolClass","This device requires a protocol that implements cnf.communication.protocols.ProtocolInterface","''",null);
		
		registerProperty("ClerkIdExpression","extract clerkid from request or info","''",null);
		registerProperty("StoreIdExpression","extract clerkid from request or info","''",null);
		registerProperty("TerminalIdExpression","extract clerkid from request or info","''",null);

		registerProperty("AccountNumberExpression","extract Account Number from response or info",null,null);
		registerProperty("ApprovalCodeExpression","extract Approval Code from response or info",null,null);
		registerProperty("PaymentConditionExpression","extract Payment Condition from response or info",null,null);
		registerProperty("PaymentMediaExpression","extract Payment Media from response or info",null,null);
		registerProperty("PaymentDetailExpression","extract Payment Detail from response or info",null,null);
		registerProperty("SlipNumberExpression","extract Slip Number from response or info",null,null);
		registerProperty("CardCompanyIdExpression","extract Credit Card Company ID from response or info",null,null);
		registerProperty("CenterResultCodeExpression","extract Error/Result Code from response or info",null,null);
		registerProperty("CardExpiryDateExpression","extract Card Expiry Date from response or info",null,null);
		registerProperty("TotalAmountExpression","extract Total Amount Charged from response or info",null,null);
		registerProperty("TransactionDateExpression","extract Transaction Date from response or info",null,null);
		registerProperty("TransactionTimeExpression","extract Transaction Time from response or info",null,null);
		registerProperty("CardBalanceExpression","extract Card Balance from response or info",null,null);
		registerProperty("AdditionalResponseInfoExpression","construct additional info from response or info",null,null);

		registerProperty("ApprovedDetectExpression","if true the response indicates 'request approved'",null,null);
		registerProperty("CancelledDetectExpression","if true the response indicates 'request cancelled'",null,null);
		registerProperty("DeclinedDetectExpression","if true the response indicates 'request declined'",null,null);
		registerProperty("ErrorDetectExpression","if true the response indicates an error has occured",null,null);
		
		registerProperty("AuthorizeCompletionCommandExpression","The command string for Authorize Completion",null,null);
		registerProperty("AuthorizePreSalesCommandExpression","The command string for Pre-Sales",null,null);
		registerProperty("AuthorizeRefundCommandExpression","The command string for Refund",null,null);
		registerProperty("AuthorizeSalesCommandExpression","The command string for Sales",null,null);
		registerProperty("AuthorizeVoidCommandExpression","The command string for Void Sales",null,null);
		registerProperty("AuthorizeVoidPreSalesCommandExpression","The command string for Voild Pre-Sales",null,null);
		registerProperty("CheckCardCommandExpression","The command string for Card Check",null,null);
        registerProperty("ResetCommandExpression","The command string for Reset Device",null,null);
        registerProperty("PingInterval","How often to ping device (ms) while waiting for response (0=disabled)","0",null);
        registerProperty("PingResponseTimeout","How long to wait for a ping response","5000",null);
        registerProperty("PingCommandExpression","The command to test connection to the device",null,null);
        registerProperty("PingResponseIdleExpression","The command response to confirm device is idle",null,null);
        registerProperty("PingResponseTxExpression","The command response to confirm device is in Tx",null,null);

		registerProperty("TransactionNumberFileName","The Store for the transaction number",null,null);
		
	}
	
	private void initializePropertyValues() throws JposException 
	{
		
		this.loadPropertyValues(entry);
		
		ex_cid = create_exp("ClerkIdExpression",null,null);
		this.addCATConstants(ex_cid);
		ex_cid.addStandardConstants();
		ex_cid.addStandardFunctions();
		HashMap s = ex_cid.getSymbolTable();
		HashMap f = ex_cid.getFunctionTable();
		ex_stid = create_exp("StoreIdExpression",s,f);
		ex_trid = create_exp("TerminalIdExpression",s,f);
		
		ex_asi_out = create_exp("AdditionalResponseInfoExpression",s,f);
		ex_an = create_exp("AccountNumberExpression",s,f);
		ex_ac = create_exp("ApprovalCodeExpression",s,f);
		ex_pc = create_exp("PaymentConditionExpression",s,f);
		ex_pm = create_exp("PaymentMediaExpression",s,f);
		ex_pd = create_exp("PaymentDetailExpression",s,f);
		ex_sln = create_exp("SlipNumberExpression",s,f);
		//ex_tn = create_exp("TransactionNumberExpression",s,f);
		//ex_tt = create_exp("TransactionTypeExpression",s,f);
		ex_ccid = create_exp("CardCompanyIdExpression",s,f);
		ex_crc = create_exp("CenterResultCodeExpression",s,f);
		//ex_cid.addVariable("response", "11010^111^");
		//try{
		//ValueElement v = ex_crc.getValue();
		//}catch(Exception e){}
		ex_ced = create_exp("CardExpiryDateExpression",s,f);
		ex_ta = create_exp("TotalAmountExpression",s,f);
		//ex_dc = create_exp("DeclineCodeExpression",s,f);
		ex_tdt = create_exp("TransactionDateExpression",s,f);
		ex_ttm = create_exp("TransactionTimeExpression",s,f);
		ex_cb = create_exp("CardBalanceExpression",s,f);

		ex_det_ad = create_exp("ApprovedDetectExpression",s,f);
		ex_det_cd = create_exp("CancelledDetectExpression",s,f);
		ex_det_dc = create_exp("DeclinedDetectExpression",s,f);
		ex_det_er = create_exp("ErrorDetectExpression",s,f);

		ex_op_ac = create_exp("AuthorizeCompletionCommandExpression",s,f);
		ex_op_aps = create_exp("AuthorizePreSalesCommandExpression",s,f);
		ex_op_ar = create_exp("AuthorizeRefundCommandExpression",s,f);
		ex_op_as = create_exp("AuthorizeSalesCommandExpression",s,f);
		ex_op_av = create_exp("AuthorizeVoidCommandExpression",s,f);
		ex_op_avps = create_exp("AuthorizeVoidPreSalesCommandExpression",s,f);
		ex_op_cc = create_exp("CheckCardCommandExpression",s,f);
        ex_op_r = create_exp("ResetCommandExpression",s,f);
        ex_op_ping = create_exp("PingCommandExpression",s,f);
        ex_op_ping_r_idle = create_exp("PingResponseIdleExpression",s,f);
        ex_op_ping_r_tx = create_exp("PingResponseTxExpression",s,f);
        pingInterval = create_int("PingInterval", 0, 100000);
        pingResponseTimeout = create_int("PingResponseTimeout",0,100000);

		txnFileName = getPropertyValue("TransactionNumberFileName");
		txnPrefix = txnFileName.split("\\.")[0];
    	
	}
	
	
	protected void addCATConstants(ExpressionParser e)
	{
		e.addVariable("CAT_DL_NONE", CATConst.CAT_DL_NONE);
		e.addVariable("CAT_DL_REPORTING", CATConst.CAT_DL_REPORTING);
		e.addVariable("CAT_DL_REPORTING_SETTLEMENT", CATConst.CAT_DL_REPORTING_SETTLEMENT);
		e.addVariable("CAT_DL_SETTLEMENT", CATConst.CAT_DL_SETTLEMENT);
		e.addVariable("CAT_DL_SETTLEMENT", CATConst.CAT_DL_SETTLEMENT);
		e.addVariable("CAT_LOGSTATUS_NEARFULL", CATConst.CAT_LOGSTATUS_NEARFULL);
		e.addVariable("CAT_LOGSTATUS_OK", CATConst.CAT_LOGSTATUS_OK);
		e.addVariable("CAT_MEDIA_CREDIT", CATConst.CAT_MEDIA_CREDIT);
		e.addVariable("CAT_MEDIA_DEBIT", CATConst.CAT_MEDIA_DEBIT);
		e.addVariable("CAT_MEDIA_ELECTRONIC_MONEY", CATConst.CAT_MEDIA_ELECTRONIC_MONEY);
		e.addVariable("CAT_MEDIA_NONDEFINE", CATConst.CAT_MEDIA_NONDEFINE);
		e.addVariable("CAT_MEDIA_UNSPECIFIED", CATConst.CAT_MEDIA_UNSPECIFIED);
		e.addVariable("CAT_PAYMENT_BONUS_1", CATConst.CAT_PAYMENT_BONUS_1);
		e.addVariable("CAT_PAYMENT_BONUS_2", CATConst.CAT_PAYMENT_BONUS_2);
		e.addVariable("CAT_PAYMENT_BONUS_3", CATConst.CAT_PAYMENT_BONUS_3);
		e.addVariable("CAT_PAYMENT_BONUS_4", CATConst.CAT_PAYMENT_BONUS_4);
		e.addVariable("CAT_PAYMENT_BONUS_5", CATConst.CAT_PAYMENT_BONUS_5);
		e.addVariable("CAT_PAYMENT_BONUS_COMBINATION_1", CATConst.CAT_PAYMENT_BONUS_COMBINATION_1);
		e.addVariable("CAT_PAYMENT_BONUS_COMBINATION_2", CATConst.CAT_PAYMENT_BONUS_COMBINATION_2);
		e.addVariable("CAT_PAYMENT_BONUS_COMBINATION_3", CATConst.CAT_PAYMENT_BONUS_COMBINATION_3);
		e.addVariable("CAT_PAYMENT_BONUS_COMBINATION_4", CATConst.CAT_PAYMENT_BONUS_COMBINATION_4);		
		e.addVariable("", CATConst.CAT_PAYMENT_DEBIT);
		e.addVariable("", CATConst.CAT_PAYMENT_ELECTRONIC_MONEY);
		e.addVariable("CAT_PAYMENT_INSTALLMENT_1", CATConst.CAT_PAYMENT_INSTALLMENT_1);
		e.addVariable("CAT_PAYMENT_INSTALLMENT_2", CATConst.CAT_PAYMENT_INSTALLMENT_2);
		e.addVariable("CAT_PAYMENT_INSTALLMENT_3", CATConst.CAT_PAYMENT_INSTALLMENT_3);		
		e.addVariable("CAT_PAYMENT_LUMP", CATConst.CAT_PAYMENT_LUMP);
		e.addVariable("CAT_PAYMENT_REVOLVING", CATConst.CAT_PAYMENT_REVOLVING);
		e.addVariable("CAT_TRANSACTION_CASHDEPOSIT", CATConst.CAT_TRANSACTION_CASHDEPOSIT);
		e.addVariable("CAT_TRANSACTION_CHECKCARD", CATConst.CAT_TRANSACTION_CHECKCARD);
		e.addVariable("CAT_TRANSACTION_COMPLETION", CATConst.CAT_TRANSACTION_COMPLETION);
		e.addVariable("CAT_TRANSACTION_PRESALES", CATConst.CAT_TRANSACTION_PRESALES);
		e.addVariable("CAT_TRANSACTION_REFUND", CATConst.CAT_TRANSACTION_REFUND);
		e.addVariable("CAT_TRANSACTION_SALES", CATConst.CAT_TRANSACTION_SALES);
		e.addVariable("CAT_TRANSACTION_VOID", CATConst.CAT_TRANSACTION_VOID);
		e.addVariable("CAT_TRANSACTION_VOIDPRESALES", CATConst.CAT_TRANSACTION_VOIDPRESALES);
		e.addVariable("JPOS_ECAT_CENTERERROR", CATConst.JPOS_ECAT_CENTERERROR);
		e.addVariable("JPOS_ECAT_COMMANDERROR", CATConst.JPOS_ECAT_COMMANDERROR);
		e.addVariable("JPOS_ECAT_COMMUNICATIONERROR", CATConst.JPOS_ECAT_COMMUNICATIONERROR);
		e.addVariable("JPOS_ECAT_DAILYLOGOVERFLOW", CATConst.JPOS_ECAT_DAILYLOGOVERFLOW);
		e.addVariable("JPOS_ECAT_DEFICIENT", CATConst.JPOS_ECAT_DEFICIENT);
		e.addVariable("JPOS_ECAT_OVERDEPOSIT", CATConst.JPOS_ECAT_OVERDEPOSIT);
		e.addVariable("JPOS_ECAT_RESET", CATConst.JPOS_ECAT_RESET);
	}
	
	protected void show_progress()
	{
		if (cwin==null) cwin=new CATTransactionProgressWindow(this);
		cwin.clear();
        //cwin.setModal(true);
		cwin.setVisible(true);
		cwin.add("Requested Operation: " + tx_name+"\r\n");
		cwin.add("    Sequence Number: " + Integer.toString(tx_sqn)+"\r\n");
		cwin.add(" Transaction Number: " + transactionNumber+"\r\n");
		cwin.add("             Amount: $" + new DecimalFormat("#,###,##0.00").format((double)tx_amt/100.0)+"\r\n");
		cwin.add("          Tax/Other: $" + new DecimalFormat("#,###,##0.00").format((double)tx_tax/100.0)+"\r\n");
		cwin.add("              Total: $" + new DecimalFormat("#,###,##0.00").format((double)(tx_amt+tx_tax)/100.0)+"\r\n\r\n");
		
	}
	
	protected void extractFieldsFromResponse() throws ParseException, NumberFormatException
	{
		//this.additionalSecurityInformation = ex_asi_out.getValue().getString();
		this.accountNumber = ex_an.getValue().getString();
		this.approvalCode = ex_ac.getValue().getString();
		this.paymentCondition = (int)ex_pc.getValue().getInteger();
		this.paymentMedia = (int)ex_pm.getValue().getInteger();
		this.paymentDetail = ex_pd.getValue().getString();
		this.slipNumber = ex_sln.getValue().getString();
		this.cardCompanyID = ex_ccid.getValue().getString();
		this.centerResultCode = ex_crc.getValue().getString();
		this.cardExpiryDate = ex_ced.getValue().getString();
		try{
		this.totalAmount = ex_ta.getValue().getInteger();
		}catch(Exception e){totalAmount=0;}
		this.transactionDate = ex_tdt.getValue().getString();
		this.transactionTime = ex_ttm.getValue().getString();
		this.cardBalance = ex_cb.getValue().getInteger();
		
	}
	protected void injectFieldsIntoParser()
	{
		ex_cid.addVariable("clerkid", this.clerkId);
		ex_cid.addVariable("storeid", this.storeId);
		ex_cid.addVariable("terminalid", this.terminalId);

		ex_cid.addVariable("info", this.additionalSecurityInformation);
		ex_cid.addVariable("accountnumber", this.accountNumber);
		ex_cid.addVariable("approvalcode", this.approvalCode);
		ex_cid.addVariable("paymentcondition", this.paymentCondition);
		ex_cid.addVariable("paymentmedia", this.paymentMedia);
		ex_cid.addVariable("paymentdetail", this.paymentDetail);
		ex_cid.addVariable("sequencenumber", this.tx_sqn);		
		ex_cid.addVariable("slipnumber", this.slipNumber);
		ex_cid.addVariable("transactionnumber", this.transactionNumber);
		ex_cid.addVariable("transactiontype", this.transactionType);
		ex_cid.addVariable("cardCompanyid", this.cardCompanyID);
		ex_cid.addVariable("centerresultcode", this.centerResultCode);
		ex_cid.addVariable("cardexpirydate", this.cardExpiryDate);
		ex_cid.addVariable("totalamount", this.totalAmount);
		//ex_cid.addVariable("declineCode", this.declineCode);
		ex_cid.addVariable("transactiondate", this.transactionDate);
		ex_cid.addVariable("transactiontime", this.transactionTime);
		ex_cid.addVariable("cardbalance", this.cardBalance);
	}
	
	
	protected String getNextTransactionNumber()
	{
		String l = FileUtil.readTextFile(txnFileName);
		int t = 0;
		try
		{
			t=Integer.parseInt(l);
		}
		catch(Exception e)
		{
			t=0;
		}		
		t++;
		l= Integer.toString(t);
		try
		{
			FileUtil.writeTextFile(l, txnFileName);
		}
		catch(Exception e){}
		return txnPrefix+"_"+l;
	}

	//@Override
	public void run() {
		try
		{
			this.executeOperation();
		}
		catch(JposException e)
		{
			synchronized(this)
			{
				busy = false;
			}
			ErrorEvent ere = new ErrorEvent(this.ecb.getEventSource(),e.getErrorCode(),e.getErrorCodeExtended(),JposConst.JPOS_EL_OUTPUT, JposConst.JPOS_ER_RETRY);
			this.fireEvent(ere);			
		}
        finally
        {
            synchronized(this)
            {
                busy = false;
            }
        }
	}
}
