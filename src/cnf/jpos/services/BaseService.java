/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
/* This file has been modified by Open Source Strategies, Inc. */
package cnf.jpos.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import jpos.services.EventCallbacks;
import jpos.JposException;
import jpos.JposConst;
import jpos.events.DataEvent;
import jpos.events.ErrorEvent;
import jpos.events.DirectIOEvent;
import jpos.events.OutputCompleteEvent;
import jpos.events.StatusUpdateEvent;
import jpos.config.JposEntry;

import apu.jpos.util.*;
import cnf.communication.ports.*;
import cnf.communication.protocols.ProtocolInterface;
public abstract class BaseService extends JposPropertyExtender implements jpos.services.BaseService, jpos.loader.JposServiceInstance, PortDataListener {

    //public static final String module = BaseService.class.getName();

    protected List eventQueue = null;
    protected JposEntry entry = null;

    protected boolean deviceOpen = false;
    protected boolean deviceClaimed = false;
    protected boolean freezeEvents = false;
    protected boolean deviceEnabled = false;
    protected boolean dataEventsEnabled = true;

    protected String deviceName = null;
    protected String healthText = null;
    protected String physicalName = null;
    protected String physicalDesc = null;
    protected String serviceDesc = null;

    protected int serviceVer = 1007000;
    protected int state = JposConst.JPOS_S_CLOSED;
    
    protected SimpleLogger logger = SimpleLogger.getDefaultLogger();
    protected PortInterface port = null;
    protected ProtocolInterface protocol = null;
    

    protected EventCallbacks ecb = null;

    // open/close methods
    public void open(String deviceName, EventCallbacks ecb) throws JposException {
        if (deviceOpen) error(JposConst.JPOS_E_ILLEGAL, "Device is already open!");
    	this.deviceName = deviceName;
        this.ecb = ecb;
        this.healthText = "Ready";
        this.state = JposConst.JPOS_S_IDLE;
        this.serviceDesc = entry.getProp(JposEntry.DEVICE_CATEGORY_PROP_NAME).getValueAsString();
        this.physicalDesc = entry.getProp(JposEntry.PRODUCT_DESCRIPTION_PROP_NAME).getValueAsString();
        this.physicalName = entry.getProp(JposEntry.PRODUCT_NAME_PROP_NAME).getValueAsString();
        if (port!=null && port.isOpen()==false)
        {
        	try
        	{
        		port.initialize(entry);
            	port.setDataEventMode(true);
        		port.open();
        	}
        	catch(PortIOException e)
        	{
        		error(JposConst.JPOS_E_NOSERVICE,"Error initializing driver port!", e);
        	}        	
        }    
        if (protocol!=null)
        {
        	try
        	{
        		protocol.initialize(entry);
            	protocol.setDataEventMode(true);
        	}
        	catch(PortIOException e)
        	{
        		error(JposConst.JPOS_E_NOSERVICE, "Error initializing driver protocol!", e);
        	}        	
        }
        
        deviceOpen = true;
        deviceEnabled = false;
        deviceClaimed = false;
        freezeEvents = false;
        dataEventsEnabled = false;
        eventQueue = new ArrayList();
        log(deviceName + " >> " + "Device has been opened!");
        
    }

    public void claim(int i) throws JposException {
        if (!deviceOpen) error(JposConst.JPOS_E_CLOSED, "Device must be opened before claiming!");
        else if (deviceClaimed) error(JposConst.JPOS_E_CLAIMED, "Device already claimed!");
        deviceClaimed = true;
        log(deviceName + " >> " + "Device has been claimed!");
    }

    public void release() throws JposException {
        if (!deviceClaimed) error(JposConst.JPOS_E_NOTCLAIMED, "Device must be claimed before release!");
    	deviceClaimed = false;
        deviceEnabled = false;
        log(deviceName + " >> " + "Device has been released!");
    }

    public void close() throws JposException {
        
    	deviceOpen = false;
    	deviceClaimed = false;
        deviceEnabled = false;
        deviceClaimed = false;
        freezeEvents = false;
        dataEventsEnabled = false;
        ecb = null;
        state = JposConst.JPOS_S_CLOSED;
        healthText = "Closed";
        if (protocol!=null && protocol.isBusy())
        	protocol.cancelCommand();
        if (protocol!=null)
        	protocol.removeDataListener(this);
        if (port!=null && port.isOpen()){
        	port.removeDataListener(this);
        	if (port.getDataListenerCount()==0)
        	{
        		if (port.isBusy()) port.cancelWrite();
        		port.close();
        	}
        }
        eventQueue = null;
        log(deviceName + " >> " + "Device has been closed!");
    }

    // field methods
    public String getCheckHealthText() throws JposException {
        //if (!deviceOpen) error(JposConst.JPOS_E_ILLEGAL,"BaseService.getCheckHealthText", "device must be opened before HealthCheck!");
    	return this.healthText;
    }

    public boolean getClaimed() throws JposException {
        return deviceClaimed;
    }

    public int getDataCount() throws JposException {
        int count = 0;
        for(Object o : eventQueue)
        	if (o instanceof DataEvent) count++;
    	return count;
    }

    public boolean getDataEventEnabled() throws JposException {
        return this.dataEventsEnabled;
    }

    public void setDataEventEnabled(boolean b) throws JposException {
        if (!deviceOpen)
            error(JposConst.JPOS_E_CLOSED, "Device must me opened first!");
    	boolean fireEvents = false;
        if (!this.dataEventsEnabled && b) {
            fireEvents = true;
        }
        this.dataEventsEnabled = b;
        //log(deviceName + " >> " + "Data_Event = "+Boolean.toString(b));

        if (fireEvents) {
            this.fireQueuedEvents();
        }
    }

    public boolean getDeviceEnabled() throws JposException {
        return this.deviceEnabled;
    }

    public void setDeviceEnabled(boolean b) throws JposException {
        if (!deviceClaimed)
            error(JposConst.JPOS_E_NOTCLAIMED, "Device must me opened and claimed first!");
        this.deviceEnabled = b;
        //log(deviceName + " >> " + "Device_Enabled = "+Boolean.toString(b));
    }

    public String getDeviceServiceDescription() throws JposException {        
    	return serviceDesc;
    }

    public int getDeviceServiceVersion() throws JposException {
        return this.serviceVer;
    }

    public boolean getFreezeEvents() throws JposException {
        return this.freezeEvents;
    }

    public void setFreezeEvents(boolean b) throws JposException {
        if (!deviceOpen) error(JposConst.JPOS_E_CLOSED, "Device must me opened first!");
    	this.freezeEvents = b;
    	if (b) fireQueuedEvents();
        //log(deviceName + " >> " + "Freeze_Events = "+Boolean.toString(b));
   }

    public String getPhysicalDeviceDescription() throws JposException {
        return this.physicalDesc;
    }

    public String getPhysicalDeviceName() throws JposException {
        return this.physicalName;
    }

    public int getState() throws JposException {
        return this.state;
    }

    public void checkHealth(int i) throws JposException {
        // This method is not used since there is no physical device to check
        logWarning(deviceName + " >> " + "WARNING: This is not implemented!");
    }

    public void directIO(int i, int[] ints, Object o) throws JposException {
        error(JposConst.JPOS_E_ILLEGAL,"This feature is not implemented!");
    }

    public void setEntry(JposEntry entry) {
        this.entry = entry;
        deviceName = entry.getLogicalName();
    }

    // JposServiceInstance
    public void deleteInstance() throws JposException {
        logWarning(deviceName + " >> " + "WARNING: This is not implemented!");
    }
    
    public void setPort(PortInterface portInstance)
    {
    	port = portInstance;
    	port.addDataListener(this);
        //log(deviceName + " >> " + "Port " + port.getPortName() + " is assigned to " + deviceName + ".");
   }
    
    public PortInterface getPort()
    {
    	return port;
    }
    public void setProtocol(ProtocolInterface protocolInstance)
    {
    	if (protocolInstance!=null)
    	{
    		port.removeDataListener(this);
    		protocol = protocolInstance;
    		protocol.addDataListener(this);
            log(deviceName + " >> " + "Protocol " + protocol.getProtocolName()+ " is assigned to " + deviceName + ".");
    	}
    	else protocol = null;
   }
    
    public ProtocolInterface getProtocol()
    {
    	return protocol;
    }

    protected void fireEvent(Object ev) {
        if (this.freezeEvents==false && this.ecb != null) {
            if (ev instanceof DataEvent) {
            	if (this.dataEventsEnabled)
            	{
            		dataEventsEnabled = false;
                    //log(deviceName + " >> " + "Firing Data Event!");
            		this.ecb.fireDataEvent((DataEvent) ev);
            	}
            	else this.eventQueue.add(ev);            	
            } else if (ev instanceof DirectIOEvent) {
                this.ecb.fireDirectIOEvent((DirectIOEvent) ev);
            } else if (ev instanceof ErrorEvent) {
                this.ecb.fireErrorEvent((ErrorEvent) ev);
            } else if (ev instanceof OutputCompleteEvent) {
                this.ecb.fireOutputCompleteEvent((OutputCompleteEvent) ev);
            } else if (ev instanceof StatusUpdateEvent) {
                this.ecb.fireStatusUpdateEvent((StatusUpdateEvent) ev);
            }
        } else {
            this.eventQueue.add(ev);
        }
    }
    
    protected void clearDataEvents()
    {
        List queuedList = new ArrayList(eventQueue);
        eventQueue = new ArrayList();

        for(Object ev: eventQueue)
        	if (ev instanceof DataEvent) {}
        	else if (ev instanceof ErrorEvent)
        	{
        		if (!(((ErrorEvent)ev).getErrorCode()==JposConst.JPOS_EL_INPUT
                        || ((ErrorEvent)ev).getErrorCode()==JposConst.JPOS_EL_INPUT_DATA))
        			eventQueue.add(ev); 
        	}
        	else eventQueue.add(ev);    	
    }

    private void fireQueuedEvents() {
        List queuedList = new ArrayList(eventQueue);
        this.eventQueue = new ArrayList();
        Iterator i = queuedList.iterator();

        while (i.hasNext()) {
            Object obj = i.next();
            //i.remove();
            this.fireEvent(obj);
        }
    }
    
    protected void error(int errType, String errMessage) throws JposException
    {
        logError(deviceName + " >> " + "error: " + errMessage);
    	throw new JposException(errType,errMessage);
    }
    protected void error(int errType, String errMessage, Exception e) throws JposException
    {
        log(e);
        logError(deviceName + " >> " + errMessage);
    	throw new JposException(errType,e.getMessage(),e);
    }
    protected void error_ext(int errType, String errMessage) throws JposException
    {
        logError(deviceName + " >> " + "error: " + errMessage);
    	throw new JposException(JposConst.JPOS_E_EXTENDED,errType,errMessage);
    }
    protected void error_ext(int errType, String errMessage, Exception e) throws JposException
    {
        log(e);
        logError(deviceName + " >> " + errMessage);
    	throw new JposException(JposConst.JPOS_E_EXTENDED,errType,e.getMessage(),e);
    }
    
    
}
