package cnf.jpos.services;

import apu.jpos.moneris.*;
import apu.jpos.util.StringUtil;
import cnf.communication.ports.PortDataEvent;
import cnf.jpos.dialogs.CATHealthWindow;
import cnf.jpos.dialogs.CATTransactionProgressWindow;
import cnf.jpos.factory.CnfJposServiceFactory;
import jpos.CATConst;
import jpos.JposConst;
import jpos.JposException;
import jpos.events.ErrorEvent;
import jpos.events.OutputCompleteEvent;
import jpos.services.CATService111;
import jpos.services.EventCallbacks;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;


public class AutoApproveCATService extends BaseService implements CATService111 {

    protected String additionalSecurityInformation;
    protected String accountNumber;
    protected String approvalCode;
    protected int paymentCondition;
    protected int paymentMedia;
    protected String paymentDetail;
    //protected int sequenceNumber;
    protected String slipNumber;
    protected String transactionNumber;
    //protected String transactionType;
    protected String cardCompanyID;
    protected String centerResultCode;
    protected String cardExpiryDate;
    protected long totalAmount;
    //protected String declineCode;
    protected String transactionDate;
    //protected String transactionTime;
    protected long cardBalance;
    protected String dailyLog;

    protected boolean asyncMode;
    protected boolean busy;
    protected boolean safEnabled;
    protected DateTime nextSafExecute = new DateTime().plusMillis(10000);

    protected String txnFileName;
    protected MonerisTransactionResponse response;

    private boolean log_requested;
    private int log_type;
    private MonerisTransactionType tx_type = null;
    private int tx_sqn=-1;
    private BigDecimal tx_amt;
    private String tx_auth;
    //private BigDecimal tx_tax;
    private BigDecimal safDollarLimit = null;
    private Thread operationThread = null;

    protected CATHealthWindow hwin = null;

    protected String merchantId = null;
    protected String ecrId = null;
    protected Map<Integer,MonerisTransactionResponse> responseHistory
            = new HashMap<Integer,MonerisTransactionResponse>();

    protected void reset()
    {
        this.additionalSecurityInformation = "";
        this.accountNumber = "";
        this.approvalCode = "";
        this.cardCompanyID = "";
        this.cardExpiryDate = "";
        this.centerResultCode = "";
        this.dataEventsEnabled = false;
        this.deviceClaimed = false;
        this.deviceEnabled = false;
        this.deviceOpen = false;
        this.freezeEvents = false;
        this.paymentCondition = CATConst.CAT_PAYMENT_LUMP;
        this.paymentDetail = "";
        this.paymentMedia = CATConst.CAT_MEDIA_UNSPECIFIED;
        this.slipNumber = "";
        this.totalAmount = 0;
        this.transactionNumber = "";
        this.transactionDate = "";
        this.cardBalance = 0;
        this.dailyLog="";

        asyncMode = false;
        busy=false;
        safEnabled=false;

        response=null;
        hwin=null;
        this.serviceVer = 1011000;

    }


    /// Main Functions

    //@Override
    public void open(String deviceName, EventCallbacks ecb) throws JposException {
        reset();
        super.open(deviceName, ecb);
    }

    //@Override
    public void close() throws JposException
    {
        asyncMode = false;
        busy=false;
        super.close();
    }










    /// Device Capabilities

    //@Override
    public boolean getCapCashDeposit() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapCompareFirmwareVersion() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapLockTerminal() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapLogStatus() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapUnlockTerminal() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapUpdateFirmware() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapStatisticsReporting() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapUpdateStatistics() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapAdditionalSecurityInformation() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapAuthorizeCompletion() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapAuthorizePreSales() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapAuthorizeRefund() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapAuthorizeVoid() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapAuthorizeVoidPreSales() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapCenterResultCode() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapCheckCard() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public int getCapDailyLog() throws JposException {
        check(true,false,false);
        return CATConst.CAT_DL_REPORTING_SETTLEMENT;
    }

    //@Override
    public boolean getCapInstallments() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public boolean getCapPaymentDetail() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public int getCapPowerReporting() throws JposException {
        check(true,false,false);
        return JposConst.JPOS_PR_NONE;
    }

    //@Override
    public boolean getCapTaxOthers() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapTrainingMode() throws JposException {
        check(true,false,false);
        return true;
    }

    //@Override
    public boolean getCapTransactionNumber() throws JposException {
        check(true,false,false);
        return true;
    }












    /// PROPERTIES

    //@Override
    public int getLogStatus() throws JposException {
        return CATConst.CAT_LOGSTATUS_OK;
    }

    //@Override
    public long getSettledAmount() throws JposException {
        return totalAmount;
    }

    //@Override
    public void lockTerminal() throws JposException {
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Lock Terminal!");
    }

    //@Override
    public void unlockTerminal() throws JposException {
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Unlock Terminal!");
    }

    //@Override
    public void updateFirmware(String arg0) throws JposException {
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Update Firmware!");
    }

    //@Override
    public void resetStatistics(String arg0) throws JposException {
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
    }

    //@Override
    public void retrieveStatistics(String[] arg0) throws JposException {
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
    }

    //@Override
    public void updateStatistics(String arg0) throws JposException {
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Statistics!");
    }

    //@Override
    public int getPaymentMedia() throws JposException {
        check(true,false,false);
        return paymentMedia;
    }

    //@Override
    public void setPaymentMedia(int arg0) throws JposException {
        check(true,false,false);
        paymentMedia = arg0;
    }

    //@Override
    public String getAccountNumber() throws JposException {
        check(true,false,false);
        return accountNumber;
    }

    //@Override
    public String getAdditionalSecurityInformation() throws JposException {
        check(true,false,false);
        return additionalSecurityInformation;
    }

    //@Override
    public String getApprovalCode() throws JposException {
        check(true,false,false);
        return approvalCode;
    }

    //@Override
    public boolean getAsyncMode() throws JposException {
        check(true,false,false);
        return asyncMode;
    }


    //@Override
    public String getCardCompanyID() throws JposException {
        check(true,false,false);
        return cardCompanyID;
    }

    //@Override
    public String getCenterResultCode() throws JposException {
        check(true,false,false);
        return centerResultCode;
    }

    //@Override
    public String getDailyLog() throws JposException {
        return dailyLog;
    }

    //@Override
    public int getPaymentCondition() throws JposException {
        check(true,false,false);
        return paymentCondition;
    }

    //@Override
    public String getPaymentDetail() throws JposException {
        check(true,false,false);
        return paymentDetail;
    }

    //@Override
    public int getPowerNotify() throws JposException {
        check(true,false,false);
        return JposConst.JPOS_PN_DISABLED;
    }

    //@Override
    public int getPowerState() throws JposException {
        check(true,false,false);
        return JposConst.JPOS_PS_UNKNOWN;
    }

    //@Override
    public int getSequenceNumber() throws JposException {
        check(true,false,false);
        return tx_sqn;
    }

    //@Override
    public long getBalance() throws JposException {
        return cardBalance;
    }

    //@Override
    public String getSlipNumber() throws JposException {
        check(true,false,false);
        return slipNumber;
    }

    //@Override
    public boolean getTrainingMode() throws JposException {
        check(true,false,false);
        return false;
    }

    //@Override
    public String getTransactionNumber() throws JposException {
        check(true,false,false);
        return transactionNumber;
    }

    //@Override
    public String getTransactionType() throws JposException {
        check(true,false,false);
        return tx_type.toString();
    }

    //@Override
    public void setAdditionalSecurityInformation(String arg0)
            throws JposException {
        check(true,false,false);
        additionalSecurityInformation = arg0;
    }

    //@Override
    public void setAsyncMode(boolean arg0) throws JposException {
        check(true,false,false);
        asyncMode=arg0;
        log(deviceName + " >> " + "Async_Mode = " + Boolean.toString(arg0));

    }

    //@Override
    public void setPowerNotify(int arg0) throws JposException {
        check(true,false,false);
        if (arg0==JposConst.JPOS_PN_ENABLED)
            error(JposConst.JPOS_E_ILLEGAL,"Power Notify is not implemented by the driver!");
    }

    //@Override
    public void setTrainingMode(boolean arg0) throws JposException {
        check(true,false,false);
        if (arg0==true)
            error(JposConst.JPOS_E_ILLEGAL,"Training Mode is not implemented by the driver!");
    }



    /// METHODS....

    //@Override
    public void cashDeposit(int arg0, long arg1, int arg2) throws JposException {
        error(JposConst.JPOS_E_ILLEGAL, "This driver does not support Cash Deposit!");
    }

    //@Override
    public void compareFirmwareVersion(String arg0, int[] arg1)
            throws JposException {
        error(JposConst.JPOS_E_ILLEGAL, "This driver does not support Compare Firmware Version!");
    }


    //@Override
    public void accessDailyLog(int seqNum, int type, int timeout)
            throws JposException {

        check(true,true,true);
        if (tx_sqn==-1 || seqNum==tx_sqn+1) tx_sqn = seqNum;
        else error(JposConst.JPOS_E_ILLEGAL,"Settlement Request is out of Sequence! Pos requested "
                + Integer.toString(seqNum)+", but the last request processed was "+Integer.toString(tx_sqn));
        try
        {
            log_requested=true;
            log_type = type;
            executeOperation();
        }
        catch(JposException e)
        {
            log(e);
            throw e;
        }
        catch(Exception e)
        {
            log(e);
            error(JposConst.JPOS_E_FAILURE,"Driver Error! " + e.getMessage());
        }
    }




    private int elapsed(DateTime start)
    {
        if (start==null) return -1;
        return (int)(new Duration(start,new DateTime()).getMillis());
    }




    //@Override
    public void authorizeCompletion(int sequenceNumber, long amount, long taxOthers, int timeout)
            throws JposException {
        check(true,true,true);
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Authorize Completion Method!");
    }

    //@Override
    public void authorizePreSales(int sequenceNumber, long amount, long taxOthers, int timeout)
            throws JposException {
        check(true,true,true);
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Authorize Presales Method!");
    }

    //@Override
    public void authorizeRefund(int sequenceNumber, long amount, long taxOthers, int timeout)
            throws JposException {
        check(true,true,true);
        doTransaction(MonerisTransactionType.Refund,sequenceNumber,amount+taxOthers);
    }

    //@Override
    public void authorizeSales(int sequenceNumber, long amount, long taxOthers, int timeout)
            throws JposException {
        check(true,true,true);
        doTransaction(MonerisTransactionType.Purchase,sequenceNumber,amount+taxOthers);
    }

    //@Override
    public void authorizeVoid(int sequenceNumber, long amount, long taxOthers, int timeout)
            throws JposException {
        check(true,true,true);
        if (responseHistory.containsKey(sequenceNumber))
        {
            MonerisTransactionResponse tx = responseHistory.get(sequenceNumber);
            if (tx.getAmount().movePointRight(2).longValue()!=amount+taxOthers)
                error(JposConst.JPOS_E_FAILURE,"Transaction void failed because amount did not match original!");
            else
            {
                if (tx.getTransactionType()==MonerisTransactionType.Purchase)
                    doTransaction(MonerisTransactionType.PurchaseVoid,sequenceNumber,amount+taxOthers);
                else if (tx.getTransactionType()==MonerisTransactionType.Refund)
                    doTransaction(MonerisTransactionType.RefundVoid,sequenceNumber,amount+taxOthers);
                else
                    error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because sequence reference is not valid!");
            }
        }
        else
            error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because sequence reference is not valid!");

    }

    //@Override
    public void authorizeVoidPreSales(int sequenceNumber, long amount, long taxOthers, int timeout)
            throws JposException {
        check(true,true,true);
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Authorize Void Presales Method!");
    }

    //@Override
    public void checkCard(int sequenceNumber, int timeout) throws JposException {
        check(true,true,true);
        error(JposConst.JPOS_E_ILLEGAL,"This device does not support Check Card Method!");
    }



    protected void doTransaction(MonerisTransactionType txType, int sqn, long amount) throws JposException
    {
        if (txType==null) txType = MonerisTransactionType.Purchase;
        if (txType==MonerisTransactionType.VoidLast)
        {
            if (sqn!=tx_sqn || amount!=totalAmount)
                error(JposConst.JPOS_E_ILLEGAL
                        ,"Please call voidLast with last transaction amount and sequence number!");
        }
        else if (txType==MonerisTransactionType.PurchaseVoid || txType==MonerisTransactionType.RefundVoid)
        {
            if (responseHistory.containsKey(sqn))
            {
                MonerisTransactionResponse tx = responseHistory.get(sqn);
                if (!tx.isApproved())
                    error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because refererred transaction is illegal!");
                if (txType==MonerisTransactionType.PurchaseVoid
                        && tx.getTransactionType()!=MonerisTransactionType.Purchase)
                    error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because refererred transaction type mismatch!");
                else if (txType==MonerisTransactionType.RefundVoid
                        && tx.getTransactionType()!=MonerisTransactionType.Refund)
                    error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because refererred transaction type mismatch!");
                else
                {
                    tx_sqn=sqn;
                    tx_auth = tx.getApprovalCode();
                }

            }
            else error(JposConst.JPOS_E_FAILURE,"Transaction Void Failed because sequence reference is not valid!");
        }
        else
        {
            if (tx_sqn==-1 || sqn==tx_sqn+1) tx_sqn = sqn;
            else error(JposConst.JPOS_E_ILLEGAL,"Transactions are out of Sequence! Pos requested "
                    + Integer.toString(sqn)+", but the last transaction processed was "+Integer.toString(tx_sqn));
        }
        try
        {
            BigDecimal amt = new BigDecimal(amount).movePointLeft(2);
            if (amt.compareTo(BigDecimal.ZERO)==-1) amt = amt.negate();
            response=null;
            tx_amt = amt;
            tx_type = txType;
            log_requested=false;
            executeOperation();
        }
        catch(JposException e)
        {
            throw e;
        }
        catch(Exception e)
        {
            error(JposConst.JPOS_E_FAILURE,"Driver Error! " + e.getMessage());
        }
    }

    protected void executeOperation() throws JposException
    {
        long logTotalCount=0;
        long logTotalValue=0;
        BigDecimal logTotalValueDec = null;
        MonerisTransactionResponse lastResponse = null;
        if (log_requested)
        {
            StringBuilder sb = null;
            boolean settle = (log_type==CATConst.CAT_DL_SETTLEMENT);
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "The settlement feature is not implemented for this dummy payment driver!");
        }
        else
        {
            if (tx_type==null || tx_sqn<0 || tx_amt==null){
                if (hwin!=null && hwin.isVisible()==true) hwin.updateError("Invalid!"
                        , "Invalid transaction arguments received!");
                error(JposConst.JPOS_E_ILLEGAL, "Invalid transaction arguments received!");
            }

            try {
                buildAdditionalInfo();
                boolean cashBack = false;
                if (additionalInfo.containsKey("CASHBACK"))
                {
                    String c = additionalInfo.get("CASHBACK");
                    if (c!=null) c=c.toLowerCase();
                    if (c!=null && c.contains("t") || c.contains("y")) cashBack=true;
                }
                assignFields();

                if (tx_type==MonerisTransactionType.Purchase
                        || tx_type==MonerisTransactionType.Refund)
                    responseHistory.put(tx_sqn,response);
                else if (response.getTransactionType()==MonerisTransactionType.PurchaseVoid
                        || response.getTransactionType()==MonerisTransactionType.RefundVoid
                        || response.getTransactionType()==MonerisTransactionType.VoidLast)
                {
                    responseHistory.remove(tx_sqn);
                    tx_sqn=-1;
                }
            }
            catch (Exception ex)
            {
                String message = ex.getMessage();
                if(message==null) message = "Null Reference Exception!";
                if (hwin!=null && hwin.isVisible()==true)
                    hwin.updateError("Transaction Failed!", message);
                error(JposConst.JPOS_E_FAILURE, "Transaction Failed!", ex);
            }
        }
    }




    //@Override
    public void clearOutput() throws JposException {

        check(true,true,false);
    }



    //@Override
    public void checkHealth(int typ) throws JposException {
        boolean async = asyncMode;
        this.setAsyncMode(true);
        if (typ==JposConst.JPOS_CH_INTERNAL) healthText = "Successful";
        else if (typ==JposConst.JPOS_CH_EXTERNAL) healthText = "Successful";
        else if (typ==JposConst.JPOS_CH_INTERACTIVE)
        {
            healthText = "Checking";
            if (hwin==null) hwin = new CATHealthWindow(this);
            hwin.clear();
            hwin.setModal(false);
            hwin.setVisible(true);
            while (hwin.isVisible())
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
        else
        {
            this.setAsyncMode(async);
            error(JposConst.JPOS_E_ILLEGAL,"Invalid checkHealth Type invoked!");
        }
        this.setAsyncMode(async);
    }

    protected void check(boolean open, boolean claim, boolean enable) throws JposException
    {
        try {
            if (enable==true && this.deviceEnabled==false)
                error(JposConst.JPOS_E_DISABLED, "Device must be opened, claimed and enabled first!");
            else if (claim==true && this.deviceClaimed==false)
                error(JposConst.JPOS_E_NOTCLAIMED, "Device must be opened and claimed first!");
            else if (open==true && this.deviceOpen==false)
                error(JposConst.JPOS_E_CLOSED, "Device must be opened first!");
        } catch (JposException e) {
            tx_sqn=-1;
            throw e;
        }
    }


    private int create_int(String propName, int min, int max) throws JposException
    {
        int t = 0;
        try
        {
            t = getPropertyValueInteger(propName);
        }
        catch(JposException e)
        {
            error(JposConst.JPOS_E_NOSERVICE,"You must assign a valid integer for " + propName);
        }
        if (t<min || t>max)
            error(JposConst.JPOS_E_NOSERVICE, "Property " + propName + "=" + t
                    +" is out of range! Valid range is "
                    + Integer.toString(min) + ".." + Integer.toString(max));

        return t;

    }


    public static long slipCounter = 1000000000;
    protected void assignFields(){
        DateTime now = new DateTime();
        cardCompanyID="451162";
        accountNumber="451162****4408";
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyyMMddHHmmss");
        transactionDate = dtf.print(now);
        transactionNumber=dtf.print(now);
        paymentCondition = CATConst.CAT_PAYMENT_LUMP;
        slipCounter++;
        slipNumber = Long.toString(slipCounter);
        approvalCode = "DUMMY123OK";
        totalAmount=tx_amt.movePointRight(2).longValue();
        centerResultCode = "ACK123";

        DateTimeFormatter dtf2 = DateTimeFormat.forPattern("dd MMM yyyy HH.mm.ss");
        String tDateString = dtf2.print(now);
        String langCode="E";

        //T662491590010710750,I20,CINTERAC,AC372641,R00,S18,ASI
        // EXP:4912|VER:Pin|SWI:C|TXD:13 Mar 2018 15.26.36|LAN:F|CND:99|ACT:1|CSB:35.00|RSP:00|B24:001|AID:A0000002771010|ANM:Interac|TSI:6800|TVR:8000008000|SAF:0
        additionalSecurityInformation = "EXP:0422"
                +"|VER:PIN"
                +"|SWI:C"
                +"|TXD:"+tDateString
                +"|LAN:"+langCode
                +"|CND:99";
        additionalSecurityInformation+="|ACT:2";
        additionalSecurityInformation+="|RSP:00";
        additionalSecurityInformation+="|B24:001";
        additionalSecurityInformation+="|AID:A0000002771010";
        additionalSecurityInformation+="|ANM:VISA";
        additionalSecurityInformation+="|TSI:6800";
        additionalSecurityInformation+="|TVR:8000008000";
        additionalSecurityInformation+="|SAF:0";
        if (asyncMode)
        {
            if (hwin!=null && hwin.isVisible()==true) hwin.updateResult();
            OutputCompleteEvent ocev = new OutputCompleteEvent(this.ecb.getEventSource(),0);
            this.fireEvent(ocev);
        }
    }



    private Map<String,String> additionalInfo = new HashMap<String, String>();
    private void buildAdditionalInfo()
    {
        additionalInfo.clear();
        if (additionalSecurityInformation!=null) {
            String[] infoElements = additionalSecurityInformation.split("\\|");
            for (String element : infoElements) {
                String[] pair = element.split("\\:");
                if (pair[0].startsWith("ASI")) pair[0] = pair[0].substring(3);
                if (pair.length == 1) additionalInfo.put(pair[0], null);
                else additionalInfo.put(pair[0], pair[1]);
            }
        }
    }



    public void onPortData(PortDataEvent evt) {
        // Nothing to do with this!
    }
}
