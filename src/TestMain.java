/*
 * @(#)SimpleRead.java	1.12 98/06/25 SMI
 * 
 * Copyright 2003 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 * Sun grants you ("Licensee") a non-exclusive, royalty free, license
 * to use, modify and redistribute this software in source and binary
 * code form, provided that i) this copyright notice and license appear
 * on all copies of the software; and ii) Licensee does not utilize the
 * software in a manner which is disparaging to Sun.
 * 
 * This software is provided "AS IS," without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND
 * ITS LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY
 * LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THE
 * SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS
 * BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING
 * OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * This software is not designed or intended for use in on-line control
 * of aircraft, air traffic, aircraft navigation or aircraft
 * communications; or in the design, construction, operation or
 * maintenance of any nuclear facility. Licensee represents and
 * warrants that it will not use or redistribute the Software for such
 * purposes.
 */
import java.io.*;
import java.util.*;
import javax.comm.*;

import cnf.communication.ports.*;
import cnf.communication.ports.PortDataEvent.PortDataEventType;
import apu.jpos.util.*;


import jpos.*;
import jpos.events.*;


import jpos.config.simple.*;
import jpos.config.simple.xml.*;
import jpos.config.*;

/**
 * Class declaration
 *
 *
 * @author
 * @version 1.8, 08/03/00
 */
public class TestMain extends Thread implements PortDataListener, DataListener, OutputCompleteListener, ErrorListener{
    static CommPortIdentifier portId;
    static Enumeration	      portList;
    InputStream		      inputStream;
    SerialPort		      serialPort;
    Thread		      readThread;


    public static void printUsage()
    {
    	System.out.println();
    	System.out.println();
		System.out.println("USAGE: java -jar GenericScannerJpos.jar [-s SCANNER_NAME] [-w SCALE_NAME] [-c CAT_NAME]");
    	System.out.println();
    	System.out.println();
    
    }

    
    
    public static void main(String[] args) {

    /*
    	try
    	{
    		GenericSerialPort port = new GenericSerialPort();
    		port.initialize("COM3", 9600, SerialPort.DATABITS_7, SerialPort.PARITY_EVEN, SerialPort.STOPBITS_1, SerialPort.FLOWCONTROL_NONE);
    		port.open();
    		StringBuilder cmd = new StringBuilder();
    		cmd.append(StringUtil.asciiParse("[02]10001^0^[03]"));
    		//cmd.append(StringUtil.asciiParse("[02]10001^1^10002^1^10007^456^10022^65613^[03]"));
    		StringUtil.AppendLRC(cmd);
    		port.write(cmd.toString());
    		//Thread.sleep(500);
    		//port.write(cmd.toString());
    		Thread.sleep(100000);
    	}
    	catch(Exception e)
    	{
    		System.out.println("Error: " + e.getMessage());
    	}
*/
    //int x = 0;    
    //if (x>0)
    //{
  	SimpleXmlRegPopulator sp = new SimpleXmlRegPopulator();
    System.out.println("Enumerating devices from " + sp.getEntriesURL()+"/jpos.xml");
    SimpleEntryRegistry reg = new SimpleEntryRegistry(new SimpleXmlRegPopulator());
	reg.load();
	Enumeration entriesEnum = reg.getEntries();
	while(entriesEnum.hasMoreElements()){
		JposEntry entry = (JposEntry)entriesEnum.nextElement();

		System.out.println("Device: " + entry.getLogicalName() + " [" + entry.getProp(JposEntry.DEVICE_CATEGORY_PROP_NAME).getValueAsString()+ "]");
		
	}
    
	String scannerName = null;
	String scaleName = null;
	String catName = null;
    String printerName = null;
	boolean valid = false;
	
	if (args.length>1 && args.length%2==0)
	{
		for (int i=0;i<args.length;i+=2)
		{
			args[i]=args[i].toLowerCase();
			if (args[i].contains("s")) scannerName = args[i+1];
			else if (args[i].contains("w")) scaleName = args[i+1];
			else if (args[i].contains("c")) catName = args[i+1];
            else if (args[i].contains("p")) printerName = args[i+1];
		}
		
	}
	
	if (scannerName!=null || scaleName!=null || catName!=null || printerName!=null) valid = true;
    
	if (valid)
	{
		TestMain mainclass = new TestMain(scannerName, scaleName, catName, printerName);
		mainclass.start();    
	}
	else printUsage();
 
    //}
    } 
    
    public GenericSerialPort comport;
    
    public TestMain(String scannerName, String scaleName, String catName, String printerName)
    {
    	
    	initDevices(scannerName, scaleName, catName, printerName);
    
    }

 
    /**
     * Method declaration
     *
     *
     * @see
     */
    public void run() {
    	if (scannerActive)
    	try{
    		SimpleLogger.getDefaultLogger().logImmediately("ScannerTestMain.run","info","Checking Scanner Health...");
        	myScannerControl.checkHealth(JposConst.JPOS_CH_INTERACTIVE);
        	} catch(Exception ex){}
       	
        if (scaleActive)
        try{
       		SimpleLogger.getDefaultLogger().logImmediately("ScannerTestMain.run","info","Checking Scale Health...");
           	myScaleControl.checkHealth(JposConst.JPOS_CH_INTERACTIVE);
         	} catch(Exception ex){}
        if (catActive)
        {
       		SimpleLogger.getDefaultLogger().logImmediately("ScannerTestMain.run","info","Checking CAT Health...");
        	try{myCATControl.checkHealth(JposConst.JPOS_CH_INTERACTIVE);}catch(Exception ex){}
        }
        if (printerActive)
        {
            SimpleLogger.getDefaultLogger().log("ScannerTestMain.run","info","Checking Printer Health...");
            try{myPrinterControl.checkHealth(JposConst.JPOS_CH_INTERACTIVE);}catch(Exception ex){}
        }
        if (scannerActive || scaleActive)
        {
	        SimpleLogger.getDefaultLogger().logImmediately("ScannerTestMain.run","info","Starting the test loop...");
	       	for (int i=0;i<20;i++)
	    	{
		    	try {
		    		Thread.sleep(5000);
		    		
		    		if (scaleActive)
		    		try
		    		{
			    		int[] ret = new int[1];
		    			System.out.println("Reading Weight...");
		    			myScaleControl.readWeight(ret, 6000);
		    			//System.out.println("Weight = " + Integer.toString(ret[0]));
		    		}
		    		catch(Exception ex)
		    		{
		    			System.out.println(ex.toString());
		    		}
			    }
		    	catch (InterruptedException ex) 
		    	{	
		    		System.out.println(ex.toString());
		    	}
		    	//catch(PortIOException ex)
		    	//{
		    	//	System.out.println(ex.toString());	    		
		    	//}
	    	}
	    	//comport.close();
	    	try{
	    		if (scannerActive) myScannerControl.close();
	    		if (scaleActive) myScaleControl.close();
	    	}catch(Exception ex){}
        }
    	SimpleLogger.getDefaultLogger().close();
    	System.exit(0);
    	
    }

	//@Override
	public void onPortData(PortDataEvent evt) {
		if (evt.getEventType()==PortDataEventType.DATA_RECEIVED)
			System.out.print(StringUtil.asciiFilter(evt.getValue()));
		else if (evt.getEventType()==PortDataEventType.WRITE_COMPLETE)
		{
			System.out.println("Done Writing!!!");
		}
	}

	//@Override
	public void dataOccurred(DataEvent e) {
				
		try
		{
			if (e.getSource() instanceof jpos.services.ScaleService13)
			{
				System.out.println("Scale Data Event(Status=" + e.getStatus() + ")");
				myScaleControl.setDataEventEnabled(true);				
			}
			else
			{
				System.out.println("Scanner Data Event(Status=" + e.getStatus() + ", Data=" + new String(myScannerControl.getScanDataLabel()) + ")");
				myScannerControl.setDataEventEnabled(true);
			}
		}
		catch(Exception ex){}
	} 
	
	jpos.Scanner myScannerControl = null;
	jpos.Scale myScaleControl = null;
	jpos.CAT myCATControl = null;
    POSPrinter myPrinterControl = null;

	boolean scannerActive = false;
	boolean scaleActive = false;
	boolean catActive = false;
	boolean printerActive = false;

	public void initDevices(String scannerName, String scaleName, String catName, String printerName)
	{
		if (scannerName!=null)
		{
			try{
				myScannerControl = new jpos.Scanner();
				myScannerControl.addDataListener(this);
				myScannerControl.open(scannerName);
				myScannerControl.claim(1000);
				myScannerControl.setDeviceEnabled(true);
				myScannerControl.setDataEventEnabled(true);
				myScannerControl.setDecodeData(true);
				scannerActive=true;
			}
			catch(JposException e)
			{
				SimpleLogger.getDefaultLogger().logImmediately("ScannerTestMain.initScanner", "error", "Failed to initialize scanner. Reason: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		if (scaleName!=null)
		{
			try{
				myScaleControl = new jpos.Scale();
				myScaleControl.addDataListener(this);
				myScaleControl.open(scaleName);
				myScaleControl.claim(1000);
				myScaleControl.setDeviceEnabled(true);
				myScaleControl.setDataEventEnabled(true);
				myScaleControl.setAsyncMode(true);
				scaleActive=true;
			}
			catch(JposException e)
			{
				SimpleLogger.getDefaultLogger().logImmediately("ScannerTestMain.initScale", "error", "Failed to initialize scale. Reason: " + e.getMessage());
				e.printStackTrace();
			}
		}

		if (catName!=null)
		{
			try{
				myCATControl = new jpos.CAT();
				myCATControl.addErrorListener(this);
				myCATControl.addOutputCompleteListener(this);
				myCATControl.open(catName);
				myCATControl.claim(1000);
				myCATControl.setDeviceEnabled(true);
				myCATControl.setAsyncMode(true);
				catActive=true;
			}
			catch(JposException e)
			{
				SimpleLogger.getDefaultLogger().logImmediately("ScannerTestMain.initScale", "error", "Failed to initialize CAT. Reason: " + e.getMessage());
				e.printStackTrace();
			}
		}

        if (printerName!=null) {
            try {
                myPrinterControl = new jpos.POSPrinter();
                myPrinterControl.addErrorListener(this);
                myPrinterControl.addOutputCompleteListener(this);
                myPrinterControl.open(printerName);
                myPrinterControl.claim(1000);
                myPrinterControl.setDeviceEnabled(true);
                myPrinterControl.setAsyncMode(true);
                printerActive = true;
            } catch (JposException e) {
                SimpleLogger.getDefaultLogger().logImmediately("ScannerTestMain.initScale", "error", "Failed to initialize CAT. Reason: " + e.getMessage());
                e.printStackTrace();
            }
        }
	}



	//@Override
	public void outputCompleteOccurred(OutputCompleteEvent arg0) {
		try{

			SimpleLogger log = SimpleLogger.getDefaultLogger();
			log.logImmediately("main", "TX", "Transaction Approved! ");
			log.logImmediately("main", "TX", "   Transaction Number: " + myCATControl.getTransactionNumber());
			log.logImmediately("main", "TX", "     Transaction Type: " + myCATControl.getTransactionType());
			log.logImmediately("main", "TX", "       Invoice Number: " + myCATControl.getSlipNumber());
			log.logImmediately("main", "TX", "       Account Number: " + myCATControl.getAccountNumber());
			log.logImmediately("main", "TX", "      Card Company ID: " + myCATControl.getCardCompanyID());
			log.logImmediately("main", "TX", "        Approval Code: " + myCATControl.getApprovalCode());
			log.logImmediately("main", "TX", "          Result Code: " + myCATControl.getCenterResultCode());
			log.logImmediately("main", "TX", "      Sequence Number: " + myCATControl.getSequenceNumber());
			log.logImmediately("main", "TX", "                 Info: " + myCATControl.getAdditionalSecurityInformation());
		}catch(Exception ex){}
		
	}



	//@Override
	public void errorOccurred(ErrorEvent e) {
		try{
			SimpleLogger log = SimpleLogger.getDefaultLogger();
    		if (e.getErrorCode()==JposConst.JPOS_E_TIMEOUT) System.out.println("Transaction Timeout :(");
    		log.logImmediately("main", "TX", "          Result Code: " + myCATControl.getCenterResultCode());
    		log.logImmediately("main", "TX", "      Sequence Number: " + myCATControl.getSequenceNumber());
    		}catch(Exception ex){}
	}

 
}




