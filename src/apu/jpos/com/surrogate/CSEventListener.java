package apu.jpos.com.surrogate;

import apu.jpos.com.surrogate.net.SocketInfo;
import apu.jpos.com.surrogate.net.protocol.CSServerEvent;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 4/25/14
 * Time: 9:59 AM
 * To change this template use File | Settings | File Templates.
 */
public interface CSEventListener {
    public void onServerEvent(SocketInfo socket, CSServerEvent event);

}
