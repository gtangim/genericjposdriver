package apu.jpos.com.surrogate;

import apu.jpos.util.*;
import apu.jpos.com.surrogate.net.*;
import apu.jpos.com.surrogate.net.protocol.*;
import cnf.jpos.factory.CnfJposServiceFactory;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.DatatypeConverter;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 4/24/14
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class ComSurrogate extends Loggable implements ClientSocketListener{
    ClientSocketPool pool;
    private int socketRetryInterval = 2000;
    private int commandTimeoutMS = 10 * 1000;
    private int connectionWaitMS = 15 * 1000;

    private int linkId = -1;
    private boolean connected = false;
    private boolean ready = false;
    private String appName;
    private List<CSEventListener> eventHandlers;


    public ComSurrogate(String address, int port, String appName) throws Exception
    {
        this.appName = appName;
        eventHandlers = new ArrayList<CSEventListener>();
        pool = new ClientSocketPool(logger
                , Utility.toList(new CSResponseDecompiler())
                ,Utility.toList(new CSPacketizer()));
        pool.setDebug(false);
        //pool.setDebug(true);
        pool.setRetryConnection(true);
        pool.setConnectionRetryInterval(socketRetryInterval);
        if (!pool.initialize())
            throw new Exception("Cannot initialize socket pool selector!");
        linkId = pool.add(address,port,this,true);
        if (linkId==-1) throw new Exception("Unable to initialize socket in the socket pool!");
        else pool.tryConnect(linkId);
        waitForConnection();
    }

    public void addEventHandler(CSEventListener handler)
    {
        for(CSEventListener h:eventHandlers)
            if (h==handler) return;
        eventHandlers.add(handler);
    }
    public void remoteEventHandler(CSEventListener handler)
    {
        for (int i=0;i<eventHandlers.size();i++)
            if (eventHandlers.get(i)==handler)
            {
                eventHandlers.remove(i);
                return;
            }
    }







    public void waitForConnection() throws Exception
    {
        DateTime time = new DateTime();
        while (!connected && Utility.getElapsed(time)<connectionWaitMS)
        {
            Thread.sleep(500);
        }
        if (!connected) throw new Exception("NodeAPI could not establish a connection!");
    }

    public void waitForLogin() throws Exception
    {
        DateTime time = new DateTime();
        while (!connected || !ready && Utility.getElapsed(time)<connectionWaitMS)
        {
            Thread.sleep(500);
        }
        if (!connected || !ready) throw new Exception("NodeAPI could not establish a connection!");
        else if (CnfJposServiceFactory.CommSurrogateVersion==null)
        {
            try{
                String res = sendCommand("version");
                CnfJposServiceFactory.CommSurrogateVersion = res;
            }
            catch (Exception ex)
            {
                CnfJposServiceFactory.CommSurrogateVersion = "Comm Surrogate V1.0";
            }
        }
    }





    public void sendData(String cmd, Object a) throws Exception
    {
        sendCommand(cmd,Utility.toList(a),false);
    }
    public void sendData(String cmd, Object a,Object b) throws Exception
    {
        sendCommand(cmd,Utility.toList(a,b),false);
    }
    public void sendData(String cmd, Object a,Object b,Object c) throws Exception
    {
        sendCommand(cmd,Utility.toList(a,b,c),false);
    }
    public void sendData(String cmd, Object a,Object b, Object c, Object d) throws Exception
    {
        sendCommand(cmd,Utility.toList(a,b,c,d),false);
    }



    public String sendCommand(String cmd) throws Exception
    {
        return sendCommand(cmd,null,true);
    }
    public String sendCommand(String cmd, Object a) throws Exception
    {
        return sendCommand(cmd,Utility.toList(a),true);
    }
    public String sendCommand(String cmd, Object a,Object b) throws Exception
    {
        return sendCommand(cmd,Utility.toList(a,b),true);
    }
    public String sendCommand(String cmd, Object a,Object b,Object c) throws Exception
    {
        return sendCommand(cmd,Utility.toList(a,b,c),true);
    }
    public String sendCommand(String cmd, Object a,Object b, Object c, Object d) throws Exception
    {
        return sendCommand(cmd,Utility.toList(a,b,c,d),true);
    }
    public String sendCommand(String cmd, List<Object> params, boolean waitForResponse) throws Exception
    {
        synchronized (this)
        {
            if (connected)
            {
                if (pool==null)
                    throw new Exception("Socket Pool is null!");
                if (pool.get(linkId)==null)
                    throw new Exception("No socket connection has been initialized!");
                int sz = 0;
                if (params!=null) sz=params.size();
                List<String> sParam = new ArrayList<String>(sz);
                for (int i=0;i<sz;i++)
                    if (params.get(i)==null) sParam.add(null);
                    else if (params.get(i) instanceof String) sParam.add((String)params.get(i));
                    else if (params.get(i) instanceof Character) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof Boolean) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof Byte) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof Short) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof Integer) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof Long) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof Float) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof Double) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof BigDecimal) sParam.add(params.get(i).toString());
                    else if (params.get(i) instanceof byte[])
                    {
                        byte[] data = (byte[])params.get(i);
                        sParam.add(DatatypeConverter.printBase64Binary(data));
                    }
                    else if (params.get(i) instanceof ByteBuffer)
                    {
                        ByteBuffer buf = (ByteBuffer)params.get(i);
                        int n = buf.position();
                        buf.rewind();
                        byte[] data = new byte[n];
                        buf.get(data);
                        sParam.add(DatatypeConverter.printBase64Binary(data));
                    }
                    else
                    {
                        sParam.add(params.get(i).toString());
                    }
                CSCommand command = new CSCommand(cmd,sParam);

                if (waitForResponse) {
                    Object resO = null;
                    Object waiter = pool.getResponseWaiter(linkId);
                    synchronized (waiter) {
                        if (!pool.send(linkId, command))
                            throw new Exception("Unable to send command to the Bongo Node!");
                        resO = pool.getFirstResponse(linkId, commandTimeoutMS);
                    }
                    if (resO == null) throw new Exception("Node command timeout!");
                    if (resO instanceof CSCommandResponse) {
                        CSCommandResponse res = (CSCommandResponse) resO;
                        //if (res.getPacketId() != command.getCmdId()) throw new Exception("Invalid Command Response!");
                        if (res.isAck()) return null;
                        else if (res.isOk()) {
                            return res.getResponse();
                        } else if (res.isErrror()) throw new Exception(res.getResponse());
                        else if (res.isTimeout()) throw new Exception("Command Timeout!");
                        else throw new Exception("Invalid Command Response!");
                    } else throw new Exception("Invalid Command Response! " + resO.toString());
                }
                else if (!pool.send(linkId, command))
                    throw new Exception("Unable to send command to the Bongo Node!");
            }
            else throw new Exception("NodeAPI is not currently connected to a Bongo Node!");
        }
        return null;
    }







    //@Override
    public void onData(ClientSocketPool pool, SocketInfo si, Object data) {
        if (data instanceof CSServerEvent)
        {
            CSServerEvent e = (CSServerEvent)data;
            if (!pool.send(linkId,"ok,"+Integer.toString(e.getPacketId())+"\r\n"))
                logError("Unable to send Command Ack!");
            for(CSEventListener handler:eventHandlers)
                handler.onServerEvent(si,e);
        }
    }

    public void onDisconnect(ClientSocketPool pool, SocketInfo si) {
        synchronized (this)
        {
            connected=false;
            logWarning("Connection Lost with Node!");
        }
    }

    public void onClose(ClientSocketPool pool, SocketInfo si) {
        synchronized (this)
        {
            connected=false;
            log("Connection to the node was closed!");
        }
    }

    public void onError(ClientSocketPool pool, SocketInfo si, SocketErrorType errorType) {
        synchronized (this)
        {
            logWarning("A socket error has occurred: " + errorType.toString() + "!");
            pool.abortConnection(linkId);
        }
    }

    public void OnConnect(ClientSocketPool pool, SocketInfo si, boolean firstTimeConnect) {
        try {
            log("Connection established with ComPort Surrogate Server!");
            pool.get(linkId).setEventEnabled(false);
            synchronized (this)
            {
                connected=true;
            }
            sendCommand("login",Utility.getHostName(), appName);
            synchronized (this)
            {
                ready=true;
            }
        } catch (Exception e) {
            log(e);
            synchronized (this)
            {
                connected=false;
                pool.abortConnection(linkId);
            }
        }
        finally
        {
            pool.get(linkId).setEventEnabled(true);
        }
    }




    public boolean isConnected() {
        return connected;
    }



    private static Map<String,ComSurrogate> instances = new HashMap<String, ComSurrogate>();
    public static ComSurrogate getInstance(String ipAddress, int port, String appName)
    {
        ipAddress = Utility.longToIpAddress(Utility.ipAddressToLong(ipAddress));
        String lookup = ipAddress+"_"+Integer.toString(port);
        if (instances.containsKey(lookup)) return instances.get(lookup);
        else
        {
            try
            {
                ComSurrogate ret = new ComSurrogate(ipAddress,port,appName);
                instances.put(lookup,ret);
                return ret;
            }
            catch (Exception ex)
            {
                SimpleLogger logger = SimpleLogger.getDefaultLogger();
                logger.log("ComSurrogateFactory","ERROR",ex.getMessage());
                logger.log("ComSurrogateFactory","ERROR","Unable to create an instance for the Com Surrogate API!");
                return null;
            }
        }
    }

}
