package apu.jpos.com.surrogate.net.protocol;

import apu.jpos.util.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/3/14
 * Time: 9:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class CSCommand {
    private static int nid = 0;
    private static Object lock = new Object();

    int cmdId;
    String cmd;

    public CSCommand(String command, List<String> params) throws Exception
    {
        StringBuilder sb = new StringBuilder();
        if (StringUtil.empty(command)) throw new Exception("Invalid command!");
        sb.append(command);
        cmdId = nextId();
        sb.append(',');
        sb.append(cmdId);
        if (params!=null && params.size()>0)
        {
            for(Object p:params)
            {
                sb.append(',');
                sb.append(StringUtil.paramEncode((String)p));
            }
        }
        sb.append("\r\n");
        cmd = sb.toString();
    }

    public int getCmdId() {
        return cmdId;
    }

    public String getCmd() {
        return cmd;
    }

    private static int nextId()
    {
        synchronized (lock)
        {
            return nid++;
        }
    }


}
