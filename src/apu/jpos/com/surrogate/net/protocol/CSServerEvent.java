package apu.jpos.com.surrogate.net.protocol;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 4:34 PM
 * To change this template use File | Settings | File Templates.
 */
public final class CSServerEvent {
    private int packetId;
    private String eventType;
    private List<String> parameters;

    public CSServerEvent(int pid, String eType, List<String> eParams)
    {
        packetId = pid;
        eventType = eType;
        parameters = eParams;
    }

    public int getPacketId() {
        return packetId;
    }

    public String getEventType() {
        return eventType;
    }

    public List<String> getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        String param = null;
        if (parameters!=null)
        {
            param="";
            for(String p: parameters) {
                if (param.length()==0) param = "[";
                else param+=", ";
                param = param + p;
            }
            if (param.length()==0) param="[]";
            else param+="]";
        }
        return "CSServerEvent{" +
                "packetId=" + packetId +
                ", eventType='" + eventType + '\'' +
                ", parameters=" + param +
                '}';
    }
}
