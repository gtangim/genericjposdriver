package apu.jpos.com.surrogate.net.protocol;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 4:34 PM
 * To change this template use File | Settings | File Templates.
 */
public final class CSCommandResponse {
    private CSResponseStatus status;
    private String response;
    private int packetId;

    public CSCommandResponse(int packetId)
    {
        status= CSResponseStatus.INVALID;
        response=null;
        this.packetId=packetId;
    }

    public CSCommandResponse(int packetId, CSResponseStatus status)
    {
        this.status=status;
        response=null;
        this.packetId=packetId;
    }
    public CSCommandResponse(int packetId, String resp)
    {
        this.status= CSResponseStatus.OK;
        response=resp;
        this.packetId=packetId;
    }
    public CSCommandResponse(int packetId, int errCode, String errMessage)
    {
        this.status = CSResponseStatus.ERROR;
        this.response = "(error code "+Integer.toString(errCode)+") "+errMessage;
        this.packetId=packetId;
    }

    public CSResponseStatus getStatus() {
        return status;
    }

    public String getResponse() {
        return response;
    }

    public boolean isOk()
    {
        return status== CSResponseStatus.OK;
    }
    public boolean isTimeout()
    {
        return status== CSResponseStatus.TIMEOUT;
    }
    public boolean isInvalid()
    {
        return status== CSResponseStatus.INVALID;
    }
    public boolean isErrror()
    {
        return status== CSResponseStatus.ERROR;
    }

    public boolean isAck()
    {
        return (status== CSResponseStatus.OK && response==null);
    }

    public int getPacketId() {
        return packetId;
    }
}
