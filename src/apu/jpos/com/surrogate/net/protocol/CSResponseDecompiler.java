package apu.jpos.com.surrogate.net.protocol;

import apu.jpos.util.*;
import apu.jpos.com.surrogate.net.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 3:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class CSResponseDecompiler implements IDataFilter{
    public List packets=null;

    private int findHead(byte[] buff, int length, int startIndex)
    {
        int i=startIndex;
        while(i<length-2)
        {
            if (buff[i]==(byte)'\r' && buff[i+1]=='\n') return i;
            i++;
        }
        return -1;
    }

    private int checkSumIndex(byte[] buf, int pos)
    {
        pos--;
        if (pos<1) return -1;
        if (buf[pos]==0x01 && buf[pos-1]==0x10) pos-=2;
        else pos--;
        if (buf[pos]==0x01 && pos>0 && buf[pos-1]==0x10) pos-=2;
        else pos--;
        return pos+1;
    }
    private int getCheckSum(byte[] buf, int pos)
    {
        int b1;
        int b2;
        if (buf[pos]==0x10 && buf[pos+1]==0x01)
        {
            pos+=2;
            b1=0x10;
        }
        else
        {
            b1=((int)buf[pos]) & 0xff;
            pos++;
        }
        if (buf[pos]==0x10 && buf[pos+1]==0x01)
        {
            pos+=2;
            b2=0x10;
        }
        else
        {
            b2=((int)buf[pos]) & 0xff;
            pos++;
        }
        return (b1<<8)+b2;
    }

    public Object process(Object partialDataContainer, List inputData) throws Exception {
        packets=null;
        StringBuilder dataContainer = (StringBuilder)partialDataContainer;
        if (dataContainer==null) dataContainer=new StringBuilder(128*1024);
        for(Object x:inputData)
            dataContainer.append(StringUtil.byteToString((byte[])x));
        packets = new ArrayList(10);
        int pos = dataContainer.indexOf("\r\n");
        if (pos>=0)
        {
            boolean takeLast = (dataContainer.substring(dataContainer.length()-2,dataContainer.length()).contentEquals("\r\n"));
            String[] lines = dataContainer.toString().split("\r\n");
            int n = lines.length;
            if (!takeLast) n--;
            for(int i=0;i<n;i++)
            {
                // process each line as a packet....
                String[] fields = lines[i].split(",");
                String cmd = fields[0];
                if (fields.length<2 || StringUtil.empty(cmd))
                    packets.add(new CSCommandResponse(0));
                else
                {
                    try{
                        int pid = Integer.parseInt(fields[1]);
                        if (cmd.equals("ok") && fields.length==2)
                            packets.add(new CSCommandResponse(pid, CSResponseStatus.OK));
                        else if (cmd.equals("ok") && fields.length==3)
                            packets.add(new CSCommandResponse(pid,StringUtil.paramDecode(fields[2])));
                        else if (cmd.equals("error") && fields.length==4)
                        {
                            int eCode = Integer.parseInt(fields[2]);
                            packets.add(new CSCommandResponse(pid,eCode,StringUtil.paramDecode(fields[3])));
                        }
                        else if (cmd.equals("async"))
                        {
                            List<String> pList = new ArrayList<String>();
                            for (int pi=3;pi<fields.length;pi++) pList.add(StringUtil.paramDecode(fields[pi]));
                            CSServerEvent e = new CSServerEvent(pid,StringUtil.paramDecode(fields[2]),pList);
                            packets.add(e);
                        }
                        else packets.add(new CSCommandResponse(pid));
                    }
                    catch (Exception ex){
                        packets.add(new CSCommandResponse(0));
                    }
                }
            }
            dataContainer.setLength(0);
            if (!takeLast) dataContainer.append(lines[n]);
        }

        return dataContainer;

    }

    public List getMaturedObjects() {
        return packets;
    }
}
