package apu.jpos.com.surrogate.net.protocol;

import apu.jpos.com.surrogate.net.*;
import apu.jpos.util.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 12:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class CSPacketizer implements IDataFilter {
    private ByteBuffer result = ByteBuffer.allocate(64*1024);
    private int n;


    public Object process(Object partialDataContainer, List inputData) throws Exception {
        result.clear();
        for(Object x: inputData)
        {
            CSCommand cmd = (CSCommand)x;
            result.put(StringUtil.stringToByte(cmd.getCmd()));
        }
        n=result.position();
        return null;
    }

    public List getMaturedObjects() {
        List ret = new ArrayList(1);
        result.rewind();
        byte[] target = new byte[n];
        result.get(target);
        ret.add(target);
        return ret;
    }
}
