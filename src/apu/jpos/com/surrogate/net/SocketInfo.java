package apu.jpos.com.surrogate.net;

import org.joda.time.DateTime;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/12/11
 * Time: 11:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class SocketInfo {


    protected String name=null;
    protected String displayName;
    protected Integer id;
    protected SocketChannel socket;
    protected String ipAddressString;
    protected int port;
    protected InetSocketAddress ipAddress;
    protected SocketStatus status;
    protected SocketState state;
    protected boolean connectedOnce;
    protected DateTime lastDisconnected=null;
    protected DateTime lastTryConnect=null;
    protected ByteBuffer readBuffer = ByteBuffer.allocate(8192*2); // use a little bit extra buffer...
    protected boolean eventEnabled;

    public SocketInfo(String address, int port)
    {
        id=getNextId();
        ipAddressString = address;
        if (ipAddressString==null) ipAddressString="127.0.0.1";
        this.port=port;
        translateAddress();
        initDisplayName();
        socket=null;
        state=SocketState.IDLE;
        status=SocketStatus.DISCONNECTED;
        connectedOnce=false;
        eventEnabled=true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return displayName;
    }

    private void initDisplayName()
    {
        if (name!=null)
            displayName = "|"+name+"|>>"+ipAddressString+":"+Integer.toString(port);
        else displayName = ipAddressString+":"+Integer.toString(port);
    }

    protected void translateAddress()
    {
        try
        {
            ipAddress = new InetSocketAddress(InetAddress.getByName(ipAddressString),port);
        }
        catch (Exception ex)
        {
        }
    }




    public void setName(String name) {
        this.name = name;
        initDisplayName();
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public SocketChannel getSocket() {
        return socket;
    }

    public String getIpAddressString() {
        return ipAddressString;
    }

    public int getPort() {
        return port;
    }

    public InetSocketAddress getIpAddress() {
        if (ipAddress==null)
            translateAddress();
        return ipAddress;
    }

    public SocketStatus getStatus() {
        return status;
    }

    public SocketState getState() {
        return state;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setStatus(SocketStatus status) {
        if (status==SocketStatus.DISCONNECTED)
            lastDisconnected=new DateTime();
        this.status = status;
    }

    public int getElapsedSinceLastDisconnect()
    {
        if (lastDisconnected==null) return 0;
        else
        {
            DateTime dt = new DateTime();
            return (int)(dt.getMillis()-lastDisconnected.getMillis());
        }
    }

    public void setState(SocketState state) {
        this.state = state;
    }

    public boolean isConnectedOnce() {
        return connectedOnce;
    }

    public void setConnectedOnce(boolean connectedOnce) {
        this.connectedOnce = connectedOnce;
    }

    public boolean isTryingToConnect()
    {
        return lastTryConnect!=null;
    }
    public void setTryConnect()
    {
        lastTryConnect=new DateTime();
        lastDisconnected=null;
    }
    public int getElapsedSinceTryConnect()
    {
        if (lastTryConnect==null) return 0;
        DateTime dt = new DateTime();
        return (int)(dt.getMillis()-lastTryConnect.getMillis());
    }
    public void resetTryConnect()
    {
        lastTryConnect=null;
    }

    public boolean isEventEnabled() {
        return eventEnabled;
    }

    public synchronized void setEventEnabled(boolean eventEnabled) {
        this.eventEnabled = eventEnabled;
    }

    public static int nextId=0;
    public static int getNextId()
    {
        return nextId++;
    }



}
