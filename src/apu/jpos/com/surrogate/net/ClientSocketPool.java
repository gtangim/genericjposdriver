package apu.jpos.com.surrogate.net;

import apu.jpos.com.surrogate.net.protocol.CSCommandResponse;
import apu.jpos.util.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/10/11
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClientSocketPool extends Loggable implements Runnable{

    Selector selector=null;
    protected boolean debug = true;

    protected boolean retryConnection=true;
    protected boolean removeClosedSocket=false;
    protected int connectionRetryInterval=30000;
    protected int commandTimeout=30000;
    protected int numWorkerThreads=10;


    protected HashMap<Integer,SocketInfo> socketMap=new HashMap<Integer, SocketInfo>();
    protected HashMap<SocketChannel,SocketInfo> socketMap2 = new HashMap<SocketChannel, SocketInfo>();
    protected List<ChangeRequest> pendingChanges = new LinkedList<ChangeRequest>();
    protected Map<Integer,List<ByteBuffer>> pendingOutgoingData = new HashMap<Integer, List<ByteBuffer>>();
    protected List<IDataFilter> inputFilters;
    protected List<IDataFilter> outputFilters;
    protected Map<Integer,Object[]> inputFilterPartials = new HashMap<Integer, Object[]>();
    protected Map<Integer,Object[]> outputFilterPartials = new HashMap<Integer, Object[]>();
    protected Map<Integer,List<ClientSocketListener>> eventHandlers = new HashMap<Integer, List<ClientSocketListener>>();
    protected Map<Integer,Object> responseWaiters = new HashMap<Integer, Object>();
    protected Map<Integer,List> lastResponses = new HashMap<Integer, List>();
    protected ExecutorService workers = null;


    protected ClientSocketPoolMonitor monitor=null;
    protected Thread ioThread=null;
    protected Thread monitorThread=null;

    public ClientSocketPool(SimpleLogger logger, List<IDataFilter> inputFilters, List<IDataFilter> outputFilters)
    {
        super(logger);
        if (inputFilters==null || inputFilters.size()==0)
        {
            this.inputFilters=new ArrayList<IDataFilter>(10);
            this.inputFilters.add(new DefaultDataFilter());
        }
        else this.inputFilters=inputFilters;
        if (outputFilters==null || outputFilters.size()==0)
        {
            this.outputFilters=new ArrayList<IDataFilter>(10);
            this.outputFilters.add(new DefaultDataFilter());
        }
        else this.outputFilters=outputFilters;
    }

    public boolean initialize()
    {
        try {
            selector = SelectorProvider.provider().openSelector();
            if (selector==null)
                throw new IOException("Could not initialize the selector!");
            workers = Executors.newFixedThreadPool(numWorkerThreads);
            ioThread = new Thread(this);
            ioThread.setDaemon(true);
            ioThread.start();
            return true;
        } catch (IOException e) {
            log(e);
            return false;
        }
    }


    public int add(String address, int port, ClientSocketListener eventHandler, boolean tryConnectImmediately)
    {
        SocketInfo si = new SocketInfo(address,port);
        synchronized (socketMap)
        {
            socketMap.put(si.getId(),si);
            if (si.socket!=null) socketMap2.put(si.socket,si);
            addEventHandler(si.getId(),eventHandler);
            if (tryConnectImmediately) tryConnect(si.getId());
            return si.getId();
        }
    }

    public void tryConnect(int handle)
    {
        SocketInfo si = null;
        try {

            synchronized (socketMap)
            {
                si = get(handle);
                if (si==null)
                    throw new Exception("Cannot Reconnect! Socket with handle "
                            + Integer.toString(handle) + " doesnt exist in the socket pool!");
                if (si.isTryingToConnect()) return;
                log("Trying to connect: " + si);
                si.setState(SocketState.CONNECTING);
                si.setTryConnect();
                SocketChannel oldSocket = si.socket;
                si.socket = SocketChannel.open();
                synchronized (socketMap)
                {
                    if (oldSocket!=null)
                    {
                        if (socketMap2.containsKey(oldSocket))
                            socketMap2.remove(oldSocket);
                        oldSocket.close();
                    }
                    socketMap2.put(si.socket, si);
                }

                si.socket.configureBlocking(false);
                InetSocketAddress addr = si.getIpAddress();
                if (addr==null)
                    throw new Exception("Cannot resolve host name while trying to connect!"
                            +" Either invalid hostname, or network is down!");
                si.socket.connect(addr);
                synchronized (this.pendingChanges)
                {
                    pendingChanges.add(new ChangeRequest(si
                            , ChangeRequest.REGISTER, SelectionKey.OP_CONNECT));
                }
                selector.wakeup();
            }
        } catch (Exception e) {
            log(e);
            abortConnection(handle);
        }
    }

    public void abortConnection(int handle)
    {
        SocketInfo si = null;
        try {
            synchronized (socketMap)
            {
                si = get(handle);
                if (si==null)
                    throw new Exception("Socket with handle "
                            + Integer.toString(handle) + " doesnt exist in the socket pool!");
                if (socketMap2.containsKey(si.socket)) socketMap2.remove(si.socket);
                if (inputFilterPartials.containsKey(handle)) inputFilterPartials.remove(handle);
                if (outputFilterPartials.containsKey(handle)) outputFilterPartials.remove(handle);
                si.socket.close();
                si.socket=null;
                si.setStatus(SocketStatus.DISCONNECTED);
                si.setState(SocketState.IDLE);
                if (si.isConnectedOnce())
                    logWarning(si+": Connection has been disrupted!");
                else logWarning("Connection Failed: " + si);
                final SocketInfo si2 = si;
                workers.execute(new Runnable() {
                    public void run() {
                        fireOnDisconnect(si2);
                    }
                });
            }
        } catch (Exception e) {
            log(e);
        }
        finally
        {
            if (si!=null) si.resetTryConnect();
        }
    }

    public void close(int handle)
    {
        synchronized (socketMap)
        {
            try {
                final SocketInfo si = get(handle);
                if (si==null)
                    throw new Exception("Socket with handle "
                            + Integer.toString(handle) + " doesnt exist in the socket pool!");
                if (socketMap2.containsKey(si.socket)) socketMap2.remove(si.socket);
                if (removeClosedSocket && socketMap.containsKey(handle)) socketMap.remove(handle);
                if (inputFilterPartials.containsKey(handle)) inputFilterPartials.remove(handle);
                if (outputFilterPartials.containsKey(handle)) outputFilterPartials.remove(handle);
                if (si.socket!=null) si.socket.close();
                si.socket=null;
                si.setStatus(SocketStatus.CLOSED);
                si.setState(SocketState.IDLE);
                if (si.isConnectedOnce())
                    logWarning(si+": Connection has been closed!");
                else logWarning("Connection Cancelled: " + si);
                workers.execute(new Runnable() {
                    public void run() {
                        fireOnClose(si);
                    }
                });
            } catch (Exception e) {
                log(e);
            }
        }
    }

    public SocketInfo get(int handle)
    {
        synchronized (socketMap)
        {
            if (socketMap.containsKey(handle))
                return socketMap.get(handle);
            else return null;
        }
    }

    public SocketInfo get(SocketChannel channel)
    {
        // all get and add method locks to the socketMap object so it is synchronized...
        // changes or access of socketMap and socketMap2 go together
        synchronized (socketMap)
        {
            if (socketMap2.containsKey(channel))
                return socketMap2.get(channel);
            else return null;
        }
    }

    public List getResponses(SocketInfo si, int waitTime)
    {
        try{
            return getResponses(si.getId(),waitTime);
        }
        catch (Exception ex)
        {
            log(ex);
            return null;
        }
    }
    public List getResponses(int handle, int waitTime)
    {
        try{
            Object responseWaiter = getResponseWaiter(handle);
            synchronized (responseWaiter)
            {
                responseWaiter.wait(waitTime);
                List lastResponse = getLastResponse(handle);
                if (lastResponse==null) throw new Exception("Timeout while waiting for a response!");
                clearLastResponse(handle);
                return lastResponse;
            }
        }
        catch (Exception ex)
        {
            log(ex);
            return null;
        }
    }

    public Object getFirstResponse(SocketInfo si, int waitTime)
    {
        try{
            return getFirstResponse(si.getId(),waitTime);
        }
        catch (Exception ex)
        {
            log(ex);
            return null;
        }
    }
    public Object getFirstResponse(int handle, int waitTime)
    {
        try{
            Object responseWaiter = getResponseWaiter(handle);
            synchronized (responseWaiter)
            {
                responseWaiter.wait(waitTime);
                List lastResponse = getLastResponse(handle);
                if (lastResponse==null) throw new Exception("Timeout while waiting for a response!");
                List ret = lastResponse;
                clearLastResponse(handle);
                if (lastResponse==null) return null;
                else if (lastResponse.size()==0) return null;
                else return lastResponse.get(0);
            }
        }
        catch (Exception ex)
        {
            log(ex);
            return null;
        }
    }


    private void processIncomingData(SocketInfo si, int numRead)
    {
        try {
            byte[] buf = new byte[numRead];
            si.readBuffer.rewind();
            si.readBuffer.get(buf);
            List data = new ArrayList(2);
            data.add(buf);
            data = processData(si.getId(),inputFilters,inputFilterPartials,data);
            if (data!=null)
            {
                Object responseWaiter = getResponseWaiter(si);
                synchronized (responseWaiter)
                {
                    putLastResponses(si.getId(),data);
                    for(Object res: data)
                        if (res instanceof CSCommandResponse) {
                            if (debug) log("Notifying: " + data.get(0).toString());
                            responseWaiter.notifyAll();
                        }
                }
                // Now launch the event input data event...
                if (si.isEventEnabled())
                {
                    List<ClientSocketListener> listeners = getEventHandlers(si.getId());
                    final SocketInfo siFinal = si;
                    final ClientSocketPool thisFinal = this;
                    for(Object dataElement:data)
                    {
                        final Object dataElementFinal = dataElement;
                        if (listeners!=null)
                            for(ClientSocketListener listener:listeners)
                            {
                                final ClientSocketListener l=listener;
                                workers.execute(new Runnable() {
                                    public void run() {
                                            l.onData(thisFinal, siFinal, dataElementFinal);
                                    }
                                });
                            }
                    }
                }
            }
        } catch (Exception e) {
            log(e);
            logWarning("Failed to process incoming data from the socket!");
        }

    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        final SocketInfo si=get(socketChannel);
        try {
            // Clear out our read buffer so it's ready for new data
            if (si==null) throw new Exception("This socket is not found in the socket pool! ["
                    +socketChannel.getRemoteAddress().toString()+"]");
            int n;
            ByteBuffer buf = ByteBuffer.allocate(8096);
            /*synchronized (si)
            {
                si.readBuffer.clear();
                // Attempt to read off the channel
                n = socketChannel.read(si.readBuffer);
            } */
            n = socketChannel.read(buf);

            final int numRead = n;
            final ByteBuffer readBuffer = buf;
            if (numRead == -1) {
                // Remote entity shut the socket down cleanly. Do the
                // same from our end and cancel the channel.
                key.channel().close();
                key.cancel();
                return;
            }
            else
            {
                // Process the data...
                if (debug)
                {
                    byte[] buf2 = new byte[numRead];
                    buf.rewind();
                    buf.get(buf2);
                    //String data = StringUtil.byteToHexSep(buf,"[","][","]");
                    //log(si + " | Received >> " + data);
                    logRaw(si + " | Received >> " + StringUtil.byteToString(buf2));
                }
                workers.execute(new Runnable() {
                    public void run() {
                        //log("Incoming Data....Locking");
                        synchronized (si)
                        {
                            //log("Incoming Data....Calling");
                            //si.readBuffer.clear();
                            //si.readBuffer.put(readBuffer,0,numRead);
                            si.readBuffer = readBuffer;
                            processIncomingData(si,numRead);
                            //log("Incoming Data....Done!");
                        }
                    }
                });
            }

        } catch (Exception e) {
            log(e);
            key.cancel();

            if (si!=null) abortConnection(si.getId());
            else socketChannel.close();
            /*if (si!=null)
            {
                si.setState(SocketState.IDLE);
                si.setStatus(SocketStatus.DISCONNECTED);
                si.socket.close();
            }
            else socketChannel.close();*/
        }

    }
    private void write(SelectionKey key) throws IOException {
        try {
            SocketChannel socketChannel = (SocketChannel) key.channel();
            synchronized (this.pendingOutgoingData) {
                SocketInfo si=get(socketChannel);
                if (si==null) throw new Exception("This socket is not found in the socket pool! ["
                        +socketChannel.getRemoteAddress().toString()+"]");
                List queue = (List) this.pendingOutgoingData.get(si.getId());
                if (queue==null)
                {
                    synchronized (socketMap)
                    {
                        si.setState(SocketState.READING);
                    }
                    key.interestOps(SelectionKey.OP_READ);
                    return;
                }
                // Write until there's not more data ...
                while (!queue.isEmpty()) {
                    ByteBuffer buf = (ByteBuffer) queue.get(0);
                    socketChannel.write(buf);
                    if (buf.remaining() > 0) {
                        // ... or the socket's buffer fills up
                        break;
                    }
                    queue.remove(0);
                }

                if (queue.isEmpty()) {
                    // We wrote away all data, so we're no longer interested
                    // in writing on this socket. Switch back to waiting for
                    // data.
                    key.interestOps(SelectionKey.OP_READ);
                    synchronized (socketMap)
                    {
                        si.setState(SocketState.READING);
                    }
                }
            }
        } catch (Exception e) {
            log(e);
            key.cancel();
        }
    }


    private void finishConnection(SelectionKey key){
        SocketChannel socketChannel = (SocketChannel) key.channel();
        final SocketInfo si = get(socketChannel);
        // Finish the connection. If the connection operation failed
        // this will raise an IOException.
        try {
            if (si==null) throw new Exception("This socket is not found in the socket pool! ["
                    +socketChannel.getRemoteAddress().toString()+"]");
            socketChannel.finishConnect();
            synchronized (socketMap)
            {
                si.setStatus(SocketStatus.CONNECTED);
                si.setState(SocketState.WRITING);
            }
            // Register an interest in writing on this channel
            key.interestOps(SelectionKey.OP_WRITE);
            final boolean firstTimeConnect = !si.isConnectedOnce();
            si.setConnectedOnce(true);
            log("Connection Established: " + si);
            workers.execute(new Runnable() {
                public void run() {
                    fireOnConnect(si, firstTimeConnect);
                }
            });

        } catch (Exception e) {
            // Cancel the channel's registration with our selector
            log(e);
            if (si!=null)
            {
                fireOnError(si,SocketErrorType.ConnetionError);
                fireOnDisconnect(si);
                si.setStatus(SocketStatus.DISCONNECTED);
                si.setState(SocketState.IDLE);
                logWarning("Connection Failed: " + si);
            }
            key.cancel();
        }
        finally {
            si.resetTryConnect();
        }

    }


    private List processData(int handle, List<IDataFilter> filters, Map<Integer,Object[]> partialMap, List inputData) throws Exception
    {
        List data = new ArrayList(inputData);
        Object[] partials = null;
        //SocketInfo si = get(handle);
        //if (si==null)
        //    throw new Exception("Socket with handle "
        //            + Integer.toString(handle) + " doesnt exist in the socket pool!");
        if (partialMap.containsKey(handle)) partials=partialMap.get(handle);
        else
        {
            partials=new Object[filters.size()];
            partialMap.put(handle,partials);
        }
        for(int i=0;i<filters.size();i++)
        {
            partials[i] = filters.get(i).process(partials[i],data);
            data=filters.get(i).getMaturedObjects();
            if (data==null) break; // There is no point going through the entire chain...
        }
        return data;
    }

    public boolean send(int handle, Object data)
    {
        List inp = new ArrayList(1);
        inp.add(data);
        return send(handle,inp);
    }
    public boolean send(int handle, List data)
    {
        try{
            synchronized (outputFilters)
            {
                data = processData(handle,outputFilters,outputFilterPartials,data);
                if (data!=null)
                    for(Object dataElement:data)
                        if(dataElement instanceof byte[]) send(handle,(byte[])dataElement);
                        else if (dataElement instanceof String) send(handle,(String)dataElement);
                        else throw new Exception("Output filter configuration is invalid! Last filter must return byte[] or String");
                else throw new Exception("Cannot send packet, object did not pass through the entire filter chain!");
                return true;
            }
        }
        catch (Exception ex)
        {
            log(ex);
            logWarning("Could not send packet because output filter chain did not manage to process data!");
            return false;
        }
    }
    public boolean send(int handle, String data)
    {
        try {
            byte[] dataBytes = data.getBytes("UTF8");
            return send(handle,dataBytes);
        } catch (UnsupportedEncodingException e) {
            log(e);
            return false;
        }
    }

    public boolean send(int handle, byte[] data) {
        // Queue the data we want written
        SocketInfo si=null;
        try {
            synchronized (getResponseWaiter(handle))
            {
                clearLastResponse(handle);
            }
            synchronized (this.socketMap)
            {
                si = get(handle);
                if (si==null)
                    throw new Exception("Cannot Write! Socket with handle "
                            + Integer.toString(handle) + " not found in socket pool!");
                if (si.getStatus()== SocketStatus.CLOSED)
                    throw new Exception("Cannot Write! Socket "+si+" is closed!");
                synchronized (this.pendingOutgoingData) {
                    if (debug)
                    {
                        //log(si+", Sending:"+StringUtil.byteToHexSep(data,"[","][","]"));
                        logRaw(si+" | Sending >> "+StringUtil.byteToString(data));
                    }
                    List queue = (List) this.pendingOutgoingData.get(handle);
                    if (queue == null) {
                        queue = new ArrayList();
                        this.pendingOutgoingData.put(handle, queue);
                    }
                    queue.add(ByteBuffer.wrap(data));
                }
                // Finally, wake up our selecting thread so it can make the required changes
                if (si.status==SocketStatus.CONNECTED)
                {
                    si.setState(SocketState.WRITING);
                    synchronized (pendingChanges)
                    {
                        pendingChanges.add(new ChangeRequest(si
                                , ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));
                    }
                    this.selector.wakeup();
                }
                else if (si.status == SocketStatus.DISCONNECTED)
                {
                    tryConnect(handle);
                }
            }
            return true;
        } catch (Exception e) {
            log(e);
            return false;
        }
    }


    public void setRemoveClosedSocket(boolean removeClosedSocket) {
        this.removeClosedSocket = removeClosedSocket;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public Selector getSelector() {
        return selector;
    }

    public void run()
    {
        Thread.currentThread().setName("SOCKET_EVENT_LOOP");
        log("Started Socket Pool IO Thread!");
        try {
            if (monitor==null)
            {
                monitor=new ClientSocketPoolMonitor(this,logger);
                monitor.setReconnectInterval(connectionRetryInterval);
                monitorThread = new Thread(monitor);
                monitorThread.setDaemon(true);
                monitorThread.start();
            }
            while(true)
            {
                // Process any pending changes
                synchronized (this.socketMap)
                {
                    synchronized (this.pendingChanges) {

                        for(ChangeRequest change:pendingChanges)
                        {
                            if (change.si!=null && change.si.socket!=null)
                            {
                                switch (change.type) {
                                case ChangeRequest.CHANGEOPS:
                                    SelectionKey key = change.si.socket.keyFor(this.selector);
                                    if (key==null)
                                    {
                                        logWarning("Cannot apply state change for "
                                                + change.si + "! This socket is closed!");
                                        synchronized (socketMap)
                                        {
                                            change.si.setState(SocketState.IDLE);
                                            change.si.setStatus(SocketStatus.DISCONNECTED);
                                        }
                                    }
                                    else key.interestOps(change.ops);
                                    break;
                                case ChangeRequest.REGISTER:
                                    try {
                                        if (change.si!=null && change.si.socket!=null)
                                            change.si.socket.register(this.selector, change.ops);
                                    } catch (ClosedChannelException e) {
                                        log(e);
                                        logWarning("Ignoring socket change request because socket is closed!");
                                    }

                                    break;
                                }
                            }
                        }
                        this.pendingChanges.clear();
                    }
                }
                // Wait for an event one of the registered channels
                try {
                    selector.select();
                } catch (IOException e) {
                    log(e);
                }
                // Iterate over the set of keys for which events are available
                synchronized (socketMap)
                {
                    Iterator it = selector.selectedKeys().iterator();
                    while(it.hasNext())
                    {
                        SelectionKey key = (SelectionKey)it.next();
                        it.remove();

                        try{
                            if (!key.isValid()) {
                                continue;
                            }
                            // Check what event is available and deal with it
                            if (key.isConnectable()) {
                                this.finishConnection(key);
                            } else if (key.isReadable()) {
                                this.read(key);
                            } else if (key.isWritable()) {
                                this.write(key);
                            }
                        }
                        catch (Exception ex)
                        {
                            log(ex);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log(e);
        }
        logWarning("Socket Pool IO Thread has been stopped!");
    }


    public void addEventHandler(int handle, ClientSocketListener eventHandler)
    {
        try {
            synchronized (eventHandlers)
            {
                List<ClientSocketListener> listeners = null;
                if (eventHandlers.containsKey(handle)) listeners=eventHandlers.get(handle);
                else
                {
                    listeners=new ArrayList<ClientSocketListener>(4);
                    eventHandlers.put(handle,listeners);
                }
                if (!listeners.contains(eventHandler)) listeners.add(eventHandler);
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to attach event handler to socket with handle: "
                    + Integer.toString(handle));
        }
    }

    public void removeEventHandler(int handle, ClientSocketListener eventHandler)
    {
        try {
            synchronized (eventHandlers)
            {
                List listeners = null;
                if (eventHandlers.containsKey(handle)) listeners=eventHandlers.get(handle);
                else throw new Exception("No event handlers exist for socket with handle: "+Integer.toString(handle));
                listeners.remove(eventHandler);
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to detach event handler from socket with handle: "
                    + Integer.toString(handle));
        }
    }

    public void removeAllEventHandlers(int handle)
    {
        try {
            synchronized (eventHandlers)
            {
                if (eventHandlers.containsKey(handle)) eventHandlers.remove(handle);
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to detach event handlers from socket with handle: "
                    + Integer.toString(handle));
        }
    }

    public List<ClientSocketListener> getEventHandlers(int handle)
    {
        try {
            synchronized (eventHandlers)
            {
                List<ClientSocketListener> listeners = null;
                if (eventHandlers.containsKey(handle)) listeners=eventHandlers.get(handle);
                else return null;
                return listeners;
            }
        } catch (Exception e) {
            log(e);
            return null;
        }
    }
    private void fireOnConnect(SocketInfo si, boolean firstTimeConnect)
    {
        synchronized (si)
        {
            final SocketInfo siFinal = si;
            final ClientSocketPool thisFinal=this;
            final boolean firstTimeConnectFinal = firstTimeConnect;
            List<ClientSocketListener> listeners = getEventHandlers(si.getId());
            if (listeners!=null)
                for(ClientSocketListener listener:listeners)
                {
                    final ClientSocketListener l = listener;
                    workers.execute(new Runnable() {
                        public void run() {
                            l.OnConnect(thisFinal,siFinal,firstTimeConnectFinal);
                        }
                    });
                }
        }
    }
    private void fireOnDisconnect(SocketInfo si)
    {
        synchronized (si)
        {
            List<ClientSocketListener> listeners = getEventHandlers(si.getId());
            if (listeners!=null)
                for(ClientSocketListener listener:listeners)
                {
                    listener.onDisconnect(this, si);
                }
        }
    }
    private void fireOnClose(SocketInfo si)
    {
        synchronized (si)
        {
            List<ClientSocketListener> listeners = getEventHandlers(si.getId());
            if (listeners!=null)
                for(ClientSocketListener listener:listeners)
                {
                    listener.onClose(this, si);
                }
        }
    }
    private void fireOnError(SocketInfo si,SocketErrorType errorType)
    {
        synchronized (si)
        {
            List<ClientSocketListener> listeners = getEventHandlers(si.getId());
            if (listeners!=null)
                for(ClientSocketListener listener:listeners)
                {
                    listener.onError(this, si, errorType);
                }
        }
    }

    protected void putLastResponses(int handle, List responseList)
    {
        try{
            Object responseWaiter = getResponseWaiter(handle);
            synchronized (responseWaiter)
            {
                if (responseList==null) throw new Exception("A null response list was forwarded, cannot add to the list!");
                if (lastResponses==null) throw new Exception("last Response map is null!");
                if (!lastResponses.containsKey(handle))
                {
                    List responses = new ArrayList(1);
                    lastResponses.put(handle,responses);
                }
                lastResponses.get(handle).addAll(responseList);
            }
        }
        catch (Exception ex)
        {
            log(ex);
        }
    }
    protected void putLastResponse(int handle, Object response)
    {
        try{
            Object responseWaiter = getResponseWaiter(handle);
            synchronized (responseWaiter)
            {
                if (lastResponses==null) throw new Exception("last Response map is null!");
                if (!lastResponses.containsKey(handle))
                {
                    List responses = new LinkedList();
                    lastResponses.put(handle,responses);
                }
                lastResponses.get(handle).add(response);
            }
        }
        catch (Exception ex)
        {
            log(ex);
        }
    }
    protected List getLastResponse(int handle)
    {
        try{
            Object responseWaiter = getResponseWaiter(handle);
            synchronized (responseWaiter)
            {
                if (lastResponses==null) throw new Exception("last Response map is null!");
                if (!lastResponses.containsKey(handle)) return null;
                else return lastResponses.get(handle);
            }
        }
        catch (Exception ex)
        {
            log(ex);
            return null;
        }
    }
    protected void clearLastResponse(int handle)
    {
        try{
            Object responseWaiter = getResponseWaiter(handle);
            synchronized (responseWaiter)
            {
                if (lastResponses==null) throw new Exception("last Response map is null!");
                if (lastResponses.containsKey(handle)) lastResponses.remove(handle);
            }
        }
        catch (Exception ex)
        {
            log(ex);
        }
    }

    protected Object getResponseWaiter(SocketInfo si)
    {
        try{
            int handle = si.getId();
            return getResponseWaiter(si.getId());
        }
        catch (Exception ex)
        {
            log(ex);
            return null;
        }
    }
    public Object getResponseWaiter(int handle)
    {
        try{
            // TODO: Investigate if this could cause a deadlock
            synchronized (responseWaiters)
            {
                if (!responseWaiters.containsKey(handle))
                    responseWaiters.put(handle, new Object());
                return responseWaiters.get(handle);
            }
        }
        catch (Exception ex)
        {
            log(ex);
            return null;
        }
    }

    public boolean isRetryConnection() {
        return retryConnection;
    }

    public void setRetryConnection(boolean retryConnection) {
        this.retryConnection = retryConnection;
    }

    public int getConnectionRetryInterval() {
        return connectionRetryInterval;
    }

    public void setConnectionRetryInterval(int connectionRetryInterval) {
        this.connectionRetryInterval = connectionRetryInterval;
    }

    public int getCommandTimeout() {
        return commandTimeout;
    }

    public void setCommandTimeout(int commandTimeout) {
        this.commandTimeout = commandTimeout;
    }

}
