package apu.jpos.com.surrogate.net;

import java.util.EventListener;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/14/11
 * Time: 8:14 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ClientSocketListener extends EventListener {
    public void onData(ClientSocketPool pool, SocketInfo si, Object data);
    public void onError(ClientSocketPool pool, SocketInfo si, SocketErrorType errorType);
    public void OnConnect(ClientSocketPool pool, SocketInfo si, boolean firstTimeConnect);
    public void onDisconnect(ClientSocketPool pool, SocketInfo si);
    public void onClose(ClientSocketPool pool, SocketInfo si);
}
