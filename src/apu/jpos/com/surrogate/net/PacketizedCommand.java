package apu.jpos.com.surrogate.net;

import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 12:09 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PacketizedCommand {
    public ByteBuffer getBinary();
}
