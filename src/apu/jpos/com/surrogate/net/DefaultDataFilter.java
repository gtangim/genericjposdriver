package apu.jpos.com.surrogate.net;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/14/11
 * Time: 12:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class DefaultDataFilter implements IDataFilter {
    public ByteBuffer buf=null;

    public DefaultDataFilter()
    {
    }

    public Object process(Object partialDataContainer, List inputData) throws Exception {
        if (inputData==null) return null;
        else if (inputData.size()==0) return null;
        int sz = 0;
        for(Object obj:inputData)
        {
            if (obj instanceof byte[])
                sz+=((byte[])obj).length;
            else throw new Exception("Default filter only accepts byte buffer. Found: " + obj.getClass().getName());
        }
        buf = ByteBuffer.allocate(sz);
        for(Object obj:inputData)
            buf.put((byte[])obj);
        return null; // There are no partials in the Default processor
    }

    public List getMaturedObjects() {
        if (buf==null) return null;
        else
        {
            List ret = new ArrayList(1);
            ret.add(buf.array());
            return ret;
        }
    }
}
