package apu.jpos.com.surrogate.net;

import apu.jpos.util.*;


/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/12/11
 * Time: 12:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClientSocketPoolMonitor extends Loggable implements Runnable{

    protected int connectionTimeout=10000;
    protected int monitorInterval=2000;
    protected int reconnectInterval=30000;
    protected SimpleLogger logger;
    protected ClientSocketPool pool;

    public ClientSocketPoolMonitor(ClientSocketPool pool, SimpleLogger logger)
    {
        super(logger);
        this.pool=pool;
    }



    public void run() {

        Thread.currentThread().setName("CONNECTION_MONITOR");
        log("Started Socket Pool Monitor Thread!");
        while(true)
        {
            try
            {
                // First see if connection requests are timing out...
                synchronized (pool.socketMap)
                {
                    for(SocketInfo si: pool.socketMap.values())
                    {
                        if (si.socket!=null && si.socket.isConnectionPending()
                                && si.getElapsedSinceTryConnect()>connectionTimeout)
                        {
                            pool.abortConnection(si.getId());
                        }
                        else if (si.socket!=null && !si.socket.isConnected()
                                && si.getStatus()== SocketStatus.CONNECTED)
                        {
                            pool.abortConnection(si.getId());
                        }
                    }
                    if (pool.retryConnection)
                    {
                        for(SocketInfo si: pool.socketMap.values())
                        {
                            if (si.status==SocketStatus.DISCONNECTED
                                    && si.getElapsedSinceLastDisconnect()>reconnectInterval)
                            {
                                pool.tryConnect(si.getId());
                            }
                        }
                    }
                }
                Thread.sleep(monitorInterval);
            }
            catch (InterruptedException iex)
            {
                logWarning("Monitor Thread has been stopped!");
                return;
            }
            catch(Exception ex)
            {
                log(ex);
            }
        }


    }

    public int getMonitorInterval() {
        return monitorInterval;
    }

    public void setMonitorInterval(int monitorInterval) {
        this.monitorInterval = monitorInterval;
    }

    public int getReconnectInterval() {
        return reconnectInterval;
    }

    public void setReconnectInterval(int reconnectInterval) {
        this.reconnectInterval = reconnectInterval;
    }
}
