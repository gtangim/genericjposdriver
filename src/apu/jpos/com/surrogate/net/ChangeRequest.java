package apu.jpos.com.surrogate.net;

class ChangeRequest {
	public static final int REGISTER = 1;
	public static final int CHANGEOPS = 2;
	public SocketInfo si;
	public int type;
	public int ops;
	public ChangeRequest(SocketInfo socketInfo, int type, int ops) {
		this.si = socketInfo;
		this.type = type;
		this.ops = ops;
	}
}
