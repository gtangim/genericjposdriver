package apu.jpos.util;

//import java.util.*;
import org.joda.time.*;
import java.io.*;

public class SimpleLogger extends Thread {
	
	protected static SimpleLogger defaultLogger = null;
	
	protected String logFileName;
	protected int logFileMaxSize;
	protected DateTime lastLogTime;
	protected StringBuilder logBuffer;
	protected StringBuilder auxBuffer;
	protected boolean delayedWrite;
	protected int delayedWriteIntervalMS;
	protected boolean silentmode;

    protected String lastModule;
    protected String lastAction;


	public SimpleLogger()
	{

        logFileName = null;
		logFileMaxSize = 16*1024*1024;
		lastLogTime = new DateTime(1978, 1, 1, 12, 0, 0, 0);
		lastModule = "UNKNOWN";
		lastAction = "";
		logBuffer = new StringBuilder();
		auxBuffer = new StringBuilder();

		delayedWrite = false;
		delayedWriteIntervalMS = 3000;
		silentmode=false;
		this.setDaemon(true);
		this.setName("SimpleLogger_DelayedWrite");
        if (defaultLogger==null) defaultLogger=this;
	}
	
	public void logImmediately(String module, String action, String data)
	{
		if (silentmode) return;
		commitLog();
		log(module,action,data);
		commitLog();
	}
	
    public void logRaw(String module, String action, String data)
	{
		if (silentmode) return;
		if (module.equals(lastModule)==false || action.equals(lastAction)==false) commitLog();
		synchronized(this)
		{
			lastModule = module;
			lastAction = action;
			logBuffer.append(data);
		}
		if (delayedWrite==false) commitLog();
	}

    public void log(String module, String action, String data)
	{
		if (silentmode) return;
		if (module.equals(lastModule)==false || action.equals(lastAction)==false) commitLog();
		synchronized(this)
		{
			lastModule = module;
			lastAction = action;
			filterData(data);
			logBuffer.append(auxBuffer);
		}
		if (delayedWrite==false) commitLog();
	}
	private void filterData(String data)
	{
		int sz = data.length();
		auxBuffer.setLength(0);
        byte[] dataBytes=null;
        try {
            dataBytes = data.getBytes("UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        for (int i=0;i<sz;i++)
		{
			char c =  data.charAt(i);
            byte b = dataBytes[i];

			if (c<' ' || c>'~') auxBuffer.append(String.format("[%02x]", (((int)b) & 0xff)));
			else auxBuffer.append(c);
		}		
	}
	private void commitLog()
	{
		synchronized(this)
		{
			//if (logFileName!=null && Utility.fileExists(logFileName) && Utility.fileSize(logFileName)>logFileMaxSize)
			//	Utility.truncateFile(logFileName, (int)logFileMaxSize/2);
			lastLogTime = new DateTime();
			if (logBuffer.length()<1) return;
			auxBuffer.setLength(0);
			auxBuffer.append("[");
			auxBuffer.append(lastLogTime.toString("MM/dd/yyyy hh:mm:ssaa SSS"));
			auxBuffer.append("][");
            auxBuffer.append(lastModule);
			auxBuffer.append("][");
			auxBuffer.append(lastAction);
			auxBuffer.append("]  ");
			auxBuffer.append(logBuffer);
			auxBuffer.append("\r\n");
			logBuffer.setLength(0);
			System.out.print(auxBuffer);
            if (logFileName!=null)
                if (!Utility.appendStringToFile(logFileName, auxBuffer.toString(), logFileMaxSize))
                {
                    System.out.println("ERROR: Could not write to log file!");
                }
		}
	}
	
	public void run()
	{
        Thread.currentThread().setName("LOG_HELPER");
		while (delayedWrite)
		{
			try
			{
				Thread.sleep(1000);
			}
			catch(InterruptedException ex)
			{
				break;
			}
			DateTime nw = new DateTime();
			Duration diff = new Duration(lastLogTime, nw);
			if (diff.getMillis()>delayedWriteIntervalMS) commitLog();
			
		}
	}
	public void setDelayedWrite(boolean enabled)
	{
		delayedWrite = enabled;
		if (delayedWrite) this.start();
	}
	public void setDelayedWrite(boolean enabled, int delayMS)
	{
		delayedWriteIntervalMS = delayMS;
		setDelayedWrite(enabled);
	}
	
	public void setLogFile(String fname)
	{
		setLogFile(fname,1024*1024);		
	}
	public void setLogFile(String fname, int maxSize)
	{
		logFileName = fname;
		logFileMaxSize = maxSize;
	}
	public String getLogFileName()
	{
		return logFileName;
	}

	public void close()
	{
		setDelayedWrite(false);
		commitLog();		
	}
	
	public void setSilentMode(boolean loggerIsSilent)
	{
		silentmode=loggerIsSilent;
	}
	public static SimpleLogger getDefaultLogger()
	{
		if (defaultLogger==null) defaultLogger = new SimpleLogger();
		return defaultLogger;
	}

}
