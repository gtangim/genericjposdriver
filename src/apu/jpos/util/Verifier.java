package apu.jpos.util;

import org.joda.time.DateTime;

import java.io.File;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 1/28/13
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class Verifier {

    private final String keyHeader = "IP360$$39288";
    private final String contentHeader = "IP360$$1010119331942";

    private long fileSize;
    private String fileHash;
    private String fileModDate;

    private String hardwareId;
    private String networkId;


    private void populateFileData(String fname) throws Exception
    {
        if (!Utility.fileExists(fname)) throw new Exception("e0x0001");
        //String fileData = Utility.Read_File(fname);
        //if (fileData==null || fileData.equals("")) throw new Exception("e0x0002");
        fileHash = Utility.md5FromFile(fname);
        if (fileHash==null || fileHash.equals("")) throw new Exception("e0x0003");
        fileSize = Utility.fileSize(fname);
        if (fileSize<=1000) throw new Exception("e0x0004");
        File f = new File(fname);
        DateTime lastMod = new DateTime(f.lastModified());
        fileModDate = lastMod.toString("yyyyMMdd");
    }

    public Verifier() throws Exception
    {
        if (Utility.isWindows)
        {
            String osPath = System.getenv("windir");
            String fname = osPath+"\\System32\\cscript.exe";
            populateFileData(fname);
            String generatorVB = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n" +
                    "Set colDrives = objFSO.Drives\n" +
                    "Set objDrive = colDrives.item(\"C\")\n" +
                    "s = CStr(objDrive.SerialNumber)\n" +
                    "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n" +
                    "Set colItems = objWMIService.ExecQuery(\"Select * from Win32_BaseBoard\")\n"+
                    "For Each objItem in colItems\n" +
                    "\ts = s + \"|\"+CStr(objItem.SerialNumber)\n" +
                    "Next\n" +
                    "Wscript.Echo s";
            String vbPath = Utility.getCurrentDir()+"\\machine.vbs";
            if (Utility.fileExists(vbPath) && !Utility.deleteFile(vbPath,10)) throw new Exception("e0x0005");
            if (!Utility.stringToFile(vbPath,generatorVB))
                throw new Exception("e0x0006");
            String res = Utility.Shell(fname+" /NoLogo "+vbPath);
            if (!Utility.deleteFile(vbPath,10)) throw new Exception("e0x0007");
            if (res==null || res.trim().equals(""))
                throw new Exception("e0x0008");
            hardwareId = res.trim();
            networkId = StringUtil.joinStrings(Utility.getAllMacAddress(), "|");
            if (networkId==null || networkId.equals(""))
                throw new Exception("e0x0009");
        }
        else
        {
            String fname = "/sbin/udevadm";
            populateFileData(fname);
            String res = Utility.Shell(fname+" info --query=property --name=sda");
            if (res==null || res.trim().equals(""))
                throw new Exception("e0x0021");
            String id1 = StringUtil.between(res,"ID_MODEL=","\n");
            if (id1.trim().equals("")) throw new Exception("e0x0022");
            String id2 = StringUtil.between(res,"ID_SERIAL=","\n");
            if (id2.trim().equals("")) throw new Exception("e0x0023");
            hardwareId=id1+"|"+id2;
            hardwareId=hardwareId.replace('_','$');
            networkId = StringUtil.joinStrings(Utility.getAllMacAddress(),"|");
            if (networkId==null || networkId.equals(""))
                throw new Exception("e0x0024");
        }



    }


    public String getEncryptionKey(DateTime timeKey) throws Exception
    {
        if (timeKey==null)
        {
            while(true)
            {
                DateTime n = new DateTime();
                if (n.getSecondOfMinute()<30) break;
                Thread.sleep(5000);
            }
            timeKey = new DateTime();
        }
        String key = Utility.md5(keyHeader+"_"+fileHash+"_"+timeKey.toString("yyyyMMddHHmm"));

        key = StringUtil.fixedLengthString(key,24,'#',false);
        return key;
    }

    public void saveLicense(String fileName) throws Exception
    {
        String content = contentHeader+"_"+Long.toString(fileSize)+"_"+fileHash+"_"+fileModDate+"_"
                +hardwareId+"_"+networkId+"_"+(new DateTime()).toString();
        String encrypted = StringUtil.encrypt(content,getEncryptionKey(null),"DESede");
        if (encrypted==null || encrypted.trim().equals(""))
            throw new Exception("e0x0010");
        if (!Utility.stringToFile(fileName,encrypted))
            throw new Exception("e0x0011");

    }

    private boolean matchNetworkId(String source, String target)
    {
        if (source==null || target==null || source.trim().equals("") || target.trim().equals("")) return false;
        String[] sourceA = source.split("\\|");
        String[] targetA = target.split("\\|");
        if (sourceA.length!=targetA.length) return false;
        HashSet<String> sHash = new HashSet<String>();
        for(String s:sourceA) sHash.add(s);
        for(String t:targetA) if (!sHash.contains(t)) return false;
        return true;
    }

    public void verifyLicense(String fileName) throws Exception
    {
        if(!Utility.fileExists(fileName)) throw new Exception("e0x0012");
        File licFile = new File(fileName);
        DateTime fileModDateTime = new DateTime(licFile.lastModified());
        String encryptedContent = Utility.Read_File(fileName);
        if (encryptedContent==null || encryptedContent.trim().equals("")) throw new Exception("e0x0012");
        String content = StringUtil.decrypt(encryptedContent,getEncryptionKey(fileModDateTime),"DESede");
        if (content==null || content.trim().equals("")) throw new Exception("e0x0013");
        String[] contentList = content.split("_");
        if (contentList.length<7) throw new Exception("e0x0013");
        if (!contentList[0].equals(contentHeader)) throw new Exception("e0x0014");
        if (!contentList[1].equals(Long.toString(fileSize))) throw new Exception("e0x0015");
        if (!contentList[2].equals(fileHash)) throw new Exception("e0x0016");
        if (!contentList[3].equals(fileModDate)) throw new Exception("e0x0017");
        //System.out.println(contentList[4]);
        //System.out.println(hardwareId);
        if (!contentList[4].equals(hardwareId)) throw new Exception("e0x0018");
        if (!matchNetworkId(contentList[5],networkId)) throw new Exception("e0x0019");
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileHash() {
        return fileHash;
    }

    public String getKeyHeader() {
        return keyHeader;
    }

    public String getContentHeader() {
        return contentHeader;
    }

    public String getFileModDate() {
        return fileModDate;
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public String getNetworkId() {
        return networkId;
    }
}
