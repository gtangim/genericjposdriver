package apu.jpos.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jpos.JposConst;
import jpos.JposException;
import jpos.config.JposEntry;

public class JposPropertyExtender extends Loggable {
    //SimpleLogger plogger;
	protected List<String> properties;
    protected Map<String, String> propertyValues;
    protected Map<String, String> propertyDescriptions;
    protected Map<String, String> propertyDefaults;
    protected Map<String, String[]> propertyValueCollections;
    private String propertyModule;
    public JposPropertyExtender()
    {
    	resetProperties();
    }
    
    protected void resetProperties()
    {
    	properties = new ArrayList<String>();
    	propertyValues = new HashMap<String,String>();
    	propertyDescriptions = new HashMap<String,String>();
    	propertyDefaults = new HashMap<String,String>();
    	propertyValueCollections = new HashMap<String, String[]>();
    	//plogger=null;
    }
    /*protected void setPropertyLogger(SimpleLogger loggerInstance)
    {
    	plogger=loggerInstance;
    }*/
    public void setPropertyModuleName(String module)
    {
    	propertyModule = module;
    }
    
    protected void registerProperty(String propertyName, String propertyDescription, String propertyDefault, String[] propertyValueCollection)
    {
    	propertyName = propertyName.toLowerCase();
    	
    	if (propertyName==null) return;
    	if (properties.contains(propertyName)==false) properties.add(propertyName);
    	if (propertyDescription!=null && propertyDescriptions.containsKey(propertyName)==false) 
    		propertyDescriptions.put(propertyName, propertyDescription); 
    	if (propertyDefault!=null && propertyDefaults.containsKey(propertyName)==false) 
    		propertyDefaults.put(propertyName, propertyDefault.toLowerCase()); 
    	if (propertyValueCollection!=null && propertyValueCollections.containsKey(propertyName)==false) 
    	{
    		for (int i=0;i<propertyValueCollection.length;i++) propertyValueCollection[i]=propertyValueCollection[i].toLowerCase();
    		propertyValueCollections.put(propertyName, propertyValueCollection); 
    	}


        //log(propertyName + ": " + (propertyDescription==null?"":propertyDescription)
    	//		+ " Default="+(propertyDefault==null?"#NULL#":propertyDefault)
    	//		+ " Values=" + StringUtil.arrayToString(propertyValueCollection));
    	
    }
    
    protected void loadPropertyValues(Map<String,String> config) throws JposException
    {
    	Set<String> keys = config.keySet();
    	for(String key : keys)
    	{   
    		String key_l = key.toLowerCase();
    		if (properties.contains(key_l)==true && propertyValues.containsKey(key_l)==false)
    		{
    			String value = config.get(key);
    			if (propertyValueCollections.containsKey(key_l))
    			{
    				// Verify that the value is one of the defined values
    				String[] valueCollection = propertyValueCollections.get(key_l);
    				boolean found =false;    				
    				for (int i=0;i<valueCollection.length;i++)
    					if (valueCollection[i].equalsIgnoreCase(value)) 
    					{found=true; break;}
    				if (!found) propertyError("Invalid Property value: "
                            +key_l+"=\""+value+"\". Expected: "+StringUtil.arrayToString(valueCollection));
    			}
    			propertyValues.put(key_l, value);
    			//log(key +"=\""+value+"\"");
    		}
    	}    	
    }
    
    protected void loadPropertyValues(JposEntry config) throws JposException
    {
    	
    	Iterator<JposEntry.Prop> keys = config.getProps();
    	while (keys.hasNext())
    	{
    		JposEntry.Prop prop = keys.next();
    		String key = prop.getName();
    		String key_l = key.toLowerCase();
    		if (properties.contains(key_l)==true && propertyValues.containsKey(key_l)==false)
    		{
    			String value = prop.getValueAsString();
    			if (propertyValueCollections.containsKey(key_l))
    			{
    				// Verify that the value is one of the defined values
    				String[] valueCollection = propertyValueCollections.get(key_l);
    				boolean found =false;    				
    				for (int i=0;i<valueCollection.length;i++)
    					if (valueCollection[i].equalsIgnoreCase(value)) 
    					{found=true; break;}
    				if (!found) propertyError("Invalid Property value: "
                            +key_l+"=\""+value+"\". Expected: "+StringUtil.arrayToString(valueCollection));
    			}
    			propertyValues.put(key_l, value);
    			//log(key +"=\""+value+"\"");
    		}
    	}
    	
    }
    
	public List<String> getPropertNames()
	{
		return properties;
	}
	
	public String getPropertyDescription(String propertyName)
	{
		propertyName = propertyName.toLowerCase();
		if (propertyDescriptions.containsKey(propertyName)) 
			return propertyDescriptions.get(propertyName);
		else return null;			
	}
	
	public String getPropertyValue(String propertyName)
	{
		propertyName = propertyName.toLowerCase();
		if (propertyValues.containsKey(propertyName)) 
			return propertyValues.get(propertyName);
		else if (propertyDefaults.containsKey(propertyName))
			return propertyDefaults.get(propertyName);			
		else return null;					
	}
	
	public int getPropertyValueInteger(String propertyName) throws JposException
	{
		String temp;
		try
		{
			temp = getPropertyValue(propertyName);
			return Integer.parseInt(temp);
		}
		catch(NumberFormatException e)
		{
			logError("Integer expected for property " + propertyName + ". temp "+ " is not a valid integer.");
    		throw new JposException(JposConst.JPOS_E_NOSERVICE,"Integer expected for property "
                    + propertyName + ". temp "+ " is not a valid integer.");
		}
	}

	private void propertyError(String errorMessage) throws JposException
	{
		logError("error: " + errorMessage);
		throw new JposException(JposConst.JPOS_E_FAILURE,propertyModule+": "+errorMessage);
	}    
}
