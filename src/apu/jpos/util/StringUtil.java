package apu.jpos.util;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class StringUtil {
	public StringUtil(){}

    public static boolean empty(String s)
    {
        if (s==null || s.trim().length()==0) return true;
        else return false;
    }

    public static String paramEncode(String p)
    {
        if (p==null) return "#NULL";
        else return p.replace("#","##").replace(",","#COMMA").replace("\r\n","#CRLF");
    }
    public static String paramDecode(String p)
    {
        if (p==null) return null;
        else if (p.equals("#NULL")) return null;
        else return p.replace("#COMMA",",").replace("#CRLF,","\r\n").replace("##","#");
    }

    public static String joinStrings(List<String> source,String seperator)
    {
        if (source.isEmpty()) return "";
        StringBuilder sb = new StringBuilder();
        boolean addSep = false;
        for(String s:source) {
            if (addSep) sb.append(seperator);
            sb.append(s);
            addSep=true;
        }
        return sb.toString();
    }

    public static String between(String source, String start, String end)
    {
        try
        {
            int i1 = source.indexOf(start);
            if (i1==-1) return "";
            int i2 = source.indexOf(end,i1+start.length()+1);
            if (i2==-1)
            {
                String ret = source.substring(i1+start.length());
                if (ret==null) return "";
                return ret.trim();
            }
            else
            {
                String ret = source.substring(i1+start.length(),i2);
                if (ret==null) return "";
                return ret.trim();
            }
        }
        catch (Exception ex)
        {
            return "";
        }

    }

    public static String arrayToString(String[] source)
	{
		if (source==null) return "NULL";
		if (source.length==0) return "[]";
		StringBuilder ret = new StringBuilder();
		ret.append('[');
		ret.append(source[0]);
		for (int i=1;i<source.length;i++) {ret.append(',');ret.append(source[i]);}
		ret.append(']');
		return ret.toString();
	}
	
	public static String asciiParse(String data)
	{
		if (data == null) return "";
		int l = data.length();
		int pos = 0;
		StringBuilder sb = new StringBuilder();
		while (pos<l)
		{
			if (pos+3<l && data.charAt(pos)=='[' && data.charAt(pos+3)==']'){
                try {
                    int v = Integer.parseInt(data.substring(pos+1, pos+3),16);
                    sb.append((char)v);
                    pos+=4;
                } catch (NumberFormatException e) {
                    sb.append(data.charAt(pos++));
                }
            }
			else sb.append(data.charAt(pos++));
		}
		
		return sb.toString();
	}

    public static byte[] asciiParseBinary(String data)
    {
        ByteBuffer ret = ByteBuffer.allocate(data.length());
        if (data == null) return new byte[0];
        int l = data.length();
        int pos = 0;
        while (pos<l)
        {
            if (pos+3<l && data.charAt(pos)=='[' && data.charAt(pos+3)==']'){
                try {
                    int v = Integer.parseInt(data.substring(pos+1, pos+3),16);
                    ret.put((byte)v);
                    pos+=4;
                    continue;
                } catch (NumberFormatException e) {
                }
            }
            ret.put((byte)data.charAt(pos++));
        }

        int n = ret.position();
        ret.rewind();
        byte[] ret_array = new byte[n];
        ret.get(ret_array);
        return ret_array;
    }


	public static String asciiFilter(String data)
	{
		if (data==null) return "";
		int sz = data.length();
		StringBuilder buffer = new StringBuilder(sz);
		buffer.setLength(0);
		for (int i=0;i<sz;i++) 
		{
			char c = data.charAt(i);
			if (c<' ' || c>'~') buffer.append(String.format("[%02x]", (int)c));
			else buffer.append(c);
		}
		return buffer.toString();
	}

    public static String asciiFilter(byte[] data)
    {
        byte a = (byte)' ';
        byte b = (byte)'~';
        if (data==null) return "";
        int sz = data.length;
        StringBuilder buffer = new StringBuilder(sz);
        buffer.setLength(0);
        for (int i=0;i<sz;i++)
        {
            if (data[i]<' ' || data[i]>'~') buffer.append(String.format("[%02x]", (int)data[i]));
            else buffer.append((char)data[i]);
        }
        return buffer.toString();
    }
	
	public static byte[] charArrayToBytes(char[] source)
	{
		return new String(source).getBytes();
	}
	
	public static void AppendLRC(StringBuilder sb)
	{
		int lrc = 0;
		int n = sb.length();
		for (int i=1;i<n;i++) lrc = lrc ^ (int)sb.charAt(i);
		sb.append((char)lrc);
	}

	public static int getLRC(String s)
	{
		int lrc = 0;
		for (int i=0;i<s.length();i++) lrc = lrc ^ (int)s.charAt(i);
		return lrc;
	}
		

	
	private static boolean isHex(char c)
	{
		if (c>='0' && c<='9') return true;
		else if (c>='a' && c<='f') return true;
		else if (c>='A' && c<='F') return true;
		else return false;
	}

	

	public static String captureRegex(Matcher m, int[] bounds)
	{
		if(m.find())
		{
			String str = m.group();
			if (m.groupCount()>0) {
				StringBuilder sb = new StringBuilder();
				for (int i=1;i<=m.groupCount();i++) sb.append(m.group(i));
				str=sb.toString();
			}
			int sz = str.length();
			bounds[0] = m.start();
			bounds[1] = m.end();
			return str;
		}
		else
		{
			bounds[0]=bounds[1]=-1;
			return null;
		}
	}

	public static byte[] toByta_BigEndian(int data) {
		return new byte[] { (byte) ((data >> 24) & 0xff),
				(byte) ((data >> 16) & 0xff), (byte) ((data >> 8) & 0xff),
				(byte) ((data >> 0) & 0xff)};
	}	
	public static byte[] toByta_LittleEndian(int data) {
		return new byte[] { (byte) ((data >> 0) & 0xff),
				(byte) ((data >> 8) & 0xff),
				(byte) ((data >> 16) & 0xff), 
				(byte) ((data >> 24) & 0xff),};
	}
	
	public static byte[] toByta_BigEndian(double data) 
	{    
		return toByta_BigEndian(Double.doubleToRawLongBits(data));
	}	
	public static byte[] toByta_LittleEndian(double data) 
	{    
		return toByta_LittleEndian(Double.doubleToRawLongBits(data));
	}	

	public static byte[] toByta_BigEndian(long data) {
		return new byte[] { 
				(byte) ((data >> 56) & 0xff),
				(byte) ((data >> 48) & 0xff), 
				(byte) ((data >> 40) & 0xff),
				(byte) ((data >> 32) & 0xff), 
				(byte) ((data >> 24) & 0xff),
				(byte) ((data >> 16) & 0xff), 
				(byte) ((data >> 8) & 0xff),
				(byte) ((data >> 0) & 0xff) };
	}
	public static byte[] toByta_LittleEndian(long data) {
		return new byte[] { 
				(byte) ((data >> 0) & 0xff), 
				(byte) ((data >> 8) & 0xff),
				(byte) ((data >> 16) & 0xff), 
				(byte) ((data >> 24) & 0xff),
				(byte) ((data >> 32) & 0xff), 
				(byte) ((data >> 40) & 0xff), 
				(byte) ((data >> 48) & 0xff), 
				(byte) ((data >> 56) & 0xff)
				};
	}	
	
	public static long getCRC32(String s)
	{
		byte bytes[] = s.getBytes();
		Checksum checksum = new CRC32();
		checksum.update(bytes,0,bytes.length);	
		return checksum.getValue();
	}
	public static long getAdler32(String s)
	{
		byte bytes[] = s.getBytes();
		Checksum checksum = new java.util.zip.Adler32();
		checksum.update(bytes,0,bytes.length);	
		return checksum.getValue();
	}
	
    private static int[] table = {
            0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
            0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
            0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
            0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
            0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
            0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
            0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
            0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
            0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
            0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
            0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
            0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
            0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
            0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
            0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
            0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
            0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
            0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
            0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
            0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
            0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
            0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
            0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
            0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
            0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
            0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
            0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
            0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
            0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
            0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
            0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
            0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040,
        };

	public static long getCRC16(String s)
	{
        byte[] bytes = s.getBytes();
        int crc = 0x0000;
        for (byte b : bytes) {
            crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
        }
        return crc;
	}
	
	public static long getCRC16CCIT(String s) {
		long crc = 0xFFFF; // initial value
		long polynomial = 0x1021; // 0001 0000 0010 0001 (0, 5, 12)

		byte[] bytes = s.getBytes();

		for (byte b : bytes) {
			for (int i = 0; i < 8; i++) {
				boolean bit = ((b >> (7 - i) & 1) == 1);
				boolean c15 = ((crc >> 15 & 1) == 1);
				crc <<= 1;
				if (c15 ^ bit)
					crc ^= polynomial;
			}
		}

		crc &= 0xffff;
		return crc;
	}
	
	public static String getMD5(String s)
	{
		byte[] defaultBytes = s.getBytes();
		try{
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] = algorithm.digest();
		            
			StringBuffer hexString = new StringBuffer();
			for (int i=0;i<messageDigest.length;i++) {
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			}
			return hexString.toString();
			
		}catch(NoSuchAlgorithmException nsae){
		    return "";        
		}
	}

    public static byte[] stringToByte(String s)
    {
        try
        {
            return s.getBytes("US-ASCII");
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    public static String stringToHex(String s)
    {
        return byteToHex(stringToByte(s));
    }

    public static String byteToHex (byte buf[]) {
        try
        {
            StringBuffer strbuf = new StringBuffer(buf.length * 2);
            int i;

            for (i = 0; i < buf.length; i++) {
                if (((int) buf[i] & 0xff) < 0x10)
                strbuf.append("0");
                strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
            }
            return strbuf.toString();
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public static String byteToHexSep(byte buf[],String startSep,String middleSep, String endSep) {
        try
        {
            StringBuffer strbuf = new StringBuffer(buf.length * 2);
            int i;
            if (startSep!=null) strbuf.append(startSep);
            for (i = 0; i < buf.length; i++) {
                if (i>0 && middleSep!=null) strbuf.append(middleSep);
                if (((int) buf[i] & 0xff) < 0x10)
                strbuf.append("0");
                strbuf.append(Long.toString((int) buf[i] & 0xff, 16));

            }
            if (endSep!=null) strbuf.append(endSep);
            return strbuf.toString();
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public static byte[] hexToByte(String hex)
    {
        try
        {
            if (hex.length()%2!=0) return null;
            byte[] ret = new byte[hex.length()/2];
            for (int i=0;i<hex.length();i+=2)
            {
              ret[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4)
                      + Character.digit(hex.charAt(i+1), 16));
            }
            return ret;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static boolean hasUpperCase(String s)
    {
        if (s==null) return false;
        for(char c:s.toCharArray())
            if (Character.isUpperCase(c)) return true;
        return false;
    }

    public static boolean hasLowerCase(String s)
    {
        if (s==null) return false;
        for(char c:s.toCharArray())
            if (Character.isLowerCase(c)) return true;
        return false;
    }

    public static String byteToString(byte[] buf)
    {
        try
        {
            String ret = new String(buf,"US-ASCII");
            return ret;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public static String byteToString(byte[] buf,int offset, int length, String encoding)
    {
        try
        {
            String ret = new String(buf,offset,length,encoding);
            return ret;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public static String hextoString(String hex)
    {
        return byteToString(hexToByte(hex));
    }

    public static String encrypt(String valueToEnc,String keyValue,String ALGORITHM)
    {
        try
        {
        Key key = new SecretKeySpec(stringToByte(keyValue), ALGORITHM);
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encValue = c.doFinal(valueToEnc.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encValue);
        return encryptedValue;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public static char[] digits = "0123456789ABCDEF".toCharArray();
    public static StringBuffer convBuffer = new StringBuffer(32);

    public static String toHex(byte value, int nDigits)
    {
        if (nDigits>2) nDigits=2;
        return toHex((long)value & 0xffl, nDigits);
    }
    public static String toHex(int value, int nDigits)
    {
        if (nDigits>8) nDigits=8;
        return toHex((long)value & 0xffffffffl, nDigits);
    }
    public static String toHex(long value,int nDigits)
    {
        if (nDigits>16) nDigits=16;
        StringBuffer b = convBuffer;
        b.setLength(0);
        for (int i=0;i<nDigits;i++)
        {
            byte v = 0;
            if (i==0) v = (byte)(value & 0x0f);
            else v = (byte)((value>>(4*i)) & 0x0f);
            b.append(digits[v]);
        }
        return b.reverse().toString();
    }


    public static String decrypt(String encryptedValue,String keyValue,String ALGORITHM)
    {
        try
        {
            Key key = new SecretKeySpec(stringToByte(keyValue), ALGORITHM);
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(Cipher.DECRYPT_MODE, key);
            byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
            byte[] decValue = c.doFinal(decordedValue);
            String decryptedValue = new String(decValue);
            return decryptedValue;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public static String encryptAES(String valueToEnc,String keyValue)
    {
        if (keyValue!=null && keyValue.length()>16) keyValue=keyValue.substring(0,16);
        return encrypt(valueToEnc,keyValue,"AES");
    }

    public static String decryptAES(String encryptedValue,String keyValue)
    {
        if (keyValue!=null && keyValue.length()>16) keyValue=keyValue.substring(0,16);
        return decrypt(encryptedValue,keyValue,"AES");
    }

    public static StringBuilder flsb = new StringBuilder();
    public static String fixedLengthString(String v, int length, char padChar, boolean padLeft)
    {
        int vl = v.length();
        if (vl>length) return v.substring(0,length);
        else if (vl==length) return v;
        flsb.setLength(0);
        if (!padLeft) flsb.append(v);
        for(int i=0;i<length-vl;i++) flsb.append(padChar);
        if (padLeft) flsb.append(v);
        return flsb.toString();
    }


	
}
