package apu.jpos.util;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/10/11
 * Time: 6:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class Loggable {
    protected SimpleLogger logger;

    public Loggable(SimpleLogger logger)
    {
        this.logger=logger;
    }
    public Loggable()
    {
        this.logger = SimpleLogger.getDefaultLogger();
    }


    protected String getModuleName()
    {
        StackTraceElement ste = null;
        StackTraceElement[] stList = Thread.currentThread().getStackTrace();
        for(StackTraceElement st:stList)
        {
            String method =  st.getMethodName().toLowerCase();
            String className = st.getClassName();
            if (!className.contains("Loggable") && !method.contains("log") && !method.contains("getstacktrace")) {ste=st;break;}
        }
        if (ste==null) ste=stList[3];

        return ste.getClassName() + "." + ste.getMethodName() +"():"+ste.getLineNumber();
    }

    protected void log(Exception ex)
    {
        try {
            if (logger!=null)
            {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                ex.printStackTrace(pw);
                String msg = " ::[" + ex.getClass().getName() + "]:: " + ex.getMessage()
                    +"\r\n\r\n"+ sw.toString() +"\r\n\r\n";
                logger.logRaw(getModuleName(), "ERROR", msg);
            }
        } catch (Exception e) {
            // Do nothing...
        }
    }

    /*protected void logAndThrow(PortIOException ex) throws PortIOException
    {
        try {
            if (logger!=null)
            {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                ex.printStackTrace(pw);
                String msg = " ::[" + ex.getClass().getName() + "]:: " + ex.getMessage()
                        +"\r\n\r\n"+ sw.toString() +"\r\n\r\n";
                logger.logRaw(getModuleName(), "ERROR", msg);
            }
        } catch (Exception e) {
            // Do nothing...
        }
        finally {
            throw ex;
        }
    }*/

    protected void logAndThrow(Exception ex) throws Exception
    {
        try {
            if (logger!=null)
            {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                ex.printStackTrace(pw);
                String msg = " ::[" + ex.getClass().getName() + "]:: " + ex.getMessage()
                        +"\r\n\r\n"+ sw.toString() +"\r\n\r\n";
                logger.logRaw(getModuleName(), "ERROR", msg);
            }
        } catch (Exception e) {
            // Do nothing...
        }
        finally {
            throw ex;
        }
    }

    protected void log(String message)
    {
        if (logger!=null)
        {
            logger.log(getModuleName(),"INFO",message);
        }
    }
    protected void logRaw(String message)
    {
        if (logger!=null)
        {
            logger.logRaw(getModuleName(),"INFO",message);
        }
    }

    protected void logError(String message)
    {
        if (logger!=null)
        {
            logger.logRaw(getModuleName(),"ERROR",message);
        }
    }

    protected void logWarning(String message)
    {
        if (logger!=null)
        {
            logger.log(getModuleName(),"WARNING",message);
        }
    }

}
