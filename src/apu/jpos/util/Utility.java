/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package apu.jpos.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;
import java.text.DateFormat;
import java.net.*;
import java.util.zip.CheckedInputStream;
import java.util.zip.CRC32;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
import java.io.*;

//import com.sun.mail.smtp.SMTPSSLTransport;
import com.sun.org.apache.bcel.internal.generic.RET;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.w3c.dom.*;
//import org.xml.sax.*;
//import javax.mail.PasswordAuthentication;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
import javax.xml.parsers.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.*;
//import javax.mail.*;
//import javax.xml.transform.*;
//import javax.xml.transform.dom.*;
//import javax.xml.transform.stream.*;
import java.security.*;
import java.math.*;
import java.io.StringWriter;



/**
 *
 * @author rapu
 */
public class Utility {
    public static boolean isWindows = false;

    public static void initOS()
    {
        String os = System.getProperty("os.name");
        if (os.toLowerCase().contains("windows"))
            isWindows=true;
        else isWindows=false;
    }

    public static String Shell(String[] command)
    {
        try
        {
            Process process = Runtime.getRuntime().exec(command);
            process.waitFor();
            InputStream pis = process.getInputStream();
            int n = pis.available();
            byte[] buf = new byte[n];
            pis.read(buf);
            return StringUtil.byteToString(buf);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public static String shellError = null;

    public static String ShellExecuteScriptAsync(String command)
    {
        try
        {
            shellError=null;
            ProcessBuilder pb = new ProcessBuilder(command);
            Process process = pb.start();
            process.waitFor();
            InputStream pis = process.getInputStream();
            int n = pis.available();
            byte[] buf = new byte[n];
            pis.read(buf);
            return StringUtil.byteToString(buf);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            shellError = "Cannot Execute \"" + command + "\"... Reason: "+ ex.toString();
            return null;
        }
    }

    public static String Shell(String command)
    {
       try
       {
           shellError=null;
           Process process = Runtime.getRuntime().exec(command);
           process.waitFor();
           InputStream pis = process.getInputStream();
           int n = pis.available();
           byte[] buf = new byte[n];
           pis.read(buf);
           return StringUtil.byteToString(buf);
       }
       catch(Exception ex)
       {
           ex.printStackTrace();
           shellError = "Cannot Execute \"" + command + "\"... Reason: "+ ex.toString();
           return null;
       }
    }
    
    public static String getExceptionTrace(Exception ex)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        return sw.toString();
   }
    
    public static String Dir(String dirname)
    {
        try
        {
            if (isWindows)
            {
                return dirname.replace('/','\\');
            }
            else
            {
                return dirname.replace('\\','/');
            }
        }
        catch(Exception ex)
        {
            return dirname;
        }
    }

    public static boolean isDirSeperator(char c)
    {
        if (c=='/' || c=='\\') return true;
        else return false;
    }


    public static String getParentDir(String fullpath)
    {
        try
        {
            int n=fullpath.length()-1;
            if (n>=0 && isDirSeperator(fullpath.charAt(n))) n--;
            while (n>=0)
            {
                if (isDirSeperator(fullpath.charAt(n)))
                {
                    //if (n>0) return fullpath.substring(0, n-1);
                    if (n>0) return fullpath.substring(0, n);
                    else return "/";
                }
                n--;
            }
            return "";
        }
        catch(Exception ex)
        {
            return "";
        }
    }

    public static String getLastDirElement(String fullpath)
    {
        try
        {
            int n=fullpath.length()-1;
            if (n>=0 && isDirSeperator(fullpath.charAt(n))) 
            {
                n--;
                fullpath=fullpath.substring(0,n);
            }
            while (n>=0)
            {
                if (isDirSeperator(fullpath.charAt(n)))
                {
                    if (n<fullpath.length()-1) return fullpath.substring(n+1);
                    else return "";
                }
                n--;
            }
            return "";
        }
        catch(Exception ex)
        {
            return "";
        }
    }

    public static String appendDir(String rootPath, String pathToAppend)
    {
        try
        {
            if (rootPath==null && pathToAppend==null) return "";
            else if (rootPath==null) return pathToAppend;
            else if (pathToAppend==null) return rootPath;
            else if (rootPath.trim().isEmpty()) return pathToAppend;
            else if (pathToAppend.trim().isEmpty()) return rootPath;
            else
            {
                String r=rootPath;
                int n=r.length();
                if (r.charAt(n-1)!='/' && r.charAt(n-1)!='\\') r+="/";
                if (pathToAppend.charAt(0)!='/'
                        && pathToAppend.charAt(0)!='\\') r+=pathToAppend;
                else r+=pathToAppend.substring(1);
                return Dir(r);
            }
        }
        catch(Exception ex)
        {
            return pathToAppend;
        }
    }

    public static boolean fileExists(String fullpath)
    {
        try
        {
            File f= new File(fullpath);
            if (f.exists())
            {
                if(f.isFile()) return true;
            }

        }
        catch(Exception ex)
        {
        }


        return false;
    }
    
    public static boolean directoryExists(String fullpath)
    {
        try
        {
            File f= new File(fullpath);
            if (f.exists())
            {
                if(f.isDirectory()) return true;
            }

        }
        catch(Exception ex)
        {
        }


        return false;
    }

    public static boolean createDirectory(String fullpath)
    {
        try
        {
            File f= new File(fullpath);
            if (f.exists() && f.isFile()) return false;
            else if (f.exists() && f.isDirectory()) return true;
            else if (f.mkdirs()) return true;
            else return false;
        }
        catch(Exception ex)
        {
        }


        return false;
        
    }

    public static String getLog(String logMessage, String ThreadID)
    {
        Date t = new Date();
        return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(t)
                + " ::"+ThreadID+":: =>> " + logMessage + "\r\n";
    }

    public static String encodeURL(String data)
    {
        try
        {
            if (data==null || data.isEmpty()) return "";
            return URLEncoder.encode(data, "UTF8");
        }
        catch(Exception ex)
        {
            return "";
        }
    }

    public static String decodeURL(String data)
    {
        try
        {
            if (data==null || data.isEmpty()) return "";
            return URLDecoder.decode(data, "UTF8");
        }
        catch(Exception ex)
        {
            return "";
        }
    }
    
    public static long fileCRC32(String fullPath, long filesize)
    {
        try {

            CheckedInputStream cis = null;
            //long fileSize = 0;
            try {
                // Computer CRC32 checksum
                cis = new CheckedInputStream(
                        new FileInputStream(fullPath), new CRC32());

                //fileSize = new File(fullPath).length();

            } 
            catch (FileNotFoundException e)
            {
                return -1;
            }
            byte[] buf = new byte[65536];
            long total=0;
            long prog = 0;
            while((prog=cis.read(buf)) >= 0) {
                total+=prog;
                prog=(total*100)/filesize;
                //progress.setState("Computing Checksum..."+Long.toString(prog)+"%");
            }

            long checksum = cis.getChecksum().getValue();
            return checksum;
        } 
        catch (IOException e)
        {
            return -1;
        }


    }

    public static long fileSize(String fullPath)
    {
        try
        {
            File f= new File(fullPath);
            if (f.exists() && f.isFile())
            {
                return f.length();
            }
            else return -1;

        }
        catch(Exception ex)
        {
            return -1;
        }
    }
    public static String Read_File(String fullPath)
    {
        try
        {
            File f= new File(fullPath);
            if (f.exists() && f.isFile())
            {
                StringBuffer fileData = new StringBuffer(1000);
                FileInputStream reader = new FileInputStream(f);
                byte[] buf = new byte[1024];
                int numRead=0;
                int totalBytes = 0;
                while((numRead=reader.read(buf)) != -1){
                    totalBytes+=numRead;
                    String readData = StringUtil.byteToString(buf,0,numRead,"US-ASCII");
                    fileData.append(readData);
                    //buf = new char[1024];
                    if (totalBytes!=fileData.length())
                    {
                        System.out.println("Something is wrong!");
                    }
                }
                reader.close();
                return fileData.toString();
            }
            else return "";

        }
        catch(Exception ex)
        {
            return "";
        }
    }

    public static boolean stringToFile(String fullPath,String contentBuffer)
    {
        try
        {
            File outFile = new File(fullPath);
            FileWriter out = new FileWriter(outFile);
            out.write(contentBuffer);
            out.flush();
            out.close();
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }

    }

    public static boolean truncateFile(String fileName, int newSize)
    {
        try
        {
            File file = new File(fileName);
            if (file.exists()==false) return false;
            int sz = (int)file.length();
            if (sz<=newSize) return true; // no need to truncate...
            int offset = sz-newSize;
            byte[] buffer = new byte[newSize];
            RandomAccessFile rFile = new RandomAccessFile(file,"rw");
            rFile.seek(offset);
            rFile.readFully(buffer);
            rFile.seek(0);
            rFile.write(buffer);
            rFile.setLength(newSize);
            rFile.close();
            return true;
        }
        catch(IOException ex)
        {
            return false;
        }
    }

    public static String readFile(String fullPathFilename) throws IOException
    {
        RandomAccessFile raf = new RandomAccessFile(fullPathFilename, "r");
        byte[] bytes = new byte[(int)raf.length()];

        raf.seek(0);
        raf.readFully(bytes);
        raf.close();
        return new String( bytes, "latin1");

    }
    public static String readFileTail(String fullPathFilename, int charsToRead, String charSet) throws IOException{
        if (charSet == null) charSet = "latin1";
        RandomAccessFile raf = new RandomAccessFile(fullPathFilename, "r");
        long posToStart = raf.length() - charsToRead;
        byte[] bytes = new byte[charsToRead];

        raf.seek(posToStart);
        raf.readFully(bytes);
        raf.close();
        return new String( bytes, charSet);
    }
    public static String readFileHead(String fullPathFilename, int charsToRead, String charSet) throws IOException{
        if (charSet == null) charSet = "latin1";
        RandomAccessFile raf = new RandomAccessFile(fullPathFilename, "r");
        byte[] bytes = new byte[charsToRead];
        raf.seek(0);
        raf.readFully(bytes);
        raf.close();
        return new String( bytes, charSet);
    }

    public static boolean appendStringToFile(String fullPath,String contentBuffer,int sizeLimit)
    {
        try
        {
            File outFile = new File(fullPath);
            if (outFile.exists() && outFile.length()>sizeLimit)
            {
                // Truncate the file size
                /*String buf = Read_File(fullPath);
                buf+=contentBuffer;
                buf=buf.substring(buf.length()/2);
                FileWriter out = new FileWriter(outFile,false);
                out.write(contentBuffer);
                out.flush();
                out.close();*/

                File outFile2 = new File(fullPath+".old");
                if (outFile2.exists()) outFile2.delete();
                outFile.renameTo(outFile2);
                RandomAccessFile source = new RandomAccessFile(outFile2,"r");
                source.seek(outFile2.length()/2);
                FileOutputStream target = new FileOutputStream(outFile,false);

                byte[] buffer = new byte[8096];
                while(true)
                {
                    int n = source.read(buffer);
                    if (n<=0) break;
                    target.write(buffer,0,n);
                }

                target.flush();
                target.close();
                source.close();

            }
            else
            {
                outFile.createNewFile();
                FileWriter out = new FileWriter(outFile,true);
                out.write(contentBuffer);
                out.flush();
                out.close();
            }
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }

    }

    public static void writeBinaryFile(byte[] contents, String fullPathFilename) throws IOException{
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fullPathFilename));
        bos.write(contents);
        bos.flush();
        bos.close();

    }

    public static boolean fileLocked(String fullPath, String accessMode)
    {
        try
        {
            if (!fileExists(fullPath)) return false;
             RandomAccessFile f = new RandomAccessFile(fullPath,accessMode);
             if (f!=null)
                f.close();

            return false;
        }
        catch(Exception ex)
        {
            return true;
        }
    }


    public static boolean clearDir(String fullPath, int retries)
    {
        if (!Utility.directoryExists(fullPath)) return false;
            try
            {
                File cdir = new File(fullPath);
                String[] files = cdir.list();
                int scount=0;
                for (int i=0;i<files.length;i++)
                {
                    String filepath=appendDir(fullPath, files[i]);
                    if (deleteFile(filepath, retries)) scount++;
                }
                if (scount==files.length) return true;
                else return false;
            }
            catch(Exception ex)
            {
                return false;
            }

    }


    public static boolean deleteFile(String fullPath, int retries)
    {
        try
        {
            if (!fileExists(fullPath)) return false;
            for (int i=0;i<retries;i++)
            {
                try
                {
                    File f = new File(fullPath);
                    if (f.delete()) return true;
                }
                catch(Exception ex)
                {
                    // nothing...
                }

            }

            return false;
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    public static int countFileWords(String fullPathFilename) throws IOException{

       BufferedReader reader = new BufferedReader(new FileReader(fullPathFilename));

       String line;
       char[] chars;
       int wordCount = 0;

       while( (line = reader.readLine()) != null){

           chars = line.toCharArray();
           wordCount++;

           for (int i = 1 ; i < chars.length - 1; i++){
               if (Character.isWhitespace(chars[i]) &&  Character.isJavaIdentifierPart(chars[i+1])) {
                   wordCount++;

               }
           }

       }

       reader.close();

       return wordCount;

    }


    public static String[][] readCSV(String fullPathFilename, int numToRead) throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader(fullPathFilename));
       String line;
       StringTokenizer st ;
       int size = 0;
       int index = 0;
       int pos = 1; //skip the first position.
       String[][] data;
       String[][] temp;
       final int DEFAULT_SIZE=250;
       final String DELIMITER = ",";
       int maxRecords;

       //read the first line to get the size of the array
       line = reader.readLine();
       st= new StringTokenizer(line, DELIMITER);
       while (st.hasMoreElements()){
           st.nextElement();
           size++;
       }

       //size the array, if parameter not set, read the entire file
       //otherwise stop as requested.
       if (numToRead == 0) {
           numToRead = DEFAULT_SIZE;
           maxRecords = Integer.MAX_VALUE;
       } else {
           maxRecords = numToRead;
       }

       data = new String[numToRead][size];

       //do it again to add to the array
       st= new StringTokenizer(line, DELIMITER);
       while (st.hasMoreElements()){
           data[0][index]= st.nextElement().toString();
           index++;
       }
       index=0;

       //now do a bunch..
       while( (line = reader.readLine()) != null && pos < maxRecords ){
           st= new StringTokenizer(line, DELIMITER);
           while (st.hasMoreElements()){
               if (index == data[0].length) break;
               data[pos][index]= st.nextElement().toString();
               index++;
           }
           index=0;
           pos++;

           if (pos == data.length - 1){ //size array if needed.
               temp = new String[data.length + numToRead][size];
               for (int n=0;n < data.length;n++){
                   System.arraycopy(data[n],0,temp[n],0,temp[n].length);
               }
               data = temp;

           }

       }

       //size the array correctly.. it may be too large.
       temp = new String[pos][size];// the size read
       for (int n=0;n < temp.length;n++){
           System.arraycopy(data[n],0,temp[n],0,temp[n].length);
       }

       reader.close();
       return temp;

    }

    public static String getCurrentDir()
    {
        try
        {
            File dir = new File(".");
            return dir.getCanonicalPath();
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    public static String getUserDir()
    {
        try
        {
            return System.getProperty("user.dir");
        }
        catch (Exception ex)
        {
            return "";
        }
    }


    public static void copyFile(File source, File destination) throws IOException{

        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(source));
       BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destination));
       int numRead;
       byte[] bytes = new byte[1024];
       while ((numRead = bis.read(bytes)) != -1) {
           bos.write(bytes,0,numRead);
       }

       try{
           bis.close();
       } catch (Exception e){}

       try{
           bos.close();
       } catch (Exception e){}
    }

    public static String mapPath(String relativePath)
    {
        try {
            return appendDir(System.getProperty("user.dir"),relativePath);
        } catch (Exception e) {
            return null;
        }
    }


    public static Document xmlFromString(String xmlText)
    {
        try
        {
            // Create a factory
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            // Use document builder factory
            DocumentBuilder builder = factory.newDocumentBuilder();
            //Parse the document
            Reader reader=new CharArrayReader(xmlText.toCharArray());
            Document doc = builder.parse(new org.xml.sax.InputSource(reader));
            return doc;
        }
        catch(Exception ex)
        {
            return null;
        }
    }


    public static Document xmlFromFile(String fullPath)
    {
        String xmlText = Read_File(fullPath);
        if (!xmlText.isEmpty()) return xmlFromString(xmlText);
        else return null;
    }


    public static String xmlElementValue(Document xml, String elementname)
    {
        try
        {
            NodeList nl = xml.getElementsByTagName(elementname);
            if (nl.getLength()<=0) return "";
            Node n=nl.item(0);
            return n.getTextContent();
        }
        catch(Exception ex)
        {
            return "";
        }
    }

    public static Element newXmlElement(Document xml, Element parent, String tagName, String tagText)
    {
        try
        {
            if (tagName==null || tagName.isEmpty()) return null;
            Element e=xml.createElement(tagName);
            if (tagText!=null && !tagText.isEmpty())
            {
                Node n = xml.createTextNode(tagText);
                e.appendChild(n);
            }
            if (parent!=null) parent.appendChild(e);
            return e;
        }
        catch(Exception ex)
        {
            return null;
        }

    }

    public static Document newXml(String nameSpace, String qualifiedName)
    {
        try
        {
            Document xmldoc = null;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation impl = builder.getDOMImplementation();
            xmldoc = impl.createDocument(nameSpace, qualifiedName, null);
            return xmldoc;
        
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public static String XML_To_String(Document xml)
    {
        try
        {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //initialize StreamResult with File object to save to file
            StreamResult result = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(xml);
            transformer.transform(source, result);

            String xmlString = result.getWriter().toString();
            return xmlString;
        }
        catch(Exception ex)
        {
            return "";
        }
    }

    public static boolean xmlToFile(Document xml, String fullPath)
    {
        try
        {
            String xmlText=Utility.XML_To_String(xml);
            return Utility.stringToFile(fullPath, xmlText);
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    public static String base64(String sourceText)
    {
        sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
        return enc.encode(sourceText.getBytes());
    }
    
    public static String md5(String sourceText)
    {
        try
        {

            MessageDigest m=MessageDigest.getInstance("MD5");
            m.update(sourceText.getBytes(),0,sourceText.length());
            return (new BigInteger(1,m.digest())).toString(16);
        }
        catch(Exception ex)
        {
            return "";
        }

    }

    public static String md5FromFile(String fileName)
    {
        try
        {
            InputStream fis =  new FileInputStream(fileName);

            byte[] buffer = new byte[1024];
            MessageDigest complete = MessageDigest.getInstance("MD5");
            int numRead;

            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            fis.close();
            return (new BigInteger(1,complete.digest())).toString(16);
        }
        catch(Exception ex)
        {
            return "";
        }

    }



    public static Date parseDate(String dateText)
    {
        int[] styles = {DateFormat.FULL,DateFormat.LONG,DateFormat.MEDIUM, DateFormat.SHORT};

        for (int i=0;i<4;i++) for (int j=0;j<4;j++)
        {
            try
            {
                Date d=DateFormat.getDateTimeInstance(styles[i],styles[j]).parse(dateText);
                return d;
            }
            catch(Exception ex)
            {
                //nothing....
            }

        }

        for (int i=0;i<4;i++)
        {
            try
            {
                Date d=DateFormat.getDateInstance(styles[i]).parse(dateText);
                return d;
            }
            catch(Exception ex)
            {
                //nothing....
            }
        }

        return null;
    }

    public static String getHostName()
    {
        try
        {
            return InetAddress.getLocalHost().getHostName();
        }
        catch (Exception ex)
        {
            return "localhost";
        }
    }

    public static List<String> getAllMacAddress()
    {
        try
        {
            ArrayList<String> ret = new ArrayList<String>();
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements())
            {
                NetworkInterface ni = networkInterfaces.nextElement();
                if (ni.isVirtual()) continue;
                byte[] mac = ni.getHardwareAddress();
                if (mac!=null && mac.length==6)
                    ret.add(StringUtil.byteToHex(mac));
            }
            return ret;
        }
        catch (Exception ex)
        {
            return new ArrayList<String>();
        }

    }

    public static String getCurrentMacAddress()
    {
        try
        {
            InetAddress ip = InetAddress.getLocalHost();
            NetworkInterface net = NetworkInterface.getByInetAddress(ip);
            if (net!=null)
            {
                byte[] macAddress = net.getHardwareAddress();
                if (macAddress!=null && macAddress.length==6)
                    return StringUtil.byteToHex(macAddress);
            }
            else
            {
                Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
                while (networkInterfaces.hasMoreElements())
                {
                    NetworkInterface ni = networkInterfaces.nextElement();
                    byte[] mac = ni.getHardwareAddress();
                    if (mac!=null && mac.length==6)
                        return StringUtil.byteToHex(mac);
                }
            }
            return "000000000000";
        }
        catch (Exception ex)
        {
            return "000000000000";
        }
    }

    public static String getMacAddress(String hostName)
    {
        try
        {
            InetAddress[] addresses = InetAddress.getAllByName(hostName);
            for(InetAddress address:addresses)
            {
                NetworkInterface ni = NetworkInterface.getByInetAddress(address);
                byte[] macaddress = ni.getHardwareAddress();
                if (macaddress!=null && macaddress.length==6)
                    return StringUtil.byteToHex(macaddress);
            }
            return "000000000000";
        }
        catch (Exception ex)
        {
            return "000000000000";
        }
    }

    public static long getElapsed(DateTime t)
    {
        DateTime now = new DateTime();
        Duration d = new Duration(t,now);
        return d.getMillis();
    }


    public static List toList(Object a)
    {
        ArrayList ret = new ArrayList(4);
        ret.add(a);
        return ret;
    }
    public static List toList(Object a,Object b)
    {
        ArrayList ret = new ArrayList(4);
        ret.add(a);
        ret.add(b);
        return ret;
    }
    public static List toList(Object a,Object b, Object c)
    {
        ArrayList ret = new ArrayList(8);
        ret.add(a);
        ret.add(b);
        ret.add(c);
        return ret;
    }
    public static List toList(Object a,Object b, Object c, Object d)
    {
        ArrayList ret = new ArrayList(8);
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        return ret;
    }
    public static List toList(Object a,Object b, Object c, Object d, Object e)
    {
        ArrayList ret = new ArrayList(16);
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        ret.add(e);
        return ret;
    }
    public static List toList(Object a,Object b, Object c, Object d, Object e, Object f)
    {
        ArrayList ret = new ArrayList(16);
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        ret.add(e);
        ret.add(f);
        return ret;
    }
    public static List toList(Object a,Object b, Object c, Object d, Object e, Object f, Object g)
    {
        ArrayList ret = new ArrayList(16);
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        ret.add(e);
        ret.add(f);
        ret.add(g);
        return ret;
    }
    public static List toList(Object a,Object b, Object c, Object d, Object e, Object f, Object g, Object h)
    {
        ArrayList ret = new ArrayList(16);
        ret.add(a);
        ret.add(b);
        ret.add(c);
        ret.add(d);
        ret.add(e);
        ret.add(f);
        ret.add(g);
        ret.add(h);
        return ret;
    }

    public static Object[] toArray(Object a)
    {
        Object[] ret = new RET[1];
        ret[0]=a;
        return ret;
    }
    public static Object[] toArray(Object a,Object b)
    {
        Object[] ret = new RET[2];
        ret[0]=a;
        ret[1]=b;
        return ret;
    }
    public static Object[] toArray(Object a,Object b,Object c)
    {
        Object[] ret = new RET[3];
        ret[0]=a;
        ret[1]=b;
        ret[2]=c;
        return ret;
    }
    public static Object[] toArray(Object a,Object b,Object c,Object d)
    {
        Object[] ret = new RET[4];
        ret[0]=a;
        ret[1]=b;
        ret[2]=c;
        ret[3]=d;
        return ret;
    }
    public static Object[] toArray(Object a,Object b,Object c,Object d,Object e)
    {
        Object[] ret = new RET[5];
        ret[0]=a;
        ret[1]=b;
        ret[2]=c;
        ret[3]=d;
        ret[4]=e;
        return ret;
    }
    public static Object[] toArray(Object a,Object b,Object c,Object d,Object e,Object f)
    {
        Object[] ret = new RET[6];
        ret[0]=a;
        ret[1]=b;
        ret[2]=c;
        ret[3]=d;
        ret[4]=e;
        ret[5]=f;
        return ret;
    }


    public static Map toMap(Object k1, Object v1)
    {
        HashMap ret = new HashMap(4);
        ret.put(k1,v1);
        return ret;
    }
    public static Map toMap(Object k1, Object v1,Object k2, Object v2)
    {
        HashMap ret = new HashMap(4);
        ret.put(k1,v1);
        ret.put(k2,v2);
        return ret;
    }
    public static Map toMap(Object k1, Object v1,Object k2, Object v2,Object k3, Object v3)
    {
        HashMap ret = new HashMap(8);
        ret.put(k1,v1);
        ret.put(k2,v2);
        ret.put(k3,v3);
        return ret;
    }
    public static Map toMap(Object k1, Object v1,Object k2, Object v2,Object k3, Object v3,Object k4, Object v4)
    {
        HashMap ret = new HashMap(8);
        ret.put(k1,v1);
        ret.put(k2,v2);
        ret.put(k3,v3);
        ret.put(k4,v4);
        return ret;
    }
    public static Map toMap(Object k1, Object v1,Object k2, Object v2,Object k3, Object v3,Object k4, Object v4,Object k5, Object v5)
    {
        HashMap ret = new HashMap(16);
        ret.put(k1,v1);
        ret.put(k2,v2);
        ret.put(k3,v3);
        ret.put(k4,v4);
        ret.put(k5,v5);
        return ret;
    }
    public static Map toMap(Object k1, Object v1,Object k2, Object v2,Object k3, Object v3,Object k4, Object v4
            ,Object k5, Object v5,Object k6, Object v6)
    {
        HashMap ret = new HashMap(16);
        ret.put(k1,v1);
        ret.put(k2,v2);
        ret.put(k3,v3);
        ret.put(k4,v4);
        ret.put(k5,v5);
        ret.put(k6,v6);
        return ret;
    }

    public static String collectionToString(Collection list)
    {
        try
        {
            StringBuilder sb=new StringBuilder();
            boolean sep=false;
            sb.append('[');
            for(Object v:list)
            {
                if (sep) sb.append(", ");
                sb.append(v.toString());
                sep=true;
            }
            sb.append(']');
            return sb.toString();
        }
        catch (Exception ex) {return "[ERROR]";}
    }

    public static int byteToInt(byte b)
    {
        return ((int)b) & 0xff;
    }

    public static int clamp(int v,int low, int high)
    {
        if (v<low) return low;
        else if (v>high) return high;
        else return v;
    }
    
    
    public static String emailHost = "";
    //public static SMTPAuthenticator emailAuthenticator = null;



    public static boolean sendEmailAlert(String subject, String message, String to, String from, SimpleLogger logger)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);

        try
        {
            if (emailHost==null || to==null || from==null) return false;
            String[] toList = to.split(";");
            if (toList==null || toList.length==0) return false;
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", emailHost);
            props.put("mail.smtp.debug","true");

            //emailAuthenticator = new SMTPAuthenticator("cnfltd\\russela","Un1verse");
            /*SMTPAuthenticator e = null;
            if (emailAuthenticator != null)
            {
                props.put("mail.smtp.port","25");
                //props.put("mail.smtp.submitter","LOGIN");
                //props.put("mail.smtp.debug","true");
                props.put("mail.smtp.auth", "true");
                //props.put("mail.smtp.auth.plain.disable", "true");
                props.put("mail.smtp.starttls.enable","true");

                props.put("mail.smtp.allow8bitmime", "true");
                //props.put("mail.smtp.socketFactory.port", "465");
                //props.put("mail.smtp.socketFactory.class", "javax.net.SocketFactory");
                //props.put("mail.smtp.socketFactory.fallback", "true");

                props.setProperty("mail.smtp.ssl.enable", "true");
                props.put("mail.smtp.socketFactory.port","25");
                props.put("mail.smtp.socketFactory.class","bongo.util.ExchangeSSLSocketFactory");
                props.put("mail.smtp.socketFactory.fallback","false");
                //props.put("mail.smtp.submitter", emailAuthenticator.getPasswordAuthentication().getUserName());
                e=emailAuthenticator;
            }*/
            //Session session = Session.getDefaultInstance(props,e);
            /*Session session = Session.getDefaultInstance(props);
            session.setDebugOut(ps);
            session.setDebug(true);
            Message msg = new MimeMessage(session);
            InternetAddress addressFrom = new InternetAddress(from);
            msg.setFrom(addressFrom);
            for(String x:toList) if (x!=null && !x.trim().equals(""))
            {
                InternetAddress addressTo = new InternetAddress(x);
                msg.addRecipient(Message.RecipientType.TO,addressTo);
            }
            if (subject==null) subject="";
            msg.setSubject(subject);
            msg.setContent(message,"text/plain");
            Transport.send(msg); */
            if (logger!=null)
            {
                logger.log("CORE_LIBRARY","INFO","Send Email LOG: " + baos.toString("UTF8"));
            }
            return true;
        }
        catch (Exception ex)
        {
            try {
                if (logger!=null)
                {
                    logger.log("CORE_LIBRARY","INFO","Send Email LOG: " + baos.toString("UTF8"));
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    ex.printStackTrace(pw);
                    String msg = " ::[" + ex.getClass().getName() + "]:: " + ex.getMessage()
                            +"\r\n\r\n"+ sw.toString() +"\r\n\r\n";
                    logger.logRaw("CORE_LIBRARY", "ERROR", msg);
                }
            } catch (Exception e) {
                // Do nothing...
            }
            return false;
        }
    }

    public static Long ipAddressToLong(String ip)
    {
        try
        {
            Inet4Address ipAddress = (Inet4Address)InetAddress.getByName(ip);
            if (ip!=null && ipAddress!=null)
            {
                byte[] adr = ipAddress.getAddress();
                ByteBuffer buf = ByteBuffer.wrap(adr).order(ByteOrder.BIG_ENDIAN);
                long adrInt = ((long)buf.getInt()) & 0xffffffffl;
                return adrInt;
            }
            else return null;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static String longToIpAddress(Long ip)
    {
        try
        {
            ByteBuffer buf = ByteBuffer.allocate(4);
            buf.putInt(ip.intValue());
            Inet4Address ipAddress = (Inet4Address)InetAddress.getByAddress(buf.array());
            if (ipAddress!=null)
            {
                return formatIPAddress(ipAddress);
            }
            else return null;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static String formatIPAddress(InetAddress adr)
    {
        try
        {
            String s = adr.getHostAddress();
            return s;
        }
        catch (Exception ex)
        {
            return null;
        }
    }




}



