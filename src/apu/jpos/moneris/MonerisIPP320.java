package apu.jpos.moneris;

import apu.jpos.com.surrogate.*;
import apu.jpos.com.surrogate.net.*;
import apu.jpos.com.surrogate.net.protocol.*;
import apu.jpos.util.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 4/25/14
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class MonerisIPP320 extends Loggable implements CSEventListener {


    String serialNumber;
    String firmwareVersion;
    int commandTimeout = 10 * 60 * 1000;
    int resetTimeout = 5 * 1000;
    boolean initRequired = false;
    boolean waitingForAsyncReset = false;


    ComSurrogate api;
    String id = "PinPad";
    String ip;
    int port;
    String txLogFileName;
    boolean ready;
    byte[] ack;
    byte[] nak;
    byte[] eot;
    byte[] fs;
    String posId;
    String merchantId;
    boolean initSuccess=false;



    public MonerisIPP320(String ipAddress, int tcpPort, String dailyLogFileName) throws Exception {
        try {
            ack = new byte[1];
            ack[0] = 0x06;
            nak = new byte[1];
            nak[0] = 0x15;
            eot = new byte[1];
            eot[0] = 0x04;
            fs = new byte[1];
            fs[0] = 0x1c;
            ready = false;
            ip = ipAddress;
            port = tcpPort;
            txLogFileName = dailyLogFileName;
            api = ComSurrogate.getInstance(ip, port, "JPOS Moneris Driver 1.0");
            api.addEventHandler(this);
            api.waitForLogin();
            ready = true;
        } catch (Exception ex) {
            log(ex);
            ready = false;
        }
    }


    public void initialize(String posId, String merchantId, boolean skipHostUpdate) throws Exception {
        this.posId=posId;
        this.merchantId=merchantId;
        api.sendCommand("SetReadDelay", 200);
        api.sendCommand("SetAckNak", id, ack, nak);
        api.sendCommand("SetPacket", id, "", eot);
        posId = StringUtil.fixedLengthString(posId,8,'X',false);
        initSuccess=true;
        reset();
        int n = 0;
        try {
            n = getSAFCount();
            if (n>0) skipHostUpdate=true;
        } catch (Exception e) {
            initSuccess=false;
            skipHostUpdate=false;   // Force a host update because initialization may be required
        }
        if (!skipHostUpdate) {
            MonerisResponse res = sendCommand("90", posId, merchantId);
            if (!res.isRequestAccepted())
                throw new Exception("802 - Error: Invalid response from PinPad on Initialization!");
            if (!res.getParam(2).equals(posId))
                throw new Exception("803 - Error: ECR_ID of Pos and Pinpad doesnt match!");
            if (!StringUtil.empty(res.getParam(3)) && !StringUtil.empty(res.getParam(4))) initSuccess = true;
            else initSuccess = false;
        }
        retrievePinpadInfo();
        reset();
    }

    public void initSAF(BigDecimal safDollarLimit, int safTxLimit) throws Exception
    {
        if (initSuccess) {
            reset();
            MonerisResponse res = sendCommand("87", "SO");
            if (!res.isRequestAccepted())
                throw new Exception("804 - Error: Unable to enable SAF Mode!");
            String limit = String.format("%07d", safDollarLimit.movePointRight(2).intValue());
            res = sendCommand("74", "V ", limit);
            if (!res.isRequestAccepted()) throw new Exception("804 - Error: Unable to enable SAF Mode!");
            res = sendCommand("74", "M ", limit);
            if (!res.isRequestAccepted()) throw new Exception("804 - Error: Unable to enable SAF Mode!");
            res = sendCommand("74", "AX", limit);
            if (!res.isRequestAccepted()) throw new Exception("804 - Error: Unable to enable SAF Mode!");
            res = sendCommand("75", String.format("%03d", safTxLimit));
            if (!res.isRequestAccepted()) throw new Exception("804 - Error: Unable to enable SAF Mode!");
        }
    }

    public void disableSAF() throws Exception
    {
        reset();
        MonerisResponse res = sendCommand("87","SF");
        if (!res.isRequestAccepted())
            throw new Exception("804 - Error: Unable to enable SAF Mode!");
    }

    public void initCashBack(int cashbackDollarLimit)throws  Exception
    {
        if(initSuccess){
            MonerisResponse res = sendCommand("87", "CE");
            if(!res.isRequestAccepted())
                throw  new Exception("803 - Error: Unable to enable debit cashback!");
            String limit = "" + cashbackDollarLimit;
            res = sendCommand("52", limit);
            if(!res.isRequestAccepted()) throw  new Exception("803 - Error:  Unable to set cashback dollar limit!");
        }
    }

    public int getSAFCount() throws Exception
    {
        reset();
        MonerisResponse res = sendCommand("83");
        if (!res.isRequestAccepted())
            throw new Exception("805 - Error: Unable to get SAF Record Count!");
        return Integer.parseInt(res.getParam(1));
    }

    public void releaseSAF() throws Exception
    {
        reset();
        MonerisResponse res = sendCommand("76");
        if (!res.isRequestAccepted())
            throw new Exception("804 - Error: Unable to process SAF Record!");
    }


    public void reinitialize() throws Exception
    {
        MonerisResponse res = sendCommand("90",posId,merchantId);
        if (!res.isRequestAccepted()) throw new Exception("802 - Error: Invalid response from PinPad on Initialization!");
        if (!res.getParam(2).equals(posId)) throw new Exception("803 - Error: ECR_ID of Pos and Pinpad doesnt match!");
        initRequired = false;
    }


    public void reset() throws Exception
    {
        MonerisResponse res = null;
        try {
            res = sendReset();
        } catch (Exception e) {
            cancelCommand();
            response=null;
            throw e;
        }
        if (res.isError())
        {
            if (res.code!=163) cancelCommand();
            throw new Exception(res.getCodeText());
        }
        else if (res.isResetAccepted()) cancelCommand();
        else throw new Exception("806 - Error: PinPad Terminal is not Ready!");
    }


    public MonerisTransactionResponse purchase(BigDecimal amount, boolean allowCashBack) throws Exception
    {
        String id = getNextId();
        String amt = Integer.toString(amount.movePointRight(2).intValue());
        //MonerisResponse res = sendCommand("00",amt,null,null,null,null,null
        //        ,(allowCashBack?"C":null),id);
        /*
        Firmware 11.20 command
         */
        MonerisResponse res = sendCommand("00",amt,null,null,null,(allowCashBack?"B":null),null,id);
        if (res.isInitRequired()) initRequired=true;
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (res.isResetAccepted())
            throw new Exception("401 - Error: Transaction Cancelled!");
        else if (res.isRetryRequested())
        {
            res = sendCommand("98");
        }
        if (!res.isTxResponse())
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        MonerisTransactionResponse ret = new MonerisTransactionResponse(res,id);
        //String tx = res.serialize();
        //MonerisResponse res2 = new MonerisResponse(tx);
        //if (ret.getConditionCode()==52 || ret.getConditionCode()==160) initRequired=true;
        return ret;
    }

    public MonerisTransactionResponse refund(BigDecimal amount) throws Exception
    {
        String id = getNextId();
        String amt = Integer.toString(amount.movePointRight(2).intValue());
        MonerisResponse res = sendCommand("04",amt,null,null,null,null,null,id);
        if (res.isInitRequired()) initRequired=true;
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (res.isResetAccepted())
            throw new Exception("401 - Error: Transaction Cancelled!");
        else if (res.isRetryRequested())
        {
            res = sendCommand("98");
        }
        if (!res.isTxResponse())
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        MonerisTransactionResponse ret = new MonerisTransactionResponse(res,id);
        //if (ret.getConditionCode()==52 || ret.getConditionCode()==160) initRequired=true;
        return ret;
    }

    public MonerisTransactionResponse voidTransaction(MonerisTransactionType txType
            , String originalApprovalCode, BigDecimal amount) throws Exception
    {
        String code;
        if (txType==MonerisTransactionType.Refund) code = "12";
        else if (txType==MonerisTransactionType.Purchase) code = "11";
        else throw new Exception("This type of Transaction Void is not implemented!");
        String id = getNextId();
        String amt = Integer.toString(amount.movePointRight(2).intValue());
        //MonerisResponse res = sendCommand(code,amt,null,originalApprovalCode,null,null,null,id);
        /*
        Firmware 11.20
         */
        MonerisResponse res = sendCommand(code,amt,null, null, null, null, null, id, null,originalApprovalCode);
        if (res.isInitRequired()) initRequired=true;
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (res.isResetAccepted())
            throw new Exception("401 - Error: Transaction Cancelled!");
        else if (res.isRetryRequested())
        {
            res = sendCommand("98");
        }
        if (!res.isTxResponse())
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        MonerisTransactionResponse ret = new MonerisTransactionResponse(res,id);
        //if (ret.getConditionCode()==52 || ret.getConditionCode()==160) initRequired=true;
        return ret;
    }

    public MonerisTransactionResponse voidLastTransaction(String originalApprovalCode) throws Exception
    {
        String id = getNextId();
        MonerisResponse res = sendCommand("13",originalApprovalCode,id);
        if (res.isInitRequired()) initRequired=true;
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (res.isResetAccepted())
            throw new Exception("401 - Error: Transaction Cancelled!");
        else if (res.isRetryRequested())
        {
            res = sendCommand("98");
        }
        if (!res.isTxResponse())
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        MonerisTransactionResponse ret = new MonerisTransactionResponse(res,id);
        //if (ret.getConditionCode()==52 || ret.getConditionCode()==160) initRequired=true;
        return ret;
    }

    public MonerisTransactionResponse voidPurchase(String originalApprovalCode,BigDecimal amount) throws Exception
    {
        String id = getNextId();
        String amt = Integer.toString(amount.movePointRight(2).intValue());
        //MonerisResponse res = sendCommand("11",amt,null,originalApprovalCode,null,null,null,id);
        /*
        Firware 11.20
         */
        MonerisResponse res = sendCommand("11",amt,null, null, null, null, null, id, null,originalApprovalCode);
        if (res.isInitRequired()) initRequired=true;
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (res.isResetAccepted())
            throw new Exception("401 - Error: Transaction Cancelled!");
        else if (res.isRetryRequested())
        {
            res = sendCommand("98");
        }
        if (!res.isTxResponse())
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        MonerisTransactionResponse ret = new MonerisTransactionResponse(res,id);
        //if (ret.getConditionCode()==52 || ret.getConditionCode()==160) initRequired=true;
        return ret;
    }

    public MonerisTransactionResponse voidRefund(String originalApprovalCode,BigDecimal amount) throws Exception
    {
        String id = getNextId();
        String amt = Integer.toString(amount.movePointRight(2).intValue());
        //MonerisResponse res = sendCommand("12",amt,null,originalApprovalCode,null,null,null,id);
        /*
        Firware 11.20
         */
        MonerisResponse res = sendCommand("12",amt,null, null, null, null, null, id, null,originalApprovalCode);
        if (res.isInitRequired()) initRequired=true;
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (res.isResetAccepted())
            throw new Exception("401 - Error: Transaction Cancelled!");
        else if (res.isRetryRequested())
        {
            res = sendCommand("98");
        }
        if (!res.isTxResponse())
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        MonerisTransactionResponse ret = new MonerisTransactionResponse(res,id);
        //if (ret.getConditionCode()==52 || ret.getConditionCode()==160) initRequired=true;
        return ret;
    }


    public MonerisBatchResponse settleBatch(boolean eraseBatch) throws Exception
    {
        String cflag = "T";
        if (eraseBatch) cflag = "C";
        else
        {
            // This code is temporary because of the bug in moneris v2.24
            // We are bypassing the batch report here, but once moneris finds a solution and updates
            // we can remove this entire else block and things should be back to normal.
            MonerisBatchResponse ret = new MonerisBatchResponse();
            ret.setTransactionLogs(getLogs());
            return ret;
        }

        /*MonerisResponse res = sendCommand("65",null,cflag);
        if (res.isInitRequired()) initRequired=true;
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (res.isResetAccepted())
            throw new Exception("401 - Error: Transaction Cancelled!");
        if (!res.isBatchClosed() && !res.isRequestAccepted())
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        MonerisBatchResponse ret = new MonerisBatchResponse(res);*/

        MonerisBatchResponse ret = new MonerisBatchResponse();
        ret.setTransactionLogs(getLogs());

        /*if (eraseBatch)
        {
            appendTxLog(res);
            finalizeTxLog();
        }*/
        if (eraseBatch) finalizeTxLog();
        return ret;
    }

    public MonerisBatchResponse queryTotals() throws Exception
    {
        MonerisResponse res = sendCommand("67");
        if (res.isInitRequired()) initRequired=true;
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (res.isResetAccepted())
            throw new Exception("401 - Error: Transaction Cancelled!");
        if (!res.isBatchClosed() && !res.isRequestAccepted())
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        MonerisBatchResponse ret = new MonerisBatchResponse(res);
        return ret;
    }




    public MonerisTransactionResponse getLastResponse() throws Exception
    {
        MonerisResponse res = sendCommand("79");
        if (res.isError()) throw new Exception(res.getCodeText());
        MonerisResponse res2 = res.getSubResponse();
        if (res2==null) throw new Exception("804 - Error: Invalid response received from PinPad!");
        if (!res2.isTxResponse()) return null; // This is not a real transaction data!
        appendTxLog(res2);
        MonerisTransactionResponse ret = new MonerisTransactionResponse(res2,null);
        return ret;
    }

    int txSeq = 0;
    public String getNextId()
    {
        try {
            if (txSeq==0)
            {
                String lastLog = getLastTxLog();
                if (lastLog!=null)
                {
                    MonerisResponse lastResp = new MonerisResponse(lastLog);
                    if (lastResp!=null)
                    {
                        MonerisTransactionResponse lastTx = new MonerisTransactionResponse(lastResp,null);
                        if (lastTx!=null && lastTx.getSequenceNumber()!=null) txSeq = Integer.parseInt(lastTx.getSequenceNumber())+1;
                    }
                }
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to lookup the last transaction from the current transaction batch!");
        }
        return Integer.toString(txSeq++);
    }
    public void resetSequence(){txSeq=0;}

    private void retrievePinpadInfo() throws Exception
    {
        MonerisResponse res = sendCommand("44");
        if (res.isError()) throw new Exception(res.getCodeText());
        else if (!res.isRequestAccepted() || !res.getParam(0).trim().equals("44"))
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        serialNumber = res.getParam(1);
        firmwareVersion = res.getParam(2);
    }

    private String getLastTxLog()
    {
        if (Utility.fileExists(txLogFileName) && Utility.fileSize(txLogFileName)>=MonerisResponse.TxLogSize)
        {
            try {
                String tx = Utility.readFileTail(txLogFileName,MonerisResponse.TxLogSize,"UTF8");
                return tx;
            } catch (IOException ex) {
                log(ex);
                logWarning("Unable to retrieve tx Log from file: "+txLogFileName);
            }
        }

        return null;
    }
    private void appendTxLog(MonerisResponse res)
    {
        try
        {
            if (res.isTxResponse() || res.isBatchClosed())
            {
                String lastTx = getLastTxLog();
                String tx = res.serialize();
                if (lastTx==null || !lastTx.equals(tx))
                {
                    Utility.appendStringToFile(txLogFileName,tx,1024*1024*1024);
                }
            }
        }
        catch(Exception ex)
        {
            log(ex);
            logWarning("Unable to save to the transaction log. Pos declare report will have errors!");
        }
    }

    public String getLogs()
    {
        try
        {
            if (!Utility.fileExists(txLogFileName)) return "";
            String ret = Utility.readFile(txLogFileName);
            return ret;
        }
        catch (Exception ex)
        {
            log(ex);
            logWarning("Unable to read log file: " + txLogFileName);
            return "";
        }
    }

    public void finalizeTxLog()
    {
        try
        {
            File f = new File(txLogFileName);
            DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            DateTime now = new DateTime();
            String[] fnameComp = f.getName().split("\\.(?=[^\\.]+$)");
            String newFileName = Utility.getParentDir(f.getAbsolutePath())
                    +"\\"+fnameComp[0]+"_" + dtf.print(now)+"."+fnameComp[1];
            if (Utility.fileExists(newFileName)) Utility.deleteFile(newFileName,10);
            Utility.copyFile(f,new File(newFileName));
            Utility.deleteFile(f.getAbsolutePath(),10);
            resetSequence();
        }
        catch (Exception ex)
        {
            log(ex);
            logWarning("Unable to erase log file: " + txLogFileName);
        }

    }




    public MonerisResponse sendCommand(String cmd) throws Exception
    {
        return sendCommand(Utility.toList(cmd));
    }
    public MonerisResponse sendCommand(String cmd, Object p1) throws Exception
    {
        return sendCommand(Utility.toList(cmd,p1));
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2) throws Exception
    {
        return sendCommand(Utility.toList(cmd,p1,p2));
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2, Object p3) throws Exception
    {
        return sendCommand(Utility.toList(cmd,p1,p2,p3));
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2, Object p3,Object p4) throws Exception
    {
        return sendCommand(Utility.toList(cmd,p1,p2,p3,p4));
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2, Object p3,Object p4,Object p5) throws Exception
    {
        return sendCommand(Utility.toList(cmd,p1,p2,p3,p4,p5));
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2, Object p3,Object p4,Object p5, Object p6) throws Exception
    {
        return sendCommand(Utility.toList(cmd,p1,p2,p3,p4,p5,p6));
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2, Object p3,Object p4,Object p5, Object p6,Object p7) throws Exception
    {
        return sendCommand(Utility.toList(cmd,p1,p2,p3,p4,p5,p6,p7));
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2, Object p3,Object p4,Object p5, Object p6,Object p7,Object p8) throws Exception
    {
        List<Object> p = Utility.toList(cmd,p1,p2,p3,p4,p5,p6,p7);
        p.add(p8);
        return sendCommand(p);
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2, Object p3,Object p4,Object p5, Object p6,Object p7,Object p8, Object p9) throws Exception
    {
        List<Object> p = Utility.toList(cmd,p1,p2,p3,p4,p5,p6,p7);
        p.add(p8);
        p.add(p9);
        return sendCommand(p);
    }
    public MonerisResponse sendCommand(String cmd, Object p1,Object p2, Object p3,Object p4,Object p5, Object p6,Object p7,Object p8, Object p9, Object p10) throws Exception
    {
        List<Object> p = Utility.toList(cmd,p1,p2,p3,p4,p5,p6,p7);
        p.add(p8);
        p.add(p9);
        p.add(p10);
        return sendCommand(p);
    }
    public MonerisResponse sendCommand(List<Object> params) throws Exception
    {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        boolean first = true;
        for (int i=0;i<params.size();i++)
        {
            if (!first) buffer.put(fs);
            first=false;
            if (params.get(i)==null) continue;
            else if (params.get(i) instanceof String) buffer.put(StringUtil.stringToByte((String)params.get(i)));
            else if (params.get(i) instanceof Character) buffer.put(StringUtil.stringToByte(params.get(i).toString()));
            else if (params.get(i) instanceof Boolean)
            {
                byte val = 48;
                if (((Boolean)params.get(i))==true) val++;
                buffer.put(val);
            }
            else if (params.get(i) instanceof Byte) buffer.put(StringUtil.stringToByte(params.get(i).toString()));
            else if (params.get(i) instanceof Short) buffer.put(StringUtil.stringToByte(params.get(i).toString()));
            else if (params.get(i) instanceof Integer) buffer.put(StringUtil.stringToByte(params.get(i).toString()));
            else if (params.get(i) instanceof Long) buffer.put(StringUtil.stringToByte(params.get(i).toString()));
            else if (params.get(i) instanceof Float)
            {
                long val = (long)Math.round(((Float)params.get(i)*100.0));
                buffer.put(StringUtil.stringToByte(Long.toString(val)));
            }
            else if (params.get(i) instanceof Double)
            {
                long val = (long)Math.round(((Double)params.get(i)*100.0));
                buffer.put(StringUtil.stringToByte(Long.toString(val)));
            }
            else if (params.get(i) instanceof BigDecimal)
            {
                BigDecimal val = (BigDecimal)params.get(i);
                val = val.movePointRight(2);
                buffer.put(StringUtil.stringToByte(Long.toString(val.longValue())));
            }
            else if (params.get(i) instanceof byte[]) buffer.put((byte[])params.get(i));
            else if (params.get(i) instanceof ByteBuffer)
            {
                ByteBuffer buf = (ByteBuffer)params.get(i);
                int n = buf.position();
                buf.rewind();
                byte[] data = new byte[n];
                buffer.put(data);
            }
            else
            {
                buffer.put(StringUtil.stringToByte(params.get(i).toString()));
            }
        }

        buffer.put(eot);
        MonerisResponse res;
        synchronized (monerisCommandWaiter) {
            api.sendCommand("send", id, buffer);
            res = waitForCommandResponse();
        }
        if (res==null) throw new Exception("801 - Error: Command Timeout!");
        else if (res.isError()) throw new Exception(res.getCodeText());
        else
        {
            if (res.isTxResponse())
            {
                appendTxLog(res);
            }
            return res;
        }
    }
    public MonerisResponse sendReset() throws Exception
    {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer.put(StringUtil.stringToByte("97"));
        buffer.put(eot);
        MonerisResponse res;
        synchronized (monerisResetWaiter) {
            api.sendCommand("send", id, buffer);
            res = waitForResetResponse();
        }
        return res;
    }



    private Object monerisCommandWaiter = new Object();
    private Object monerisResetWaiter = new Object();
    private MonerisResponse response;
    private MonerisResponse resetResponse;
    private MonerisResponse waitForCommandResponse()
    {
        synchronized (monerisCommandWaiter)
        {
            response=null;
            try {
                monerisCommandWaiter.wait(commandTimeout);
                return response;
            } catch (InterruptedException e) {
                return null;
            }
        }
    }
    private MonerisResponse waitForResetResponse()
    {
        synchronized (monerisResetWaiter)
        {
            resetResponse=null;
            try {
                monerisResetWaiter.wait(resetTimeout);
                return resetResponse;
            } catch (InterruptedException e) {
                return null;
            }
        }
    }
    public void cancelCommand()
    {
        synchronized (monerisCommandWaiter)
        {
            response=null;
            if (resetResponse!=null && resetResponse.code==98) response=resetResponse;
            monerisCommandWaiter.notifyAll();
        }
    }



    //@Override
    public void onServerEvent(SocketInfo socket, CSServerEvent event) {
        if (event.getEventType().equals("data"))
        {
            String source = event.getParameters().get(0);
            if (source.trim().equalsIgnoreCase(id))
            {
                byte[] data = DatatypeConverter.parseBase64Binary(event.getParameters().get(1));
                try {
                    MonerisResponse res = new MonerisResponse(data);
                    if (res.code==98 || res.code==163)
                    {
                        synchronized (monerisResetWaiter) {
                            resetResponse = res;
                            monerisResetWaiter.notifyAll();
                        }
                    }
                    else {
                        synchronized (monerisCommandWaiter) {
                            response = res;
                            monerisCommandWaiter.notifyAll();
                        }
                    }
                } catch (Exception e) {
                    log(e);
                }
            }
        }
    }


    public boolean isReady() {
        if (ready) return true;
        try {
            api.waitForLogin();
            ready = true;
            return ready;
        } catch (Exception ex) {
            log(ex);
            return false;
        }

    }


    public String getSerialNumber() {
        return serialNumber;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public int getCommandTimeout() {
        return commandTimeout;
    }

    public void setCommandTimeout(int commandTimeout) {
        this.commandTimeout = commandTimeout;
    }

    public boolean isInitRequired() {
        return initRequired;
    }
}
