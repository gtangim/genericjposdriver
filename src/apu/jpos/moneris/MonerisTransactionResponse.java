package apu.jpos.moneris;

import apu.jpos.util.StringUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 6/6/14
 * Time: 11:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class MonerisTransactionResponse {

    MonerisTransactionType transactionType;
    DateTime transactionDate;
    BigDecimal amount;
    BigDecimal cashBackAmount;
    String accountNumber;
    String expiry;
    String responseCode;
    String responseBase24;
    String language;
    String approvalCode;
    String uniqueId;
    String ecrId;
    MonerisCardMethod cardMethod;
    MonerisCardType cardType;
    MonerisCardVerificationMethod verificationMethod;
    String cardAccountType;
    String sequenceNumber;
    boolean isSAF;

    String emvApplicationId;
    String emvApplicationName;
    String emvTerminalVerificationResult;
    String emvTransactionStatusInformation;

    String cardMethodIndicator;
    int conditionCode;



    public MonerisTransactionResponse(MonerisResponse res, String sourceId) throws Exception
    {
        if (!res.isTxResponse()) throw new Exception("804 - Error: Invalid response received from PinPad!");
        /*else if (res.paramCount()==26 || res.paramCount()==18)
        {
            sequenceNumber = res.getParam(res.paramCount()-1);
            if (StringUtil.empty(sequenceNumber)) sequenceNumber = sourceId;
            else if (sourceId!=null && !sequenceNumber.equals(sourceId))
                throw new Exception("804 - Error: Invalid response received from PinPad!");
        }*/
        /*
        Firmware 11.20
         */
        else if (res.paramCount()==43)
        {
            sequenceNumber = res.getParam(29);
            if (StringUtil.empty(sequenceNumber)) sequenceNumber = sourceId;
            else if (sourceId!=null && !sequenceNumber.equals(sourceId))
                throw new Exception("804 - Error: Invalid response received from PinPad!");
        }
        else throw new Exception("804 - Error: Invalid response received from PinPad!");
        conditionCode = res.getCode();

        /*
        String tt, String ecr, String saf, String resp24, String resp, String lang, String dt, String amt,String partialAuth, String availBal, String tipAmt, String cashBack ,
          String sa, String fca, String fcc, String cr, String markup,String acNum, String cPlan, String cName, String caType, String swp, String formFactor,
          String cvm, String appCode, String txId, String invNum, String echData, String appId, String appLabel,
          String appPreferredName, String termVerification1, String termVerification2, String txStat, String tokenRespCode, String token,
          String logOnReq, String enc
         */

        /**
         * Firmware 11.20 response
         */
        init(res.getParam(0), res.getParam(1),  res.getParam(2), res.getParam(3), res.getParam(4), res.getParam(5), res.getParam(6)
                , res.getParam(7), res.getParam(8), res.getParam(9), res.getParam(10), res.getParam(11), res.getParam(12), res.getParam(13)
                , res.getParam(14), res.getParam(15), res.getParam(16), res.getParam(17), res.getParam(18), res.getParam(19), res.getParam(20)
                , res.getParam(21), res.getParam(22), res.getParam(23), res.getParam(26), res.getParam(27), res.getParam(28), res.getParam(29)
                , res.getParam(32), res.getParam(33), res.getParam(34), res.getParam(36), res.getParam(38), res.getParam(39), res.getParam(40)
                , res.getParam(41), res.getParam(42));
        int i = 0;
        /*if (res.paramCount()==18)
        {
            init(res.getParam(0),res.getParam(1),res.getParam(2),res.getParam(3),res.getParam(4),res.getParam(5)
                    ,res.getParam(6),res.getParam(7),res.getParam(8),res.getParam(9)
                    ,res.getParam(10),res.getParam(11),res.getParam(12)
                    ,res.getParam(13),res.getParam(14),res.getParam(15),res.getParam(16));
        }
        else
        {
            init(res.getParam(0),res.getParam(1),res.getParam(2),res.getParam(3),res.getParam(4),res.getParam(5)
                    ,res.getParam(6),res.getParam(7),res.getParam(8), res.getParam(9)
                    ,res.getParam(10),res.getParam(11),res.getParam(12)
                    ,res.getParam(13),res.getParam(14),res.getParam(15),res.getParam(24));

            initEmv(res.getParam(13), res.getParam(16), res.getParam(17)
                    , res.getParam(18), res.getParam(20), res.getParam(22), res.getParam(23));
        }*/
        /*else if (res.paramCount()==17)
        {
            init(res.getParam(0),res.getParam(1),res.getParam(2),res.getParam(3),res.getParam(4),res.getParam(5)
                    ,res.getParam(6),res.getParam(7),res.getParam(8),res.getParam(9)
                    ,res.getParam(10),res.getParam(11),res.getParam(12)
                    ,res.getParam(13),res.getParam(14),res.getParam(15),res.getParam(16));
        }
        else if (res.paramCount()==16)
        {
            init(res.getParam(0),res.getParam(1),res.getParam(2),res.getParam(3),res.getParam(4),res.getParam(5)
                    ,res.getParam(6),res.getParam(7),res.getParam(8),res.getParam(9)
                    ,res.getParam(10),res.getParam(11),res.getParam(12)
                    ,res.getParam(13),res.getParam(14),res.getParam(15),null);
        }*/
        /*else if (res.paramCount()==25)
        {
            init(res.getParam(0),res.getParam(1),res.getParam(2),res.getParam(3),res.getParam(4),res.getParam(5)
                    ,res.getParam(6),res.getParam(7),res.getParam(8), res.getParam(9)
                    ,res.getParam(10),res.getParam(11),res.getParam(12)
                    ,res.getParam(13),res.getParam(14),res.getParam(15),res.getParam(24));

            initEmv(res.getParam(13), res.getParam(16), res.getParam(17)
                    , res.getParam(18), res.getParam(20), res.getParam(22), res.getParam(23));
        }
        else if (res.paramCount()==24)
        {
            init(res.getParam(0),res.getParam(1),res.getParam(2),res.getParam(3),res.getParam(4),res.getParam(5)
                    ,res.getParam(6),res.getParam(7),res.getParam(8), res.getParam(9)
                    ,res.getParam(10),res.getParam(11),res.getParam(12)
                    ,res.getParam(13),res.getParam(14),res.getParam(15),null);

            initEmv(res.getParam(13), res.getParam(16), res.getParam(17)
                    , res.getParam(18), res.getParam(20), res.getParam(22), res.getParam(23));
        }
        else  throw new Exception("804 - Error: Invalid response received from PinPad!");*/
    }

    /*
    Firmware 11.20 response
     */
    private void init(String tt, String ecr, String saf, String resp24, String resp, String lang, String dt, String amt,String partialAuth, String availBal, String tipAmt, String cashBack ,
                      String sa, String fca, String fcc, String cr, String markup,String acNum, String cPlan, String cName, String caType, String swp, String formFactor,
                      String cvm, String appCode, String txId, String invNum, String echData, String appId, String appLabel,
                      String appPreferredName, String termVerification1, String termVerification2, String txStat, String tokenRespCode, String token,
                      String logOnReq
            ) throws Exception
    {
        if (tt.equals("00")) transactionType = MonerisTransactionType.Purchase;
        else if (tt.equals("04")) transactionType = MonerisTransactionType.Refund;
        else if (tt.equals("11")) transactionType = MonerisTransactionType.PurchaseVoid;
        else if (tt.equals("12")) transactionType = MonerisTransactionType.RefundVoid;
        else if (tt.equals("13")) transactionType = MonerisTransactionType.VoidLast;
        else throw new Exception("805 - Error: Unknown transaction response received from the PinPad!");
        transactionDate = stringToDate(dt,"yyMMddHHmmss");
        if (StringUtil.empty(amt)) amount=BigDecimal.ZERO;
        else amount = new BigDecimal(amt).movePointLeft(2);
        if (StringUtil.empty(cashBack)) cashBackAmount=BigDecimal.ZERO;
        else
            try {
                cashBackAmount = new BigDecimal(cashBack).movePointLeft(2);
            } catch (Exception e) {
                cashBackAmount=BigDecimal.ZERO;
            }
        accountNumber = acNum;
        //expiry = exp;
        responseCode = resp;
        responseBase24 = resp24;
        approvalCode = appCode;
        uniqueId = txId;
        ecrId = ecr;
        language = lang;

        cardMethodIndicator = swp;
        if (swp.equals("S")) cardMethod = MonerisCardMethod.Swiped;
        else if (swp.equals("M")) cardMethod = MonerisCardMethod.ManualEntry;
        else if (swp.equals("C")) cardMethod = MonerisCardMethod.Chip;
        else if (swp.equals("Q")) cardMethod = MonerisCardMethod.ChipFailedButApproved;
        else if (swp.equals("F")) cardMethod = MonerisCardMethod.FallbackSwiped;
        else if (swp.equals("G")) cardMethod = MonerisCardMethod.FallbackManualEntry;
        else if (swp.equals("T")) cardMethod = MonerisCardMethod.Contactless;
        else if (swp.equals("H")) cardMethod = MonerisCardMethod.ContactlessEMV;
        else throw new Exception("806 - Error: Unknown card presentation method indicator!");

        if (!StringUtil.empty(caType)) cardAccountType = caType;
        if (!StringUtil.empty(cName)) cardType = MonerisCardType.valueOf(cName);
        if (cardType==MonerisCardType.FLASH) cardType = MonerisCardType.INTERAC;

        if (StringUtil.empty(cvm)) verificationMethod=MonerisCardVerificationMethod.None;
        else if (cvm.equals("S")) verificationMethod = MonerisCardVerificationMethod.Signature;
        else if (cvm.equals("P")) verificationMethod = MonerisCardVerificationMethod.Pin;
        else if (cvm.equals("B")) verificationMethod = MonerisCardVerificationMethod.SignatureAndPin;
        else if (cvm.equals("N")) verificationMethod = MonerisCardVerificationMethod.None;
        else throw new Exception("807 - Error: Unknown card verification method indicator!");

        if (!StringUtil.empty(saf) && saf.equals("S")) isSAF = true;
        else isSAF=false;

        if(!StringUtil.empty(appId)){
            emvApplicationId = appId;
            if (!StringUtil.empty(appPreferredName)) emvApplicationName = appPreferredName;
            else if (!StringUtil.empty(appLabel)) emvApplicationName = appLabel;
            else emvApplicationName = cName;
            emvTransactionStatusInformation = txStat;
            if (StringUtil.empty(termVerification2)) emvTerminalVerificationResult = termVerification1;
            else emvTerminalVerificationResult = termVerification2;
        }

    }

    private void init(String tt,String dt, String amt, String acNum, String exp, String resp, String resp24,
                      String appCode, String txId, String lang, String ecr, String swp, String caType
            , String cName, String cvm, String saf, String cashBack) throws Exception
    {
        if (tt.equals("00")) transactionType = MonerisTransactionType.Purchase;
        else if (tt.equals("04")) transactionType = MonerisTransactionType.Refund;
        else if (tt.equals("11")) transactionType = MonerisTransactionType.PurchaseVoid;
        else if (tt.equals("12")) transactionType = MonerisTransactionType.RefundVoid;
        else if (tt.equals("13")) transactionType = MonerisTransactionType.VoidLast;
        else throw new Exception("805 - Error: Unknown transaction response received from the PinPad!");
        transactionDate = stringToDate(dt,"yyMMddHHmmss");
        if (StringUtil.empty(amt)) amount=BigDecimal.ZERO;
        else amount = new BigDecimal(amt).movePointLeft(2);
        if (StringUtil.empty(cashBack)) cashBackAmount=BigDecimal.ZERO;
        else
            try {
                cashBackAmount = new BigDecimal(cashBack).movePointLeft(2);
            } catch (Exception e) {
                cashBackAmount=BigDecimal.ZERO;
            }
        accountNumber = acNum;
        expiry = exp;
        responseCode = resp;
        responseBase24 = resp24;
        approvalCode = appCode;
        uniqueId = txId;
        ecrId = ecr;
        language = lang;

        cardMethodIndicator = swp;
        if (swp.equals("S")) cardMethod = MonerisCardMethod.Swiped;
        else if (swp.equals("M")) cardMethod = MonerisCardMethod.ManualEntry;
        else if (swp.equals("C")) cardMethod = MonerisCardMethod.Chip;
        else if (swp.equals("Q")) cardMethod = MonerisCardMethod.ChipFailedButApproved;
        else if (swp.equals("F")) cardMethod = MonerisCardMethod.FallbackSwiped;
        else if (swp.equals("G")) cardMethod = MonerisCardMethod.FallbackManualEntry;
        else if (swp.equals("T")) cardMethod = MonerisCardMethod.Contactless;
        else if (swp.equals("H")) cardMethod = MonerisCardMethod.ContactlessEMV;
        else throw new Exception("806 - Error: Unknown card presentation method indicator!");

        if (!StringUtil.empty(caType)) cardAccountType = caType;
        if (!StringUtil.empty(cName)) cardType = MonerisCardType.valueOf(cName);
        if (cardType==MonerisCardType.FLASH) cardType = MonerisCardType.INTERAC;

        if (StringUtil.empty(cvm)) verificationMethod=MonerisCardVerificationMethod.None;
        else if (cvm.equals("S")) verificationMethod = MonerisCardVerificationMethod.Signature;
        else if (cvm.equals("P")) verificationMethod = MonerisCardVerificationMethod.Pin;
        else if (cvm.equals("B")) verificationMethod = MonerisCardVerificationMethod.SignatureAndPin;
        else if (cvm.equals("N")) verificationMethod = MonerisCardVerificationMethod.None;
        else throw new Exception("807 - Error: Unknown card verification method indicator!");

        if (!StringUtil.empty(saf) && saf.equals("S")) isSAF = true;
        else isSAF=false;

    }


    public void initEmv(String cardName, String appId, String appLabel,
                        String appPreferredName, String termVerification1, String termVerification2, String txStat)
    {
        emvApplicationId = appId;
        if (!StringUtil.empty(appPreferredName)) emvApplicationName = appPreferredName;
        else if (!StringUtil.empty(appLabel)) emvApplicationName = appLabel;
        else emvApplicationName = cardName;
        emvTransactionStatusInformation = txStat;
        if (StringUtil.empty(termVerification2)) emvTerminalVerificationResult = termVerification1;
        else emvTerminalVerificationResult = termVerification2;
    }


    public static DateTime stringToDate(String dt, String pattern)
    {
        try
        {
            DateTimeFormatter dtf = DateTimeFormat.forPattern(pattern);
            DateTime ret = dtf.parseDateTime(dt);
            return ret;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public boolean isApproved()
    {
        return !StringUtil.empty(approvalCode);
    }

    public MonerisTransactionType getTransactionType() {
        return transactionType;
    }

    public DateTime getTransactionDate() {
        return transactionDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getCashBackAmount() {
        return cashBackAmount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getExpiry() {
        return expiry;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getEcrId() {
        return ecrId;
    }

    public MonerisCardMethod getCardMethod() {
        return cardMethod;
    }

    public MonerisCardType getCardType() {
        return cardType;
    }

    public MonerisCardVerificationMethod getVerificationMethod() {
        return verificationMethod;
    }

    public String getCardAccountType() {
        return cardAccountType;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public String getResponseBase24() {
        return responseBase24;
    }

    public String getLanguage() {
        return language;
    }

    public String getEmvApplicationId() {
        return emvApplicationId;
    }

    public String getEmvApplicationName() {
        return emvApplicationName;
    }

    public String getEmvTerminalVerificationResult() {
        return emvTerminalVerificationResult;
    }

    public String getEmvTransactionStatusInformation() {
        return emvTransactionStatusInformation;
    }

    public String getCardMethodIndicator() {
        return cardMethodIndicator;
    }

    public int getConditionCode() {
        return conditionCode;
    }

    public boolean isSAF() {
        return isSAF;
    }
}
