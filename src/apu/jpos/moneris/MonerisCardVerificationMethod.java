package apu.jpos.moneris;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 6/6/14
 * Time: 12:45 PM
 * To change this template use File | Settings | File Templates.
 */
public enum MonerisCardVerificationMethod {
    Signature, Pin, SignatureAndPin, None
}
