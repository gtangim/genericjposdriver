package apu.jpos.moneris;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 6/6/14
 * Time: 12:39 PM
 * To change this template use File | Settings | File Templates.
 */
public enum MonerisCardType {
    INTERAC, VISA, MASTERCARD, AMEX, JCB, DISCOVER, FLASH
}
