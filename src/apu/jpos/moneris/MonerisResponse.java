package apu.jpos.moneris;

import apu.jpos.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 5/13/14
 * Time: 10:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class MonerisResponse {

    static final String Head =  "#M_IPP320#,";
    public static final int TxLogSize = 300;
    public int code;
    public List<String> params;


    public MonerisResponse(byte[] data) throws Exception {
        if (codeLookup==null) createCodeLookup();
        code = -1;
        params = new ArrayList<String>();
        if (data == null) throw new Exception("Invalid Response from Moneris!");
        int n = data.length;
        if (n == 0) throw new Exception("Invalid Response from Moneris!");
        if (data[n - 1] != 0x04) throw new Exception("Invalid Response from Moneris!");

        int ind = 0;
        StringBuilder sb = new StringBuilder();
        while (ind < n) {
            sb.setLength(0);
            while (data[ind] != 0x1c && data[ind] != 0x04) {
                sb.append((char) data[ind]);
                ind++;
            }
            if (code == -1) code = Integer.parseInt(sb.toString().trim());
            else params.add(sb.toString().trim());
            ind++;
        }
    }

    public MonerisResponse(String serializedData) throws Exception
    {
        if (StringUtil.empty(serializedData) || !serializedData.startsWith(Head))
            throw  new Exception("Invalid Moneris Response data!");
        int hn = Head.length();
        int n =serializedData.length()-hn-1;
        if (!serializedData.endsWith("\r\n")) n+=2;
        serializedData = serializedData.substring(hn,n).trim();
        String[] fields = serializedData.split("\\,");

        params = new ArrayList<String>(fields.length-1);
        code = Integer.parseInt(fields[0]);
        for(int i=1;i<fields.length;i++) params.add(fields[i]);
    }

    private MonerisResponse(List<String> subResponse) throws Exception
    {
        int tcode = Integer.parseInt(subResponse.get(0));
        if (tcode!=79) throw new Exception("This is not a sub-response!");
        code = Integer.parseInt(subResponse.get(1));
        params = subResponse.subList(2,subResponse.size());
    }


    public int paramCount() {
        return params.size();
    }

    protected void removeParam(int pos)
    {
        if (params!=null && params.size()>pos) params.remove(pos);
    }

    public String getParam(int index) {
        if (index < 0 || index > params.size()) return null;
        else return params.get(index);
    }

    public int getCode() {
        return code;
    }

    public List<String> getParams() {
        return params;
    }

    public String getCodeText() {
        if (codeLookup.containsKey(code))
            return Integer.toString(code) + " - " + codeLookup.get(code);
        else return Integer.toString(code) + " - " + "Unknown error response!";
    }

    public MonerisResponse getSubResponse()
    {
        if (params.size()<2) return null;
        if (code!=99) return null;

        try {
            MonerisResponse ret = new MonerisResponse(params);
            return ret;
        } catch (Exception e) {
            return null;
        }
    }


    public String serialize()
    {
        StringBuilder sb = new StringBuilder(1000);
        sb.append(Head);
        sb.append(code);
        for(String p: params)
        {
            sb.append(',');
            sb.append(p);
        }
        if(TxLogSize <=0) sb.append("\r\n");
        else
        {
            int pad = TxLogSize -sb.length()-2;
            while(pad-->0) sb.append(' ');
            sb.append("\r\n");
        }
        return sb.toString();
    }




    public boolean isError()
    {
        return params.size()<4 && (code==52 || code>=100);
    }
    public boolean isInitRequired() {return (code==52 || code==160);}
    public boolean isRequestAccepted()
    {
        return code==99;
    }


    public boolean isTxResponse()
    {
        try {
            //if (params.size()==18 || params.size()==17 || params.size()==16 || params.size()==26 || params.size()==25 || params.size()==24)
            if (params.size()>10)
            {
                int tcode = Integer.parseInt(params.get(0));
                if (tcode==0 || tcode==4 || tcode==11 || tcode==12 || tcode==13 || tcode==3
                        || tcode==30 || tcode==31)
                {
                    /*if (params.size()<18)
                    {
                        while (params.size()<18) params.add("");
                    }
                    else if (params.size()<26)
                    {
                        while (params.size()<26) params.add("");
                    }*/
                    /*
                    Firmware 11.20
                    Response parameter size is 43 excluding EOT
                     */
                    if(params.size() < 43){
                        while (params.size()<43) params.add("");
                    }
                    return true;
                }
                else return false;
            }
            else return false;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean isResetAccepted()
    {
        return code==98;
    }
    public boolean isInitRequiredResponse()
    {
        return code==52;
    }
    public boolean isRetryRequested()
    {
        return code==51;
    }
    public boolean isBatchClosed()
    {
        return (code==6 || code==7);
    }

    public boolean isBatchBalanced()
    {
        return code==6;
    }




    private static Map<Integer, String> codeLookup = null;

    private void createCodeLookup() {
        codeLookup = new HashMap<Integer, String>();
        codeLookup.put(6,"Batch closed and balanced!");
        codeLookup.put(7,"Batch closed but out of balance!");
        codeLookup.put(51,"Retry requested by terminal!");
        codeLookup.put(52,"Initialization requested by terminal!");
        codeLookup.put(98,"Reset request accepted by terminal!");
        codeLookup.put(99,"Transaction completed!");
        codeLookup.put(100,"Error: Invalid Transaction Code!");
        codeLookup.put(101,"Error: Invalid Amount!");
        codeLookup.put(102,"Error: Missing Amount Field!");
        codeLookup.put(103,"Error: Invalid Track 2 Data!");
        codeLookup.put(105,"Error: Invalid Approval Number!");
        codeLookup.put(106,"Error: Missing Approval Number!");
        codeLookup.put(107,"Error: Missing or Invalid Field Seperator!");
        codeLookup.put(108,"Error: EOT Missing in Command!");
        codeLookup.put(109,"Error: Invalid Close Batch Flag!");
        codeLookup.put(110,"Error: Missing or Invalid Clear Totals Flag!");
        codeLookup.put(111,"Error: Invalid Merchant ID!");
        codeLookup.put(112,"Error: Missing Merchant ID!");
        codeLookup.put(113,"Error: Invalid ECR ID!");
        codeLookup.put(114,"Error: Missing ECR ID!");
        codeLookup.put(115,"Error: Invalid Clerk ID!");
        codeLookup.put(116,"Error: Invalid Echo Data!");
        codeLookup.put(117,"Error: Missing Clerk ID!");
        codeLookup.put(120,"Error: Illegal Field in Command!");
        codeLookup.put(121,"Error: Invalid Cashback Flag!");
        codeLookup.put(122,"Error: Cashback Flag Not Allowed!");
        codeLookup.put(123,"Error: Attempt to set floor limit without initialization!");
        codeLookup.put(124,"Error: Card Plan is not Credit!");
        codeLookup.put(125,"Error: Missing or Invalid Floor Limit!");
        codeLookup.put(126,"Error: Missing Maximum SAF Record!");
        codeLookup.put(127,"Error: SAF Mode is OFF!");
        codeLookup.put(128,"Error: SAF was Declined by Host!");
        codeLookup.put(129,"Error: Missing BIN Range Indicator!");
        codeLookup.put(130,"Error: Missing Low BIN Range Flag!");
        codeLookup.put(131,"Error: Missing High BIN Range Flag!");
        codeLookup.put(132,"Error: BIN Range Table is Full!");
        codeLookup.put(133,"Error: Invalid Extended Configuration!");
        codeLookup.put(134,"Error: Missing Extended Configuration!");
        codeLookup.put(135,"Error: Missing Clerk List!");
        codeLookup.put(136,"Error: Missing Display Timeout!");
        codeLookup.put(137,"Error: Expecting Invoice Number and Promo Code!");
        codeLookup.put(138,"Error: Invalid Message Text!");
        codeLookup.put(139,"Error: BIN Range not Found!");
        codeLookup.put(140,"Error: There is no previous PED message to resend!");
        codeLookup.put(142,"Error: Missing Entry Method Flag!");
        codeLookup.put(143,"Error: PED is not in FT Mode!");
        codeLookup.put(144,"Error: Missing Merchant Terminal ID!");
        codeLookup.put(145,"Error: Missing Track Selection Flag!");
        codeLookup.put(149,"Error: Code Download Application is Missing!");
        codeLookup.put(150,"Error: Transaction Type Not Supported!");
        codeLookup.put(151,"Error: Card Type Not Supported!");
        codeLookup.put(152,"Error: Refund Limit Exceeded (Transaction Not Allowed)!");
        codeLookup.put(153,"Error: Wrong Type of Card was Presented!");
        codeLookup.put(154,"Error: Missing Card Acceptor Terminal ID!");
        codeLookup.put(158,"Error: Cannot void, No Transaction Previously Completed!");
        codeLookup.put(159,"Error: Cannot Void, Approval code must match!");
        codeLookup.put(160,"Error: Reinitialization Required Before Transaction!");
        codeLookup.put(161,"Error: Only One Transaction Retry is Allowed!");
        codeLookup.put(162,"Error: Another transaction is in Progress!");
        codeLookup.put(163,"Error: Cannot Reset, Payment Server Already Processing!");
        codeLookup.put(164,"Error: Invalid MOTO Flag!");
        codeLookup.put(167,"Error: Missing Download Type!");
        codeLookup.put(170,"Error: Invalid Invoice Number!");
        codeLookup.put(171,"Error: Invoice Number is Not Allowed!");
        codeLookup.put(172,"Error: Invalid Promo Code!");
        codeLookup.put(173,"Error: Promo Code is Not Allowed!");
        codeLookup.put(174,"Error: Cannot Release SAF, SAF is Empty!");
        codeLookup.put(183,"Error: Missing Download Communication Method!");
        codeLookup.put(184,"Error: Missing Download Terminal ID!");
        codeLookup.put(185,"Error: Missing Download Application ID!");
        codeLookup.put(186,"Error: Missing Download Group ID!");
        codeLookup.put(187,"Error: Missing Download IP Port!");
        codeLookup.put(188,"Error: BIN Range Already Added!");
        codeLookup.put(189,"Error: Unable to Close Batch if SAF Pending!");
        codeLookup.put(190,"Error: Please use chip method!");
        codeLookup.put(191,"Error: SAF Cannot be Released (Host Disconnected)!");
        codeLookup.put(192,"Error: SAF Overflow!");
        codeLookup.put(193,"Error: SAF Cannot be accepted!");
        codeLookup.put(196,"Error: Invalid Expiry Date!");
        codeLookup.put(200,"Error: Merchant ID Not Found!");
        codeLookup.put(201,"Error: PED Serial Number Not Found!");
        codeLookup.put(205,"Error: Initialization Failed (205: MAC Error)!");
        codeLookup.put(206,"Error: Initialization Failed (206: Bad Response)!");
        codeLookup.put(210,"Error: Initialization Failed (210: EMV Failure)!");
        codeLookup.put(250,"Error: Network Error!");
        codeLookup.put(307,"Error: Transaction Declined!");
        codeLookup.put(308,"Error: Invalid Response from Host!");
        codeLookup.put(350,"Error: Unrecoverable Data Error, All Retries Failed!");
        codeLookup.put(400,"Error: Terminal Timed Out Waiting for Customer Response!");
        codeLookup.put(401,"Error: Transaction Cancelled!");
        codeLookup.put(402,"Error: A Transaction Response was Received from Host After Timeout!");
        codeLookup.put(450,"Error: PED Master Key(s) are Corrupted!");
        codeLookup.put(460,"Error: Resend Last Response Failed!");
        codeLookup.put(461,"Error: Must Close Batch Before Code Download!");
        codeLookup.put(462,"Error: SAF Must be Processed Before Code Download!");
        codeLookup.put(500,"Error: E-Commerce Flag Not Allowed!");
        codeLookup.put(501,"Error: Fallback Transaction Cannot be added to SAF!");
        codeLookup.put(600,"Error: Invalid POS Entry Mode!");
        codeLookup.put(601,"Error: Invalid EMV Request Data!");
        codeLookup.put(602,"Error: Invalid EMV Request Data!");
        codeLookup.put(603,"Error: Invalid Chip Condition Code!");
        codeLookup.put(604,"Error: Invalid EMV Reason Online Code!");
        codeLookup.put(605,"Error: Invalid EMV Issuer Script Results!");
        codeLookup.put(606,"Error: PIN Retried Too Many Times!");
        codeLookup.put(607,"Error: Card Removed Prematurely!");
        codeLookup.put(608,"Error: Transaction was Declined!");
        codeLookup.put(609,"Error: Transaction was Declined!");
        codeLookup.put(610,"Error: EMV Not Allowed in Pre-Authorization Transaction!");
        codeLookup.put(611,"Error: Fallback Swipe is Not Allowed for This Card!");
        codeLookup.put(612,"Error: Card Read Error!");
        codeLookup.put(613,"Error: Card Read Error!");
        codeLookup.put(614,"Error: Track Data Missing from EMV Card!");
        codeLookup.put(615,"Error: TC Advice is Not Supported!");
        codeLookup.put(616,"Error: Transaction Not Completed!");
        codeLookup.put(620,"Error: Could Not Fetch Mastercard EMV Data!");
        codeLookup.put(640,"Error: Non-EMV Reversal Code!");
    }
}
