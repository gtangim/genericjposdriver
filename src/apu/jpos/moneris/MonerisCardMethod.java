package apu.jpos.moneris;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 6/6/14
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */
public enum MonerisCardMethod {
    Swiped, ManualEntry, Chip, ChipFailedButApproved, FallbackSwiped, FallbackManualEntry, Contactless, ContactlessEMV
}
