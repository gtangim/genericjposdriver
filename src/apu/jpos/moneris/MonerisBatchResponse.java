package apu.jpos.moneris;

import apu.jpos.util.StringUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 6/6/14
 * Time: 11:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class MonerisBatchResponse {

    MonerisBatchType batchType;
    DateTime transactionDate;
    String responseCode;
    String ecrId;
    int batchNumber;

    int serverPurchaseCount;
    int serverRefundCount;
    int serverVoidCount;
    int serverPaymentCount;         // must be zero
    int serverPaymentVoidCount;     // must be zero
    int serverCreditAppCount;       // must be zero
    int serverTotalCount;


    int pinpadPurchaseCount;
    int pinpadRefundCount;
    int pinpadVoidCount;
    int pinpadPaymentCount;         // must be zero
    int pinpadPaymentVoidCount;     // must be zero
    int pinpadCreditAppCount;       // must be zero
    int pinpadTotalCount;

    BigDecimal serverPurchaseValue;
    BigDecimal serverRefundValue;
    BigDecimal serverVoidValue;
    BigDecimal serverPaymentValue;          // must be zero
    BigDecimal serverPaymentVoidValue;      // must be zero
    BigDecimal serverTotalValue;

    BigDecimal pinpadPurchaseValue;
    BigDecimal pinpadRefundValue;
    BigDecimal pinpadVoidValue;
    BigDecimal pinpadPaymentValue;          // must be zero
    BigDecimal pinpadPaymentVoidValue;      // must be zero
    BigDecimal pinpadTotalValue;


    boolean isBalanced;
    String transactionLogs;


    public MonerisBatchResponse()
    {
        isBalanced=true;
        batchType = MonerisBatchType.BatchClose;
        transactionDate = new DateTime();
        responseCode = "099";
        ecrId = "";
        batchNumber = 0;
        serverPurchaseCount = 0;
        serverPurchaseValue = BigDecimal.ZERO;
        serverRefundCount = 0;
        serverRefundValue = BigDecimal.ZERO;
        serverVoidCount = 0;
        serverVoidValue = BigDecimal.ZERO;
        serverPaymentCount = 0;
        serverPaymentValue = BigDecimal.ZERO;
        serverPaymentVoidCount = 0;
        serverPaymentVoidValue = BigDecimal.ZERO;
        serverCreditAppCount = 0;

        pinpadPurchaseCount = 0;
        pinpadPurchaseValue = BigDecimal.ZERO;
        pinpadRefundCount = 0;
        pinpadRefundValue = BigDecimal.ZERO;
        pinpadVoidCount = 0;
        pinpadVoidValue = BigDecimal.ZERO;
        pinpadPaymentCount = 0;
        pinpadPaymentValue = BigDecimal.ZERO;
        pinpadPaymentVoidCount = 0;
        pinpadPaymentVoidValue = BigDecimal.ZERO;
        pinpadCreditAppCount = 0;

        serverTotalCount = 0;
        pinpadTotalCount = 0;

        serverTotalValue = BigDecimal.ZERO;
        pinpadTotalValue = BigDecimal.ZERO;
    }


    public MonerisBatchResponse(MonerisResponse res) throws Exception
    {
        if (res.isRequestAccepted())
            isBalanced=true;
        else if (res.isBatchClosed())
            isBalanced=res.isBatchBalanced();
        else
            throw new Exception("804 - Error: Invalid response received from PinPad!");
        if (res.paramCount()==28)
        {

            init(res.getParam(0),res.getParam(1), res.getParam(2), res.getParam(4), res.getParam(5)
                    ,res.getParam(6),res.getParam(7),res.getParam(8),res.getParam(9),res.getParam(10),res.getParam(11)
                    ,res.getParam(12),res.getParam(13),res.getParam(14),res.getParam(15),res.getParam(16)
                    ,res.getParam(17),res.getParam(18),res.getParam(19),res.getParam(20),res.getParam(21)
                    ,res.getParam(22),res.getParam(23),res.getParam(24),res.getParam(25),res.getParam(26)
                    ,res.getParam(27));

        }
        else if (res.paramCount()==16)
        {
            init(res.getParam(0),res.getParam(1), res.getParam(2), res.getParam(4), res.getParam(5)
                    ,res.getParam(6),res.getParam(7),res.getParam(8),res.getParam(9),res.getParam(10),res.getParam(11)
                    ,res.getParam(12),res.getParam(13),res.getParam(14),res.getParam(15),null
                    ,null,null,null,null,null
                    ,null,null,null,null,null, null);
        }
        else  throw new Exception("804 - Error: Invalid response received from PinPad!");




    }

    private void init(String tt,String dt, String resp, String ecr, String bnum,
                      String hPurCnt, String hPurAmt, String hRefCnt, String hRefAmt,
                      String hVoidCnt, String hVoidAmt, String hPayCnt, String hPayAmt,
                      String hPayVoidCnt, String hPayVoidAmt, String hCreditAppCnt,
                      String pPurCnt, String pPurAmt, String pRefCnt, String pRefAmt,
                      String pVoidCnt, String pVoidAmt, String pPayCnt, String pPayAmt,
                      String pPayVoidCnt, String pPayVoidAmt, String pCreditAppCnt) throws Exception
    {
        tt = tt.trim();
        if (tt.equals("65")) batchType = MonerisBatchType.BatchClose;
        else if (tt.equals("67")) batchType = MonerisBatchType.DepositTotals;
        else if (tt.equals("64")) batchType = MonerisBatchType.ClerkSubTotals;
        else throw new Exception("810 - Error: Unknown batch response received from the PinPad!");
        transactionDate = stringToDate(dt,"yyMMddHHmmss");
        responseCode = resp;
        ecrId = ecr;
        batchNumber = getCount(bnum);
        serverPurchaseCount = getCount(hPurCnt);
        serverPurchaseValue = getAmount(hPurAmt);
        serverRefundCount = getCount(hRefCnt);
        serverRefundValue = getAmount(hRefAmt);
        serverVoidCount = getCount(hVoidCnt);
        serverVoidValue = getAmount(hVoidAmt);
        serverPaymentCount = getCount(hPayCnt);
        serverPaymentValue = getAmount(hPayAmt);
        serverPaymentVoidCount = getCount(hPayVoidCnt);
        serverPaymentVoidValue = getAmount(hPayVoidAmt);
        serverCreditAppCount = getCount(hCreditAppCnt);

        pinpadPurchaseCount = getCount(pPurCnt);
        pinpadPurchaseValue = getAmount(pPurAmt);
        pinpadRefundCount = getCount(pRefCnt);
        pinpadRefundValue = getAmount(pRefAmt);
        pinpadVoidCount = getCount(pVoidCnt);
        pinpadVoidValue = getAmount(pVoidAmt);
        pinpadPaymentCount = getCount(pPayCnt);
        pinpadPaymentValue = getAmount(pPayAmt);
        pinpadPaymentVoidCount = getCount(pPayVoidCnt);
        pinpadPaymentVoidValue = getAmount(pPayVoidAmt);
        pinpadCreditAppCount = getCount(pCreditAppCnt);

        serverTotalCount = serverPurchaseCount+serverRefundCount+serverVoidCount
                +serverPaymentCount+serverPaymentVoidCount+serverCreditAppCount;

        pinpadTotalCount = pinpadPurchaseCount+pinpadRefundCount+pinpadVoidCount
                +pinpadPaymentCount+pinpadPaymentVoidCount+pinpadCreditAppCount;


        serverTotalValue = serverPurchaseValue.subtract(serverRefundValue).subtract(serverVoidValue)
                .add(serverPaymentValue).subtract(serverPaymentVoidValue);

        pinpadTotalValue = pinpadPurchaseValue.subtract(pinpadRefundValue).subtract(pinpadVoidValue)
                .add(pinpadPaymentValue).subtract(pinpadPaymentVoidValue);

        if (serverTotalCount!=pinpadTotalCount || serverTotalValue.compareTo(pinpadTotalValue)!=0) isBalanced=false;

    }

    private int getCount(String c)
    {
        if (StringUtil.empty(c)) return 0;
        try
        {
            int res = Integer.parseInt(c);
            return  res;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    private BigDecimal getAmount(String c)
    {
        if (StringUtil.empty(c)) return BigDecimal.ZERO;
        try
        {
            BigDecimal res = new BigDecimal(c);
            res = res.movePointLeft(2);
            return  res;
        }
        catch (Exception ex)
        {
            return BigDecimal.ZERO;
        }
    }


    public static DateTime stringToDate(String dt, String pattern)
    {
        try
        {
            DateTimeFormatter dtf = DateTimeFormat.forPattern(pattern);
            DateTime ret = dtf.parseDateTime(dt);
            return ret;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public String getEcrId() {
        return ecrId;
    }

    public MonerisBatchType getBatchType() {
        return batchType;
    }

    public DateTime getTransactionDate() {
        return transactionDate;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public int getBatchNumber() {
        return batchNumber;
    }

    public int getServerPurchaseCount() {
        return serverPurchaseCount;
    }

    public int getServerRefundCount() {
        return serverRefundCount;
    }

    public int getServerVoidCount() {
        return serverVoidCount;
    }

    public int getServerPaymentCount() {
        return serverPaymentCount;
    }

    public int getServerPaymentVoidCount() {
        return serverPaymentVoidCount;
    }

    public int getServerCreditAppCount() {
        return serverCreditAppCount;
    }

    public int getPinpadPurchaseCount() {
        return pinpadPurchaseCount;
    }

    public int getPinpadRefundCount() {
        return pinpadRefundCount;
    }

    public int getPinpadVoidCount() {
        return pinpadVoidCount;
    }

    public int getPinpadPaymentCount() {
        return pinpadPaymentCount;
    }

    public int getPinpadPaymentVoidCount() {
        return pinpadPaymentVoidCount;
    }

    public int getPinpadCreditAppCount() {
        return pinpadCreditAppCount;
    }

    public BigDecimal getServerPurchaseValue() {
        return serverPurchaseValue;
    }

    public BigDecimal getServerRefundValue() {
        return serverRefundValue;
    }

    public BigDecimal getServerVoidValue() {
        return serverVoidValue;
    }

    public BigDecimal getServerPaymentValue() {
        return serverPaymentValue;
    }

    public BigDecimal getServerPaymentVoidValue() {
        return serverPaymentVoidValue;
    }

    public BigDecimal getPinpadPurchaseValue() {
        return pinpadPurchaseValue;
    }

    public BigDecimal getPinpadRefundValue() {
        return pinpadRefundValue;
    }

    public BigDecimal getPinpadVoidValue() {
        return pinpadVoidValue;
    }

    public BigDecimal getPinpadPaymentValue() {
        return pinpadPaymentValue;
    }

    public BigDecimal getPinpadPaymentVoidValue() {
        return pinpadPaymentVoidValue;
    }

    public boolean isBalanced() {
        return isBalanced;
    }

    public String getTransactionLogs() {
        return transactionLogs;
    }

    public void setTransactionLogs(String transactionLogs) {
        this.transactionLogs = transactionLogs;
    }

    public int getServerTotalCount() {
        return serverTotalCount;
    }

    public int getPinpadTotalCount() {
        return pinpadTotalCount;
    }

    public BigDecimal getServerTotalValue() {
        return serverTotalValue;
    }

    public BigDecimal getPinpadTotalValue() {
        return pinpadTotalValue;
    }

    public void setPinPadTotals(int count, BigDecimal value)
    {
        pinpadTotalCount = count;
        pinpadTotalValue = value;
    }
    public void setServerTotals(int count, BigDecimal value)
    {
        serverTotalCount = count;
        serverTotalValue = value;
    }
}
